from diamonds.models import Diamond
import requests
import ftplib
import os
import string
from django.conf import settings
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from django.core.mail import EmailMultiAlternatives, send_mail
# from django.template.loader import render_to_string
# import time
# from dateutil import parser

Countries = [{"countryName":"Afghanistan","iso2":"AF","iso3":"AFG","phoneCode":"93"},{"countryName":"Albania","iso2":"AL","iso3":"ALB","phoneCode":"355"},{"countryName":"Algeria","iso2":"DZ","iso3":"DZA","phoneCode":"213"},{"countryName":"American Samoa","iso2":"AS","iso3":"ASM","phoneCode":"1 684"},{"countryName":"Andorra","iso2":"AD","iso3":"AND","phoneCode":"376"},{"countryName":"Angola","iso2":"AO","iso3":"AGO","phoneCode":"244"},{"countryName":"Anguilla","iso2":"AI","iso3":"AIA","phoneCode":"1 264"},{"countryName":"Antarctica","iso2":"AQ","iso3":"ATA","phoneCode":"672"},{"countryName":"Antigua and Barbuda","iso2":"AG","iso3":"ATG","phoneCode":"1 268"},{"countryName":"Argentina","iso2":"AR","iso3":"ARG","phoneCode":"54"},{"countryName":"Armenia","iso2":"AM","iso3":"ARM","phoneCode":"374"},{"countryName":"Aruba","iso2":"AW","iso3":"ABW","phoneCode":"297"},{"countryName":"Australia","iso2":"AU","iso3":"AUS","phoneCode":"61"},{"countryName":"Austria","iso2":"AT","iso3":"AUT","phoneCode":"43"},{"countryName":"Azerbaijan","iso2":"AZ","iso3":"AZE","phoneCode":"994"},{"countryName":"Bahamas","iso2":"BS","iso3":"BHS","phoneCode":"1 242"},{"countryName":"Bahrain","iso2":"BH","iso3":"BHR","phoneCode":"973"},{"countryName":"Bangladesh","iso2":"BD","iso3":"BGD","phoneCode":"880"},{"countryName":"Barbados","iso2":"BB","iso3":"BRB","phoneCode":"1 246"},{"countryName":"Belarus","iso2":"BY","iso3":"BLR","phoneCode":"375"},{"countryName":"Belgium","iso2":"BE","iso3":"BEL","phoneCode":"32"},{"countryName":"Belize","iso2":"BZ","iso3":"BLZ","phoneCode":"501"},{"countryName":"Benin","iso2":"BJ","iso3":"BEN","phoneCode":"229"},{"countryName":"Bermuda","iso2":"BM","iso3":"BMU","phoneCode":"1 441"},{"countryName":"Bhutan","iso2":"BT","iso3":"BTN","phoneCode":"975"},{"countryName":"Bolivia","iso2":"BO","iso3":"BOL","phoneCode":"591"},{"countryName":"Bosnia and Herzegovina","iso2":"BA","iso3":"BIH","phoneCode":"387"},{"countryName":"Botswana","iso2":"BW","iso3":"BWA","phoneCode":"267"},{"countryName":"Brazil","iso2":"BR","iso3":"BRA","phoneCode":"55"},{"countryName":"British Indian Ocean Territory","iso2":"IO","iso3":"IOT","phoneCode":""},{"countryName":"British Virgin Islands","iso2":"VG","iso3":"VGB","phoneCode":"1 284"},{"countryName":"Brunei","iso2":"BN","iso3":"BRN","phoneCode":"673"},{"countryName":"Bulgaria","iso2":"BG","iso3":"BGR","phoneCode":"359"},{"countryName":"Burkina Faso","iso2":"BF","iso3":"BFA","phoneCode":"226"},{"countryName":"Burma (Myanmar)","iso2":"MM","iso3":"MMR","phoneCode":"95"},{"countryName":"Burundi","iso2":"BI","iso3":"BDI","phoneCode":"257"},{"countryName":"Cambodia","iso2":"KH","iso3":"KHM","phoneCode":"855"},{"countryName":"Cameroon","iso2":"CM","iso3":"CMR","phoneCode":"237"},{"countryName":"Canada","iso2":"CA","iso3":"CAN","phoneCode":"1"},{"countryName":"Cape Verde","iso2":"CV","iso3":"CPV","phoneCode":"238"},{"countryName":"Cayman Islands","iso2":"KY","iso3":"CYM","phoneCode":"1 345"},{"countryName":"Central African Republic","iso2":"CF","iso3":"CAF","phoneCode":"236"},{"countryName":"Chad","iso2":"TD","iso3":"TCD","phoneCode":"235"},{"countryName":"Chile","iso2":"CL","iso3":"CHL","phoneCode":"56"},{"countryName":"China","iso2":"CN","iso3":"CHN","phoneCode":"86"},{"countryName":"Christmas Island","iso2":"CX","iso3":"CXR","phoneCode":"61"},{"countryName":"Cocos (Keeling) Islands","iso2":"CC","iso3":"CCK","phoneCode":"61"},{"countryName":"Colombia","iso2":"CO","iso3":"COL","phoneCode":"57"},{"countryName":"Comoros","iso2":"KM","iso3":"COM","phoneCode":"269"},{"countryName":"Cook Islands","iso2":"CK","iso3":"COK","phoneCode":"682"},{"countryName":"Costa Rica","iso2":"CR","iso3":"CRC","phoneCode":"506"},{"countryName":"Croatia","iso2":"HR","iso3":"HRV","phoneCode":"385"},{"countryName":"Cuba","iso2":"CU","iso3":"CUB","phoneCode":"53"},{"countryName":"Cyprus","iso2":"CY","iso3":"CYP","phoneCode":"357"},{"countryName":"Czech Republic","iso2":"CZ","iso3":"CZE","phoneCode":"420"},{"countryName":"Democratic Republic of the Congo","iso2":"CD","iso3":"COD","phoneCode":"243"},{"countryName":"Denmark","iso2":"DK","iso3":"DNK","phoneCode":"45"},{"countryName":"Djibouti","iso2":"DJ","iso3":"DJI","phoneCode":"253"},{"countryName":"Dominica","iso2":"DM","iso3":"DMA","phoneCode":"1 767"},{"countryName":"Dominican Republic","iso2":"DO","iso3":"DOM","phoneCode":"1 809"},{"countryName":"Ecuador","iso2":"EC","iso3":"ECU","phoneCode":"593"},{"countryName":"Egypt","iso2":"EG","iso3":"EGY","phoneCode":"20"},{"countryName":"El Salvador","iso2":"SV","iso3":"SLV","phoneCode":"503"},{"countryName":"Equatorial Guinea","iso2":"GQ","iso3":"GNQ","phoneCode":"240"},{"countryName":"Eritrea","iso2":"ER","iso3":"ERI","phoneCode":"291"},{"countryName":"Estonia","iso2":"EE","iso3":"EST","phoneCode":"372"},{"countryName":"Ethiopia","iso2":"ET","iso3":"ETH","phoneCode":"251"},{"countryName":"Falkland Islands","iso2":"FK","iso3":"FLK","phoneCode":"500"},{"countryName":"Faroe Islands","iso2":"FO","iso3":"FRO","phoneCode":"298"},{"countryName":"Fiji","iso2":"FJ","iso3":"FJI","phoneCode":"679"},{"countryName":"Finland","iso2":"FI","iso3":"FIN","phoneCode":"358"},{"countryName":"France","iso2":"FR","iso3":"FRA","phoneCode":"33"},{"countryName":"French Polynesia","iso2":"PF","iso3":"PYF","phoneCode":"689"},{"countryName":"Gabon","iso2":"GA","iso3":"GAB","phoneCode":"241"},{"countryName":"Gambia","iso2":"GM","iso3":"GMB","phoneCode":"220"},{"countryName":"Gaza Strip","iso2":"","iso3":"","phoneCode":"970"},{"countryName":"Georgia","iso2":"GE","iso3":"GEO","phoneCode":"995"},{"countryName":"Germany","iso2":"DE","iso3":"DEU","phoneCode":"49"},{"countryName":"Ghana","iso2":"GH","iso3":"GHA","phoneCode":"233"},{"countryName":"Gibraltar","iso2":"GI","iso3":"GIB","phoneCode":"350"},{"countryName":"Greece","iso2":"GR","iso3":"GRC","phoneCode":"30"},{"countryName":"Greenland","iso2":"GL","iso3":"GRL","phoneCode":"299"},{"countryName":"Grenada","iso2":"GD","iso3":"GRD","phoneCode":"1 473"},{"countryName":"Guam","iso2":"GU","iso3":"GUM","phoneCode":"1 671"},{"countryName":"Guatemala","iso2":"GT","iso3":"GTM","phoneCode":"502"},{"countryName":"Guinea","iso2":"GN","iso3":"GIN","phoneCode":"224"},{"countryName":"Guinea-Bissau","iso2":"GW","iso3":"GNB","phoneCode":"245"},{"countryName":"Guyana","iso2":"GY","iso3":"GUY","phoneCode":"592"},{"countryName":"Haiti","iso2":"HT","iso3":"HTI","phoneCode":"509"},{"countryName":"Holy See (Vatican City)","iso2":"VA","iso3":"VAT","phoneCode":"39"},{"countryName":"Honduras","iso2":"HN","iso3":"HND","phoneCode":"504"},{"countryName":"Hong Kong","iso2":"HK","iso3":"HKG","phoneCode":"852"},{"countryName":"Hungary","iso2":"HU","iso3":"HUN","phoneCode":"36"},{"countryName":"Iceland","iso2":"IS","iso3":"IS","phoneCode":"354"},{"countryName":"India","iso2":"IN","iso3":"IND","phoneCode":"91"},{"countryName":"Indonesia","iso2":"ID","iso3":"IDN","phoneCode":"62"},{"countryName":"Iran","iso2":"IR","iso3":"IRN","phoneCode":"98"},{"countryName":"Iraq","iso2":"IQ","iso3":"IRQ","phoneCode":"964"},{"countryName":"Ireland","iso2":"IE","iso3":"IRL","phoneCode":"353"},{"countryName":"Isle of Man","iso2":"IM","iso3":"IMN","phoneCode":"44"},{"countryName":"Israel","iso2":"IL","iso3":"ISR","phoneCode":"972"},{"countryName":"Italy","iso2":"IT","iso3":"ITA","phoneCode":"39"},{"countryName":"Ivory Coast","iso2":"CI","iso3":"CIV","phoneCode":"225"},{"countryName":"Jamaica","iso2":"JM","iso3":"JAM","phoneCode":"1 876"},{"countryName":"Japan","iso2":"JP","iso3":"JPN","phoneCode":"81"},{"countryName":"Jersey","iso2":"JE","iso3":"JEY","phoneCode":""},{"countryName":"Jordan","iso2":"JO","iso3":"JOR","phoneCode":"962"},{"countryName":"Kazakhstan","iso2":"KZ","iso3":"KAZ","phoneCode":"7"},{"countryName":"Kenya","iso2":"KE","iso3":"KEN","phoneCode":"254"},{"countryName":"Kiribati","iso2":"KI","iso3":"KIR","phoneCode":"686"},{"countryName":"Kosovo","iso2":"","iso3":"","phoneCode":"381"},{"countryName":"Kuwait","iso2":"KW","iso3":"KWT","phoneCode":"965"},{"countryName":"Kyrgyzstan","iso2":"KG","iso3":"KGZ","phoneCode":"996"},{"countryName":"Laos","iso2":"LA","iso3":"LAO","phoneCode":"856"},{"countryName":"Latvia","iso2":"LV","iso3":"LVA","phoneCode":"371"},{"countryName":"Lebanon","iso2":"LB","iso3":"LBN","phoneCode":"961"},{"countryName":"Lesotho","iso2":"LS","iso3":"LSO","phoneCode":"266"},{"countryName":"Liberia","iso2":"LR","iso3":"LBR","phoneCode":"231"},{"countryName":"Libya","iso2":"LY","iso3":"LBY","phoneCode":"218"},{"countryName":"Liechtenstein","iso2":"LI","iso3":"LIE","phoneCode":"423"},{"countryName":"Lithuania","iso2":"LT","iso3":"LTU","phoneCode":"370"},{"countryName":"Luxembourg","iso2":"LU","iso3":"LUX","phoneCode":"352"},{"countryName":"Macau","iso2":"MO","iso3":"MAC","phoneCode":"853"},{"countryName":"Macedonia","iso2":"MK","iso3":"MKD","phoneCode":"389"},{"countryName":"Madagascar","iso2":"MG","iso3":"MDG","phoneCode":"261"},{"countryName":"Malawi","iso2":"MW","iso3":"MWI","phoneCode":"265"},{"countryName":"Malaysia","iso2":"MY","iso3":"MYS","phoneCode":"60"},{"countryName":"Maldives","iso2":"MV","iso3":"MDV","phoneCode":"960"},{"countryName":"Mali","iso2":"ML","iso3":"MLI","phoneCode":"223"},{"countryName":"Malta","iso2":"MT","iso3":"MLT","phoneCode":"356"},{"countryName":"Marshall Islands","iso2":"MH","iso3":"MHL","phoneCode":"692"},{"countryName":"Mauritania","iso2":"MR","iso3":"MRT","phoneCode":"222"},{"countryName":"Mauritius","iso2":"MU","iso3":"MUS","phoneCode":"230"},{"countryName":"Mayotte","iso2":"YT","iso3":"MYT","phoneCode":"262"},{"countryName":"Mexico","iso2":"MX","iso3":"MEX","phoneCode":"52"},{"countryName":"Micronesia","iso2":"FM","iso3":"FSM","phoneCode":"691"},{"countryName":"Moldova","iso2":"MD","iso3":"MDA","phoneCode":"373"},{"countryName":"Monaco","iso2":"MC","iso3":"MCO","phoneCode":"377"},{"countryName":"Mongolia","iso2":"MN","iso3":"MNG","phoneCode":"976"},{"countryName":"Montenegro","iso2":"ME","iso3":"MNE","phoneCode":"382"},{"countryName":"Montserrat","iso2":"MS","iso3":"MSR","phoneCode":"1 664"},{"countryName":"Morocco","iso2":"MA","iso3":"MAR","phoneCode":"212"},{"countryName":"Mozambique","iso2":"MZ","iso3":"MOZ","phoneCode":"258"},{"countryName":"Namibia","iso2":"NA","iso3":"NAM","phoneCode":"264"},{"countryName":"Nauru","iso2":"NR","iso3":"NRU","phoneCode":"674"},{"countryName":"Nepal","iso2":"NP","iso3":"NPL","phoneCode":"977"},{"countryName":"Netherlands","iso2":"NL","iso3":"NLD","phoneCode":"31"},{"countryName":"Netherlands Antilles","iso2":"AN","iso3":"ANT","phoneCode":"599"},{"countryName":"New Caledonia","iso2":"NC","iso3":"NCL","phoneCode":"687"},{"countryName":"New Zealand","iso2":"NZ","iso3":"NZL","phoneCode":"64"},{"countryName":"Nicaragua","iso2":"NI","iso3":"NIC","phoneCode":"505"},{"countryName":"Niger","iso2":"NE","iso3":"NER","phoneCode":"227"},{"countryName":"Nigeria","iso2":"NG","iso3":"NGA","phoneCode":"234"},{"countryName":"Niue","iso2":"NU","iso3":"NIU","phoneCode":"683"},{"countryName":"Norfolk Island","iso2":"","iso3":"NFK","phoneCode":"672"},{"countryName":"North Korea","iso2":"KP","iso3":"PRK","phoneCode":"850"},{"countryName":"Northern Mariana Islands","iso2":"MP","iso3":"MNP","phoneCode":"1 670"},{"countryName":"Norway","iso2":"NO","iso3":"NOR","phoneCode":"47"},{"countryName":"Oman","iso2":"OM","iso3":"OMN","phoneCode":"968"},{"countryName":"Pakistan","iso2":"PK","iso3":"PAK","phoneCode":"92"},{"countryName":"Palau","iso2":"PW","iso3":"PLW","phoneCode":"680"},{"countryName":"Panama","iso2":"PA","iso3":"PAN","phoneCode":"507"},{"countryName":"Papua New Guinea","iso2":"PG","iso3":"PNG","phoneCode":"675"},{"countryName":"Paraguay","iso2":"PY","iso3":"PRY","phoneCode":"595"},{"countryName":"Peru","iso2":"PE","iso3":"PER","phoneCode":"51"},{"countryName":"Philippines","iso2":"PH","iso3":"PHL","phoneCode":"63"},{"countryName":"Pitcairn Islands","iso2":"PN","iso3":"PCN","phoneCode":"870"},{"countryName":"Poland","iso2":"PL","iso3":"POL","phoneCode":"48"},{"countryName":"Portugal","iso2":"PT","iso3":"PRT","phoneCode":"351"},{"countryName":"Puerto Rico","iso2":"PR","iso3":"PRI","phoneCode":"1"},{"countryName":"Qatar","iso2":"QA","iso3":"QAT","phoneCode":"974"},{"countryName":"Republic of the Congo","iso2":"CG","iso3":"COG","phoneCode":"242"},{"countryName":"Romania","iso2":"RO","iso3":"ROU","phoneCode":"40"},{"countryName":"Russia","iso2":"RU","iso3":"RUS","phoneCode":"7"},{"countryName":"Rwanda","iso2":"RW","iso3":"RWA","phoneCode":"250"},{"countryName":"Saint Barthelemy","iso2":"BL","iso3":"BLM","phoneCode":"590"},{"countryName":"Saint Helena","iso2":"SH","iso3":"SHN","phoneCode":"290"},{"countryName":"Saint Kitts and Nevis","iso2":"KN","iso3":"KNA","phoneCode":"1 869"},{"countryName":"Saint Lucia","iso2":"LC","iso3":"LCA","phoneCode":"1 758"},{"countryName":"Saint Martin","iso2":"MF","iso3":"MAF","phoneCode":"1 599"},{"countryName":"Saint Pierre and Miquelon","iso2":"PM","iso3":"SPM","phoneCode":"508"},{"countryName":"Saint Vincent and the Grenadines","iso2":"VC","iso3":"VCT","phoneCode":"1 784"},{"countryName":"Samoa","iso2":"WS","iso3":"WSM","phoneCode":"685"},{"countryName":"San Marino","iso2":"SM","iso3":"SMR","phoneCode":"378"},{"countryName":"Sao Tome and Principe","iso2":"ST","iso3":"STP","phoneCode":"239"},{"countryName":"Saudi Arabia","iso2":"SA","iso3":"SAU","phoneCode":"966"},{"countryName":"Senegal","iso2":"SN","iso3":"SEN","phoneCode":"221"},{"countryName":"Serbia","iso2":"RS","iso3":"SRB","phoneCode":"381"},{"countryName":"Seychelles","iso2":"SC","iso3":"SYC","phoneCode":"248"},{"countryName":"Sierra Leone","iso2":"SL","iso3":"SLE","phoneCode":"232"},{"countryName":"Singapore","iso2":"SG","iso3":"SGP","phoneCode":"65"},{"countryName":"Slovakia","iso2":"SK","iso3":"SVK","phoneCode":"421"},{"countryName":"Slovenia","iso2":"SI","iso3":"SVN","phoneCode":"386"},{"countryName":"Solomon Islands","iso2":"SB","iso3":"SLB","phoneCode":"677"},{"countryName":"Somalia","iso2":"SO","iso3":"SOM","phoneCode":"252"},{"countryName":"South Africa","iso2":"ZA","iso3":"ZAF","phoneCode":"27"},{"countryName":"South Korea","iso2":"KR","iso3":"KOR","phoneCode":"82"},{"countryName":"Spain","iso2":"ES","iso3":"ESP","phoneCode":"34"},{"countryName":"Sri Lanka","iso2":"LK","iso3":"LKA","phoneCode":"94"},{"countryName":"Sudan","iso2":"SD","iso3":"SDN","phoneCode":"249"},{"countryName":"Suriname","iso2":"SR","iso3":"SUR","phoneCode":"597"},{"countryName":"Svalbard","iso2":"SJ","iso3":"SJM","phoneCode":""},{"countryName":"Swaziland","iso2":"SZ","iso3":"SWZ","phoneCode":"268"},{"countryName":"Sweden","iso2":"SE","iso3":"SWE","phoneCode":"46"},{"countryName":"Switzerland","iso2":"CH","iso3":"CHE","phoneCode":"41"},{"countryName":"Syria","iso2":"SY","iso3":"SYR","phoneCode":"963"},{"countryName":"Taiwan","iso2":"TW","iso3":"TWN","phoneCode":"886"},{"countryName":"Tajikistan","iso2":"TJ","iso3":"TJK","phoneCode":"992"},{"countryName":"Tanzania","iso2":"TZ","iso3":"TZA","phoneCode":"255"},{"countryName":"Thailand","iso2":"TH","iso3":"THA","phoneCode":"66"},{"countryName":"Timor-Leste","iso2":"TL","iso3":"TLS","phoneCode":"670"},{"countryName":"Togo","iso2":"TG","iso3":"TGO","phoneCode":"228"},{"countryName":"Tokelau","iso2":"TK","iso3":"TKL","phoneCode":"690"},{"countryName":"Tonga","iso2":"TO","iso3":"TON","phoneCode":"676"},{"countryName":"Trinidad and Tobago","iso2":"TT","iso3":"TTO","phoneCode":"1 868"},{"countryName":"Tunisia","iso2":"TN","iso3":"TUN","phoneCode":"216"},{"countryName":"Turkey","iso2":"TR","iso3":"TUR","phoneCode":"90"},{"countryName":"Turkmenistan","iso2":"TM","iso3":"TKM","phoneCode":"993"},{"countryName":"Turks and Caicos Islands","iso2":"TC","iso3":"TCA","phoneCode":"1 649"},{"countryName":"Tuvalu","iso2":"TV","iso3":"TUV","phoneCode":"688"},{"countryName":"Uganda","iso2":"UG","iso3":"UGA","phoneCode":"256"},{"countryName":"Ukraine","iso2":"UA","iso3":"UKR","phoneCode":"380"},{"countryName":"United Arab Emirates","iso2":"AE","iso3":"ARE","phoneCode":"971"},{"countryName":"United Kingdom","iso2":"GB","iso3":"GBR","phoneCode":"44"},{"countryName":"United States","iso2":"US","iso3":"USA","phoneCode":"1"},{"countryName":"Uruguay","iso2":"UY","iso3":"URY","phoneCode":"598"},{"countryName":"US Virgin Islands","iso2":"VI","iso3":"VIR","phoneCode":"1 340"},{"countryName":"Uzbekistan","iso2":"UZ","iso3":"UZB","phoneCode":"998"},{"countryName":"Vanuatu","iso2":"VU","iso3":"VUT","phoneCode":"678"},{"countryName":"Venezuela","iso2":"VE","iso3":"VEN","phoneCode":"58"},{"countryName":"Vietnam","iso2":"VN","iso3":"VNM","phoneCode":"84"},{"countryName":"Wallis and Futuna","iso2":"WF","iso3":"WLF","phoneCode":"681"},{"countryName":"West Bank","iso2":"","iso3":"","phoneCode":"970"},{"countryName":"Western Sahara","iso2":"EH","iso3":"ESH","phoneCode":""},{"countryName":"Yemen","iso2":"YE","iso3":"YEM","phoneCode":"967"},{"countryName":"Zambia","iso2":"ZM","iso3":"ZMB","phoneCode":"260"},{"countryName":"Zimbabwe","iso2":"ZW","iso3":"ZWE","phoneCode":"263"}]
Languages = [{"English": "Afar", "alpha2": "aa", "alpha3-b": "aar"},{"English": "Abkhazian", "alpha2": "ab", "alpha3-b": "abk"},{"English": "Afrikaans", "alpha2": "af", "alpha3-b": "afr"},{"English": "Akan", "alpha2": "ak", "alpha3-b": "aka"},{"English": "Albanian", "alpha2": "sq", "alpha3-b": "alb"},{"English": "Amharic", "alpha2": "am", "alpha3-b": "amh"},{"English": "Arabic", "alpha2": "ar", "alpha3-b": "ara"},{"English": "Aragonese", "alpha2": "an", "alpha3-b": "arg"},{"English": "Armenian", "alpha2": "hy", "alpha3-b": "arm"},{"English": "Assamese", "alpha2": "as", "alpha3-b": "asm"},{"English": "Avaric", "alpha2": "av", "alpha3-b": "ava"},{"English": "Avestan", "alpha2": "ae", "alpha3-b": "ave"},{"English": "Aymara", "alpha2": "ay", "alpha3-b": "aym"},{"English": "Azerbaijani", "alpha2": "az", "alpha3-b": "aze"},{"English": "Bashkir", "alpha2": "ba", "alpha3-b": "bak"},{"English": "Bambara", "alpha2": "bm", "alpha3-b": "bam"},{"English": "Basque", "alpha2": "eu", "alpha3-b": "baq"},{"English": "Belarusian", "alpha2": "be", "alpha3-b": "bel"},{"English": "Bengali", "alpha2": "bn", "alpha3-b": "ben"},{"English": "Bihari languages", "alpha2": "bh", "alpha3-b": "bih"},{"English": "Bislama", "alpha2": "bi", "alpha3-b": "bis"},{"English": "Bosnian", "alpha2": "bs", "alpha3-b": "bos"},{"English": "Breton", "alpha2": "br", "alpha3-b": "bre"},{"English": "Bulgarian", "alpha2": "bg", "alpha3-b": "bul"},{"English": "Burmese", "alpha2": "my", "alpha3-b": "bur"},{"English": "Catalan; Valencian", "alpha2": "ca", "alpha3-b": "cat"},{"English": "Chamorro", "alpha2": "ch", "alpha3-b": "cha"},{"English": "Chechen", "alpha2": "ce", "alpha3-b": "che"},{"English": "Chinese", "alpha2": "zh", "alpha3-b": "chi"},{"English": "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic", "alpha2": "cu", "alpha3-b": "chu"},{"English": "Chuvash", "alpha2": "cv", "alpha3-b": "chv"},{"English": "Cornish", "alpha2": "kw", "alpha3-b": "cor"},{"English": "Corsican", "alpha2": "co", "alpha3-b": "cos"},{"English": "Cree", "alpha2": "cr", "alpha3-b": "cre"},{"English": "Czech", "alpha2": "cs", "alpha3-b": "cze"},{"English": "Danish", "alpha2": "da", "alpha3-b": "dan"},{"English": "Divehi; Dhivehi; Maldivian", "alpha2": "dv", "alpha3-b": "div"},{"English": "Dutch; Flemish", "alpha2": "nl", "alpha3-b": "dut"},{"English": "Dzongkha", "alpha2": "dz", "alpha3-b": "dzo"},{"English": "English", "alpha2": "en", "alpha3-b": "eng"},{"English": "Esperanto", "alpha2": "eo", "alpha3-b": "epo"},{"English": "Estonian", "alpha2": "et", "alpha3-b": "est"},{"English": "Ewe", "alpha2": "ee", "alpha3-b": "ewe"},{"English": "Faroese", "alpha2": "fo", "alpha3-b": "fao"},{"English": "Fijian", "alpha2": "fj", "alpha3-b": "fij"},{"English": "Finnish", "alpha2": "fi", "alpha3-b": "fin"},{"English": "French", "alpha2": "fr", "alpha3-b": "fre"},{"English": "Western Frisian", "alpha2": "fy", "alpha3-b": "fry"},{"English": "Fulah", "alpha2": "ff", "alpha3-b": "ful"},{"English": "Georgian", "alpha2": "ka", "alpha3-b": "geo"},{"English": "German", "alpha2": "de", "alpha3-b": "ger"},{"English": "Gaelic; Scottish Gaelic", "alpha2": "gd", "alpha3-b": "gla"},{"English": "Irish", "alpha2": "ga", "alpha3-b": "gle"},{"English": "Galician", "alpha2": "gl", "alpha3-b": "glg"},{"English": "Manx", "alpha2": "gv", "alpha3-b": "glv"},{"English": "Greek, Modern (1453-)", "alpha2": "el", "alpha3-b": "gre"},{"English": "Guarani", "alpha2": "gn", "alpha3-b": "grn"},{"English": "Gujarati", "alpha2": "gu", "alpha3-b": "guj"},{"English": "Haitian; Haitian Creole", "alpha2": "ht", "alpha3-b": "hat"},{"English": "Hausa", "alpha2": "ha", "alpha3-b": "hau"},{"English": "Hebrew", "alpha2": "he", "alpha3-b": "heb"},{"English": "Herero", "alpha2": "hz", "alpha3-b": "her"},{"English": "Hindi", "alpha2": "hi", "alpha3-b": "hin"},{"English": "Hiri Motu", "alpha2": "ho", "alpha3-b": "hmo"},{"English": "Croatian", "alpha2": "hr", "alpha3-b": "hrv"},{"English": "Hungarian", "alpha2": "hu", "alpha3-b": "hun"},{"English": "Igbo", "alpha2": "ig", "alpha3-b": "ibo"},{"English": "Icelandic", "alpha2": "is", "alpha3-b": "ice"},{"English": "Ido", "alpha2": "io", "alpha3-b": "ido"},{"English": "Sichuan Yi; Nuosu", "alpha2": "ii", "alpha3-b": "iii"},{"English": "Inuktitut", "alpha2": "iu", "alpha3-b": "iku"},{"English": "Interlingue; Occidental", "alpha2": "ie", "alpha3-b": "ile"},{"English": "Interlingua (International Auxiliary Language Association)", "alpha2": "ia", "alpha3-b": "ina"},{"English": "Indonesian", "alpha2": "id", "alpha3-b": "ind"},{"English": "Inupiaq", "alpha2": "ik", "alpha3-b": "ipk"},{"English": "Italian", "alpha2": "it", "alpha3-b": "ita"},{"English": "Javanese", "alpha2": "jv", "alpha3-b": "jav"},{"English": "Japanese", "alpha2": "ja", "alpha3-b": "jpn"},{"English": "Kalaallisut; Greenlandic", "alpha2": "kl", "alpha3-b": "kal"},{"English": "Kannada", "alpha2": "kn", "alpha3-b": "kan"},{"English": "Kashmiri", "alpha2": "ks", "alpha3-b": "kas"},{"English": "Kanuri", "alpha2": "kr", "alpha3-b": "kau"},{"English": "Kazakh", "alpha2": "kk", "alpha3-b": "kaz"},{"English": "Central Khmer", "alpha2": "km", "alpha3-b": "khm"},{"English": "Kikuyu; Gikuyu", "alpha2": "ki", "alpha3-b": "kik"},{"English": "Kinyarwanda", "alpha2": "rw", "alpha3-b": "kin"},{"English": "Kirghiz; Kyrgyz", "alpha2": "ky", "alpha3-b": "kir"},{"English": "Komi", "alpha2": "kv", "alpha3-b": "kom"},{"English": "Kongo", "alpha2": "kg", "alpha3-b": "kon"},{"English": "Korean", "alpha2": "ko", "alpha3-b": "kor"},{"English": "Kuanyama; Kwanyama", "alpha2": "kj", "alpha3-b": "kua"},{"English": "Kurdish", "alpha2": "ku", "alpha3-b": "kur"},{"English": "Lao", "alpha2": "lo", "alpha3-b": "lao"},{"English": "Latin", "alpha2": "la", "alpha3-b": "lat"},{"English": "Latvian", "alpha2": "lv", "alpha3-b": "lav"},{"English": "Limburgan; Limburger; Limburgish", "alpha2": "li", "alpha3-b": "lim"},{"English": "Lingala", "alpha2": "ln", "alpha3-b": "lin"},{"English": "Lithuanian", "alpha2": "lt", "alpha3-b": "lit"},{"English": "Luxembourgish; Letzeburgesch", "alpha2": "lb", "alpha3-b": "ltz"},{"English": "Luba-Katanga", "alpha2": "lu", "alpha3-b": "lub"},{"English": "Ganda", "alpha2": "lg", "alpha3-b": "lug"},{"English": "Macedonian", "alpha2": "mk", "alpha3-b": "mac"},{"English": "Marshallese", "alpha2": "mh", "alpha3-b": "mah"},{"English": "Malayalam", "alpha2": "ml", "alpha3-b": "mal"},{"English": "Maori", "alpha2": "mi", "alpha3-b": "mao"},{"English": "Marathi", "alpha2": "mr", "alpha3-b": "mar"},{"English": "Malay", "alpha2": "ms", "alpha3-b": "may"},{"English": "Malagasy", "alpha2": "mg", "alpha3-b": "mlg"},{"English": "Maltese", "alpha2": "mt", "alpha3-b": "mlt"},{"English": "Mongolian", "alpha2": "mn", "alpha3-b": "mon"},{"English": "Nauru", "alpha2": "na", "alpha3-b": "nau"},{"English": "Navajo; Navaho", "alpha2": "nv", "alpha3-b": "nav"},{"English": "Ndebele, South; South Ndebele", "alpha2": "nr", "alpha3-b": "nbl"},{"English": "Ndebele, North; North Ndebele", "alpha2": "nd", "alpha3-b": "nde"},{"English": "Ndonga", "alpha2": "ng", "alpha3-b": "ndo"},{"English": "Nepali", "alpha2": "ne", "alpha3-b": "nep"},{"English": "Norwegian Nynorsk; Nynorsk, Norwegian", "alpha2": "nn", "alpha3-b": "nno"},{"English": "Bokm\u00e5l, Norwegian; Norwegian Bokm\u00e5l", "alpha2": "nb", "alpha3-b": "nob"},{"English": "Norwegian", "alpha2": "no", "alpha3-b": "nor"},{"English": "Chichewa; Chewa; Nyanja", "alpha2": "ny", "alpha3-b": "nya"},{"English": "Occitan (post 1500)", "alpha2": "oc", "alpha3-b": "oci"},{"English": "Ojibwa", "alpha2": "oj", "alpha3-b": "oji"},{"English": "Oriya", "alpha2": "or", "alpha3-b": "ori"},{"English": "Oromo", "alpha2": "om", "alpha3-b": "orm"},{"English": "Ossetian; Ossetic", "alpha2": "os", "alpha3-b": "oss"},{"English": "Panjabi; Punjabi", "alpha2": "pa", "alpha3-b": "pan"},{"English": "Persian", "alpha2": "fa", "alpha3-b": "per"},{"English": "Pali", "alpha2": "pi", "alpha3-b": "pli"},{"English": "Polish", "alpha2": "pl", "alpha3-b": "pol"},{"English": "Portuguese", "alpha2": "pt", "alpha3-b": "por"},{"English": "Pushto; Pashto", "alpha2": "ps", "alpha3-b": "pus"},{"English": "Quechua", "alpha2": "qu", "alpha3-b": "que"},{"English": "Romansh", "alpha2": "rm", "alpha3-b": "roh"},{"English": "Romanian; Moldavian; Moldovan", "alpha2": "ro", "alpha3-b": "rum"},{"English": "Rundi", "alpha2": "rn", "alpha3-b": "run"},{"English": "Russian", "alpha2": "ru", "alpha3-b": "rus"},{"English": "Sango", "alpha2": "sg", "alpha3-b": "sag"},{"English": "Sanskrit", "alpha2": "sa", "alpha3-b": "san"},{"English": "Sinhala; Sinhalese", "alpha2": "si", "alpha3-b": "sin"},{"English": "Slovak", "alpha2": "sk", "alpha3-b": "slo"},{"English": "Slovenian", "alpha2": "sl", "alpha3-b": "slv"},{"English": "Northern Sami", "alpha2": "se", "alpha3-b": "sme"},{"English": "Samoan", "alpha2": "sm", "alpha3-b": "smo"},{"English": "Shona", "alpha2": "sn", "alpha3-b": "sna"},{"English": "Sindhi", "alpha2": "sd", "alpha3-b": "snd"},{"English": "Somali", "alpha2": "so", "alpha3-b": "som"},{"English": "Sotho, Southern", "alpha2": "st", "alpha3-b": "sot"},{"English": "Spanish; Castilian", "alpha2": "es", "alpha3-b": "spa"},{"English": "Sardinian", "alpha2": "sc", "alpha3-b": "srd"},{"English": "Serbian", "alpha2": "sr", "alpha3-b": "srp"},{"English": "Swati", "alpha2": "ss", "alpha3-b": "ssw"},{"English": "Sundanese", "alpha2": "su", "alpha3-b": "sun"},{"English": "Swahili", "alpha2": "sw", "alpha3-b": "swa"},{"English": "Swedish", "alpha2": "sv", "alpha3-b": "swe"},{"English": "Tahitian", "alpha2": "ty", "alpha3-b": "tah"},{"English": "Tamil", "alpha2": "ta", "alpha3-b": "tam"},{"English": "Tatar", "alpha2": "tt", "alpha3-b": "tat"},{"English": "Telugu", "alpha2": "te", "alpha3-b": "tel"},{"English": "Tajik", "alpha2": "tg", "alpha3-b": "tgk"},{"English": "Tagalog", "alpha2": "tl", "alpha3-b": "tgl"},{"English": "Thai", "alpha2": "th", "alpha3-b": "tha"},{"English": "Tibetan", "alpha2": "bo", "alpha3-b": "tib"},{"English": "Tigrinya", "alpha2": "ti", "alpha3-b": "tir"},{"English": "Tonga (Tonga Islands)", "alpha2": "to", "alpha3-b": "ton"},{"English": "Tswana", "alpha2": "tn", "alpha3-b": "tsn"},{"English": "Tsonga", "alpha2": "ts", "alpha3-b": "tso"},{"English": "Turkmen", "alpha2": "tk", "alpha3-b": "tuk"},{"English": "Turkish", "alpha2": "tr", "alpha3-b": "tur"},{"English": "Twi", "alpha2": "tw", "alpha3-b": "twi"},{"English": "Uighur; Uyghur", "alpha2": "ug", "alpha3-b": "uig"},{"English": "Ukrainian", "alpha2": "uk", "alpha3-b": "ukr"},{"English": "Urdu", "alpha2": "ur", "alpha3-b": "urd"},{"English": "Uzbek", "alpha2": "uz", "alpha3-b": "uzb"},{"English": "Venda", "alpha2": "ve", "alpha3-b": "ven"},{"English": "Vietnamese", "alpha2": "vi", "alpha3-b": "vie"},{"English": "Volap\u00fck", "alpha2": "vo", "alpha3-b": "vol"},{"English": "Welsh", "alpha2": "cy", "alpha3-b": "wel"},{"English": "Walloon", "alpha2": "wa", "alpha3-b": "wln"},{"English": "Wolof", "alpha2": "wo", "alpha3-b": "wol"},{"English": "Xhosa", "alpha2": "xh", "alpha3-b": "xho"},{"English": "Yiddish", "alpha2": "yi", "alpha3-b": "yid"},{"English": "Yoruba", "alpha2": "yo", "alpha3-b": "yor"},{"English": "Zhuang; Chuang", "alpha2": "za", "alpha3-b": "zha"},{"English": "Zulu", "alpha2": "zu", "alpha3-b": "zul"}]
BusinessType = {"Wholesaler": "DW", "Retailer": "JR", "Manufacturer": "JM", "": "OT", }


@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)


def get_specific_value(new_val, old_val):
    temp = new_val if new_val and new_val != '' and new_val != 'NULL' else old_val
    return temp


def remove_spaces(in_string: str):
    return in_string.translate(str.maketrans({' ': ''}))


def unformat_phone_no(in_string: str):
    try:
        return in_string.translate(str.maketrans({' ': '', '(': '', ')': '', '-': '', '.': ''}))
    except Exception as e:
        return ''


def get_country_iso3(name):
    try:
        return [x for x in Countries if x['countryName'] == name][0]['iso3']
    except Exception as e:
        return ''


def get_language_iso2(name):
    try:
        return [x for x in Languages if x['alpha3-b'] == name.lower()][0]['alpha2'].upper()
    except Exception as e:
        return ''


def get_business_type(name):
    try:
        return BusinessType[name]
    except Exception as e:
        return 'OT'


def remove_whitespace(in_string: str):
    return in_string.translate(str.maketrans(dict.fromkeys(string.whitespace)))


def smart_str2bool(v):
    try:
        return v.lower() in ('yes', 'true', 't', '1')
    except Exception as e:
        return False


def simple_to_float(s):
    new_float = s
    try:
        new_float = "{:.2f}".format(float(s))
    except Exception as e:
        # print(e)
        pass
    return new_float


def sophisticated_to_float(s):
    new_float = float(0)
    try:
        new_float = float(s)
    except ValueError as v_err:
        # does not contain anything convertible to float
        print(v_err)
    except Exception as ex:
        # Exception occurred while converting to float
        print(ex)

    return new_float


def sophisticated_to_int(s):
    new_int = 0
    try:
        new_int = int(s)
    except ValueError as v_err:
        # print(v_err)
        new_int = sophisticated_to_float_to_int(s)
        # does not contain anything convertible to int
    except Exception as ex:
        # Exception occurred while converting to int
        print(ex)

    return new_int


def sophisticated_to_float_to_int(s):
    new_int = 0
    try:
        new_int = sophisticated_to_int(sophisticated_to_float(s))
    except ValueError as v_err:
        # does not contain anything convertible to int
        print(v_err)
    except Exception as ex:
        # Exception occurred while converting to int
        print(ex)

    return new_int


def reset_stones():
    ok = False
    try:
        Diamond.objects.all().delete()
        status = 'Success'
        message = 'Reset done'
        ok = True
    except Exception as e:
        print(e)
        status = 'Failed'
        message = 'Error on reset'

    return {'ok': ok, 'status': status, 'message': message}


def fetch_rapnetfeed_data():
    ok = False
    status = 'Failed'
    message = 'Something went wrong. Please report!'

    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    payloads = {'username': '78382', 'password': 'rapaport613'}
    response = requests.post('https://technet.rapaport.com/HTTP/Authenticate.aspx', data=payloads, headers=headers)
    if response.status_code == 200:
        auth_ticket = response.content
        # print(auth_ticket)
        payloads = {'ticket': auth_ticket}
        response = requests.post('https://technet.rapaport.com/HTTP/DLS/GetFile.aspx', data=payloads,
                                 headers=headers)
        if response.status_code == 200:
            file_data = response.content
            with open(os.path.join(settings.CSV_PATH, 'rapnetfeed.csv'), 'wb') as output_file:
                output_file.write(file_data)
                ok = True
                status = 'Success'
                message = 'RapnetFeed CSV file fetched & saved'
        else:
            message = 'Unable to get file data from API'
    else:
        message = 'Unable to fetch token.'

    return {'ok': ok, 'status': status, 'message': message}


def fetch_kiran_data():
    ok = False
    status = 'Failed'
    message = 'Something went wrong. Please report!'

    # response = requests.post('http://diamonds.kirangems.com/GemKOnline/jsnsearch/csvdetail/query?username=sdegroup&password=diamond1&view=apitest')
    response = requests.post('http://diamonds.kirangems.com/GemKOnline/jsnsearch/csvdetail/query?username=sdegroup&password=diamond1&view=sdegroup')
    if response.status_code == 200:
        file_data = response.content
        with open(os.path.join(settings.CSV_PATH, 'Report.csv'), 'wb') as output_file:
            output_file.write(file_data)
            ok = True
            status = 'Success'
            message = 'Kiran CSV file fetched & saved'
    else:
        message = 'Unable to get file data from API'

    return {'ok': ok, 'status': status, 'message': message}


def fetch_srk_data():
    ok = False
    status = 'Failed'

    headers = {"X-ACCESS-KEY": "2A336A776D598EF3", "Content-Type": "text/plain"}
    response = requests.post('https://esbprodwso2ei.srk.one/services/SMART_DIAMOND_5772_LTD', headers=headers)
    # response = requests.post('http://hdfiles.in/inventory/myinventory.asmx/GetSRKLiveStockCSV?ShapeList=&ColorList=&ClarityList=&FromCarat=&ToCarat=&StoneID=&LoginName=smart&PassWord=srk613', data=payloads, headers=headers)
    if response.status_code == 200:
        file_data = response.content
        with open(os.path.join(settings.CSV_PATH, 'srkfile.csv'), 'wb') as output_file:
            output_file.write(file_data)
            ok = True
            status = 'Success'
            message = 'SRK CSV file fetched & saved'
    else:
        message = response.content.decode("utf-8")

    return {'ok': ok, 'status': status, 'message': message}


class SmartFTP(ftplib.FTP):
    def makepasv(self):
        invalidhost, port = super(SmartFTP, self).makepasv()
        return self.host, port


def fetch_niru_data_ftp():
    ok = False
    status = 'Failed'
    message = 'Connecting!'

    # ftp = ftplib.FTP(host="3.18.70.68")
    ftp = SmartFTP(host="3.18.70.68")
    ftp.login(user="niru", passwd="@niru#$")
    # ftp.cwd("/")

    # ftp.retrlines('LIST')
    # ftp.set_pasv(True)

    names = ftp.nlst()
    final_names = [line for line in names if '' in line]

    latest_time = None
    latest_name = None

    for name in final_names:
        time = ftp.sendcmd("MDTM " + name)
        if (latest_time is None) or (time > latest_time):
            latest_name = name
            latest_time = time

    # print(latest_name)
    # file = open(latest_name, 'wb')
    # ftp.retrbinary('RETR '+ latest_name, file.write)

    try:
        with open(os.path.join(settings.CSV_PATH, 'niru.csv'), 'wb') as output_file:
            ftp.retrbinary("RETR " + latest_name, output_file.write)
            ok = True
            status = 'Success'
            message = 'Niru CSV file fetched & saved'
    except Exception as e:
        print(e)
        message = 'Error Copying Niru'

    return {'ok': ok, 'status': status, 'message': message}


def send_using_ftp():
    ok = False
    status = 'Failed'
    message = 'Something went wrong. Please report!'

    session = ftplib.FTP('smbs.harmonycrm.com', 'login@sde', 'sde!1234')
    # session = ftplib.FTP_TLS('smbs.harmonycrm.com', 'login@sde', 'sde!1234')
    myfolder = '/Upload/'
    session.cwd(myfolder)  # Change Directory
    try:
        with open(os.path.join(settings.CSV_PATH, 'Results', 'SDE.csv'), 'rb') as file: # file to send
            session.storbinary('STOR SDE.csv', file)  # send the file
            ok = True
            status = 'Success'
            message = 'LAST SDE file sent to Harmony'

    except OSError as e:
        print('exception', e)

    session.quit()

    return {'ok': ok, 'status': status, 'message': message}


def send_custom_email(**kwargs):
        """
        send html email with wrt subject

        """
        try:
            subject = kwargs['subject']
            from_email = kwargs['from_email']
            to_email = kwargs['to_email']
            cc = kwargs['cc']
            bcc = kwargs['bcc']
            text = kwargs['text']
            html = kwargs['html']

            # html_content = render_to_string(template_name=html, context=None, dictionary=kwargs)
            # text_content = render_to_string(template_name=text, context=None, dictionary=kwargs)
            html_content = html
            text_content = text

            msg = EmailMultiAlternatives(subject=subject, body=text_content, from_email=from_email, to=to_email,
                                         cc=cc, bcc=bcc)

            msg.attach_alternative(html_content, "text/html")

            msg.mixed_subtype = 'related'

            # for attachment
            # logo = settings.BASE_DIR + '/media/Images/Property/australia1.jpg'
            # for f in [logo, ]:
            #     fp = open(os.path.join(os.path.dirname(__file__), f), 'rb')
            #     msg_img = MIMEImage(fp.read())
            #     fp.close()
            #     msg_img.add_header('Content-ID', '<{}>'.format(f))
            #     msg.attach(msg_img)

            msg.send()
            msg = 'Email sent to %s' % to_email
        except Exception as e:
            raise


def send_simple_email(**kwargs):
    subject = kwargs['subject']
    from_email = kwargs['from_email']
    to_email = kwargs['to_email']
    body = kwargs['body']

    try:
        send_mail(
            subject=subject,
            message=body,
            from_email=from_email,
            recipient_list=[to_email, ],
            fail_silently=False,
        )
        msg = "Success! Email sent"
        # code = status.HTTP_200_OK
        # return Response(data=msg, status=code)
    except Exception as e:
        raise
        # msg = "Failed! Email not sent Please watch logs for detail"
        # code = status.HTTP_417_EXPECTATION_FAILED
        # return Response(data=msg, status=code)
