import csv
import os
import glob
from datetime import datetime
from django.conf import settings
from diamonds.models import Diamond, CronLog
from YCS.shared_utils import sophisticated_to_int, simple_to_float
from copy import deepcopy
# import math
# from openpyxl import load_workbook
import pandas as pd


def process_vendor():

    RAPNETFEED = 'RapnetFeed'
    SRK = 'SRK'
    KIRAN = 'Kiran'
    NIRU = 'NIRU'

    print('Searching Vendor Files...')
    inputs = [i for i in glob.glob(settings.CSV_PATH + '/*.{}'.format(settings.CSV_EXTENSION))]

    fieldnames = ['LotID', 'LotIDStr', 'Shape', 'Weight', 'Color', 'Clarity', 'Lab', 'Polish', 'Symmetry', 'Fluorescence',
                  'CertNo', 'm1', 'm2', 'm3', 'Depth', 'TablePercent', 'CrownAngle', 'FluorescenceColor', 'Girdle', 'Culet',
                  'Quantity', 'ListPrice', 'RapDiscount', 'SalePrice', 'Status', 'Cut', 'Certificate Filename',
                  'Diamond Filename', 'DateCreated', 'Feed', 'CountryLocation', 'FancyIntensity', 'FancyOvertone']

    # 1-rapfeed 2-srk 3-kiran

    LotID_variations = ['Stock Number', 'STONEID', 'Stone No', 'Stock #']
    LotIDStr_variations = ['Stock Number', 'STONEID', 'Stone No', 'Stock #']
    Shape_variations = ['Shape', 'SHPCODE', 'Shp']
    Weight_variations = ['Weight', 'WEIGHT', 'Cts']
    Color_variations = ['Color', 'COLOR', 'Col']
    Clarity_variations = ['Clarity', 'CLARITY', 'Clr']
    Lab_variations = ['Lab', 'LAB', 'Lab']
    Polish_variations = ['Polish', 'POLISH', 'Pol']
    Symmetry_variations = ['Symmetry', 'SYMMETRY', 'Sym']
    Fluorescence_variations = ['Fluorescence Intensity', 'FLUORESCENCE', 'Flr']
    CertNo_variations = ['Certificate Number', 'CERTIFICATENO', 'Rep No', 'RepNo', 'Certificate ID']
    m1_variations = ['Measurements', 'MINDIA', 'Max', 'Measurements Length']  # extract from "Measurements" for rapfeed,
    m2_variations = ['Measurements', 'MAXDIA', 'Min', 'Measurements Width']  # extract from "Measurements" for rapfeed,
    m3_variations = ['Measurements', 'HEIGHT', 'Hgt', 'Measurements Depth']  # extract from "Measurements" for rapfeed,
    Depth_variations = ['Depth Percent', 'TOTDEPTH', 'TD%', 'TD', 'Depth']
    TablePercent_variations = ['Table Percent', 'TABLED', 'Tbl', 'Table']

    CrownAngle_variations = []  # None for rapfeed, srk, kiran
    FluorescenceColor_variations = []  # None for rapfeed, srk, kiran
    Girdle_variations = []  # None for rapfeed, srk, kiran
    Culet_variations = []  # None for rapfeed, srk, kiran

    Quantity_variations = []  # always "1" for rapfeed, srk, kiran

    ListPrice_variations = ['PRRATE', 'Rap']  # None for rapfeed

    RapDiscount_variations = []  # Calculated for rapfeed, srk
    SalePrice_variations = []  # Calculated for rapfeed, srk

    Status_variations = []  # always "STOCK"

    Cut_variations = ['Cut', 'CUT', 'Cut Grade']
    Certificate_Filename_variations = ['Certificate URL', 'CERT_PATH', 'Certificate', 'certificate', 'CERT PATH',
                                       'CertificateLink']
    Diamond_Filename_variations = ['Image URL', 'IMAGE_PATH', 'Daylight', 'daylight', 'IMAGE PATH', 'Diamond Image']
    DateCreated_variations = ['Date Updated', 'timestamp', 'timestamp']
    Feed_variations = ['Seller Name']  # Split at space for rapnetfeed only
    CountryLocation_variations = []
    FancyIntensity_variations = ['Fancy Intensity', 'Fancy Color Intensity']  # None for srk, kiran
    FancyOvertone_variations = ['Fancy color overtones', 'Fancy Color Overtone']  # None for srk, kiran

    # for filename in inputs:
    #   with open(filename, "r", newline="") as f_in:
    #     reader = csv.reader(f_in)
    #     headers = next(reader)
    #     for h in headers:
    #       if h not in fieldnames:
    #         fieldnames.append(h)

    temp_obj = {'LotID': None, 'LotIDStr': None, 'Shape': None, 'Weight': 0.0, 'Color': None, 'Clarity': None,
                'Lab': None, 'Polish': None, 'Symmetry': None, 'Fluorescence': None, 'CertNo': None, 'm1': 0.0,
                'm2': 0.0, 'm3': 0.0, 'Depth': None, 'TablePercent': None, 'CrownAngle': None,
                'FluorescenceColor': None, 'Girdle': None, 'Culet': None, 'Quantity': '1', 'ListPrice': None,
                'RapDiscount': None, 'SalePrice': None, 'Status': 'STOCK', 'Cut': None, 'Certificate Filename': None,
                'Diamond Filename': None, 'DateCreated': None, 'Feed': None, 'CountryLocation': None,
                'FancyIntensity': None, 'FancyOvertone': None}

    shape_format = {
        'Asscher': 'Sq Emerald',
        'Sq. Emerald': 'Sq Emerald',
        'Baguette': 'Baguette',
        'Cushion Modified': 'Cushion',
        'Emerald': 'Emerald',
        'Heart': 'Heart',
        'Marquise': 'Marquise',
        'Oval': 'Oval',
        'Pear': 'Pear',
        'Princess': 'Princess',
        'Radiant': 'Radiant',
        'Round': 'Round',
        'Trilliant': 'Trilliant',
        'ASH': 'Sq Emerald',
        'CU': 'Cushion',
        'EM': 'Emerald',
        'HT': 'Heart',
        'MQ': 'Marquise',
        'OL': 'Oval',
        'PE': 'Pear',
        'PR': 'Princess',
        'RB': 'Round',
        'RD': 'Radiant',
        'CU_B': 'Cushion',
        'L_RD': 'Radiant',
        'OV': 'Oval',
        'PRN': 'Princess',
        'PS': 'Pear',
        'RBC': 'Round',
        'SQ_E': 'Sq Emerald',
        'SQ_RD': 'Radiant',
    }

    polish_format = {
        'Excellent': 'EXCELLENT',
        'Very Good': 'VERY GOOD',
        'Good': 'GOOD',
        'Fair': 'FAIR',
        'EX': 'EXCELLENT',
        'G': 'GOOD',
        'NONE': '',
        'VG': 'VERY GOOD',
        '-': '',
        'F': 'FAIR',
        '': ''
    }

    symmetry_format = {
        'Excellent': 'EXCELLENT',
        'Very Good': 'VERY GOOD',
        'Good': 'GOOD',
        'Fair': 'FAIR',
        'EX': 'EXCELLENT',
        'G': 'GOOD',
        'NONE': '',
        'VG': 'VERY GOOD',
        '-': '',
        'F': 'FAIR',
        '': ''
    }

    cut_format = {
        'Excellent': 'EXCELLENT',
        'Very Good': 'VERY GOOD',
        'Good': 'GOOD',
        'Fair': 'FAIR',
        'EX': 'EXCELLENT',
        'G': 'GOOD',
        'NONE': '',
        'None': '',
        'VG': 'VERY GOOD',
        '-': '',
        'F': 'FAIR',
        '': '',
        'Poor': 'POOR',
    }

    fluorescence_format = {
        'Faint': 'F',
        'Medium': 'M',
        'None': 'N',
        'Slight': 'SL',
        'Strong': 'STR',
        'Very Slight': 'VSL',
        'Very Strong': 'VST',
        'Fnt': 'F',
        'Med': 'M',
        'Non': 'N',
        'Stg': 'STR',
        'Vst': 'VST',
        'FAINT': 'F',
        'MEDIUM': 'M',
        'NONE': 'N',
        'STRONG': 'STR',
        'VERY STRONG': 'VST',
        '': ''
    }

    country_format = {
        'EZ': 'ISRAEL',
        'NIRU': 'ISRAEL',
        'RAMA': 'ISRAEL',
        'ERAN': 'ISRAEL',
        'KIRAN': 'HONG KONG',
        'SRK': 'HONG KONG',
        'AMC': 'BELGIUM',
        'GUT': 'BELGIUM',
        'SDE': 'BELGIUM',

        'GUTFRUND': 'BELGIUM',
        'GUTFREUND': 'BELGIUM',
        'SMART': 'BELGIUM',
    }

    shape_skip_list = ['Trilliant', ]
    polish_skip_list = ['Fair', 'F']
    symmetry_skip_list = ['Fair', 'F']
    cut_skip_list = ['Fair', 'F', 'Poor']
    # lab_skip_list = ['IIDGR', 'NGTC', 'NON', 'SRK', 'GSI']
    lab_keep_list = ['GIA', 'IGI', 'HRD']
    clarity_skip_list = ['I1', 'I2', 'I3']
    color_skip_list = ['I', 'J', 'K', 'L', 'M', 'N', 'O', 'Q', 'S', 'U', 'W', 'Y', 'FANCY', 'O-Z']

    id_prefix = {
        'EZ': '44',
        'NIRU': '44',
        'RAMA': '',
        'ERAN': '',
        'KIRAN': '',
        'SRK': '88',
        'AMC': '660060',
        'GUT': '9099009',
        'SDE': '',

        'GUTFREUND': '9099009',
        'GUTFRUND': '9099009',
        'SMART': '',
    }

    id_value = {
        'EZ': 'Certificate Number',
        'NIRU': 'Stock #',
        'RAMA': 'Stock Number',
        'ERAN': 'Certificate Number',
        'KIRAN': 'Stone No',
        'SRK': 'STONEID',
        'AMC': 'Stock Number',
        'GUT': 'Stock Number',
        'SDE': 'Stock Number',

        'GUTFRUND': 'Stock Number',
        'GUTFREUND': 'Stock Number',
        'SMART': 'Stock Number',
    }

    vendor_feed = {
        SRK: ['SRK'],
        NIRU: ['NIRU'],
        RAPNETFEED: ['EZ', 'RAMA', 'ERAN', 'AMC', 'GUT', 'SDE', 'GUTFRUND', 'SMART'],
        KIRAN: ['KIRAN'],
        None: ['']
    }

    def format_id(lot_id, feed):
        return id_prefix[feed] + str(lot_id)

    # function to round the number
    def round_10(n):
        # Smaller multiple
        a = (n // 10) * 10

        # Larger multiple
        b = a + 10

        # Return of closest of two
        return b if n - a > b - n else a

    with open(os.path.join(settings.CSV_PATH, 'Results', 'SDE.csv'), "w", newline="") as f_out, \
            open(os.path.join(settings.CSV_PATH, 'Results', 'Rejected.csv'), "w", newline="") as rejected:
        # writer = csv.DictWriter(f_out, fieldnames=fieldnames, delimiter=',')
        writer = csv.DictWriter(f_out, fieldnames=fieldnames, delimiter="\t")
        writer.writeheader()

        # writer_rejected = csv.DictWriter(rejected, fieldnames=fieldnames, delimiter=',')
        writer_rejected = csv.DictWriter(rejected, fieldnames=fieldnames, delimiter="\t")
        writer_rejected.writeheader()

        print('Generating Files! Please Wait...')
        for filename in inputs:
            # date_created = datetime.now().strftime("%B %d %Y %H:%M %p %a %Z")
            date_created = datetime.now().strftime("%b. %d %Y %H:%M %p")

            if 'rapnetfeed' in filename.lower():
                vendor = RAPNETFEED
            elif 'srk' in filename.lower():
                vendor = SRK
            elif 'report' in filename.lower() or 'kiran' in filename.lower():
                vendor = KIRAN
            elif 'niru' in filename.lower():
                vendor = NIRU
                feed_name = NIRU
            else:
                vendor = None

            print('clearing old records of this vendor')
            Diamond.objects.filter(feed__in=vendor_feed[vendor]).delete()

            list_prices = {}

            # wb2 = load_workbook(os.path.join(settings.CSV_PATH, '310120 RAP PRICES.xlsx'))

            list_prices = {}

            if vendor == RAPNETFEED or vendor == KIRAN or vendor == SRK or vendor == NIRU:

                df = pd.read_excel(os.path.join(settings.MEDIA_PATH, '310120 RAP PRICES.xlsx'))
                df = df.fillna('')

                for index, row in df.iterrows():
                    size = row['Size']
                    size = size.split('-')
                    if len(size) > 1:
                        f = float(size[0])
                        t = float(size[1])
                    else:
                        f = float(size[0][:-1])
                        t = 100

                    range = {'from': f, 'to': t, 'list_price': int(row['List']), 'shape': row['Shape']}
                    try:
                        c = list_prices[row['Color']]
                    except:
                        c = {}
                    try:
                        c[row['Clarity']]
                    except:
                        c[row['Clarity']] = []

                    c[row['Clarity']].append(range)
                    list_prices[row['Color']] = c

            # from encodings.aliases import aliases
            # for encoding in set(aliases.values()):
            #     try:
            #         print(encoding)
            #         df = pd.read_csv(filename, encoding=encoding)
            #         print('successful', encoding)
            #     except:
            #         pass

            # import chardet
            # with open(filename, 'rb') as f:
            #     result = chardet.detect(f.readline())
            #     print(result)

            # import codecs
            # csvReader = csv.reader(codecs.open('file.csv', 'rU', 'utf-16'))

            # Check this cp437, utf8,
            # ignore, replace, encoding=None
            with open(filename, "r", newline="") as f_in:
                reader = csv.DictReader(f_in)
                diamonds_list = []
                # skip_types = {}
                for row in reader:
                    try:
                        skip = False
                        sde_item = False
                        temp = deepcopy(temp_obj)
                        temp['DateCreated'] = date_created

                        if vendor is RAPNETFEED:

                            feed_name = row['Seller Name'].split()[0].upper()
                            if feed_name == 'GUTFREUND':
                                feed_name = 'GUTFRUND'

                            if feed_name == 'SMART':
                                feed_name = 'SDE'
                                sde_item = True

                            # if row['Price Percentage'] is not '':

                            price_per_carat = float(row['Price Per Carat'])

                            if sde_item and row['Color'] is '':
                                list_price = price_per_carat
                            else:
                                # old formula
                                # list_price = round_10(price_per_carat / (1 + price_percent))

                                list_price = 0

                                shape = row['Shape']
                                if shape == 'Round':
                                    shape = 'BR'
                                else:
                                    shape = 'PS'

                                weight = float(row['Weight'])
                                # clarity patch
                                clarity = row['Clarity']
                                if clarity == 'FL':
                                    clarity = 'IF'
                                price_range = list_prices[row['Color']][clarity]
                                for range in price_range:
                                    if weight >= range['from'] and weight <= range['to'] and shape == range['shape']:
                                            list_price = range['list_price']
                                            break

                            try:
                                # case if divided by 0
                                price_percent = (price_per_carat / list_price) - 1
                            except:
                                price_percent = 0


                            # if feed_name == 'ERAN':
                            #     rap_disc = (sophisticated_to_int(price_percent * -100) - 12) * -1
                            # elif feed_name == 'AMC' or feed_name == 'GUTFRUND':
                            #     rap_disc = (sophisticated_to_int(price_percent * -100) - 10) * -1
                            # elif feed_name == 'RAMA' or feed_name == 'EZ' or feed_name == 'NIRU':
                            #     rap_disc = (sophisticated_to_int(price_percent * -100) - 9) * -1
                            # elif feed_name == 'SMART' or feed_name == 'SDE':
                            #     rap_disc = (sophisticated_to_int(price_percent * -100)) * -1
                            # else:
                            #     rap_disc = 0

                            if feed_name == 'ERAN' or feed_name == 'AMC' or feed_name == 'GUTFRUND':
                                rap_disc = (sophisticated_to_int(price_percent * -100) - 10)
                            elif feed_name == 'RAMA' or feed_name == 'EZ':

                                # rap_disc = (sophisticated_to_int(price_percent * -100) - 9)
                                rap_disc = (sophisticated_to_int(price_percent * -100) - 10)

                            elif feed_name == 'SMART' or feed_name == 'SDE':
                                rap_disc = (sophisticated_to_int(price_percent * -100))
                            else:
                                rap_disc = 0

                            # if rap_disc < 0:

                            rap_disc *= -1

                            sale_price = (100 + rap_disc) * 0.01 * list_price

                            list_price = sophisticated_to_int(list_price)
                            rap_disc = sophisticated_to_int(rap_disc)
                            sale_price = sophisticated_to_int(sale_price)
                            # else:
                            #     list_price = None
                            #     rap_disc = None
                            #     sale_price = sophisticated_to_int(row['Price Per Carat'])

                            temp['ListPrice'] = list_price
                            temp['RapDiscount'] = rap_disc
                            temp['SalePrice'] = sale_price
                            # temp['DateCreated'] = row['Date Updated']

                            # New Filters
                            if sde_item is False and row['Country'].lower() != 'belgium' and row['Country'].lower() != 'israel':
                                skip = True

                        elif vendor is NIRU:
                            # if row['Price Percentage'] is not '':

                            price_per_carat = float(row['Price Per Carat'])

                            # old formula
                            # list_price = round_10(price_per_carat / (1 + price_percent))

                            list_price = 0

                            shape = row['Shape']
                            if shape == 'RB':
                                shape = 'BR'
                            else:
                                shape = 'PS'


                            weight = float(row['Weight'])
                            price_range = list_prices[row['Color']][row['Clarity']]
                            for range in price_range:
                                if weight >= range['from'] and weight <= range['to'] and shape == range['shape']:
                                        list_price = range['list_price']
                                        break

                            try:
                                # case if divided by 0
                                price_percent = (price_per_carat / list_price) - 1
                            except:
                                price_percent = 0

                            # rap_disc = (sophisticated_to_int(price_percent * -100) - 9)
                            rap_disc = (sophisticated_to_int(price_percent * -100) - 10)

                            # if rap_disc < 0:

                            rap_disc *= -1

                            sale_price = (100 + rap_disc) * 0.01 * list_price

                            list_price = sophisticated_to_int(list_price)
                            rap_disc = sophisticated_to_int(rap_disc)
                            sale_price = sophisticated_to_int(sale_price)

                            temp['ListPrice'] = list_price
                            temp['RapDiscount'] = rap_disc
                            temp['SalePrice'] = sale_price
                            # temp['DateCreated'] = row['Date Updated']

                        elif vendor is SRK:
                            feed_name = vendor.upper()

                            # pper = float(row['PPER'])

                            # try:
                            #     list_price = float(row['PRRATE'].split('"')[0])
                            # except ValueError:
                            #     list_price = float(row['PRRATE'])
                            #
                            # rap_disc = sophisticated_to_int(pper - 10) * -1
                            # sale_price = (100 + rap_disc) * 0.01 * list_price

                            list_price = 0

                            shape = row['SHAPE']
                            if shape == 'ROUND':
                                shape = 'BR'
                            else:
                                shape = 'PS'

                            try:
                                weight = float(row['WEIGHT'])
                                price_range = list_prices[row['COLOR']][row['CLARITY']]

                                for range in price_range:
                                    if weight >= range['from'] and weight <= range['to'] and shape == range['shape']:
                                        list_price = range['list_price']
                                        break
                            except Exception as e:
                                list_price = 0

                            pper = float(row['PPER'])

                            # try:
                            #     list_price = float(row['PRRATE'].split('"')[0])
                            # except ValueError:
                            #     list_price = float(row['PRRATE'])
                            #
                            rap_disc = sophisticated_to_int(pper - 10) * -1
                            # sale_price = (100 + rap_disc) * 0.01 * list_price

                            if list_price > 0:
                                # p_rate = float(row['PRATE'])
                                #
                                # disc = (1-(p_rate / list_price)) * 100
                                #
                                # if disc > 0:
                                #     rap_disc = sophisticated_to_int(disc - 10) * -1
                                # else:
                                #     rap_disc = round(disc) + 10
                                #     if rap_disc < 0:
                                #         rap_disc *= -1

                                sale_price = (100 + rap_disc) * 0.01 * list_price
                            else:
                                rap_disc = 0
                                sale_price = 0

                            temp['ListPrice'] = sophisticated_to_int(list_price)
                            temp['RapDiscount'] = sophisticated_to_int(rap_disc)
                            temp['SalePrice'] = sophisticated_to_int(sale_price)
                            # temp['DateCreated'] = datetime.now()

                            # New Filters
                            if row['STONESTATUS'].lower() != 'available':
                                skip = True

                            if row['BROWN INCLUSION'] != 'NONE' and row['BROWN INCLUSION'] != 'BROWN TINGE' and \
                                    row['BROWN INCLUSION'] != 'GREEN TINGE' and row['BROWN INCLUSION'] != 'MIX TINGE 1' \
                                    and row['BROWN INCLUSION'] != 'PINK TINGE':
                                skip = True

                            if row['LUSTER'] != 'EXCELLENT' and row['LUSTER'] != 'VERY GOOD' and row['LUSTER'] != 'GOOD':
                                skip = True

                        elif vendor is KIRAN:
                            feed_name = vendor.upper()

                            price = float(row['Price'])

                            list_price = 0

                            shape = row['Shp']
                            if shape == 'RB' or shape == 'BR' or shape == 'RBC':
                                shape = 'BR'
                            else:
                                shape = 'PS'

                            try:
                                weight = float(row['Cts'])
                                price_range = list_prices[row['Col']][row['Clr']]

                                for range in price_range:
                                    if weight >= range['from'] and weight <= range['to'] and shape == range['shape']:
                                        list_price = range['list_price']
                                        break
                            except Exception as e:
                                # print('err', e)
                                list_price = 0

                            if list_price != 0:
                                disc = (1 - (price / list_price)) * 100
                            else:
                                disc = 0

                            if disc == 0:
                                rap_disc = 0
                            elif disc > 0:
                                rap_disc = sophisticated_to_int(disc - 10) * -1
                            else:
                                rap_disc = round(disc) + 10
                                if rap_disc < 0:
                                    rap_disc *= -1

                            # list_price = float(row['Rap'])
                            # disc = float(row['Disc%'])
                            # rap_disc = sophisticated_to_int(disc - 10) * -1

                            sale_price = (100 + rap_disc) * 0.01 * list_price

                            temp['ListPrice'] = sophisticated_to_int(list_price)
                            temp['RapDiscount'] = sophisticated_to_int(rap_disc)
                            temp['SalePrice'] = sophisticated_to_int(sale_price)
                            # temp['DateCreated'] = datetime.now()

                            # New Filters
                            if row['Status'].lower() != 'available':
                                # try:
                                #     skip_types['Status']
                                # except:
                                #     skip_types['Status'] = {}
                                #
                                # try:
                                #     skip_types['Status'][row['Status']] = skip_types['Status'][row['Status']] + 1
                                # except:
                                #     skip_types['Status'][row['Status']] = 1
                                skip = True

                            if row['Loc'].lower() != 'belgium' and row['Loc'].lower() != 'india':
                                skip = True

                            if row['ColTinge'] != 'BRN1' and row['ColTinge'] != 'Mix Tinge1' and row['ColTinge'] != '':
                                skip = True

                            if row['Luster'] != 'M1' and row['Luster'] != '':
                                skip = True

                        else:
                            feed_name = None

                        # # TEMPORARY MASK
                        # if feed_name == 'NIRU':
                        #     feed_name = 'EZ'

                        temp['Feed'] = feed_name
                        temp['CountryLocation'] = country_format[feed_name]

                        if feed_name == 'EZ':
                            try:
                                temp['LotID'] = format_id(row[id_value[feed_name]], feed_name)
                            except:
                                temp['LotID'] = row['Stock #']
                        else:
                            temp['LotID'] = format_id(row[id_value[feed_name]], feed_name)

                        temp['LotIDStr'] = temp['LotID']

                        for col in row:
                            # if col in LotID_variations:

                            if col in Shape_variations:
                                if sde_item is False and row[col] in shape_skip_list:
                                    skip = True
                                    # break
                                temp['Shape'] = shape_format[row[col]]

                            elif col in Weight_variations:
                                temp['Weight'] = row[col]

                            elif col in Color_variations:
                                if sde_item is False and (row[col] in color_skip_list or "NATURAL " in row[col]):
                                    skip = True
                                    # break
                                temp['Color'] = row[col]
                                if vendor is RAPNETFEED and row[col] is '' and (feed_name is 'SMART' or feed_name is 'SDE'):
                                    temp['Color'] = row['Fancy Color']

                            elif col in Clarity_variations:
                                if sde_item is False and row[col] in clarity_skip_list:
                                    skip = True
                                    # break

                                if row[col] == 'LC':
                                    temp['Clarity'] = 'IF'

                                temp['Clarity'] = row[col]

                            elif col in Lab_variations:
                                if sde_item is False and row[col] not in lab_keep_list:
                                    skip = True

                                # if sde_item is False and row[col] in lab_skip_list:
                                #     skip = True
                                #     # break
                                temp['Lab'] = row[col]

                            elif col in Polish_variations:
                                if sde_item is False and row[col] in polish_skip_list:
                                    skip = True
                                    # break
                                temp['Polish'] = polish_format[row[col]]

                            elif col in Symmetry_variations:
                                if sde_item is False and row[col] in symmetry_skip_list:
                                    skip = True
                                    # break
                                temp['Symmetry'] = symmetry_format[row[col]]

                            elif col in Fluorescence_variations:
                                temp['Fluorescence'] = fluorescence_format[row[col]]

                            elif col in CertNo_variations:
                                temp['CertNo'] = row[col]

                            elif col in m1_variations:
                                if row[col] is not '':
                                    temp['m1'] = row[col]
                                    if col == m1_variations[0]:
                                        mm = row[col].split(' x ')
                                        temp['m1'] = "{:.2f}".format(float(mm[0]))
                                        temp['m2'] = "{:.2f}".format(float(mm[1]))
                                        temp['m3'] = "{:.2f}".format(float(mm[2]))
                                    else:
                                        temp['m1'] = "{:.2f}".format(float(temp['m1']))
                                # if row[col] is not '':
                                #     temp['m1'] = row[col]
                                #     if col == m1_variations[0]:
                                #         mm = row[col].split(' x ')
                                #         temp['m1'] = simple_to_float(mm[0])
                                #         temp['m2'] = simple_to_float(mm[1])
                                #         temp['m3'] = simple_to_float(mm[2])
                                #     else:
                                #         temp['m1'] = simple_to_float(temp['m1'])
                            elif col in m2_variations:
                                temp['m2'] = "{:.2f}".format(float(row[col]))
                                # temp['m2'] = simple_to_float(row[col])
                            elif col in m3_variations:
                                temp['m3'] = "{:.2f}".format(float(row[col]))
                                # temp['m3'] = simple_to_float(row[col])

                            elif col in Depth_variations:
                                if row[col] is not '':
                                    temp['Depth'] = "{:.1f}".format(float(row[col]))
                                    # print(temp['Depth'])

                            elif col in TablePercent_variations:
                                if row[col] is not '':
                                    temp['TablePercent'] = sophisticated_to_int(float(row[col]))

                            elif col in CrownAngle_variations:
                                temp['CrownAngle'] = row[col]

                            elif col in FluorescenceColor_variations:
                                temp['FluorescenceColor'] = row[col]

                            elif col in Girdle_variations:
                                temp['Girdle'] = row[col]

                            elif col in Culet_variations:
                                temp['Culet'] = row[col]

                            elif col in Cut_variations:
                                if sde_item is False and row[col] in cut_skip_list:
                                    skip = True
                                    # break
                                temp['Cut'] = cut_format[row[col]]

                            elif col in Certificate_Filename_variations:
                                # temp['Certificate Filename'] = ''
                                file_path = row[col]
                                temp['Certificate Filename'] = file_path
                                if vendor is KIRAN:
                                    file_path = file_path.replace('http://diamonds.kirangems.com', 'http://cdn1.sdegroup.com')
                                    temp['Certificate Filename'] = file_path

                                # New Masking
                                if vendor is SRK:
                                    if row['CERTIFICATENO'] == '':
                                        temp['Certificate Filename'] = ''
                                    else:
                                        temp['Certificate Filename'] = settings.HTTP_SCHEME + settings.HOST_NAME + \
                                                                       '/diamonds/stone_certificate?vendor=1&c_no=' + \
                                                                       row['CERTIFICATENO']

                                if vendor is KIRAN:
                                    if row[col] == '-':
                                        temp['Certificate Filename'] = ''
                                    else:
                                        temp['Certificate Filename'] = settings.HTTP_SCHEME + settings.HOST_NAME + \
                                                                       '/diamonds/stone_certificate?vendor=2&c_no=' + \
                                                                       row['RepNo']
                                if vendor is RAPNETFEED or vendor is NIRU:
                                    # temp['Certificate Filename'] = ''
                                    if row[col] == '':
                                        temp['Certificate Filename'] = ''
                                    else:
                                        import urllib
                                        from urllib.parse import urlparse
                                        data = urlparse(row[col])
                                        if data.netloc == 's3.amazonaws.com':
                                            domain = 1
                                            new_path = data.path.replace('rap.certs', 'x.certs')
                                            temp['Certificate Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + new_path, None))

                                        elif data.netloc == 'segoma.com':
                                            domain = 2
                                            # scheme, netloc, url, params, query, fragment, _coerce_result
                                            temp['Certificate Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path + '&q=' + data.query,
                                                 None))

                                        elif data.netloc == 'viewer.feedcenter.net':
                                            domain = 3
                                            from urllib import parse
                                            query_dict = parse.parse_qs(data.query)
                                            # path_list = data.path.split('/')
                                            id = query_dict['ID'][0]

                                            # if len(path_list) == 2:
                                            #     type = '2'
                                            #     v_id = path_list[1]
                                            #     vendor_id = query_dict['VendorID'][0]
                                            #     doc_type = query_dict['DocType'][0]
                                            #
                                            #     # scheme, netloc, url, params, query, fragment, _coerce_result
                                            #     temp['Certificate Filename'] = urllib.parse.urlunparse(
                                            #         (data.scheme, settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                            #          'vendor=3&domain=' + str(domain) + '&s_id=' + id + '&type=' + type +
                                            #          '&v_id=' + v_id + '&vendor_id=' + vendor_id + '&doc_type=' + doc_type,
                                            #          None))
                                            # else:
                                            #     type = '1'
                                            #     # scheme, netloc, url, params, query, fragment, _coerce_result
                                            #     temp['Certificate Filename'] = urllib.parse.urlunparse(
                                            #         (data.scheme, settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                            #          'vendor=3&domain=' + str(domain) + '&s_id=' + id + '&type=' + type +
                                            #          '&v_id=' + path_list[2],
                                            #          None))
                                            temp['Certificate Filename'] = urllib.parse.urlunparse(
                                                (data.scheme, settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                                 'vendor=3&domain=' + str(domain) + '&s_id=' + id,
                                                 # '&v_id=' + path_list[2],
                                                 None))

                                        elif data.netloc == 'www.feedcenter.net':
                                            domain = 4
                                            temp['Certificate Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path, None))

                                        elif data.netloc == 'certs.rapnet.com':
                                            domain = 5
                                            temp['Certificate Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_certificate', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path, None))

                                        else:
                                            print('Unknown net loc', data.netloc)

                            elif col in Diamond_Filename_variations:
                                # temp['Diamond Filename'] = ''
                                file_path = row[col]
                                temp['Diamond Filename'] = file_path
                                if vendor is KIRAN:
                                    file_path = file_path.replace('http://diamonds.kirangems.com', 'http://cdn1.sdegroup.com')
                                    temp['Diamond Filename'] = file_path
                                if feed_name == 'ERAN':
                                    temp['Diamond Filename'] = ''

                                # New Masking
                                if vendor is SRK:
                                    temp['Diamond Filename'] = settings.HTTP_SCHEME + settings.HOST_NAME + \
                                                                   '/diamonds/stone_image?vendor=1&s_id=' + \
                                                                   row['STONEID']

                                if vendor is KIRAN:
                                    if row[col] == '-':
                                        temp['Diamond Filename'] = ''
                                    else:
                                        temp['Diamond Filename'] = settings.HTTP_SCHEME + settings.HOST_NAME + \
                                                                       '/diamonds/stone_image?vendor=2&s_id=' + \
                                                                       row['RepNo']

                                if vendor is RAPNETFEED or vendor is NIRU:
                                    # temp['Diamond Filename'] = ''
                                    if row[col] == '':
                                        temp['Diamond Filename'] = ''
                                    else:
                                        import urllib
                                        from urllib.parse import urlparse
                                        data = urlparse(row[col])
                                        if data.netloc == 'viewer.amcdiamonds.be':
                                            domain = 1
                                            new_query = data.query.replace('http://media.amcdiamonds.com/view/',
                                                                           'secret_js_1?p=')
                                            # new_query = data.query
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path + '&' + new_query,
                                                 None))
                                        elif data.netloc == 'img.diacertimages.com':
                                            domain = 2
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path, None))
                                        elif data.netloc == 'sdegroup.com':
                                            domain = 3
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path, None))
                                        elif data.netloc == 'certs.rapnet.com':
                                            domain = 4
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path, None))
                                        elif data.netloc == 'media.feedcenter.net':
                                            domain = 5
                                            from urllib import parse
                                            query_dict = parse.parse_qs(data.query)
                                            id = query_dict['id'][0]
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&id=' + id, None))

                                        elif data.netloc == 'segoma.com':
                                            domain = 6
                                            from urllib import parse
                                            query_dict = parse.parse_qs(data.query)
                                            id = query_dict['id'][0]
                                            type = query_dict['type'][0]
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&id=' + id + '&type=' + type, None))

                                        elif data.netloc == 'viewer.diamondofantwerpen.com':
                                            domain = 7
                                            new_query = data.query.replace('http://media.diamondofantwerpen.com/view/',
                                                                           'secret_js_2?p=')
                                            # new_query = data.query
                                            temp['Diamond Filename'] = urllib.parse.urlunparse(
                                                ('http', settings.HOST_NAME, '/diamonds/stone_image', None,
                                                 'vendor=3&domain=' + str(domain) + '&p=' + data.path + '&' + new_query,
                                                 None))

                                        else:
                                            print('Unknown net loc', data.netloc)

                            # elif col in DateCreated_variations:
                            #     temp['DateCreated'] = row[col]

                            elif col in FancyIntensity_variations:
                                temp['FancyIntensity'] = row[col]

                            elif col in FancyOvertone_variations:
                                temp['FancyOvertone'] = row[col]

                        if temp['Shape'] == 'Round':
                            if temp['m1'] > temp['m2']:
                                swapper = temp['m2']
                                temp['m2'] = temp['m1']
                                temp['m1'] = swapper

                        # Empty URL Case
                        if temp['Certificate Filename'] == '':
                            if temp['Lab'] == 'GIA':
                                temp['Certificate Filename'] = 'https://www.gia.edu/report-check?reportno=' + temp['CertNo']
                            elif temp['Lab'] == 'IGI':
                                temp['Certificate Filename'] = 'https://www.igi.org/reports/verify-your-report?r=' + temp['CertNo']
                            else:  # temp['Lab'] == 'HRD':
                                temp['Certificate Filename'] = 'https://my.hrdantwerp.com/?id=34&record_number=' + temp['CertNo']

                        if not skip:
                            writer.writerow(temp)
                            diamonds_list.append(
                                Diamond(
                                    lot_id=temp['LotID'],
                                    lot_id_str=temp['LotIDStr'],
                                    shape=temp['Shape'],
                                    weight=temp['Weight'],
                                    color=temp['Color'],
                                    clarity=temp['Clarity'],
                                    lab=temp['Lab'],
                                    polish=temp['Polish'],
                                    symmetry=temp['Symmetry'],
                                    fluorescence=temp['Fluorescence'],
                                    cert_no=temp['CertNo'],
                                    m1=temp['m1'],
                                    m2=temp['m2'],
                                    m3=temp['m3'],
                                    depth=temp['Depth'],
                                    table_percent=temp['TablePercent'],
                                    crown_angle=temp['CrownAngle'],
                                    fluorescence_color=temp['FluorescenceColor'],
                                    girdle=temp['Girdle'],
                                    culet=temp['Culet'],
                                    quantity=temp['Quantity'],
                                    list_price=temp['ListPrice'],
                                    rap_discount=temp['RapDiscount'],
                                    sale_price=temp['SalePrice'],
                                    status=temp['Status'],
                                    cut=temp['Cut'],
                                    certificate_filename=temp['Certificate Filename'],
                                    diamond_filename=temp['Diamond Filename'],
                                    feed=temp['Feed'],
                                    country_location=temp['CountryLocation'],
                                    fancy_intensity=temp['FancyIntensity'],
                                    fancy_overtone=temp['FancyOvertone'],
                                    fluorescence_order=Diamond.FLUORESCENCE_SORTING[temp['Fluorescence']]
                                )
                            )

                            if len(diamonds_list) == 10000:
                                print('bulk create phase')
                                Diamond.objects.bulk_create(diamonds_list)
                                diamonds_list = []

                        else:
                            writer_rejected.writerow(temp)
                    except Exception as e:
                        print(e)
                        CronLog.objects.create(vendor=vendor, event='Compiling', description=e,
                                               tech_description=str(row)[:2047], status=CronLog.Failed, type=CronLog.Current)

            if len(diamonds_list) > 0:
                print('bulk create phase last')
                Diamond.objects.bulk_create(diamonds_list)

            os.rename(filename, os.path.join(settings.CSV_PATH, 'Archived', '%s')
                      % vendor+datetime.now().strftime("-%Y-%m-%d-%H-%M-%S.")+settings.CSV_EXTENSION)
