# SDE Dashboard
This is dashboard for SDE Group. It is coded in Django.

## Installation
It requires Python 3.6.3 and a virtual environment. 

#### Virtual Environment
Create a virtualenv to isolate package dependencies locally 
```sh
virtualenv env
source env/bin/activate  # On Windows use `env\Scripts\activate`
```

#### Installing dependencies
To install the dependencies use the requirements.txt file.

```sh
$ pip3 install -r requirements.txt
```

#### Apply migrations
To apply migrations run this command
```sh
$ python manage.py migrate
```

#### Creating SuperUser
Create superuser by running this
```sh
$ python manage.py createsuperuser --email admin@example.com --username admin
```

#### Running the server
We're now ready to test the dashboard. Let's fire up the server from the command line.
```sh
$ python manage.py runserver
```
### Database
We are using Postgres DB Instance.

### Build & Deployment
Code is deployed at http://backoffice.sdegroup.com/ using github and AWS EC2 instance.

### License and author info
