from django.core.management.base import BaseCommand, CommandError
from accounts.models import User


class Command(BaseCommand):
    help = 'Performs users_cleaning operations.'

    def add_arguments(self, parser):
        # parser.add_argument('--id')
        pass

    def handle(self, *args, **options):
        try:
            User.objects.filter(user_type=User.Customer).delete()

            msg = 'users_cleaning command executed'
            self.stdout.write(self.style.SUCCESS(msg))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error -> ' + str(e)))
            raise CommandError("Error in users_cleaning command")

# command: python manage.py clear_users


