from django.core.management.base import BaseCommand, CommandError
from diamonds.models import CronLog
# from oauth2_provider.models import AccessToken
# from django.db import transaction
from datetime import timedelta   # date, datetime
from django.utils import timezone
from django.conf import settings
from django.contrib.sessions.models import Session
from django.contrib.admin.models import LogEntry
import glob, os, os.path


class Command(BaseCommand):
    help = 'Performs weekly_cleaning operations.'

    def add_arguments(self, parser):
        # parser.add_argument('--id')
        pass

    def handle(self, *args, **options):
        try:

            # Dump to other database
            # with transaction.atomic():
            #     old_log = APIRequestLog.objects.all()
            #     APIRequestLog.objects.using('dump_data').bulk_create(old_log)
            #     old_log.delete()

            CronLog.objects.filter(type=CronLog.Archived).delete()
            # AccessToken.objects.filter(expires__lte=timezone.now()).delete()
            Session.objects.filter(expire_date__lte=timezone.now()).delete()
            # delete records older than 3 days
            cutoff = timezone.now() - timedelta(days=3)        # days=31 minutes=5
            LogEntry.objects.filter(action_time__lte=cutoff).delete()

            # vendor CSV
            files_list = [i for i in glob.glob(os.path.join(settings.CSV_PATH, 'Archived') + '/*.{}'.format(settings.CSV_EXTENSION))]
            for f in files_list:
                os.remove(f)

            # user CSV
            files_list = [i for i in glob.glob(os.path.join(settings.CSV_PATH, 'Users', 'Archived') + '/*.{}'.format(settings.CSV_EXTENSION))]
            for f in files_list:
                os.remove(f)

            # Clearing logs older than 7 days
            os.system("journalctl --vacuum-time=7d")

            msg = 'weekly_cleaning command executed'
            self.stdout.write(self.style.SUCCESS(msg))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error -> ' + str(e)))
            raise CommandError("Error in weekly_cleaning command")

# command: python manage.py weekly_cleaning


