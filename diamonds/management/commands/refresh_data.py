from django.core.management.base import BaseCommand, CommandError
from diamonds.models import CronSchedule, CronLog
from datetime import datetime
from YCS.merger_fast import process_vendor
from YCS.shared_utils import reset_stones, fetch_rapnetfeed_data, fetch_niru_data_ftp, \
    fetch_kiran_data, fetch_srk_data, send_using_ftp
# import traceback
# import sys
import os
import glob
from django.conf import settings


class Command(BaseCommand):
    help = 'Refresh Stones Data on specified intervals'

    def add_arguments(self, parser):
        # parser.add_argument('--id')
        pass

    def handle(self, *args, **options):

        try:
            # First Method
            # time_now = datetime.datetime.now()
            # time_5min_ago = time_now - datetime.timedelta(minutes=5)
            # self.stdout.write(self.style.SUCCESS('Now: ' + str(time_now)))
            # self.stdout.write(self.style.SUCCESS('5 min. ago: ' + str(time_5min_ago)))
            # crons = CronSchedule.objects.filter(schedule_time__gte=time_5min_ago.strftime('%H:%M')).exclude(
            #     schedule_time__gt=time_now.strftime('%H:%M'))
            #
            # Second Method
            crons = CronSchedule.objects.filter(schedule_time=datetime.now().strftime('%H:%M'))

            crons = crons.exclude(status=CronSchedule.running)
            for cron in crons:
                cron.status = CronSchedule.running
                cron.save()
                self.automate_tasks()
                # self.stdout.write(self.style.SUCCESS('scheduled: ' + str(cron.schedule_time) + ' ' + cron.status))
                cron.status = CronSchedule.idle
                cron.save()
                # raise CommandError("Testing if it reached here!")
                # self.stdout.write(self.style.SUCCESS('scheduled: ' + str(cron.schedule_time) + ' ' + cron.status))
            # cron = CronSchedule.objects.create(schedule_time=time)
            # self.stdout.write(self.style.SUCCESS(str(cron.pk)))
            # self.stdout.write(self.style.SUCCESS('Success: Cron added'))

        except Exception as e:
            self.stdout.write(self.style.ERROR('Error -> ' + str(e)))
            # traceback.print_exc(file=sys.stderr)

        # if options['id']:
        # else:

    def automate_tasks(self):
        try:
            # Archive old record
            CronLog.objects.filter(type=CronLog.Current).update(type=CronLog.Archived)

            # Archive Directory
            inputs = [i for i in glob.glob(settings.CSV_PATH + '/*.{}'.format(settings.CSV_EXTENSION))]
            for filename in inputs:
                os.rename(filename, os.path.join(settings.CSV_PATH, 'Archived', '%s')
                          % 'JunkFile' + datetime.now().strftime("-%Y-%m-%d-%H-%M-%S.") + settings.CSV_EXTENSION)

            # RESET Log
            CronLog.objects.create(vendor=None, event='DB Reset', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            # RESET Stones
            response = reset_stones()
            if response['ok']:
                CronLog.objects.create(vendor=None, event='DB Reset', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor=None, event='DB Reset', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)

            # RAPNETFEED
            CronLog.objects.create(vendor='RapnetFeed', event='Fetching', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            response = fetch_rapnetfeed_data()
            if response['ok']:
                CronLog.objects.create(vendor='RapnetFeed', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor='RapnetFeed', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)

            # NIRU
            CronLog.objects.create(vendor='NIRU', event='Copying', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            response = fetch_niru_data_ftp()
            if response['ok']:
                CronLog.objects.create(vendor='NIRU', event='Copying', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor='NIRU', event='Copying', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)

            # Kiran
            CronLog.objects.create(vendor='Kiran', event='Fetching', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            response = fetch_kiran_data()
            if response['ok']:
                CronLog.objects.create(vendor='Kiran', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor='Kiran', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)

            # SRK
            CronLog.objects.create(vendor='SRK', event='Fetching', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            response = fetch_srk_data()
            if response['ok']:
                CronLog.objects.create(vendor='SRK', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor='SRK', event='Fetching', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)

            try:
                CronLog.objects.create(vendor=None, event='Compiling', description=None,
                                       tech_description=None, status=CronLog.Running, type=CronLog.Current)
                process_vendor()
                CronLog.objects.create(vendor=None, event='Compiling', description='Files processed',
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            except Exception as e:
                print(e)
                CronLog.objects.create(vendor=None, event='Compiling', description='Something went wrong. Please report',
                                       tech_description=e, status=CronLog.Failed, type=CronLog.Current)

            # FTP
            CronLog.objects.create(vendor=None, event='Sending to Harmony', description=None,
                                   tech_description=None, status=CronLog.Running, type=CronLog.Current)
            response = send_using_ftp()
            if response['ok']:
                CronLog.objects.create(vendor=None, event='Sending to Harmony', description=response['message'],
                                       tech_description=None, status=CronLog.Complete, type=CronLog.Current)
            else:
                CronLog.objects.create(vendor=None, event='Sending to Harmony', description=response['message'],
                                       tech_description=None, status=CronLog.Failed, type=CronLog.Current)
        except Exception as e:
            CronLog.objects.create(vendor=None, event='Automation', description=
                                   'Error in Automate Series',
                                   tech_description=e, status=CronLog.Failed, type=CronLog.Current)
            raise CommandError("Error in Automate Series")
