from django.db import models
from django.conf import settings

User = settings.AUTH_USER_MODEL


class Diamond(models.Model):
    FLUORESCENCE_SORTING = {
        '': 1,
        'N': 2,
        'F': 3,
        'VSL': 4,
        'SL': 5,
        'M': 6,
        'STR': 7,
        'VST': 8,
    }

    DISPLAY_ABBREVIATIONS = {
        'EXCELLENT': 'EX',
        'VERY GOOD': 'VG',
        'GOOD': 'G',
        'FAIR': 'F',
        '': '',
    }
    SUPPLIER_CODES = {
        'AMC': '66',
        'ERAN': '0',
        # 'EZ': '00',
        # TEMPORARY MASK
        'EZ': '44',
        'NIRU': '44',
        'GUTFRUND': '9099',
        'KIRAN': '1007',
        'LSDCO IL': '000',
        'SDE': '1010',
        'SRK': '88',
    }

    # TODO size and validity of fields
    lot_id = models.CharField(max_length=32, null=True, blank=True)
    lot_id_str = models.CharField(max_length=32, null=True, blank=True)
    shape = models.CharField(max_length=16, null=True, blank=True)
    weight = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    color = models.CharField(max_length=8, null=True, blank=True)

    clarity = models.CharField(max_length=8, null=True, blank=True)
    lab = models.CharField(max_length=8, null=True, blank=True)
    polish = models.CharField(max_length=16, null=True, blank=True)
    symmetry = models.CharField(max_length=16, null=True, blank=True)
    fluorescence = models.CharField(max_length=8, null=True, blank=True)

    cert_no = models.CharField(max_length=32, null=True, blank=True)
    m1 = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    m2 = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    m3 = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    depth = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)

    table_percent = models.PositiveIntegerField(null=True, blank=True)
    crown_angle = models.CharField(max_length=16, null=True, blank=True)
    fluorescence_color = models.CharField(max_length=16, null=True, blank=True)
    girdle = models.CharField(max_length=16, null=True, blank=True)
    culet = models.CharField(max_length=16, null=True, blank=True)

    quantity = models.PositiveIntegerField(default=1)
    list_price = models.PositiveIntegerField(null=True, blank=True)
    rap_discount = models.IntegerField(null=True, blank=True)
    sale_price = models.PositiveIntegerField(null=True, blank=True)
    status = models.CharField(max_length=8, null=True, blank=True)

    cut = models.CharField(max_length=16, null=True, blank=True)
    certificate_filename = models.URLField(max_length=256, null=True, blank=True)
    diamond_filename = models.URLField(max_length=256, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    feed = models.CharField(max_length=16, null=True, blank=True)
    country_location = models.CharField(max_length=16, null=True, blank=True)
    fancy_intensity = models.CharField(max_length=16, null=True, blank=True)
    fancy_overtone = models.CharField(max_length=16, null=True, blank=True)
    fluorescence_order = models.PositiveIntegerField(default=1)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'diamond'
        verbose_name_plural = 'diamonds'

    # TO STRING METHOD
    def __str__(self):
        return self.lot_id + ' ' + self.feed + ' ' + self.pk

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class CronSchedule(models.Model):
    daily = 'daily'
    weekly = 'weekly'
    monthly = 'monthly'
    FREQUENCY_CHOICES = (
        (daily, 'daily'),
        (weekly, 'weekly'),
        (monthly, 'monthly'),
    )

    running = 'running'
    paused = 'paused'
    idle = 'idle'
    STATUS_CHOICES = (
        (running, 'running'),
        (paused, 'paused'),
        (idle, 'idle'),
    )

    frequency = models.CharField(max_length=20, choices=FREQUENCY_CHOICES, default=daily)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=idle)
    schedule_time = models.TimeField(u"Cron Schedule Time", null=False, blank=False)
    schedule_day = models.PositiveIntegerField(u"Cron Schedule Day", null=True, blank=True)
    schedule_week = models.PositiveIntegerField(u"Cron Schedule Week", null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    last_run = models.DateTimeField(null=True, blank=True)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Cron Schedule'
        verbose_name_plural = 'Cron Schedules'

    # TO STRING METHOD
    def __str__(self):
        return str(self.schedule_time)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class CronLog(models.Model):
    Complete = 'Complete'
    Running = 'Running'
    Failed = 'Failed'
    STATUS_CHOICES = (
        (Complete, 'Complete'),
        (Running, 'Running'),
        (Failed, 'Failed'),
    )

    RapnetFeed = 'RapnetFeed'
    SRK = 'SRK'
    Kiran = 'Kiran'
    VENDOR_CHOICES = (
        (RapnetFeed, 'RapnetFeed'),
        (SRK, 'SRK'),
        (Kiran, 'Kiran'),
    )

    Archived = 'Archived'
    Current = 'Current'
    TYPE_CHOICES = (
        (Archived, 'Archived'),
        (Current, 'Current'),
    )

    date_time = models.DateTimeField(auto_now_add=True)
    vendor = models.CharField(max_length=20, choices=VENDOR_CHOICES, null=True, blank=True)
    event = models.CharField(max_length=64)
    description = models.CharField(max_length=255, null=True, blank=True)
    tech_description = models.CharField(max_length=2048, null=True, blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    type = models.CharField(max_length=20, choices=TYPE_CHOICES)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Cron Log'
        verbose_name_plural = 'Cron Logs'

    # TO STRING METHOD
    def __str__(self):
        return str(self.date_time)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class ActivityLog(models.Model):
    Archived = 'Archived'
    Current = 'Current'
    TYPE_CHOICES = (
        (Archived, 'Archived'),
        (Current, 'Current'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    search_time = models.DateTimeField(auto_now_add=True)
    state = models.TextField(null=False, blank=False)
    search_source = models.CharField(max_length=256, null=False, blank=False)
    results_no = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=20, choices=TYPE_CHOICES, default=Current)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Activity Log'
        verbose_name_plural = 'Activity Logs'

    # TO STRING METHOD
    def __str__(self):
        return str(self.search_source)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.
