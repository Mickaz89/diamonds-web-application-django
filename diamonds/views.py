from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseServerError
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.cache import cache
# from django.conf import settings
# import os
from requests.exceptions import ConnectionError

import csv
import glob
# import requests
from django.core.serializers import serialize
import json
from django.http import JsonResponse
from django.forms.models import model_to_dict
from decimal import Decimal
# import ftplib
# import datetime
from YCS.shared_utils import *
from YCS.merger_fast import process_vendor
import xlsxwriter
import io
from datetime import datetime
from django.db.models.functions import Lower

from .models import Diamond, CronSchedule, CronLog, ActivityLog


diamond_columns = [
    'id', 'lot_id', 'lot_id_str', 'shape', 'weight', 'color',  # 5
    'clarity', 'lab', 'polish', 'symmetry', 'fluorescence', 'cert_no', 'm1', 'm2', 'm3', 'depth', 'table_percent',  # 16
    'crown_angle', 'fluorescence_color', 'girdle', 'culet', 'quantity', 'list_price', 'rap_discount', 'sale_price',  # 24
    'status', 'cut', 'certificate_filename', 'diamond_filename', 'date_created', 'updated_at',
    'feed', 'country_location', 'fancy_intensity', 'fancy_overtone']

activity_columns = [
                'id', 'user__last_login', 'user__company_name', 'name', 'user__email',
                'search_time', 'state', 'state', 'search_source', 'results_no', '']

direction_symbol = {
    'asc': '-',
    'desc': ''
}


# A view to report back on upload progress:

def upload_progress(request):
    """
    Return JSON object with information about the progress of an upload.
    """
    progress_id = ''
    if 'X-Progress-ID' in request.GET:
        progress_id = request.GET['X-Progress-ID']
    elif 'X-Progress-ID' in request.META:
        progress_id = request.META['X-Progress-ID']
    if progress_id:
        from django.utils import simplejson
        cache_key = "%s_%s" % (request.META['REMOTE_ADDR'], progress_id)
        data = cache.get(cache_key)
        return HttpResponse(simplejson.dumps(data))
    else:
        return HttpResponseServerError('Server Error: You must provide X-Progress-ID header or query param.')


@csrf_exempt
@login_required
def upload(request):
    files = request.FILES['files[]']
    path = os.path.join(settings.BASE_DIR, "media", "CSV_Files", '%s') % files.name
    destination = open(path, 'wb+')
    for chunk in files.chunks():
        destination.write(chunk)
    destination.close()
    messages.success(request, 'Files uploaded without error', extra_tags='Success')
    # return status to client
    return HttpResponse(status=201)


@csrf_exempt
@login_required
def remove_file(request):
    """ Deletes file from filesystem. """
    file_name = request.POST.get('file', None)
    path = os.path.join(settings.BASE_DIR, "media", "CSV_Files", '%s') % file_name
    if os.path.isfile(path):
        os.remove(path)
        messages.success(request, 'Files removed without error', extra_tags='Success')
        return HttpResponse(status=204)
    return HttpResponse(status=400)


@csrf_exempt
@login_required
def process_vendor_files(request):
    """ process files from filesystem. """
    try:
        process_vendor()
        messages.success(request, 'Files processed', extra_tags='Success')
    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@login_required
def list_diamonds(request):
    diamond_list = Diamond.objects.all().order_by('date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(diamond_list, 10)

    try:
        diamonds = paginator.page(page)
    except PageNotAnInteger:
        diamonds = paginator.page(1)
    except EmptyPage:
        diamonds = paginator.page(paginator.num_pages)
        # messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'diamonds.html', {'diamonds': diamonds})


@csrf_exempt
@login_required
def dt_list_diamonds(request):
    draw = request.POST.get('draw')
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    order_col = request.POST.get('order[0][column]')
    order_dir = request.POST.get('order[0][dir]')
    custom_order = request.POST.getlist('custom_order[]')

    order_by = direction_symbol[order_dir] + diamond_columns[int(custom_order[int(order_col)])]
    if diamond_columns[int(custom_order[int(order_col)])] == 'fluorescence':
        order_by = direction_symbol[order_dir] + 'fluorescence_order'

    # print(order_by)
    # if order_col == '10':
    #     N = Diamond.objects.filter(fluorescence='N')
    #     F = Diamond.objects.filter(fluorescence='F')
    #     VSL = Diamond.objects.filter(fluorescence='VSL')
    #     SL = Diamond.objects.filter(fluorescence='SL')
    #     M = Diamond.objects.filter(fluorescence='M')
    #     STR = Diamond.objects.filter(fluorescence='STR')
    #     VST = Diamond.objects.filter(fluorescence='VST')
    #
    #     if order_dir == 'asc':
    #         diamonds = N | F | VSL | SL | M | STR | VST
    #     else:
    #         diamonds = VST | STR | M | SL | VSL | F | N
    # else:
    diamonds = Diamond.objects.all().order_by(order_by)
    shape_list = request.POST.getlist('shape[]')
    id_list = request.POST.getlist('id[]')
    # id_list = [int(i) for i in id_list]
    weight_from = request.POST.get('weight_from')
    weight_to = request.POST.get('weight_to')
    color_list = request.POST.getlist('color[]')
    clarity_list = request.POST.getlist('clarity[]')
    lab_list = request.POST.getlist('lab[]')
    location_list = request.POST.getlist('location[]')
    feed_list = request.POST.getlist('feed[]')
    polish_list = request.POST.getlist('polish[]')
    symmetry_list = request.POST.getlist('symmetry[]')
    cut_list = request.POST.getlist('cut[]')
    fluorescence_list = request.POST.getlist('fluorescence[]')
    depth_from = request.POST.get('depth_from')
    depth_to = request.POST.get('depth_to')
    table_from = request.POST.get('table_from')
    table_to = request.POST.get('table_to')
    discount_from = request.POST.get('discount_from')
    discount_to = request.POST.get('discount_to')

    if len(shape_list) > 0:
        diamonds = diamonds.filter(shape__in=shape_list)
    if len(id_list) > 0:
        temp_diamonds = diamonds.filter(cert_no__in=id_list)
        temp_diamonds_2 = diamonds.filter(lot_id__in=id_list)
        diamonds = temp_diamonds | temp_diamonds_2

    if not(weight_from == '0' and weight_to == '200'):
        diamonds = diamonds.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    if len(color_list) > 0:
        diamonds = diamonds.filter(color__in=color_list)
    if len(clarity_list) > 0:
        diamonds = diamonds.filter(clarity__in=clarity_list)
    if len(lab_list) > 0:
        diamonds = diamonds.filter(lab__in=lab_list)
    if len(location_list) > 0:
        location_list = [x.upper() for x in location_list]
        diamonds = diamonds.filter(country_location__in=location_list)
    if len(feed_list) > 0:
        # TEMPORARY MASK
        # feed_list = ['EZ' if x == 'NIRU' else x.upper() for x in feed_list]
        feed_list = [x.upper() for x in feed_list]
        diamonds = diamonds.filter(feed__in=feed_list)
    if len(polish_list) > 0:
        polish_list = [x.upper() for x in polish_list]
        diamonds = diamonds.filter(polish__in=polish_list)
    if len(symmetry_list) > 0:
        symmetry_list = [x.upper() for x in symmetry_list]
        diamonds = diamonds.filter(symmetry__in=symmetry_list)
    if len(cut_list) > 0:
        cut_list = [x.upper() for x in cut_list]
        diamonds = diamonds.filter(cut__in=cut_list)
    if len(fluorescence_list) > 0:
        fluorescence_list = [x.upper() for x in fluorescence_list]
        diamonds = diamonds.filter(fluorescence__in=fluorescence_list)
    if not(depth_from == '0' and depth_to == '200'):
        diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    if not(table_from == '0' and table_to == '200'):
        diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    if not(discount_from == '-200' and discount_to == '200'):
        diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    # page = request.POST.get('page', 1)
    # paginator = Paginator(diamonds, 10)
    #
    # try:
    #     diamond_page = paginator.page(page)
    # except PageNotAnInteger:
    #     diamond_page = paginator.page(1)
    # except EmptyPage:
    #     diamond_page = paginator.page(paginator.num_pages)
    #     # messages.warning(request, 'This page has no data', extra_tags='Failed')

    temp = dict()
    temp['draw'] = int(draw)
    temp['recordsTotal'] = diamonds.count()
    temp['recordsFiltered'] = diamonds.count()

    # temp['count'] = diamond_page.paginator.count
    # temp['pages'] = diamond_page.paginator.num_pages
    # temp['next'] = diamond_page.has_next()
    # temp['previous'] = diamond_page.has_previous()
    # temp['old_data'] = (request.body.decode('utf-8'))
    # temp['page'] = page

    # diamond_a = diamonds.first()
    # temp_dict = model_to_dict(Diamond.objects.all().first())
    # del temp_dict['crown_angle']
    # del temp_dict['culet']
    # del temp_dict['depth']
    # del temp_dict['fancy_intensity']
    # del temp_dict['fancy_overtone']
    # del temp_dict['fluorescence_color']
    # del temp_dict['girdle']
    # del temp_dict['weight']
    # del temp_dict['m1']
    # del temp_dict['m2']
    # del temp_dict['m3']
    # temp_list = [temp_dict]
    temp_list = []
    # , diamond_a.updated_at
    diamonds = diamonds[start:start+length]

    # for diamond_a in diamond_page:
    for diamond_a in diamonds:
        temp_list.append(
            [diamond_a.id, diamond_a.lot_id, diamond_a.lot_id_str, diamond_a.shape, diamond_a.weight, diamond_a.color,
             diamond_a.clarity, diamond_a.lab, Diamond.DISPLAY_ABBREVIATIONS[diamond_a.polish],
             Diamond.DISPLAY_ABBREVIATIONS[diamond_a.symmetry], diamond_a.fluorescence,
             diamond_a.cert_no, diamond_a.m1, diamond_a.m2, diamond_a.m3, diamond_a.depth, diamond_a.table_percent,
             diamond_a.crown_angle, diamond_a.fluorescence_color, diamond_a.girdle, diamond_a.culet, diamond_a.quantity,
             diamond_a.list_price, diamond_a.rap_discount, diamond_a.sale_price, diamond_a.status,
             Diamond.DISPLAY_ABBREVIATIONS[diamond_a.cut],
             ('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+diamond_a.certificate_filename+'">View</a>' if (diamond_a.certificate_filename != ' ' and diamond_a.certificate_filename != '') else""),
             ('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+diamond_a.diamond_filename+'">View</a>' if (diamond_a.diamond_filename != ' ' and diamond_a.diamond_filename != '') else""),
             diamond_a.date_created.strftime("%B %d, %Y"),
             Diamond.SUPPLIER_CODES[diamond_a.feed], diamond_a.country_location, diamond_a.fancy_intensity, diamond_a.fancy_overtone]
        )

    temp['data'] = temp_list
    # temp['data'] = [
    #                 [
    #                     "Angelica",
    #                     "Ramos",
    #                     "System Architect",
    #                     "London",
    #                     "9th Oct 09",
    #                     "$2,875"
    #                 ],
    #                 [
    #                     "Ashton",
    #                     "Cox",
    #                     "Technical Author",
    #                     "San Francisco",
    #                     "12th Jan 09",
    #                     "$4,800"
    #                 ],
    #             ]
    # print(json.loads(serialize('json', diamond_page)))
    # print(temp_list)
    # print(diamonds.values())
    # print(json.dumps(diamonds, indent=4))
    return JsonResponse(temp, safe=True)


# def get_queryset(self):
#     if self.action in ('my_payouts', ):
#         queryset = PayoutRequest.objects.filter(user_id=self.request.user.id)
#         type_list = self.request.GET.getlist('type')
#         allow_null = self.request.GET.get('null')
#
#         if len(type_list) > 0:
#             queryset1 = queryset.filter(transaction_status__in=type_list)
#             if allow_null == 'true':
#                 queryset2 = queryset.filter(transaction_status__isnull=True)
#                 queryset = queryset1 | queryset2
#             else:
#                 queryset = queryset1
#         return queryset
#     else:
#         return PayoutRequest.objects.all()


@csrf_exempt
@login_required
def filter_diamonds(request):
    diamonds = Diamond.objects.all().order_by('date_created')
    shape_list = request.POST.getlist('shape[]')
    id_list = request.POST.getlist('id[]')
    # id_list = [int(i) for i in id_list]
    weight_from = request.POST.get('weight_from')
    weight_to = request.POST.get('weight_to')
    color_list = request.POST.getlist('color[]')
    clarity_list = request.POST.getlist('clarity[]')
    lab_list = request.POST.getlist('lab[]')
    location_list = request.POST.getlist('location[]')
    feed_list = request.POST.getlist('feed[]')
    polish_list = request.POST.getlist('polish[]')
    symmetry_list = request.POST.getlist('symmetry[]')
    cut_list = request.POST.getlist('cut[]')
    fluorescence_list = request.POST.getlist('fluorescence[]')
    depth_from = request.POST.get('depth_from')
    depth_to = request.POST.get('depth_to')
    table_from = request.POST.get('table_from')
    table_to = request.POST.get('table_to')
    discount_from = request.POST.get('discount_from')
    discount_to = request.POST.get('discount_to')

    if len(shape_list) > 0:
        diamonds = diamonds.filter(shape__in=shape_list)
    if len(id_list) > 0:
        diamonds = diamonds.filter(lot_id__in=id_list)
    if not(weight_from == '0' and weight_to == '200'):
        diamonds = diamonds.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    if len(color_list) > 0:
        diamonds = diamonds.filter(color__in=color_list)
    if len(clarity_list) > 0:
        diamonds = diamonds.filter(clarity__in=clarity_list)
    if len(lab_list) > 0:
        diamonds = diamonds.filter(lab__in=lab_list)
    if len(location_list) > 0:
        location_list = [x.upper() for x in location_list]
        diamonds = diamonds.filter(country_location__in=location_list)
    if len(feed_list) > 0:
        # TEMPORARY MASK
        # feed_list = ['EZ' if x == 'NIRU' else x.upper() for x in feed_list]
        feed_list = [x.upper() for x in feed_list]
        diamonds = diamonds.filter(feed__in=feed_list)
    if len(polish_list) > 0:
        polish_list = [x.upper() for x in polish_list]
        diamonds = diamonds.filter(polish__in=polish_list)
    if len(symmetry_list) > 0:
        symmetry_list = [x.upper() for x in symmetry_list]
        diamonds = diamonds.filter(symmetry__in=symmetry_list)
    if len(cut_list) > 0:
        cut_list = [x.upper() for x in cut_list]
        diamonds = diamonds.filter(cut__in=cut_list)
    if len(fluorescence_list) > 0:
        fluorescence_list = [x.upper() for x in fluorescence_list]
        diamonds = diamonds.filter(fluorescence__in=fluorescence_list)
    if not(depth_from == '0' and depth_to == '200'):
        diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    if not(table_from == '0' and table_to == '200'):
        diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    if not(discount_from == '-200' and discount_to == '200'):
        diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    page = request.POST.get('page', 1)
    paginator = Paginator(diamonds, 10)

    try:
        diamond_page = paginator.page(page)
    except PageNotAnInteger:
        diamond_page = paginator.page(1)
    except EmptyPage:
        diamond_page = paginator.page(paginator.num_pages)
        # messages.warning(request, 'This page has no data', extra_tags='Failed')

    temp = dict()
    temp['count'] = diamond_page.paginator.count
    temp['pages'] = diamond_page.paginator.num_pages
    temp['next'] = diamond_page.has_next()
    temp['previous'] = diamond_page.has_previous()
    temp['page'] = page
    temp['data'] = json.loads(serialize('json', diamond_page))
    # temp['data'] = model_to_dict(Diamond.objects.all().first())
    # temp['data'] = diamonds.values()
    # temp['data'] = json.dumps(diamonds, indent=4)

    return JsonResponse(temp, safe=True)


@csrf_exempt
@login_required
def interactive_count(request):
    diamonds = Diamond.objects.all()
    shape_list = request.POST.getlist('shape[]')
    id_list = request.POST.getlist('id[]')
    # id_list = [int(i) for i in id_list]
    weight_from = request.POST.get('weight_from')
    weight_to = request.POST.get('weight_to')
    color_list = request.POST.getlist('color[]')
    clarity_list = request.POST.getlist('clarity[]')
    lab_list = request.POST.getlist('lab[]')
    location_list = request.POST.getlist('location[]')
    feed_list = request.POST.getlist('feed[]')
    polish_list = request.POST.getlist('polish[]')
    symmetry_list = request.POST.getlist('symmetry[]')
    cut_list = request.POST.getlist('cut[]')
    fluorescence_list = request.POST.getlist('fluorescence[]')
    depth_from = request.POST.get('depth_from')
    depth_to = request.POST.get('depth_to')
    table_from = request.POST.get('table_from')
    table_to = request.POST.get('table_to')
    discount_from = request.POST.get('discount_from')
    discount_to = request.POST.get('discount_to')

    if len(shape_list) > 0:
        diamonds = diamonds.filter(shape__in=shape_list)
    if len(id_list) > 0:
        diamonds = diamonds.filter(lot_id__in=id_list)
    if len(color_list) > 0:
        diamonds = diamonds.filter(color__in=color_list)
    if len(clarity_list) > 0:
        diamonds = diamonds.filter(clarity__in=clarity_list)
    if len(lab_list) > 0:
        diamonds = diamonds.filter(lab__in=lab_list)
    if len(location_list) > 0:
        location_list = [x.upper() for x in location_list]
        diamonds = diamonds.filter(country_location__in=location_list)
    if len(feed_list) > 0:
        # TEMPORARY MASK
        # feed_list = ['EZ' if x == 'NIRU' else x.upper() for x in feed_list]
        feed_list = [x.upper() for x in feed_list]
        diamonds = diamonds.filter(feed__in=feed_list)
    if len(polish_list) > 0:
        polish_list = [x.upper() for x in polish_list]
        diamonds = diamonds.filter(polish__in=polish_list)
    if len(symmetry_list) > 0:
        symmetry_list = [x.upper() for x in symmetry_list]
        diamonds = diamonds.filter(symmetry__in=symmetry_list)
    if len(cut_list) > 0:
        cut_list = [x.upper() for x in cut_list]
        diamonds = diamonds.filter(cut__in=cut_list)
    if len(fluorescence_list) > 0:
        fluorescence_list = [x.upper() for x in fluorescence_list]
        diamonds = diamonds.filter(fluorescence__in=fluorescence_list)

    if not(weight_from == '0' and weight_to == '200'):
        diamonds = diamonds.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    if not(depth_from == '0' and depth_to == '200'):
        diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    if not(table_from == '0' and table_to == '200'):
        diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    if not(discount_from == '-200' and discount_to == '200'):
        diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    return JsonResponse({'interactive_count': diamonds.count()}, safe=True)


@login_required
def vendors(request):
    try:
        files = [os.path.basename(i) for i in glob.glob(settings.CSV_PATH + '/*.{}'.format(settings.CSV_EXTENSION))]
        schedules = CronSchedule.objects.all()
        logs = CronLog.objects.filter(type=CronLog.Current).order_by('date_time')
    except Exception as e:
        print(e)
        files = None
        schedules = None
        logs = None
    return render(request, 'vendors.html', {'files': files, 'schedules': schedules, 'logs': logs})


@login_required
def reset_table(request):
    try:
        reset_stones()
    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')
    return redirect('vendors')


@csrf_exempt
@login_required
def fetch_rap(request):
    """ fetch_rap """
    try:
        response = fetch_rapnetfeed_data()
        if response['ok']:
            messages.success(request, response['message'], extra_tags=response['status'])
        else:
            messages.error(request, response['message'], extra_tags=response['status'])

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report!', extra_tags='Failed')

    return redirect('vendors')


@csrf_exempt
@login_required
def fetch_kiran(request):
    """ fetch_kiran """
    try:
        response = fetch_kiran_data()
        if response['ok']:
            messages.success(request, response['message'], extra_tags=response['status'])
        else:
            messages.error(request, response['message'], extra_tags=response['status'])

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@csrf_exempt
@login_required
def fetch_srk(request):
    """ fetch_srk """
    try:
        response = fetch_srk_data()
        if response['ok']:
            messages.success(request, response['message'], extra_tags=response['status'])
        else:
            messages.error(request, response['message'], extra_tags=response['status'])

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@csrf_exempt
@login_required
def copy_niru_ftp(request):
    """ copy_niru_ftp """
    try:
        response = fetch_niru_data_ftp()
        if response['ok']:
            messages.success(request, response['message'], extra_tags=response['status'])
        else:
            messages.error(request, response['message'], extra_tags=response['status'])

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@csrf_exempt
@login_required
def dwn_kiran(request):
    """ dwn_kiran """
    try:
        with open('media/CSV_Files/Report.csv', 'rb') as myfile:
            response = HttpResponse(myfile, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=Report.csv'
            return response
    except Exception as e:
        messages.error(request, 'KIRAN file not found!', extra_tags='Failed')
        print(e)
        return redirect('vendors')


@csrf_exempt
@login_required
def dwn_rap(request):
    """ dwn_rap """
    try:
        with open('media/CSV_Files/rapnetfeed.csv', 'rb') as myfile:
            response = HttpResponse(myfile, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=rapnetfeed.csv'
            return response
    except Exception as e:
        messages.error(request, 'RAPNETFEED file not found!', extra_tags='Failed')
        print(e)
        return redirect('vendors')


@csrf_exempt
@login_required
def dwn_srk(request):
    """ dwn_srk """
    try:
        with open('media/CSV_Files/srkfile.csv', 'rb') as myfile:
            response = HttpResponse(myfile, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=srkfile.csv'
            return response
    except Exception as e:
        messages.error(request, 'SRK file not found!', extra_tags='Failed')
        print(e)
        return redirect('vendors')


@csrf_exempt
@login_required
def dwn_niru(request):
    """ dwn_niru """
    try:
        with open('media/CSV_Files/niru.csv', 'rb') as myfile:
            response = HttpResponse(myfile, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=niru.csv'
            return response
    except Exception as e:
        messages.error(request, 'NIRU file not found!', extra_tags='Failed')
        print(e)
        return redirect('vendors')


@csrf_exempt
@login_required
def export_sde_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="SDE.csv"'

    # writer = csv.writer(response, delimiter=',')
    writer = csv.writer(response, delimiter="\t")

    fieldnames = ['LotID', 'LotIDStr', 'Shape', 'Weight', 'Color', 'Clarity', 'Lab', 'Polish', 'Symmetry',
                  'Fluorescence', 'CertNo', 'm1', 'm2', 'm3', 'Depth', 'TablePercent', 'CrownAngle',
                  'FluorescenceColor', 'Girdle', 'Culet', 'Quantity', 'ListPrice', 'RapDiscount', 'SalePrice', 'Status',
                  'Cut', 'Certificate Filename', 'Diamond Filename', 'DateCreated', 'Feed', 'CountryLocation',
                  'FancyIntensity', 'FancyOvertone']

    writer.writerow(fieldnames)

    diamonds = Diamond.objects.all().values_list('lot_id', 'lot_id_str', 'shape', 'weight', 'color', 'clarity', 'lab',
                                                 'polish', 'symmetry', 'fluorescence', 'cert_no', 'm1', 'm2', 'm3',
                                                 'depth', 'table_percent', 'crown_angle', 'fluorescence_color',
                                                 'girdle', 'culet', 'quantity', 'list_price', 'rap_discount',
                                                 'sale_price', 'status', 'cut', 'certificate_filename',
                                                 'diamond_filename', 'date_created', 'feed', 'country_location',
                                                 'fancy_intensity', 'fancy_overtone')
    for diamond in diamonds:
        temp = list(diamond)
        temp[28] = diamond[28].strftime("%b. %d %Y %H:%M %p")
        # # TEMPORARY MASK
        # if temp[29] == 'NIRU':
        #     temp[29] = 'EZ'
        writer.writerow(temp)

    return response


@csrf_exempt
@login_required
def send_via_ftp(request):
    try:
        response = send_using_ftp()
        if response['ok']:
            messages.success(request, response['message'], extra_tags=response['status'])
        else:
            messages.error(request, response['message'], extra_tags=response['status'])

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@csrf_exempt
@login_required
def clear_console(request):
    try:
        CronLog.objects.filter(type=CronLog.Current).update(type=CronLog.Archived)

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('vendors')


@login_required
def add_schedule(request):
    time = request.POST.get('time', None)
    if time:
        # time_30min_ago = time - datetime.timedelta(minutes=30)
        # time_id = request.POST.get('time_id', None)
        CronSchedule.objects.create(schedule_time=time)
    return redirect('vendors')


@login_required
def delete_schedule(request, pk):
    try:
        CronSchedule.objects.get(pk=pk).delete()

    except CronSchedule.DoesNotExist:
        messages.warning(request, 'Schedule does not exist. Please retry ', extra_tags='Warning')

    return redirect('vendors')


@login_required
def clear_all(request):
    try:
        files = glob.glob(settings.CSV_PATH + '/*.{}'.format(settings.CSV_EXTENSION))
        for f in files:
            os.remove(f)

    except Exception as e:
        print(e)
        messages.warning(request, 'Files not cleared. Please retry ', extra_tags='Warning')

    return redirect('vendors')


# urls_masking = {
#     '': 'https://pck.azureedge.net',
#     '': 'http://diamonds.kirangems.com:8080',
#     '': 'http://cert.feedcenter.net',
#     '': 'http://www.feedcenter.net',
#     '': 'http://s3.amazonaws.com',
# }

# https://pck2.azureedge.net/hdfile/HD4/imaged/{{STONEID}}/still.jpg    SRK Image
# https://pck2.azureedge.net/hdfile/CERT/{{CERTIFICATENO}}.pdf     SRK Cert         None


# https://diamonds.kirangems.com:8080/Picture/13014908006.pdf  RepNo
# https://diamonds.kirangems.com:8080/imagesM/13014908006.jpg - CASE

# cdn_masking = {
#     'cdn.sdegroup.com': 'https://pck.azureedge.net',
#     'cdn1.sdegroup.com': 'http://diamonds.kirangems.com:8080',
#     '': 'http://cert.feedcenter.net',
#     '': 'http://www.feedcenter.net',
#     '': 'http://s3.amazonaws.com',
# }


# @login_required
def get_diamond_image(request):
    domain = None
    content_type = "image/jpg"
    vendor = request.GET.get('vendor', None)
    url = ''
    s = requests.Session()

    if vendor == '1':
        s_id = request.GET.get('s_id', None)
        url = 'https://pck2.azureedge.net/hdfile/HD4/imaged/' + s_id + '/still.jpg'

    if vendor == '2':
        s_id = request.GET.get('s_id', None)
        url = 'https://diamonds.kirangems.com:8080/imagesM/' + s_id + '.jpg'

    if vendor == '3':
        domain = request.GET.get('domain', None)
        p = request.GET.get('p', None)
        if domain == '1':
            d = request.GET.get('d', None)
            sv = request.GET.get('sv', None)
            z = request.GET.get('z', None)
            surl = request.GET.get('surl', None)
            surl = surl.replace('true', 'http://media.amcdiamonds.com/view/')
            # 'http://viewer.amcdiamonds.be/index.html?d=714131&sv=0&z=1&surl=http://media.amcdiamonds.com/view/'
            url = 'http://viewer.amcdiamonds.be' + p + '?d=' + d + '&sv=' + sv + '&z=' + z + '&surl=' + surl
            content_type = "text/html"

        if domain == '2':
            # https://img.diacertimages.com/images/5805235/embed.html
            url = 'https://img.diacertimages.com' + p
        if domain == '3':
            # http://sdegroup.com/images/17487600.JPG
            url = 'http://sdegroup.com' + p
        if domain == '4':
            # http://certs.rapnet.com/userfolders/78382/Images/2191330622.jpg
            url = 'http://certs.rapnet.com' + p
        if domain == '5':
            content_type = "text/html"
            temp_id = request.GET.get('id', None)
            url = 'http://media.feedcenter.net/?id=' + temp_id
            # print(url)
        if domain == '6':
            content_type = "text/html"
            temp_id = request.GET.get('id', None)
            temp_type = request.GET.get('type', None)
            url = 'https://segoma.com/v.aspx?type=' + temp_type + '&id=' + temp_id
            # print(url)
        if domain == '7':
            d = request.GET.get('d', None)
            sv = request.GET.get('sv', None)
            z = request.GET.get('z', None)
            surl = request.GET.get('surl', None)
            surl = surl.replace('secret_js', 'http://media.diamondofantwerpen.com/view/')
            # 'http://viewer.amcdiamonds.be/index.html?d=714131&sv=0&z=1&surl=http://media.amcdiamonds.com/view/'
            url = 'http://viewer.diamondofantwerpen.com' + p + '?d=' + d + '&sv=' + sv + '&z=' + z + '&surl=' + surl
            content_type = "text/html"
            # s.headers.update({'referer': 'http://viewer.diamondofantwerpen.com/index.html?d=c536831&sv=0&z=1&surl=http://media.diamondofantwerpen.com/view/'})

    try:
        # print(url)
        response = s.get(url)
    except ConnectionError as e:  # This is the correct syntax
        return HttpResponse(content='No Response', content_type=content_type)
    except Exception as e:
        return HttpResponse(content='Something went wrong! Please report.', content_type=content_type)

    if domain and domain == '1':
        response._content = response.content.replace(b'javascripts/', b'http://viewer.amcdiamonds.be/javascripts/')
        response._content = response.content.replace(b'css/', b'http://viewer.amcdiamonds.be/css/')
    if domain and domain == '7':
        response._content = response.content.replace(b'javascripts/', b'http://viewer.diamondofantwerpen.com/javascripts/')
        response._content = response.content.replace(b'css/', b'http://viewer.diamondofantwerpen.com/css/')
    # response = requests.get('https://certs.rapnet.com/userfolders/78382/Images/2195405311.jpg')
    if response.status_code == 200:
        return HttpResponse(response, content_type=content_type)
    else:
        return HttpResponse(response, content_type=content_type)


# @login_required
def get_diamond_certificate(request):
    content_type = "application/pdf"
    vendor = request.GET.get('vendor', None)
    url = ''

    if vendor == '1':
        c_no = request.GET.get('c_no', None)
        url = 'https://pck2.azureedge.net/hdfile/CERT/' + c_no + '.pdf'

    if vendor == '2':
        c_no = request.GET.get('c_no', None)
        url = 'https://diamonds.kirangems.com:8080/Picture/' + c_no + '.pdf'

    if vendor == '3':
        domain = request.GET.get('domain', None)
        p = request.GET.get('p', None)
        if domain == '1':
            p = p.replace('x.certs', 'rap.certs')
            url = 'http://s3.amazonaws.com' + p
        if domain == '2':
            q = request.GET.get('q', None)
            url = 'https://segoma.com' + p + '?' + q
        if domain == '3':
            # type = request.GET.get('type', None)
            # if type == '2':
            #     vendor_id = request.GET.get('vendor_id', None)
            #     doc_type = request.GET.get('doc_type', None)
            #     v_id = request.GET.get('v_id', None)
            #     s_id = request.GET.get('s_id', None)
            #     url = 'http://viewer.feedcenter.net/' + v_id + '?VendorID=' + vendor_id + '&DocType=' + doc_type + '&ID=' + s_id
            #     # http://viewer.feedcenter.net/FantasyFeederFiles.aspx?VendorID=AMC_NET&DocType=Cert&ID=1293126880
            # else:
            #     v_id = request.GET.get('v_id', None)
            #     v_id = request.GET.get('v_id', None)
            #     s_id = request.GET.get('s_id', None)
            #     url = 'http://viewer.feedcenter.net/Certs/' + v_id + '/' + s_id + '.pdf'
            v_id = request.GET.get('v_id', None)
            s_id = request.GET.get('s_id', None)
            # url = 'http://viewer.feedcenter.net/Certs/AMC_NET' + '/' + s_id + '.pdf'
            url = 'http://viewer.feedcenter.net/Certs/AA2/' + s_id + '.pdf'
        if domain == '4':
            url = 'http://www.feedcenter.net' + p
        if domain == '5':
            url = 'http://certs.rapnet.com' + p
            # print(url)

    # print(url)
    try:
        response = requests.get(url)
    except ConnectionError as e:  # This is the correct syntax
        return HttpResponse(content='No Response', content_type=content_type)
    except Exception as e:
        return HttpResponse(content='Something went wrong! Please report.', content_type=content_type)

    # response = requests.get('http://s3.amazonaws.com/rap.certs/Gia/Certificates/2018/6/1/2195405311.pdf')
    if response.status_code == 200:
        return HttpResponse(response, content_type=content_type)
    else:
        return HttpResponse(response, content_type=content_type)


# @login_required
def get_ant_js(request):
    content_type = "application/octet-stream"
    p = request.GET.get('p', None)
    url = 'http://media.diamondofantwerpen.com/view/' + p   # imaged/705779/2.js

    try:
        response = requests.get(url)
    except ConnectionError as e:  # This is the correct syntax
        return HttpResponse(content='No Response', content_type=content_type)
    except Exception as e:
        return HttpResponse(content='Something went wrong! Please report.', content_type=content_type)

    if response.status_code == 200:
        return HttpResponse(response, content_type=content_type)
    else:
        return HttpResponse(response, content_type=content_type)


# @login_required
def get_amc_js(request):
    content_type = "application/octet-stream"
    p = request.GET.get('p', None)
    url = 'http://media.amcdiamonds.com/view/' + p   # imaged/705779/2.js

    try:
        response = requests.get(url)
    except ConnectionError as e:  # This is the correct syntax
        return HttpResponse(content='No Response', content_type=content_type)
    except Exception as e:
        return HttpResponse(content='Something went wrong! Please report.', content_type=content_type)

    if response.status_code == 200:
        return HttpResponse(response, content_type=content_type)
    else:
        return HttpResponse(response, content_type=content_type)


# @login_required
def get_amc_images(request, string):
    content_type = "image/png"
    url = settings.HTTP_SCHEME + settings.HOST_NAME + '/static/diamonds/images/icon/' + string

    try:
        response = requests.get(url)
    except ConnectionError as e:  # This is the correct syntax
        return HttpResponse(content='No Response', content_type=content_type)
    except Exception as e:
        return HttpResponse(content='Something went wrong! Please report.', content_type=content_type)

    if response.status_code == 200:
        return HttpResponse(response, content_type=content_type)
    else:
        return HttpResponse(response, content_type=content_type)


def create_excel_stones(diamonds, fieldnames, worksheet='full inventory'):
    output = io.BytesIO()
    # output = io.StringIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(worksheet)
    worksheet.hide_gridlines(0)    # 1 hide printed, 0 hide nothing

    row = 0
    column = 0
    for item in fieldnames:
        worksheet.write(row, column, item)
        column += 1

    for diamond in diamonds:
        row += 1
        column = 0
        temp = list(diamond)
        temp[7] = Diamond.DISPLAY_ABBREVIATIONS[diamond[7]]
        temp[8] = Diamond.DISPLAY_ABBREVIATIONS[diamond[8]]
        temp[9] = Diamond.DISPLAY_ABBREVIATIONS[diamond[9]]
        temp[16] = diamond[2] * diamond[14]
        temp[17] = str(diamond[16]) + 'x' + str(diamond[17]) + 'x' + str(diamond[19])
        temp.pop(19)
        for item in temp:
            worksheet.write(row, column, item)
            column += 1

    # close workbook
    workbook.close()
    xlsx_data = output.getvalue()
    # xlsx_data = output.read()
    return xlsx_data


@csrf_exempt
@login_required
def export_filtered_diamonds(request):
    request_post = json.loads(request.body)
    draw = request_post.get('draw')
    start = int(request_post.get('start'))
    length = int(request_post.get('length'))
    order_col = request_post.get('order')[0]['column']
    order_dir = request_post.get('order')[0]['dir']
    custom_order = request_post.get('custom_order')

    order_by = direction_symbol[order_dir] + diamond_columns[int(custom_order[int(order_col)])]
    # print(order_by)

    diamonds = Diamond.objects.all().order_by(order_by)
    # diamonds = Diamond.objects.all()
    # diamonds = Diamond.objects.filter(shape='Baguette')
    shape_list = request_post.get('shape')
    id_list = request_post.get('id')
    # id_list = [int(i) for i in id_list]
    weight_from = request_post.get('weight_from')
    weight_to = request_post.get('weight_to')
    color_list = request_post.get('color')
    clarity_list = request_post.get('clarity')
    lab_list = request_post.get('lab')
    location_list = request_post.get('location')
    feed_list = request_post.get('feed')
    polish_list = request_post.get('polish')
    symmetry_list = request_post.get('symmetry')
    cut_list = request_post.get('cut')
    fluorescence_list = request_post.get('fluorescence')
    depth_from = request_post.get('depth_from')
    depth_to = request_post.get('depth_to')
    table_from = request_post.get('table_from')
    table_to = request_post.get('table_to')
    discount_from = request_post.get('discount_from')
    discount_to = request_post.get('discount_to')

    if len(shape_list) > 0:
        diamonds = diamonds.filter(shape__in=shape_list)
    # print(diamonds.count())
    if len(id_list) > 0:
        temp_diamonds = diamonds.filter(cert_no__in=id_list)
        temp_diamonds_2 = diamonds.filter(lot_id__in=id_list)
        diamonds = temp_diamonds | temp_diamonds_2

    # print(diamonds.count())
    if not(weight_from == '0' and weight_to == '200'):
        diamonds = diamonds.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    # print(diamonds.count())
    if len(color_list) > 0:
        diamonds = diamonds.filter(color__in=color_list)
    # print(diamonds.count())
    if len(clarity_list) > 0:
        diamonds = diamonds.filter(clarity__in=clarity_list)
    # print(diamonds.count())
    if len(lab_list) > 0:
        diamonds = diamonds.filter(lab__in=lab_list)
    # print(diamonds.count())
    if len(location_list) > 0:
        location_list = [x.upper() for x in location_list]
        diamonds = diamonds.filter(country_location__in=location_list)
    # print(diamonds.count())
    if len(feed_list) > 0:
        # TEMPORARY MASK
        # feed_list = ['EZ' if x == 'NIRU' else x.upper() for x in feed_list]
        feed_list = [x.upper() for x in feed_list]
        diamonds = diamonds.filter(feed__in=feed_list)
    # print(diamonds.count())
    if len(polish_list) > 0:
        polish_list = [x.upper() for x in polish_list]
        diamonds = diamonds.filter(polish__in=polish_list)
    # print(diamonds.count())
    if len(symmetry_list) > 0:
        symmetry_list = [x.upper() for x in symmetry_list]
        diamonds = diamonds.filter(symmetry__in=symmetry_list)
    # print(diamonds.count())
    if len(cut_list) > 0:
        cut_list = [x.upper() for x in cut_list]
        diamonds = diamonds.filter(cut__in=cut_list)
    # print(diamonds.count())
    if len(fluorescence_list) > 0:
        fluorescence_list = [x.upper() for x in fluorescence_list]
        diamonds = diamonds.filter(fluorescence__in=fluorescence_list)
    # print(diamonds.count())
    if not(depth_from == 0 and depth_to == 200):
        diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    # print(diamonds.count())
    if not(table_from == 0 and table_to == 200):
        diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    # print(diamonds.count())
    if not(discount_from == -200 and discount_to == 200):
        diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    # response = HttpResponse(content_type='text/csv')
    # response['Content-Disposition'] = 'attachment; filename="full_inventory.csv"'
    response = HttpResponse(content_type='application/vnd.ms-excel')
    # response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=full_inventory.xlsx'

    # writer = csv.writer(response, delimiter=',')
    # writer = csv.writer(response, delimiter="\t")

    fieldnames = ['LotID', 'Shape', 'Cts', 'Col', 'Clar', 'Lab', 'CertNo', 'Cut', 'Polish', 'Symmetry',
                  'Fluorescence', 'TD', 'TablePercent', 'ListPrice', 'SalePrice', 'RapDiscount', 'TotalPrice',
                  'Measurements', 'Certificate Filename']

    # writer.writerow(fieldnames)

    diamonds = diamonds.values_list('lot_id', 'shape', 'weight', 'color', 'clarity', 'lab', 'cert_no', 'cut',
                                    'polish', 'symmetry', 'fluorescence', 'depth', 'table_percent', 'list_price',
                                    'sale_price', 'rap_discount', 'm1', 'm2', 'certificate_filename', 'm3')

    # print(diamonds.count())
    xlsx_data = create_excel_stones(diamonds, fieldnames)
    response.write(xlsx_data)

    # for diamond in diamonds:
    #     temp = list(diamond)
    #     temp[7] = Diamond.DISPLAY_ABBREVIATIONS[diamond[7]]
    #     temp[8] = Diamond.DISPLAY_ABBREVIATIONS[diamond[8]]
    #     temp[9] = Diamond.DISPLAY_ABBREVIATIONS[diamond[9]]
    #     temp[16] = diamond[2] * diamond[14]
    #     temp[17] = str(diamond[16]) + 'x' + str(diamond[17]) + 'x' + str(diamond[19])
    #     temp.pop(19)
    #     writer.writerow(temp)

    return response


@csrf_exempt
@login_required
def get_selected_stones(request):
    id_list = request.POST.getlist('selected_stones[]', None)
    temp_list = []
    try:
        stones = Diamond.objects.filter(id__in=id_list)
        for stone in stones:
            stone.polish = Diamond.DISPLAY_ABBREVIATIONS[stone.polish]
            stone.symmetry = Diamond.DISPLAY_ABBREVIATIONS[stone.symmetry]
            stone.cut = Diamond.DISPLAY_ABBREVIATIONS[stone.cut]

    except Exception as e:
        print(e)
        return JsonResponse(temp_list, safe=False, status=400)

    return JsonResponse({"stones": json.loads(serialize('json', stones))}, safe=False, status=200)


def create_excel_stones_selected(diamonds, fieldnames, worksheet, is_full=False):
    output = io.BytesIO()
    # output = io.StringIO()
    workbook = xlsxwriter.Workbook(output)
    top_border = workbook.add_format({'top': 3, 'top_color': '#000000'})   # , 'top_pattern': 1
    bold = workbook.add_format({'bold': True})
    weight = workbook.add_format({'num_format': '0.00'})
    depth = workbook.add_format({'num_format': '0.0'})
    money = workbook.add_format({'num_format': '$#,##0.00'})
    h_center = workbook.add_format({'align': 'center'})
    v_center = workbook.add_format({'align': 'vcenter'})
    bold.set_align('center')
    money.set_align('center')
    weight.set_align('center')
    depth.set_align('center')

    worksheet = workbook.add_worksheet(worksheet)
    worksheet.set_landscape()
    worksheet.set_paper(9)         # A4
    worksheet.fit_to_pages(1, 0)   # 1 page wide and as long as necessary.
    worksheet.hide_gridlines(0)    # 1 hide printed, 0 hide nothing
    worksheet.set_column(0, 0, 15)
    worksheet.set_column(6, 6, 13)
    worksheet.set_column(14, 14, 15)
    worksheet.set_column(3, 3, 7)
    worksheet.set_column(8, 13, 7)
    worksheet.set_column(16, 16, 7)
    worksheet.set_column(18, 18, 15)

    row = 0
    column = 0
    for item in fieldnames:
        worksheet.write(row, column, item, h_center)
        column += 1

    total_weight = 0
    total_price = 0
    for diamond in diamonds:
        row += 1
        column = 0
        temp = list(diamond)
        temp[2] = round(diamond[2], 2)
        temp[6] = {'string': diamond[6], 'link': diamond[19]}
        temp[8] = Diamond.DISPLAY_ABBREVIATIONS[diamond[8]]
        temp[9] = Diamond.DISPLAY_ABBREVIATIONS[diamond[9]]
        temp[10] = Diamond.DISPLAY_ABBREVIATIONS[diamond[10]]
        if diamond[12]:
            temp[12] = round(diamond[12], 1)
        if diamond[2] and diamond[15]:
            temp[18] = round((diamond[2]*diamond[15])*Decimal((100+diamond[16])*0.01), 2)
        else:
            temp[18] = 0
        temp[14] = str(diamond[14]) + 'x' + str(diamond[18]) + 'x' + str(diamond[20])
        temp.pop(20)
        temp.pop(19)

        if not is_full:
            total_weight += diamond[2]
            total_price += temp[18]

        for item in temp:
            if column == 6:
                if item['link'] != '' and item['link'] != ' ':
                    worksheet.write_url(row=row, col=column, url=item['link'], cell_format=h_center, string=item['string'])
                else:
                    worksheet.write(row, column, item['string'], h_center)
            elif column == 7:
                if item != '' and item != ' ':
                    worksheet.write_url(row=row, col=column, url=item, cell_format=h_center, string='view')
                else:
                    worksheet.write(row, column, item, h_center)
            elif column == 2:
                worksheet.write(row, column, item, weight)
            elif column == 12:
                worksheet.write(row, column, item, depth)
            elif column == 18:
                worksheet.write(row, column, item, money)
            else:
                worksheet.write(row, column, item, h_center)
            column += 1

    if not is_full:
        row += 1
        worksheet.write(row, 2, '', top_border)
        worksheet.write(row, 18, '', top_border)
        row += 1
        worksheet.write(row, 1, 'Total Cts', bold)
        worksheet.write(row, 2, total_weight, weight)
        # worksheet.write(row, 3, '=SUM(C2:C' + str(row-2) + ')')
        worksheet.write(row, 17, 'Total$', bold)
        worksheet.write(row, 18, total_price, money)
        # worksheet.write(row, 17, '=SUM(R2:R' + str(row-2) + ')', money)

    # close workbook
    workbook.close()
    xlsx_data = output.getvalue()
    # xlsx_data = output.read()
    return xlsx_data


@csrf_exempt
@login_required
def export_diamonds(request):
    request_post = json.loads(request.body)
    id_list = request_post.get('selected_stones', None)
    is_full = request_post.get('is_full', False)
    file_name = request_post.get('file_name', 'selected_stones.xlsx')
    # id_list = request.POST.getlist('selected_stones[]', None)

    if is_full:
        diamonds = Diamond.objects.all()
    else:
        diamonds = Diamond.objects.filter(id__in=id_list)
    # response = HttpResponse(content_type='text/csv')
    # response['Content-Disposition'] = 'attachment; filename="full_inventory.csv"'

    # writer = csv.writer(response, delimiter=',')
    # writer = csv.writer(response, delimiter="\t")
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + file_name

    fieldnames = ['LotID', 'Shape', 'Cts', 'Col', 'Clar', 'Lab', 'CertNo', 'Image', 'Cut', 'Pol', 'Sym',
                  'Fluo', 'TD', 'T', 'Measurements', 'List', '%', '$/CT', 'Total$']

    # writer.writerow(fieldnames)

    diamonds = diamonds.values_list(
        'lot_id', 'shape', 'weight', 'color', 'clarity', 'lab', 'cert_no', 'diamond_filename', 'cut',
        'polish', 'symmetry', 'fluorescence', 'depth', 'table_percent', 'm1', 'list_price',
        'rap_discount', 'sale_price', 'm2', 'certificate_filename', 'm3')

    xlsx_data = create_excel_stones_selected(diamonds, fieldnames, file_name, is_full)
    response.write(xlsx_data)

    # for diamond in diamonds:
    #     temp = list(diamond)
    #     temp[7] = Diamond.DISPLAY_ABBREVIATIONS[diamond[7]]
    #     temp[8] = Diamond.DISPLAY_ABBREVIATIONS[diamond[8]]
    #     temp[9] = Diamond.DISPLAY_ABBREVIATIONS[diamond[9]]
    #     temp[16] = diamond[2] * diamond[14]
    #     temp[17] = str(diamond[16]) + 'x' + str(diamond[17]) + 'x' + str(diamond[19])
    #     temp.pop(19)
    #     writer.writerow(temp)

    return response


@csrf_exempt
@login_required
def print_selected_stones(request):
    id_list = request.POST.getlist('selected_stones[]', None)
    diamonds = Diamond.objects.filter(id__in=id_list)

    fieldnames = ['LotID', 'Shape', 'Cts', 'Col', 'Clar', 'Lab', 'CertNo', 'Image', 'Cut', 'Pol', 'Sym',
                  'Fluo', 'TD', 'T', 'Measurements', 'List', '%', '$/CT', 'Total$']

    diamonds = diamonds.values_list('lot_id', 'shape', 'weight', 'color', 'clarity', 'lab', 'cert_no', 'cut',
                                    'polish', 'symmetry', 'fluorescence', 'depth', 'table_percent', 'list_price',
                                    'rap_discount', 'sale_price', 'm1', 'm2', 'certificate_filename', 'm3',
                                    'diamond_filename')

    stones_list = []

    total_weight = 0
    total_price = 0

    for diamond in diamonds:
        image_val = ''
        file_val = diamond[6]
        if diamond[20] != '' and diamond[20] != ' ':
            image_val = '<a target="_BLANK" href=' + diamond[20] + '>View</a>'
        if diamond[18] != '' and diamond[18] != ' ':
            file_val = '<a target="_BLANK" href=' + diamond[18] + '>' + diamond[6] + '</a>'

        temp = dict()
        temp[fieldnames[0]] = diamond[0]
        temp[fieldnames[1]] = diamond[1]
        temp[fieldnames[2]] = diamond[2]
        temp[fieldnames[3]] = diamond[3]
        temp[fieldnames[4]] = diamond[4]
        temp[fieldnames[5]] = diamond[5]
        temp[fieldnames[6]] = file_val
        temp[fieldnames[7]] = image_val
        temp[fieldnames[8]] = Diamond.DISPLAY_ABBREVIATIONS[diamond[7]]
        temp[fieldnames[9]] = Diamond.DISPLAY_ABBREVIATIONS[diamond[8]]
        temp[fieldnames[10]] = Diamond.DISPLAY_ABBREVIATIONS[diamond[9]]
        temp[fieldnames[11]] = diamond[10]
        temp[fieldnames[12]] = diamond[11]
        temp[fieldnames[13]] = diamond[12]
        temp[fieldnames[15]] = diamond[13]
        temp[fieldnames[16]] = diamond[14]
        temp[fieldnames[17]] = diamond[15]
        if diamond[2] and diamond[13]:
            # temp[fieldnames[18]] = '$' + str(diamond[2] * diamond[14])
            temp[fieldnames[18]] = round((diamond[2] * diamond[13]) * Decimal((100 + diamond[14]) * 0.01), 2)
            # temp[fieldnames[18]] = '$' + str(round((diamond[2]*diamond[13])*Decimal((100+diamond[14])*0.01), 2))
        else:
            temp[fieldnames[18]] = 0

        temp[fieldnames[14]] = str(diamond[16]) + 'x' + str(diamond[17]) + 'x' + str(diamond[19])
        # temp[fieldnames[19]] = '<a target="_BLANK" href=' + diamond[18] + '>URL</a>'

        stones_list.append(temp)

        total_weight += diamond[2]
        total_price += temp[fieldnames[18]]
        temp[fieldnames[18]] = '$' + "{:,}".format(temp[fieldnames[18]])

    temp = dict()
    temp[fieldnames[0]] = ''
    temp[fieldnames[1]] = 'Total Cts'
    temp[fieldnames[2]] = total_weight
    temp[fieldnames[3]] = ''
    temp[fieldnames[4]] = ''
    temp[fieldnames[5]] = ''
    temp[fieldnames[6]] = ''
    temp[fieldnames[7]] = ''
    temp[fieldnames[8]] = ''
    temp[fieldnames[9]] = ''
    temp[fieldnames[10]] = ''
    temp[fieldnames[11]] = ''
    temp[fieldnames[12]] = ''
    temp[fieldnames[13]] = ''
    temp[fieldnames[15]] = ''
    temp[fieldnames[16]] = ''
    temp[fieldnames[17]] = 'Total$'
    temp[fieldnames[18]] = '$' + "{:,}".format(total_price)
    temp[fieldnames[14]] = ''

    stones_list.append(temp)

    return JsonResponse({'stones_list': stones_list, 'fieldnames': fieldnames}, safe=True, status=200)


@csrf_exempt
@login_required
def get_stone_ids(request):
    lot_ids = request.POST.get('lot_ids', None)

    try:
        if lot_ids:
            # get list from comma separated
            lot_ids = lot_ids.split(",")
            # queryset to list
            stones_ids = list(Diamond.objects.filter(lot_id__in=lot_ids).values_list('id', flat=True))
            stones_ids = [str(i) for i in stones_ids]
        else:
            stones_ids = list(get_filtered_diamond_queryset(request).values_list('id', flat=True))
            stones_ids = [str(i) for i in stones_ids]

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(stones_ids, safe=False, status=200)


@csrf_exempt
@login_required
def dt_list_activities(request):
    draw = request.POST.get('draw')
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    order_col = request.POST.get('order[0][column]')
    order_dir = request.POST.get('order[0][dir]')
    # custom_order = request.POST.getlist('custom_order[]')
    
    order_by = direction_symbol[order_dir] + activity_columns[int(order_col)]

    # order_by = direction_symbol[order_dir] + activity_columns[int(custom_order[int(order_col)])]
    # activities = ActivityLog.objects.all().order_by(order_by)
    activities = ActivityLog.objects.filter(status=ActivityLog.Current).order_by('-search_time')
    # shape_list = request.POST.getlist('shape[]')
    # id_list = request.POST.getlist('id[]')
    # # id_list = [int(i) for i in id_list]
    email = request.POST.get('email')
    from_date = request.POST.get('from_date', None)
    to_date = request.POST.get('to_date', None)
    # weight_to = request.POST.get('weight_to')
    # color_list = request.POST.getlist('color[]')
    # clarity_list = request.POST.getlist('clarity[]')
    # lab_list = request.POST.getlist('lab[]')
    # location_list = request.POST.getlist('location[]')
    # feed_list = request.POST.getlist('feed[]')
    # polish_list = request.POST.getlist('polish[]')
    # symmetry_list = request.POST.getlist('symmetry[]')
    # cut_list = request.POST.getlist('cut[]')
    # fluorescence_list = request.POST.getlist('fluorescence[]')
    # depth_from = request.POST.get('depth_from')
    # depth_to = request.POST.get('depth_to')
    # table_from = request.POST.get('table_from')
    # table_to = request.POST.get('table_to')
    # discount_from = request.POST.get('discount_from')
    # discount_to = request.POST.get('discount_to')

    # if len(shape_list) > 0:
    #     activities = activities.filter(shape__in=shape_list)
    # if len(id_list) > 0:
    #     temp_activities = activities.filter(cert_no__in=id_list)
    #     temp_activities_2 = activities.filter(lot_id__in=id_list)
    #     activities = temp_activities | temp_activities_2
    #
    if email:
        activities = activities.filter(user_id=email)
    # if len(color_list) > 0:
    #     activities = activities.filter(color__in=color_list)
    # if len(clarity_list) > 0:
    #     activities = activities.filter(clarity__in=clarity_list)
    # if len(lab_list) > 0:
    #     activities = activities.filter(lab__in=lab_list)
    # if len(location_list) > 0:
    #     location_list = [x.upper() for x in location_list]
    #     activities = activities.filter(country_location__in=location_list)
    # if len(feed_list) > 0:
    #     feed_list = [x.upper() for x in feed_list]
    #     activities = activities.filter(feed__in=feed_list)
    # if len(polish_list) > 0:
    #     polish_list = [x.upper() for x in polish_list]
    #     activities = activities.filter(polish__in=polish_list)
    # if len(symmetry_list) > 0:
    #     symmetry_list = [x.upper() for x in symmetry_list]
    #     activities = activities.filter(symmetry__in=symmetry_list)
    # if len(cut_list) > 0:
    #     cut_list = [x.upper() for x in cut_list]
    #     activities = activities.filter(cut__in=cut_list)
    # if len(fluorescence_list) > 0:
    #     fluorescence_list = [x.upper() for x in fluorescence_list]
    #     activities = activities.filter(fluorescence__in=fluorescence_list)
    if not(from_date == '' and to_date == ''):
        from_datetime = datetime.strptime(from_date + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
        # from_datetime = from_datetime.replace(tzinfo=settings.TIME_ZONE)
        to_datetime = datetime.strptime(to_date + ' 23:59:59', '%Y-%m-%d %H:%M:%S')
        activities = activities.filter(search_time__gte=from_datetime, search_time__lte=to_datetime)
    # if not(table_from == '0' and table_to == '200'):
    #     activities = activities.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    # if not(discount_from == '-200' and discount_to == '200'):
    #     activities = activities.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    if activity_columns[int(order_col)] == 'name':
        if order_dir == 'asc':
            activities = activities.order_by(Lower('user__first_name').asc(), Lower('user__last_name').asc())
        else:
            activities = activities.order_by(Lower('user__first_name').desc(), Lower('user__last_name').desc())
    # elif activity_columns[int(custom_order[int(order_col)])] == 'sales_manager':
    #     if order_dir == 'asc':
    #         users = users.order_by(Lower('sales_manager__first_name').asc(), Lower('sales_manager__last_name').asc())
    #     else:
    #         users = users.order_by(Lower('sales_manager__first_name').desc(), Lower('sales_manager__last_name').desc())
    elif activity_columns[int(order_col)] == 'id' or \
            activity_columns[int(order_col)] == 'user__last_login' or \
            activity_columns[int(order_col)] == 'search_time' or \
            activity_columns[int(order_col)] == 'results_no':
        activities = activities.order_by(order_by)
    else:
        if order_dir == 'asc':
            activities = activities.order_by(Lower(activity_columns[int(order_col)]).asc())
        else:
            activities = activities.order_by(Lower(activity_columns[int(order_col)]).desc())

    temp = dict()
    temp['draw'] = int(draw)
    temp_list = []
    activities = activities[start:start+length]
    current_date = datetime.now()
    week_no = current_date.strftime("%V")
    month_no = current_date.month
    weekly_count = 0
    monthly_count = 0
    total_count = 0
    ignore_dict = "{'shape': [], 'id': [], 'weight_from': 0, 'weight_to': 200, 'color': [], 'clarity': [], " \
                  "'lab': [], 'location': [], 'polish': [], 'symmetry': [], 'cut': [], 'fluorescence': [], " \
                  "'depth_from': 0, 'depth_to': 200, 'table_from': 0, 'table_to': 200, 'discount_from': -200, " \
                  "'discount_to': 200}"

    for activity_a in activities:
        json_state = json.loads(activity_a.state)
        del json_state['feed']

        if ignore_dict != str(json_state):
            temp_list.append(
                [
                    activity_a.id, activity_a.user.last_login.strftime("%b %d, %Y %H:%M") if activity_a.user.last_login else '', activity_a.user.company_name,
                    activity_a.user.get_full_name(), activity_a.user.email,
                    activity_a.search_time.strftime("%b %d, %Y %H:%M:%S"), activity_a.state,
                    activity_a.state, activity_a.search_source, activity_a.results_no, '', activity_a.user.id
                 # ('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+activity_a.certificate_filename+'">View</a>' if (activity_a.certificate_filename != ' ' and activity_a.certificate_filename != '') else""),
                 # ('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+activity_a.activity_filename+'">View</a>' if (activity_a.activity_filename != ' ' and activity_a.activity_filename != '') else""),
                 ]
            )
            if activity_a.search_time.strftime("%V") == week_no:
                weekly_count += 1
            if activity_a.search_time.month == month_no:
                monthly_count += 1
            total_count += 1

    # print(weekly_count, monthly_count)
    temp['data'] = temp_list
    temp['recordsTotal'] = total_count
    temp['recordsWeekly'] = weekly_count
    temp['recordsMonthly'] = monthly_count
    temp['recordsFiltered'] = total_count

    return JsonResponse(temp, safe=True)


@csrf_exempt
@login_required
def create_activity_log(request):
    user_id = request.user.id
    search_source = request.POST.get('search_source', None)
    results_no = request.POST.get('results_no', None)
    state = request.POST.get('state', None)
    try:
        ActivityLog.objects.create(
            user_id=user_id,
            search_source=search_source,
            results_no=results_no,
            state=state
        )

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse({}, safe=True, status=201)


@csrf_exempt
@login_required
def get_activity_log(request):
    id = request.POST.get('id', None)
    user_id = request.user.id

    try:
        table = ActivityLog.objects.get(id=id)
        data = model_to_dict(table)

    except ActivityLog.DoesNotExist:
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(data, safe=True, status=200)


def get_filtered_diamond_queryset(request):
    diamonds = Diamond.objects.all()
    shape_list = request.POST.getlist('shape[]')
    id_list = request.POST.getlist('id[]')
    # id_list = [int(i) for i in id_list]
    weight_from = request.POST.get('weight_from')
    weight_to = request.POST.get('weight_to')
    color_list = request.POST.getlist('color[]')
    clarity_list = request.POST.getlist('clarity[]')
    lab_list = request.POST.getlist('lab[]')
    location_list = request.POST.getlist('location[]')
    feed_list = request.POST.getlist('feed[]')
    polish_list = request.POST.getlist('polish[]')
    symmetry_list = request.POST.getlist('symmetry[]')
    cut_list = request.POST.getlist('cut[]')
    fluorescence_list = request.POST.getlist('fluorescence[]')
    depth_from = request.POST.get('depth_from')
    depth_to = request.POST.get('depth_to')
    table_from = request.POST.get('table_from')
    table_to = request.POST.get('table_to')
    discount_from = request.POST.get('discount_from')
    discount_to = request.POST.get('discount_to')

    if len(shape_list) > 0:
        diamonds = diamonds.filter(shape__in=shape_list)
    if len(id_list) > 0:
        temp_diamonds = diamonds.filter(cert_no__in=id_list)
        temp_diamonds_2 = diamonds.filter(lot_id__in=id_list)
        diamonds = temp_diamonds | temp_diamonds_2

    if not(weight_from == '0' and weight_to == '200'):
        diamonds = diamonds.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    if len(color_list) > 0:
        diamonds = diamonds.filter(color__in=color_list)
    if len(clarity_list) > 0:
        diamonds = diamonds.filter(clarity__in=clarity_list)
    if len(lab_list) > 0:
        diamonds = diamonds.filter(lab__in=lab_list)
    if len(location_list) > 0:
        location_list = [x.upper() for x in location_list]
        diamonds = diamonds.filter(country_location__in=location_list)
    if len(feed_list) > 0:
        # TEMPORARY MASK
        # feed_list = ['EZ' if x == 'NIRU' else x.upper() for x in feed_list]
        feed_list = [x.upper() for x in feed_list]
        diamonds = diamonds.filter(feed__in=feed_list)
    if len(polish_list) > 0:
        polish_list = [x.upper() for x in polish_list]
        diamonds = diamonds.filter(polish__in=polish_list)
    if len(symmetry_list) > 0:
        symmetry_list = [x.upper() for x in symmetry_list]
        diamonds = diamonds.filter(symmetry__in=symmetry_list)
    if len(cut_list) > 0:
        cut_list = [x.upper() for x in cut_list]
        diamonds = diamonds.filter(cut__in=cut_list)
    if len(fluorescence_list) > 0:
        fluorescence_list = [x.upper() for x in fluorescence_list]
        diamonds = diamonds.filter(fluorescence__in=fluorescence_list)
    if not(depth_from == '0' and depth_to == '200'):
        diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    if not(table_from == '0' and table_to == '200'):
        diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    if not(discount_from == '-200' and discount_to == '200'):
        diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    return diamonds
