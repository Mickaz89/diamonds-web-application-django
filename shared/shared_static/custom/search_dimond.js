var TOTAL_WEIGHT = '';
var TOTAL_PRICE = '';

$('#search-block').keydown(function(e) {
  var key = e.which;
  if (key === 13) {
    // As ASCII code for ENTER key is "13"
    // console.warn('Enter is pressed');
    get_id();
    diamond_table_server.ajax.reload();
  }
});


function fix_col_issues() {
    multi_table.button(0).remove();
    multi_table.button().add(0,
    {
        extend: 'colvis',
        text: 'Show/Hide Columns',
        className: "btn btn-primary",
        autoClose: false,
        fade: 0,
        columns: ':not(.noColVis)',
        colVis: { showAll: "Show all" }
    });

}


var multi_table = $('#multi-select-diamonds').DataTable({
    "initComplete": function(settings, json) {
        this.api().button(0).remove();
        this.api().button().add(0,
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            className: "btn btn-primary",
            autoClose: false,
            fade: 0,
            columns: ':not(.noColVis)',
            colVis: { showAll: "Show all" }
        });

    },

    dom: 'Bfrtip',
    paging: false,
    searching: false,
    colReorder: true,
    info: false,
    destroy: false,
    stateSave: true,
    stateDuration: 60 * 60 * 24 * 365,
    stateSaveCallback: function(settings,data) {
      // console.log('state save called');
      localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
    },
    stateLoadCallback: function(settings) {
      // console.log('state load called');
      return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
    },
    // retrieve: false,
    language: {
        decimal: ".",
        thousands: ","
    },
    // columns: [
    //     { "orderable": false , targets: 0}
    // ],
    columnDefs: [{
        orderable: false,
        // className: 'select-checkbox',
        className: 'noVis',
        targets: 0,
        visible: false
    }],
    buttons: [
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            columns: ':not(.noVis)',
            className: "btn btn-primary"
        }
        // 'selected',
        // 'selectedSingle',
        // 'selectAll',
        // 'selectNone',
        // 'selectRows',
        // 'selectColumns',
        // 'selectCells'
    ],
    select: {
        // style: 'multi'
        // selector: 'td:first-child'
        style: 'os'
    }
});


multi_table.on('column-reorder', function () {
    fix_col_issues();
});

var diamonds_ready = false;
shape_list = ['Round'];
id_list = [];
color_list = [];
clarity_list = [];
lab_list = [];
location_list = [];
feed_list = [];
polish_list = [];
symmetry_list = [];
cut_list = [];
fluorescence_list = [];
weight_from = 0;
weight_to = 200;
depth_from = 0;
depth_to = 200;
table_from = 0;
table_to = 200;
discount_from = -200;
discount_to = 200;
// var ckeditor;
var interactive_count = 0;

var stones_col_order_local = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33];
var stones_col_vis_local = [{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}];
var selected_stones = [];


// var shortDateFormat = 'dd/MM/yyyy';
var shortDateFormat = 'MMM. dd, yyyy';
var longDateFormat  = 'MMM. dd, yyyy, hh:mm a';

var running = true;
var count = 0;
var pages = 0;
var next = false;
var previous = false;
var page = 1;

// var url = "filter/?shape=shape_list".replace('shape_list', shape_list);
// document.location.href = url;

// $('#shape #select_all').toggle(
//     function() {
//         $('#shape .shp').prop('checked', true);
//     },
//     function() {
//         $('#shape .shp').prop('checked', false);
//     }
// );

// $('#multi-select-diamonds').on('scroll', function() {
//     console.log('scrolled');
//     if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight +600) {
//         alert('end reached');
//     }
// });


// scroll case
$(window).on('scroll', function() {
    if(!running && next) {
        if($(window).scrollTop() >= $('#multi-select-diamonds').offset().top + $('#multi-select-diamonds').outerHeight() - window.innerHeight) {
            get_stones();
            // console.log('from scroll');
        }
    }
});


function get_id(id=null) {
    if(!id) {
        id = $('#lotid').val();
    }
    // get array from comma separated
    id_list = id.split(',');
    while($.inArray("", id_list)!=-1) {
        var index = $.inArray("", id_list);
        id_list.splice( index, 1 );
    }

    // if (id && id !== "") {
    //     id_list.push(parseInt(id));
    // }
}
// $("#lotid").change(function () {
//     get_set_interactive_count();
// });

// shape select_all case
$( "#shape #select_all" ).click(function(shp) {
    if ($('#' + shp.target.id).is(":checked")) {
        $('.shp').prop('checked', true);
        shape_list = ['Round', 'Radiant', 'Princess', 'Pear', 'Oval', 'Marquise', 'Heart', 'Emerald', 'Cushion',
            'Baguette', 'Sq Emerald', 'Triangle', 'Other'];
    }
    else {
        $('.shp').prop('checked', false);
        shape_list = [];
    }
    // console.log('shape_LIST: ', shape_list);
    get_set_interactive_count();

});


// prepare shape list
$( ".shp" ).click(function(shp) {
    var name = shp.target.id;
    if (shp.target.id === 'Asscher') name = 'Sq Emerald';
    if ($('#' + shp.target.id).is(":checked")) {
        shp.checked=!shp.checked;
        shape_list.push(name);
    }
    else {
        var index = shape_list.indexOf(name);
        if (index !== -1) shape_list.splice(index, 1);
        $('#select_all').prop('checked', false);
    }
    // console.log('shape_LIST: ', shape_list);
    get_set_interactive_count();

  $(".shape input").each(function( index, element ) {
    // console.log('index', index);
    // console.log('element', element);
    // console.log('this', this);
    // element == this
    // $( element ).css( "backgroundColor", "yellow" );
    // if ( $( this ).is( "#stop" ) ) {
    //   $( "span" ).text( "Stopped at div index #" + index );
    //   return false;
    // }
  });
});


// on search

// $("#search").unbind().click(function () {
//     $("#pagination").hide();
//
//     next = true;
//     running = false;
//     page = 1;
//     // $("#multi-select-diamonds").find("tr:gt(0)").remove();
//     multi_table.clear().draw();
//     get_id();
//     get_stones();
//
//     // $('#diamond-table-server').DataTable();
//     // $("#stone-count").hide();
//     // console.log('from click');
// });
$("#search").unbind().click(function () {
    // multi_table.clear().draw();
    get_id();
    diamond_table_server.ajax.reload();
    create_activity_log();
    save_filters_state();
});


function get_stones() {
  running = true;
  $("#more-loader").show();
  $.ajax({
    url: '/diamonds/filter/',
    data: {
      'shape': shape_list,
      'id': id_list,
      'weight_from': weight_from,
      'weight_to': weight_to,
      'color': color_list,
      'clarity': clarity_list,
      'lab': lab_list,
      'location': location_list,
      'feed': feed_list,
      'polish': polish_list,
      'symmetry': symmetry_list,
      'cut': cut_list,
      'fluorescence': fluorescence_list,
      'depth_from': depth_from,
      'depth_to': depth_to,
      'table_from': table_from,
      'table_to': table_to,
      'discount_from': discount_from,
      'discount_to': discount_to,
      'page': page
    },
    type: "POST",
    dataType: 'json',
    success: function (data) {
        count = data.count;
        pages = data.pages;
        next = data.next;
        previous = data.previous;
        page = parseInt(data.page, 10);
        $.each(data.data, function(key, value) {

            multi_table.row.add( [
                value.pk,
                value.fields.lot_id,
                value.fields.lot_id_str,
                value.fields.shape,
                value.fields.weight,
                value.fields.color,

                value.fields.clarity,
                value.fields.lab,
                value.fields.polish,
                value.fields.symmetry,
                value.fields.fluorescence,

                value.fields.cert_no,
                value.fields.m1,
                value.fields.m2,
                value.fields.m3,
                value.fields.depth,

                value.fields.table_percent,
                value.fields.crown_angle,
                value.fields.fluorescence_color,
                value.fields.girdle,
                value.fields.culet,

                value.fields.quantity,
                value.fields.list_price,
                value.fields.rap_discount,
                value.fields.sale_price,
                value.fields.status,

                value.fields.cut,
                (value.fields.certificate_filename!==' '&&value.fields.certificate_filename!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value.fields.certificate_filename+'">View</a>':""),
                (value.fields.diamond_filename!==' '&&value.fields.diamond_filename!==''?'<a class="btn btn-info " style="padding:1px 4px;" target="_blank" href="'+value.fields.diamond_filename+'">View</a>':""),
                $.format.date(value.fields.date_created, longDateFormat),
                value.fields.feed,

                value.fields.country_location,
                value.fields.fancy_intensity,
                value.fields.fancy_overtone
            ] ).draw();


            // $('#multi-select-diamonds').append(
            //     '<tr><td>'+value.fields.lot_id+'</td><td>'+value.fields.lot_id_str+'</td><td>'+value.fields.shape+
            //     '</td><td>'+value.fields.weight+'</td><td>'+value.fields.color+'</td><td>'+value.fields.clarity+
            //     '</td><td>'+value.fields.lab+'</td><td>'+value.fields.polish+'</td><td>'+value.fields.symmetry+
            //     '</td><td>'+value.fields.fluorescence+'</td><td>'+value.fields.cert_no+'</td><td>'+value.fields.m1+
            //     '</td><td>'+value.fields.m2+'</td><td>'+value.fields.m3+'</td><td>'+(value.fields.depth?value.fields.depth:"")+
            //     '</td><td>'+value.fields.table_percent+'</td><td>'+(value.fields.crown_angle?value.fields.crown_angle:"")+
            //     '</td><td>'+(value.fields.fluorescence_color?value.fields.fluorescence_color:"")+'</td><td>'+(value.fields.girdle?value.fields.girdle:"")+
            //     '</td><td>'+(value.fields.culet?value.fields.culet:"")+'</td><td>'+value.fields.quantity+'</td><td>'+(value.fields.list_price?value.fields.list_price:"")+
            //     '</td><td>'+(value.fields.rap_discount?value.fields.rap_discount:"")+'</td><td>'+value.fields.sale_price+'</td><td>'+value.fields.status+
            //     '</td><td>'+value.fields.cut+'</td><td>'+(value.fields.certificate_filename!==' '&&value.fields.certificate_filename!==''?'<a class="btn btn-info " style="padding: 5px;" target="_blank" href="'+value.fields.certificate_filename+'">View</a>':"")+
            //     '</td><td>'+(value.fields.diamond_filename!==' '&&value.fields.diamond_filename!==''?'<a class="btn btn-info " style="padding:5px;" target="_blank" href="'+value.fields.diamond_filename+'">View</a>':"")+
            //     '</td><td>'+$.format.date(value.fields.date_created, shortDateFormat)+'</td><td>'+value.fields.feed+'</td><td>'+value.fields.country_location+
            //     '</td><td>'+value.fields.fancy_intensity+'</td><td>'+value.fields.fancy_overtone+'</td></tr>');
        });
        running = false;
        page += 1;
        $("#more-loader").hide();
        $('#stone-count').html('Total ' + count + ' Recor' + (parseInt(count) !== 1 ? 'ds' : 'd') + ' Found');
    },
    error: function(xhr, error){
        $("#more-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


// quick change case
$("#weight_select").change(function () {
    var quick_weight = this.value;
    // var quick_weight = $('#weight_select').val();

    var quick_weight_list = quick_weight.split(' - ');
    $('#weight_from').val(quick_weight_list[0]);
    $('#weight_to').val(quick_weight_list[1]);

    weight_from = quick_weight_list[0];
    weight_to = quick_weight_list[1];
    // if (quick_weight_list[0] && quick_weight_list[1]) {
    // }
    // else {
    //     weight_from = 0.00;
    //     weight_to = 100;
    // }
    // console.log(weight_from, weight_to);
    get_set_interactive_count();
});


// manual change case
$("#weight_from").change(function () {
    weight_from = this.value;
    get_set_interactive_count();
});
$("#weight_to").change(function () {
    weight_to = this.value;
    get_set_interactive_count();
});


//color type case
$('input:radio[name="color_type"]').change(function(){
    color_list = [];
    if ($(this).is(':checked') && $(this).val() === 'type1'){
       $('input.type2').prop('checked', false).attr("disabled", true);
       $("div.type2").removeClass('checkbox-primary').addClass('checkbox-default');

       $("input.type1").attr("disabled", false);
       $("div.type1").removeClass('checkbox-default').addClass('checkbox-primary');
    }
    if ($(this).is(':checked') && $(this).val() === 'type2'){
       $('input.type1').prop('checked', false).attr("disabled", true);
       $("div.type1").removeClass('checkbox-primary').addClass('checkbox-default');

       $("input.type2").attr("disabled", false);
       $("div.type2").removeClass('checkbox-default').addClass('checkbox-primary');
    }
});


// prepare color list
$( "input.type1" ).click(function(color) {
    var id = color.target.id;
    if (id === 'color1_all') {
        if ($('#' + id).is(":checked")) {
            $('input.type1').prop('checked', true);
            color_list = ['D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O_Z'];
        }
        else {
            $('input.type1').prop('checked', false);
            color_list = [];
        }
    } else {
        if ($('#' + id).is(":checked")) {
            color.checked=!color.checked;
            color_list.push(id.split('color_')[1]);
        }
        else {
            var index = color_list.indexOf(id.split('color_')[1]);
            if (index !== -1) color_list.splice(index, 1);
            $('#color1_all').prop('checked', false);
        }
    }
    // console.log('COLOR_LIST: ', color_list);
    get_set_interactive_count();
});
$( "input.type2" ).click(function(color) {
    var id = color.target.id;
    if (id === 'color2_all') {
        if ($('#' + id).is(":checked")) {
            $('input.type2').prop('checked', true);
            color_list = ['Yellow', 'Pink', 'Others'];
        }
        else {
            $('input.type2').prop('checked', false);
            color_list = [];
        }
    } else {
        if ($('#' + id).is(":checked")) {
            color.checked=!color.checked;
            color_list.push(id);
        }
        else {
            var index = color_list.indexOf(id);
            if (index !== -1) color_list.splice(index, 1);
            $('#color2_all').prop('checked', false);
        }
    }
    // console.log('COLOR_LIST: ', color_list);
    get_set_interactive_count();
});


// prepare clarity list
$( "input.clarity" ).click(function(clarity) {
    var id = clarity.target.id;
    var trimmed_id = id.split('clarity_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.clarity').prop('checked', true);
            clarity_list = ['IF', 'VVS1', 'VVS2', 'VS1', 'VS2', 'SI1', 'SI2', 'I1'];
        }
        else {
            $('input.clarity').prop('checked', false);
            clarity_list = [];
        }
    } else {
        if ($('#' + id).is(":checked")) {
            clarity.checked=!clarity.checked;
            clarity_list.push(trimmed_id);
        }
        else {
            var index = clarity_list.indexOf(trimmed_id);
            if (index !== -1) clarity_list.splice(index, 1);
            $('#clarity_all').prop('checked', false);
        }
    }
    // console.log('clarity_LIST: ', clarity_list);
    get_set_interactive_count();
});


// prepare lab list
$( "input.lab" ).click(function(lab) {
    var id = lab.target.id;
    var trimmed_id = id.split('lab_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.lab').prop('checked', true);
            lab_list = ['GIA', 'HRD', 'IGI', 'Other'];
        }
        else {
            $('input.lab').prop('checked', false);
            lab_list = [];
        }
    } else {
        if ($('#' + id).is(":checked")) {
            lab.checked=!lab.checked;
            lab_list.push(trimmed_id);
        }
        else {
            var index = lab_list.indexOf(trimmed_id);
            if (index !== -1) lab_list.splice(index, 1);
            $('#lab_all').prop('checked', false);
        }
    }
    // console.log('lab_LIST: ', lab_list);
    get_set_interactive_count();
});


// prepare location list
$( "input.location" ).click(function(location) {
    var id = location.target.id;
    var trimmed_id = id.split('location_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.location').prop('checked', true);
            location_list = ['Belgium', 'Israel', 'Hong Kong'];
        }
        else {
            $('input.location').prop('checked', false);
            location_list = [];
        }
    } else {
        if (trimmed_id === 'Hong_Kong')
            trimmed_id = 'Hong Kong';

        if ($('#' + id).is(":checked")) {
            location.checked=!location.checked;
            location_list.push(trimmed_id);
        }
        else {
            var index = location_list.indexOf(trimmed_id);
            if (index !== -1) location_list.splice(index, 1);
            $('#location_all').prop('checked', false);
        }
    }
    // console.log('location_LIST: ', location_list);
    get_set_interactive_count();
});


// prepare feed list
$( "input.stonegroup" ).click(function(feed) {
    var id = feed.target.id;
    var trimmed_id = id.split('stonegroup_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.stonegroup').prop('checked', true);
            feed_list = ['AMC', 'ERAN', 'EZ', 'NIRU', 'GUTFRUND', 'KIRAN', 'LSDCO IL', 'SDE', 'SRK'];
        }
        else {
            $('input.stonegroup').prop('checked', false);
            feed_list = [];
        }
    } else {
        if (trimmed_id === 'LSDCO_IL')
            trimmed_id = 'LSDCO IL';

        if ($('#' + id).is(":checked")) {
            feed.checked=!feed.checked;
            feed_list.push(trimmed_id);
        }
        else {
            var index = feed_list.indexOf(trimmed_id);
            if (index !== -1) feed_list.splice(index, 1);
            $('#stonegroup_all').prop('checked', false);
        }
    }
    // console.log('feed_LIST: ', feed_list);
    get_set_interactive_count();
});


// prepare polish list
$( "input.polish" ).click(function(polish) {
    var id = polish.target.id;
    var trimmed_id = id.split('polish_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.polish').prop('checked', true);
            polish_list = ['EXCELLENT', 'FAIR', 'GOOD', 'VERY GOOD'];
        }
        else {
            $('input.polish').prop('checked', false);
            polish_list = [];
        }
    } else {
        if (trimmed_id === 'VERY_GOOD')
            trimmed_id = 'VERY GOOD';

        if ($('#' + id).is(":checked")) {
            polish.checked=!polish.checked;
            polish_list.push(trimmed_id);
        }
        else {
            var index = polish_list.indexOf(trimmed_id);
            if (index !== -1) polish_list.splice(index, 1);
            $('#polish_all').prop('checked', false);
        }
    }
    // console.log('polish_LIST: ', polish_list);
    get_set_interactive_count();
});


// prepare symmetry list
$( "input.symmetry" ).click(function(symmetry) {
    var id = symmetry.target.id;
    var trimmed_id = id.split('symmetry_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.symmetry').prop('checked', true);
            symmetry_list = ['EXCELLENT', 'FAIR', 'GOOD', 'VERY GOOD'];
        }
        else {
            $('input.symmetry').prop('checked', false);
            symmetry_list = [];
        }
    } else {
        if (trimmed_id === 'VERY_GOOD')
            trimmed_id = 'VERY GOOD';

        if ($('#' + id).is(":checked")) {
            symmetry.checked=!symmetry.checked;
            symmetry_list.push(trimmed_id);
        }
        else {
            var index = symmetry_list.indexOf(trimmed_id);
            if (index !== -1) symmetry_list.splice(index, 1);
            $('#symmetry_all').prop('checked', false);
        }
    }
    // console.log('symmetry_LIST: ', symmetry_list);
    get_set_interactive_count();
});


// prepare cut list
$( "input.cut" ).click(function(cut) {
    var id = cut.target.id;
    var trimmed_id = id.split('cut_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.cut').prop('checked', true);
            cut_list = ['EXCELLENT', 'FAIR', 'GOOD', 'VERY GOOD', 'Other'];
        }
        else {
            $('input.cut').prop('checked', false);
            cut_list = [];
        }
    } else {
        if (trimmed_id === 'VERY_GOOD')
            trimmed_id = 'VERY GOOD';

        if ($('#' + id).is(":checked")) {
            cut.checked=!cut.checked;
            cut_list.push(trimmed_id);
        }
        else {
            var index = cut_list.indexOf(trimmed_id);
            if (index !== -1) cut_list.splice(index, 1);
            $('#cut_all').prop('checked', false);
        }
    }
    // console.log('cut_LIST: ', cut_list);
    get_set_interactive_count();
});


// 3X
$("#3x").unbind().click(function () {
    var ex = 'EXCELLENT';
    if ($('#3x').is(":checked")) {
        cut_list = [ex];
        symmetry_list = [ex];
        polish_list = [ex];
        $('input#cut_' + ex).prop('checked', true);
        $('input#symmetry_' + ex).prop('checked', true);
        $('input#polish_' + ex).prop('checked', true);
    }
    else {
        var index = cut_list.indexOf(ex);
        if (index !== -1) cut_list.splice(index, 1);
        index = symmetry_list.indexOf(ex);
        if (index !== -1) symmetry_list.splice(index, 1);
        index = polish_list.indexOf(ex);
        if (index !== -1) polish_list.splice(index, 1);

        $('input#cut_' + ex).prop('checked', false);
        $('input#symmetry_' + ex).prop('checked', false);
        $('input#polish_' + ex).prop('checked', false);
    }
});


// VG+
$("#vg_plus").unbind().click(function () {
    var ex = 'EXCELLENT';
    var vg = 'VERY GOOD';
    var vg_id = 'VERY_GOOD';
    if ($('#vg_plus').is(":checked")) {
        cut_list = [ex, vg];
        symmetry_list = [ex, vg];
        polish_list = [ex, vg];
        $('input#cut_' + ex).prop('checked', true);
        $('input#symmetry_' + ex).prop('checked', true);
        $('input#polish_' + ex).prop('checked', true);
        $('input#cut_' + vg_id).prop('checked', true);
        $('input#symmetry_' + vg_id).prop('checked', true);
        $('input#polish_' + vg_id).prop('checked', true);
    }
    else {
        var index = cut_list.indexOf(ex);
        if (index !== -1) cut_list.splice(index, 1);
        index = symmetry_list.indexOf(ex);
        if (index !== -1) symmetry_list.splice(index, 1);
        index = polish_list.indexOf(ex);
        if (index !== -1) polish_list.splice(index, 1);
        index = cut_list.indexOf(vg);
        if (index !== -1) cut_list.splice(index, 1);
        index = symmetry_list.indexOf(vg);
        if (index !== -1) symmetry_list.splice(index, 1);
        index = polish_list.indexOf(vg);
        if (index !== -1) polish_list.splice(index, 1);

        $('input#cut_' + ex).prop('checked', false);
        $('input#symmetry_' + ex).prop('checked', false);
        $('input#polish_' + ex).prop('checked', false);
        $('input#cut_' + vg_id).prop('checked', false);
        $('input#symmetry_' + vg_id).prop('checked', false);
        $('input#polish_' + vg_id).prop('checked', false);
    }
});




// prepare fluorescence list
$( "input.fluorescence" ).click(function(fluorescence) {
    var id = fluorescence.target.id;
    var trimmed_id = id.split('fluorescence_')[1];
    if (trimmed_id === 'all') {
        if ($('#' + id).is(":checked")) {
            $('input.fluorescence').prop('checked', true);
            fluorescence_list = ['F', 'M', 'N', 'SL', 'VSL', 'STR', 'VST'];
        }
        else {
            $('input.fluorescence').prop('checked', false);
            fluorescence_list = [];
        }
    } else {
        if ($('#' + id).is(":checked")) {
            fluorescence.checked=!fluorescence.checked;
            fluorescence_list.push(trimmed_id);
        }
        else {
            var index = fluorescence_list.indexOf(trimmed_id);
            if (index !== -1) fluorescence_list.splice(index, 1);
            $('#fluorescence_all').prop('checked', false);
        }
    }
    // console.log('fluorescence_LIST: ', fluorescence_list);
    get_set_interactive_count();
});


// depth case
$("#depth_from").change(function () {
    depth_from = this.value;
    get_set_interactive_count();
});
$("#depth_to").change(function () {
    depth_to = this.value;
    get_set_interactive_count();
});


// table case
$("#table_from").change(function () {
    table_from = this.value;
    get_set_interactive_count();
});
$("#table_to").change(function () {
    table_to = this.value;
    get_set_interactive_count();
});


// discount case
$("#discount_from").change(function () {
    discount_from = this.value;
    get_set_interactive_count();
});
$("#discount_to").change(function () {
    discount_to = this.value;
    get_set_interactive_count();
});


// ajax data table with server side processing
var diamond_table_server = $('#diamond-table-server').DataTable({
    "initComplete": function(settings, json) {
        this.api().button(0).remove();
        this.api().button().add(0,
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            className: "btn btn-primary",
            autoClose: false,
            fade: 0,
            columns: ':not(.noColVis)',
            colVis: { showAll: "Show all" }
        });
        // this.api().state.save(custom_flag);
        // this.api().order([[ $.inArray(23, stones_col_order_local), "desc" ]]);   // not effective
        // console.log('init complete', settings, json);
    },

    // dom: 'Bfritip',
    dom: 'Biti',
    // "dom": '<"top"i>rt<"bottom"flp><"clear">',
    // processing: true,
    "serverSide": true,
    // "paging": true,
    // "pageLength": 50,
    // "length ": 50,
    // rowId: 'DT_RowId',
    "searching": false,
    // "colReorder": true,
    colReorder: {
        // true create problems on bigger data.
        realtime: false
    },
    "ordering": true,
    // order: [[ $.inArray(23, stones_col_order_local), "desc" ]],
    // "info": true,
    // deferRender: true,
    // rowId: 'extn',
    // select: true,
    stateSave: true,
    stateDuration: 60 * 60 * 24 * 365,

    stateSaveCallback: function(settings, data) {
        // console.log('state save called', data.order[0][0]);
        // var temp = data.order;
        data.start = 0;
        data.scroller.topRow = 0;
        // data.order = [[ $.inArray(23, stones_col_order_local), "desc" ]];   // problem on manual sort
        // data.order = [[ $.inArray(data.order[0][0], stones_col_order_local), data.order[0][1] ]];
        // data.order = [[ stones_col_order_local[data.order[0][0]], data.order[0][1] ]];
        data.scroller.baseScrollTop = 0;
        data.scroller.baseRowTop = 0;
        // console.log(data.order);

        localStorage.setItem(settings.sInstance, JSON.stringify(data));
        set_diamonds_counter_distance(settings);
        stones_col_vis_local = data.columns;
        stones_col_order_local = data.ColReorder;

        var api = new $.fn.dataTable.Api( settings );
        // api.order([[ $.inArray(23, data.ColReorder), "desc" ]]);   // problem on manual sort
        // api.order([[data.order[0][0],data.order[0][1]]]);   // problem on manual sort

        // // console.log(stones_col_order_local, data.ColReorder);
        // // delete data.start;
        //
        // if (JSON.stringify(stones_col_order_local) === JSON.stringify(data.ColReorder) &&
        //     JSON.stringify(stones_col_vis_local) === JSON.stringify(data.columns)) {
        //     // console.log('state save skipped');
        //     return;
        // }
        // // stones_col_order_local = data.ColReorder;
        // data.start = 0;
        // // skip from saving
        // // data.order = [[ stones_col_order_local[$.inArray(23, stones_col_order_local)], "desc" ]];
        // data.order = [[ $.inArray(23, stones_col_order_local), "desc" ]];
        // data.scroller.topRow = 0;
        // data.scroller.baseScrollTop = 0;
        // data.scroller.baseRowTop = 0;
        // // console.log(5, data.order);
        // $.ajax({
        //     url: '/accounts/update_state/',
        //     data: {
        //       'table_name': settings.sInstance,
        //       'table_state': JSON.stringify(data)
        //     },
        //     type: "POST",
        //     async: false,
        //     dataType: 'json',
        //     success: function (response) {
        //       // localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data));
        //       // var temp = JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
        //       //  if removed problem on reorder but creating sorting on wrong col
        //       var temp = data;
        //       if (temp) {
        //         stones_col_order_local = temp.ColReorder;
        //         stones_col_vis_local = temp.columns;
        //       }
        //     },
        //     error: function(xhr, error){
        //       console.log('error on state update');
        //     }
        // });
    },
    stateLoadCallback: function(settings, data) {
        let api = new $.fn.dataTable.Api( settings );
        // console.log('state load called', settings, data);
        // console.log(settings);
        var o = "";
        $.ajax({
            url: '/accounts/get_state/',
            data: {
              'table_name': settings.sInstance
            },
            type: "POST",
            async: false,
            dataType: 'json',
            success: function (data) {
                o = JSON.parse(data.state);
                let col = $.inArray(23, o.ColReorder);
                let col_dir = "desc";
                stones_col_order_local = o.ColReorder;
                stones_col_vis_local = o.columns;
                // handle old save case
                // console.log(stones_col_order_local);
                // o.order = [[ 23, "desc" ]];  // handle initial case
                o.order = [[ col, col_dir ]];  // handle initial case
                // o.order = [[ $.inArray(23, o.ColReorder), "desc" ]];  // handle initial case
                // o.order = [[ $.inArray(o.order[0][0], o.ColReorder), o.order[0][1] ]];
                // api.order([{column: o.order[0][0], dir: o.order[0][1]}]);
                api.order([[col, col_dir]]);
                console.log(o.ColReorder, o.order, $.inArray(23, o.ColReorder), col, col_dir);
                // return JSON.parse(data.state);
                // return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
            },
            error: function(xhr, error){
              console.log('error on state get');
              // var temp = JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
              // if (temp) {
              //   stones_col_order_local = temp.ColReorder;
              // }
              // o = temp;
            }
        });
        return o;
        // return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
    },
    stateLoaded: function (e, settings, data) {
        // console.log('state loaded with:', e, settings, data);
    },

    // stateLoadParams: function( settings, data ) {
    //   console.log('state load param');
    //   if (data.start) delete data.start;
    // },
    rowCallback: function(row, data, displayNum, displayIndex, dataIndex ) {
        // console.log(data.DT_RowId);
        if ( $.inArray(data.DT_RowId.toString(), selected_stones) !== -1 ) {
            if (!$(row).hasClass("selected")) {
                $(row).addClass('selected');
            }
            // this.api().row( dataIndex ).select();
            this.api().row( displayIndex ).select();
        }
    },

    columnDefs: [{
        // orderable: false,
        // className: 'select-checkbox',
        className: 'noVis',
        targets: [stones_col_order_local[0],stones_col_order_local[2],stones_col_order_local[17],stones_col_order_local[21],stones_col_order_local[29]],
        visible: false
    }],
    buttons: [
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            columns: ':not(.noVis)',
            className: "btn btn-primary"
        },
        {
            extend: 'selectNone',
            text: 'Reset Selection',
            className: "btn btn-warning",
            action: function (e, dt, node, config) {
                dt.rows().deselect();
                selected_stones = [];
            }
        },
        // {
        //     className: "btn btn-primary",
        //     // id: 'ExportButton',
        //     text: "Export Filtered Inventory",
        //     action: function (e, dt, node, config)
        //     {
        //         // Option 1
        //         // complex in case of filtering
        //         // var params = dt.ajax.params();
        //         // window.location.href = 'export_filtered_diamonds/?dt=' + params;
        //
        //         // Option 2
        //         // $.ajax({
        //         //     "url": "export_filtered_diamonds/",
        //         //     // ContentType: "application/x-www-form-urlencoded;charset=utf-8",
        //         //     // ContentType: "application/vnd.ms-excel",
        //         //     // ContentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //         //     // responseType: 'blob',
        //         //     // responseType: 'arraybuffer',
        //         //     data: Object.assign({}, dt.ajax.params(), {
        //         //       'custom_order': stones_col_order_local,
        //         //       'shape': shape_list,
        //         //       'id': id_list,
        //         //       'weight_from': weight_from,
        //         //       'weight_to': weight_to,
        //         //       'color': color_list,
        //         //       'clarity': clarity_list,
        //         //       'lab': lab_list,
        //         //       'location': location_list,
        //         //       'feed': feed_list,
        //         //       'polish': polish_list,
        //         //       'symmetry': symmetry_list,
        //         //       'cut': cut_list,
        //         //       'fluorescence': fluorescence_list,
        //         //       'depth_from': depth_from,
        //         //       'depth_to': depth_to,
        //         //       'table_from': table_from,
        //         //       'table_to': table_to,
        //         //       'discount_from': discount_from,
        //         //       'discount_to': discount_to,
        //         //       'page': page
        //         //     }),
        //         //     type: "POST",
        //         //
        //         //     "success": function(res, status, xhr) {
        //         //         var filename = "";
        //         //         var disposition = xhr.getResponseHeader('Content-Disposition');
        //         //         // check if filename is given
        //         //         if (disposition && disposition.indexOf('attachment') !== -1) {
        //         //             var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        //         //             var matches = filenameRegex.exec(disposition);
        //         //             if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        //         //         }
        //         //
        //         //         var blob = res;
        //         //         // blob = window.btoa(unescape(encodeURIComponent(blob)));
        //         //         // blob = btoa(blob.replace(/[\u00A0-\u2666]/g, function(c) {
        //         //         //     return '&#' + c.charCodeAt(0) + ';';
        //         //         // }));
        //         //         if (window.navigator.msSaveOrOpenBlob) {
        //         //             window.navigator.msSaveBlob(blob, filename);
        //         //         } else {
        //         //             var downloadLink = window.document.createElement('a');
        //         //             var contentTypeHeader = xhr.getResponseHeader("Content-Type");
        //         //             downloadLink.href = new Blob([s2ab(atob(blob))], {
        //         //                 type: contentTypeHeader
        //         //             });
        //         //             // downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
        //         //             downloadLink.download = filename;
        //         //             document.body.appendChild(downloadLink);
        //         //             downloadLink.click();
        //         //             document.body.removeChild(downloadLink);
        //         //         }
        //         //     }
        //         // });
        //
        //         // Option 3
        //         // var data = new FormData();
        //         // data.append('custom_order', stones_col_order_local);
        //         //
        //         // var keys=Object.keys(dt.ajax.params());
        //         // var dt = dt.ajax.params();
        //         // for (var key of keys) {
        //         //     data.append(key, dt[key]);
        //         // }
        //
        //         var data = Object.assign({}, dt.ajax.params(), {
        //               'custom_order': stones_col_order_local,
        //               'shape': shape_list,
        //               'id': id_list,
        //               'weight_from': weight_from,
        //               'weight_to': weight_to,
        //               'color': color_list,
        //               'clarity': clarity_list,
        //               'lab': lab_list,
        //               'location': location_list,
        //               'feed': feed_list,
        //               'polish': polish_list,
        //               'symmetry': symmetry_list,
        //               'cut': cut_list,
        //               'fluorescence': fluorescence_list,
        //               'depth_from': depth_from,
        //               'depth_to': depth_to,
        //               'table_from': table_from,
        //               'table_to': table_to,
        //               'discount_from': discount_from,
        //               'discount_to': discount_to,
        //               'page': page
        //             });
        //         var request = new XMLHttpRequest();
        //         request.open('POST', 'export_filtered_diamonds/', true);
        //         request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        //         request.responseType = 'blob';
        //
        //         request.onload = function (e) {
        //             if (this.status === 200) {
        //                 var filename = "";
        //                 var disposition = request.getResponseHeader('Content-Disposition');
        //                 // check if filename is given
        //                 if (disposition && disposition.indexOf('attachment') !== -1) {
        //                     var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        //                     var matches = filenameRegex.exec(disposition);
        //                     if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        //                 }
        //                 var blob = this.response;
        //                 if (window.navigator.msSaveOrOpenBlob) {
        //                     window.navigator.msSaveBlob(blob, filename);
        //                 }
        //                 else {
        //                     var downloadLink = window.document.createElement('a');
        //                     var contentTypeHeader = request.getResponseHeader("Content-Type");
        //                     downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
        //                     downloadLink.download = filename;
        //                     document.body.appendChild(downloadLink);
        //                     downloadLink.click();
        //                     document.body.removeChild(downloadLink);
        //                 }
        //             } else {
        //                 alert('Download failed.')
        //             }
        //         };
        //         request.send(JSON.stringify(data));
        //     }
        // },
        {
            className: "btn btn-primary",
            text: "Export Full Inventory",
            action: function (e, dt, node, config) {
                var data = {
                  'is_full': true,
                  'file_name': 'full inventory.xlsx'
                };
                var request = new XMLHttpRequest();
                request.open('POST', 'export_diamonds/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function (e) {
                    if (this.status === 200) {
                        var filename = "";
                        var disposition = request.getResponseHeader('Content-Disposition');
                        // check if filename is given
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }
                        var blob = this.response;
                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveBlob(blob, filename);
                        }
                        else {
                            var downloadLink = window.document.createElement('a');
                            var contentTypeHeader = request.getResponseHeader("Content-Type");
                            downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                            downloadLink.download = filename;
                            document.body.appendChild(downloadLink);
                            downloadLink.click();
                            document.body.removeChild(downloadLink);
                        }
                    } else {
                        alert('Download failed.')
                    }
                };
                request.send(JSON.stringify(data));
            }
        },
        {
            extend: 'selectAll',
            text: 'Select All Results',
            className: "btn btn-primary",
            action : function(e, dt, node, config) {
                $.ajax({
                    url: '/diamonds/get_stone_ids/',
                    data: {
                      'shape': shape_list,
                      'id': id_list,
                      'weight_from': weight_from,
                      'weight_to': weight_to,
                      'color': color_list,
                      'clarity': clarity_list,
                      'lab': lab_list,
                      'location': location_list,
                      'feed': feed_list,
                      'polish': polish_list,
                      'symmetry': symmetry_list,
                      'cut': cut_list,
                      'fluorescence': fluorescence_list,
                      'depth_from': depth_from,
                      'depth_to': depth_to,
                      'table_from': table_from,
                      'table_to': table_to,
                      'discount_from': discount_from,
                      'discount_to': discount_to
                    },

                    type: "POST",
                    dataType: 'json',
                    success: function (response) {
                        // console.log('success', response);
                        for (var id of response) {
                            index = $.inArray(id, selected_stones);
                            if (index === -1) {
                                selected_stones.push( id );
                            }
                        }
                    },
                    error: function(xhr, error){
                      alert('error on stone adding');
                    }
                });

                // e.preventDefault();
                // table.rows({ search: 'applied'}).deselect();
                dt.rows().select();
            }
        },
        {
            extend: 'selected',
            className: "btn btn-primary",
            // id: 'ExportButton',
            text: "Export Selected",
            action: function (e, dt, node, config) {
                // Option 1
                // $.ajax({
                //     "url": "/diamonds/export_diamonds/",
                //     data: {
                //       'selected_stones': selected_stones
                //     },
                //     type: "POST",
                //     "success": function(res, status, xhr) {
                //         var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
                //         var csvURL = window.URL.createObjectURL(csvData);
                //         var tempLink = document.createElement('a');
                //         tempLink.href = csvURL;
                //         tempLink.setAttribute('download', 'selected_stones.csv');
                //         tempLink.click();
                //     },
                //     "error": function(xhr, error){
                //         // $("#more-loader").hide();
                //         console.debug(xhr);
                //         console.debug(error);
                //     }
                //
                // });

                // Option 2

                var data = {
                  'selected_stones': selected_stones
                };
                var request = new XMLHttpRequest();
                request.open('POST', 'export_diamonds/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function (e) {
                    if (this.status === 200) {
                        var filename = "";
                        var disposition = request.getResponseHeader('Content-Disposition');
                        // check if filename is given
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }
                        var blob = this.response;
                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveBlob(blob, filename);
                        }
                        else {
                            var downloadLink = window.document.createElement('a');
                            var contentTypeHeader = request.getResponseHeader("Content-Type");
                            downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                            downloadLink.download = filename;
                            document.body.appendChild(downloadLink);
                            downloadLink.click();
                            document.body.removeChild(downloadLink);
                        }
                    } else {
                        alert('Download failed.')
                    }
                };
                request.send(JSON.stringify(data));

            }
        },
        {
            extend: 'selected',
            className: "btn btn-primary",
            text: "Print Selected",
            action: function (e, dt, node, config) {
                // Option
                $.ajax({
                    "url": "/diamonds/print_selected_stones/",
                    data: {
                      'selected_stones': selected_stones
                    },
                    type: "POST",
                    "success": function(res, status, xhr) {
                        printJS({
                            printable: res.stones_list,
                            properties: res.fieldnames,
                            type: 'json',
                            header: 'SDE - Selected Stones',
                            gridHeaderStyle: 'border: 1px solid grey; padding: 10px;',
                            gridStyle: 'text-align: center; border: 1px solid grey; padding: 10px;'
                        })
                    },
                    "error": function(xhr, error){
                        // $("#more-loader").hide();
                        console.debug(xhr);
                        console.debug(error);
                    }

                });

            }
        },
        {
            extend: 'selected',
            className: "btn btn-primary",
            // id: 'ExportButton',
            text: "Add to Email",
            action: function (e, dt, node, config)
            {
                var temp = [];
                if ('selected_stones' in localStorage) {
                    // console.log('array found');
                    temp = JSON.parse(localStorage.getItem('selected_stones'));
                } else {
                    // console.log('array not found', selected_users)
                }
                temp = temp.concat(selected_stones);
                localStorage.setItem('selected_stones', JSON.stringify(temp));
                window.location.href = "/accounts/compose_email/?tab=stone";
            }
        }
        // 'selected',
        // 'selectedSingle',
        // 'selectAll',
        // 'selectNone',
        // 'selectRows',
        // 'selectColumns',
        // 'selectCells'
    ],
    "select": {
        // style: 'multi'
        style: 'os'
    },
    ajax: function ( data, callback, settings ) {
        // console.log(data, data.order, settings);
        // data.order = [[ $.inArray(23, stones_col_order_local), "desc" ]];
        if (diamonds_ready === false) {
            return;
        }
        $.ajax({
        url: '/diamonds/dt_list_diamonds/',
        data: Object.assign({}, data, {
          'custom_order': stones_col_order_local,
          'shape': shape_list,
          'id': id_list,
          'weight_from': weight_from,
          'weight_to': weight_to,
          'color': color_list,
          'clarity': clarity_list,
          'lab': lab_list,
          'location': location_list,
          'feed': feed_list,
          'polish': polish_list,
          'symmetry': symmetry_list,
          'cut': cut_list,
          'fluorescence': fluorescence_list,
          'depth_from': depth_from,
          'depth_to': depth_to,
          'table_from': table_from,
          'table_to': table_to,
          'discount_from': discount_from,
          'discount_to': discount_to,
          'page': page
        }),
        type: "POST",
        dataType: 'json',
        success: function (data) {
            count = data.count;
            pages = data.pages;
            next = data.next;
            previous = data.previous;
            page = parseInt(data.page, 10);
            var out = [];
            // console.log(stones_col_order_local);
            $.each(data.data, function(key, value) {
                // console.log(value);
                // out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
                // this.api.row.add( [
                out.push( {
                "0": value[stones_col_order_local[0]],
                "1": value[stones_col_order_local[1]],
                "2": value[stones_col_order_local[2]],
                "3": value[stones_col_order_local[3]],
                "4": value[stones_col_order_local[4]],
                "5": value[stones_col_order_local[5]],

                "6": value[stones_col_order_local[6]],
                "7": value[stones_col_order_local[7]],
                "8": value[stones_col_order_local[8]],
                "9": value[stones_col_order_local[9]],
                "10": value[stones_col_order_local[10]],

                "11": value[stones_col_order_local[11]],
                "12": value[stones_col_order_local[12]],
                "13": value[stones_col_order_local[13]],
                "14": value[stones_col_order_local[14]],
                "15": value[stones_col_order_local[15]],

                "16": value[stones_col_order_local[16]],
                "17": value[stones_col_order_local[17]],
                "18": value[stones_col_order_local[18]],
                "19": value[stones_col_order_local[19]],
                "20": value[stones_col_order_local[20]],

                "21": value[stones_col_order_local[21]],
                "22": value[stones_col_order_local[22]],
                "23": value[stones_col_order_local[23]],
                "24": value[stones_col_order_local[24]],
                "25": value[stones_col_order_local[25]],

                "26": value[stones_col_order_local[26]],
                "27": value[stones_col_order_local[27]],
                "28": value[stones_col_order_local[28]],
                // (value[stones_col_order_local[27]]!==' '&&value[stones_col_order_local[27]]!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value[stones_col_order_local[27]]+'">View</a>':""),
                // (value[stones_col_order_local[28]]!==' '&&value[stones_col_order_local[28]]!==''?'<a class="btn btn-info " style="padding:1px 4px;" target="_blank" href="'+value[stones_col_order_local[28]]+'">View</a>':""),
                // $.format.date(value[stones_col_order_local[29]], longDateFormat),
                "29": value[stones_col_order_local[29]],
                "30": value[stones_col_order_local[30]],

                "31": value[stones_col_order_local[31]],
                "32": value[stones_col_order_local[32]],
                "33": value[stones_col_order_local[33]],
                "DT_RowId": value[0]
                } );

            });
            // setTimeout( function () {
            //     callback( {
            //         draw: data.draw,
            //         data: out,
            //         recordsTotal: 5000000,
            //         recordsFiltered: 5000000
            //     } );
            // }, 50 );

            // multi_table.draw();
            running = false;
            page += 1;
            $("#more-loader").hide();

            callback( {
                draw: data.draw,
                data: out,
                recordsTotal: data.recordsTotal,
                recordsFiltered: data.recordsFiltered
            } );

        },
        // error: function(xhr, error){
        //     $("#more-loader").hide();
        //     console.debug(xhr); console.debug(error);
        // },
        //   success:function(data){
        //     callback(data);
        //     // Do whatever you want.
        //   }
        });

        // var out = [];
        //
        // for ( var i=data.start, ien=data.start+data.length ; i<ien ; i++ ) {
        //     out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
        // }
        //
        // setTimeout( function () {
        //     callback( {
        //         draw: data.draw,
        //         data: out,
        //         recordsTotal: 5000000,
        //         recordsFiltered: 5000000
        //     } );
        // }, 50 );
    },
    scrollY: 650,
    scrollX: "100%",
    scrollCollapse: true,
    scrollInfinite: true,
    // "scrollY": $(window).scrollTop() >= $('#diamond-table-server').offset().top + $('#diamond-table-server').outerHeight() - window.innerHeight,
    scroller: {
        loadingIndicator: true
    },
    // "ajax": {
    //     url: '/diamonds/dt_list_diamonds/',
    //     data: {
    //       'shape': shape_list,
    //       'id': id_list,
    //       'weight_from': weight_from,
    //       'weight_to': weight_to,
    //       'color': color_list,
    //       'clarity': clarity_list,
    //       'lab': lab_list,
    //       'location': location_list,
    //       'feed': feed_list,
    //       'polish': polish_list,
    //       'symmetry': symmetry_list,
    //       'cut': cut_list,
    //       'fluorescence': fluorescence_list,
    //       'depth_from': depth_from,
    //       'depth_to': depth_to,
    //       'table_from': table_from,
    //       'table_to': table_to,
    //       'discount_from': discount_from,
    //       'discount_to': discount_to,
    //       'page': page
    //     },
    //     type: "POST",
    //     dataType: 'json',
    //     success: function (data) {
    //         count = data.count;
    //         pages = data.pages;
    //         next = data.next;
    //         previous = data.previous;
    //         page = parseInt(data.page, 10);
    //         var out = [];
    //         $.each(data.data, function(key, value) {
    //             // console.log(value);
    //             // out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
    //             out.push( [
    //             // this.api.row.add( [
    //             value[0],
    //             value[0],
    //             value[1],
    //             value[2],
    //             value[3],
    //             value[4],
    //             value[5],
    //
    //             value[6],
    //             value[7],
    //             value[8],
    //             value[9],
    //             value[10],
    //
    //             value[11],
    //             value[12],
    //             value[13],
    //             value[14],
    //             value[15],
    //
    //             value[16],
    //             value[17],
    //             value[18],
    //             value[19],
    //             value[20],
    //
    //             value[21],
    //             value[22],
    //             value[23],
    //             value[24],
    //             value[25],
    //
    //             value[26],
    //             (value[27]!==' '&&value[27]!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value[27]+'">View</a>':""),
    //             (value[28]!==' '&&value[28]!==''?'<a class="btn btn-info " style="padding:1px;" target="_blank" href="'+value[28]+'">View</a>':""),
    //             $.format.date(value[29], longDateFormat),
    //             value[30],
    //
    //             value[31],
    //             value[32],
    //             value[33]
    //             ] );
    //
    //         });
    //         // setTimeout( function () {
    //         //     callback( {
    //         //         draw: data.draw,
    //         //         data: out,
    //         //         recordsTotal: 5000000,
    //         //         recordsFiltered: 5000000
    //         //     } );
    //         // }, 50 );
    //
    //         // multi_table.draw();
    //         running = false;
    //         page += 1;
    //         $("#more-loader").hide();
    //
    //         this.draw = data.draw;
    //         this.data = out;
    //         this.recordsTotal = 5000000;
    //         this.recordsFiltered = 5000000;
    //
    //     },
    //     error: function(xhr, error){
    //         $("#more-loader").hide();
    //         console.debug(xhr); console.debug(error);
    //     },
    //     // callback: {
    //     //     draw: data.draw,
    //     //     data: out,
    //     //     recordsTotal: 5000000,
    //     //     recordsFiltered: 5000000
    //     // }
    // },

    // "columns": [
    //     { "data": "first_name" },
    //     { "data": "last_name" },
    //     { "data": "position" },
    //     { "data": "office" },
    //     { "data": "start_date" },
    //     { "data": "salary" }
    // ]
});


// function fix_col_reorder_issues() {
//     diamond_table_server.button(0).remove();
//     diamond_table_server.button().add(0,
//     {
//         extend: 'colvis',
//         text: 'Show/Hide Columns',
//         className: "btn btn-primary",
//         autoClose: false,
//         fade: 0,
//         columns: ':not(.noColVis)',
//         colVis: { showAll: "Show all" }
//     });
//
// }


diamond_table_server.on('column-reorder', function (e, settings, details) {
    // swap elements
    // [stones_col_order_local[details.from], stones_col_order_local[details.to]] = [stones_col_order_local[details.to], stones_col_order_local[details.from]];
    // console.log('', stones_col_order_local);
    fix_col_reorder_issues(diamond_table_server);
});



// function dt_get_stones() {
//   running = true;
//   // console.log('old', diamond_table_server.ajax.json().old_data);
//   $("#more-loader").show();
//   $.ajax({
//     url: '/diamonds/dt_list_diamonds/',
//     data: diamond_table_server.ajax.json().old_data,
//     // data: {
//     //   'shape': shape_list,
//     //   'id': id_list,
//     //   'weight_from': weight_from,
//     //   'weight_to': weight_to,
//     //   'color': color_list,
//     //   'clarity': clarity_list,
//     //   'lab': lab_list,
//     //   'location': location_list,
//     //   'feed': feed_list,
//     //   'polish': polish_list,
//     //   'symmetry': symmetry_list,
//     //   'cut': cut_list,
//     //   'fluorescence': fluorescence_list,
//     //   'depth_from': depth_from,
//     //   'depth_to': depth_to,
//     //   'table_from': table_from,
//     //   'table_to': table_to,
//     //   'discount_from': discount_from,
//     //   'discount_to': discount_to,
//     //   'page': page
//     // },
//     type: "POST",
//     dataType: 'json',
//     success: function (data) {
//         count = data.count;
//         pages = data.pages;
//         next = data.next;
//         previous = data.previous;
//         page = parseInt(data.page, 10);
//         $.each(data.data, function(key, value) {
//         // console.log('key', key);
//         // console.log('value', value);
//
//             diamond_table_server.row.add( [
//                 value[0],
//                 value[1],
//                 value[2],
//                 value[3],
//                 value[4],
//                 value[5],
//
//                 value[6],
//                 value[7],
//                 value[8],
//                 value[9],
//                 value[10],
//
//                 value[11],
//                 value[12],
//                 value[13],
//                 value[14],
//                 value[15],
//
//                 value[16],
//                 value[17],
//                 value[18],
//                 value[19],
//                 value[20],
//
//                 value[21],
//                 value[22],
//                 value[23],
//                 value[24],
//                 value[25],
//
//                 value[26],
//                 (value[27]!==' '&&value[27]!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value[27]+'">View</a>':""),
//                 (value[28]!==' '&&value[28]!==''?'<a class="btn btn-info " style="padding:1px;" target="_blank" href="'+value[28]+'">View</a>':""),
//                 $.format.date(value[29], longDateFormat),
//                 value[30],
//
//                 value[31],
//                 value[32],
//                 value[33]
//             ] ).draw();
//
//
//             // $('#multi-select-diamonds').append(
//             //     '<tr><td>'+value.fields.lot_id+'</td><td>'+value.fields.lot_id_str+'</td><td>'+value.fields.shape+
//             //     '</td><td>'+value.fields.weight+'</td><td>'+value.fields.color+'</td><td>'+value.fields.clarity+
//             //     '</td><td>'+value.fields.lab+'</td><td>'+value.fields.polish+'</td><td>'+value.fields.symmetry+
//             //     '</td><td>'+value.fields.fluorescence+'</td><td>'+value.fields.cert_no+'</td><td>'+value.fields.m1+
//             //     '</td><td>'+value.fields.m2+'</td><td>'+value.fields.m3+'</td><td>'+(value.fields.depth?value.fields.depth:"")+
//             //     '</td><td>'+value.fields.table_percent+'</td><td>'+(value.fields.crown_angle?value.fields.crown_angle:"")+
//             //     '</td><td>'+(value.fields.fluorescence_color?value.fields.fluorescence_color:"")+'</td><td>'+(value.fields.girdle?value.fields.girdle:"")+
//             //     '</td><td>'+(value.fields.culet?value.fields.culet:"")+'</td><td>'+value.fields.quantity+'</td><td>'+(value.fields.list_price?value.fields.list_price:"")+
//             //     '</td><td>'+(value.fields.rap_discount?value.fields.rap_discount:"")+'</td><td>'+value.fields.sale_price+'</td><td>'+value.fields.status+
//             //     '</td><td>'+value.fields.cut+'</td><td>'+(value.fields.certificate_filename!==' '&&value.fields.certificate_filename!==''?'<a class="btn btn-info " style="padding: 5px;" target="_blank" href="'+value.fields.certificate_filename+'">View</a>':"")+
//             //     '</td><td>'+(value.fields.diamond_filename!==' '&&value.fields.diamond_filename!==''?'<a class="btn btn-info " style="padding:5px;" target="_blank" href="'+value.fields.diamond_filename+'">View</a>':"")+
//             //     '</td><td>'+$.format.date(value.fields.date_created, shortDateFormat)+'</td><td>'+value.fields.feed+'</td><td>'+value.fields.country_location+
//             //     '</td><td>'+value.fields.fancy_intensity+'</td><td>'+value.fields.fancy_overtone+'</td></tr>');
//         });
//         running = false;
//         page += 1;
//         $("#more-loader").hide();
//         $('#stone-count').html('Total ' + count + ' Recor' + (parseInt(count) !== 1 ? 'ds' : 'd') + ' Found');
//     },
//     error: function(xhr, error){
//         $("#more-loader").hide();
//         console.debug(xhr); console.debug(error);
//     }
//   });
// }



function get_set_interactive_count() {
  $("#stone-count-loader").show();
  // $('#interactive-stone-count').html('');
  $.ajax({
    url: '/diamonds/interactive_count/',
    data: {
      'shape': shape_list,
      'id': id_list,
      'weight_from': weight_from,
      'weight_to': weight_to,
      'color': color_list,
      'clarity': clarity_list,
      'lab': lab_list,
      'location': location_list,
      'feed': feed_list,
      'polish': polish_list,
      'symmetry': symmetry_list,
      'cut': cut_list,
      'fluorescence': fluorescence_list,
      'depth_from': depth_from,
      'depth_to': depth_to,
      'table_from': table_from,
      'table_to': table_to,
      'discount_from': discount_from,
      'discount_to': discount_to
    },
    type: "POST",
    dataType: 'json',
    success: function (data) {
        interactive_count = data.interactive_count;
        $("#stone-count-loader").hide();
        $('#interactive-stone-count').html('Total ' + data.interactive_count + ' ro' +
            (parseInt(data.interactive_count) !== 1 ? 'ws' : 'w') + ' match your search criteria');
    },
    error: function(xhr, error){
        $("#stone-count-loader").hide();
        $('#interactive-stone-count').html('Failed! Interactive Count Not Updated.');
    }
  });
}


// on reset
$("#reset").unbind().click(function () {
    $("#stone-count-loader").hide();
    $('#interactive-stone-count').html('Success! All filters cleared');
    reset_diamond_filters();

    save_filters_state();
    // Option 1 -> count problems
    // diamond_table_server.clear().draw(false);
    // diamond_table_server.ajax.reload();
    // Option 2
    // window.location.reload(false);
    window.location.href = "/diamonds/";
});


function reset_diamond_filters() {
    // shape case
    $('.cut_type').prop('checked', false);

    $('.shp').prop('checked', false);
    shape_list = [];

    // weight case
    $('#weight_from').val(null);
    $('#weight_to').val(null);
    weight_from = 0;
    weight_to = 200;

    // color case
    $('input.type1').prop('checked', false);
    $('input.type2').prop('checked', false);
    // $('input.type1').prop('checked', false).attr("disabled", false);
    // $('input.type2').prop('checked', false).attr("disabled", true);
    // $("div.type2").removeClass('checkbox-primary').addClass('checkbox-default');
    // $("div.type1").removeClass('checkbox-default').addClass('checkbox-primary');
    color_list = [];

    // clarity case
    $('input.clarity').prop('checked', false);
    clarity_list = [];

    $('input.lab').prop('checked', false);
    lab_list = [];

    $('input.location').prop('checked', false);
    location_list = [];

    $('input.stonegroup').prop('checked', false);
    feed_list = [];

    $('input.polish').prop('checked', false);
    polish_list = [];

    $('input.symmetry').prop('checked', false);
    symmetry_list = [];

    $('input.cut').prop('checked', false);
    cut_list = [];

    $('input.fluorescence').prop('checked', false);
    fluorescence_list = [];

    $('#depth_from').val(null);
    $('#depth_to').val(null);
    depth_from = 0;
    depth_to = 200;

    $('#table_from').val(null);
    $('#table_to').val(null);
    table_from = 0;
    table_to = 200;

    $('#discount_from').val(null);
    $('#discount_to').val(null);
    discount_from = -200;
    discount_to = 200;

    $('#lotid').val(null);
    id_list = [];

}



var searches = [];
var selected_search;

$("#manage").unbind().click(function () {
    // console.log('search clicked');
    $.ajax({
    url: '/accounts/list_search/',
    // processData : false,
    // contentType: false,
    // cache : false,
    dataType    : 'json',
    type: "GET",

    success: function (data) {
        $('#searches').empty();
        $.each(data, function(key, value){
            $('#searches').append(
            '<option value="' + value.pk + '">' + value.fields.name + '</option>');
        });
        searches = data;
        if (data.length >= 1) {
            selected_search = data[0];
        }
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong, please refresh the page' + '</div>');
        // $("#new-user-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
});


$("#save-search").unbind().click(function () {
    // console.log('save search called');
    // diamond_table_server.ajax.reload();
    var data = get_search_data();

    $.ajax({
        url: '/accounts/save_search/',
        data: {
          'name': $('#search_name').val(),
          'state': JSON.stringify(data)
        },
        type: "POST",
        // async: false,
        dataType: 'json',
        success: function (data) {
            $('#searches').empty();
            $.each(data, function(key, value){
                $('#searches').append(
                '<option value="' + value.pk + '">' + value.fields.name + '</option>');
            });
            searches = data;
            if (data.length >= 1) {
                selected_search = data[0];
            }

            $('#search_name').val('');
            // console.log('success');
        },
        error: function(xhr, error){
          console.log('error on state update');
        }
    });
});

$("#load-search").unbind().click(function () {
    // console.log('load search called');
    // console.log(searches);
    // console.log($('#searches').val());
    reset_diamond_filters();
    var state = JSON.parse(selected_search.fields.state);
    update_diamonds_filters(state, true);
    $('#Modal-search').modal('hide');

    get_set_interactive_count();
    diamond_table_server.ajax.reload();

    save_filters_state();
});

$("#searches").change(function () {
    // var quick_weight = this.value;
    // console.log(this.value, searches);
    selected_search = searches.find(x => x.pk == this.value);
    // console.log(selected_search);
});



$("#delete-search").unbind().click(function () {
    var search_id = $('#searches').val();
    $.ajax({
        url: '/accounts/delete_search/',
        data: {
          'search_id': search_id
        },
        type: "POST",
        dataType: 'json',
        success: function (response) {
            // $('#search_name').val('');
            // console.log('success', response);
            $("#searches option[value=" + search_id + "]").remove();
            // searches.;
        },
        error: function(xhr, error){
          console.log('error on delete');
        }
    });
});


$(document).ready(function(){

    // disables all forms auto submit
    // $('form').submit(false);



    // $('#diamond-table-server tbody').on('click', 'tr', function () {
    //     // $(this).toggleClass('selected');
    //     // console.log(this);
    //     var id = this.id;
    //     var index = $.inArray(id, selected_stones);
    //
    //     if (!$(this).hasClass("selected")) {
    //         if ( index === -1 ) {
    //             selected_stones.push( id );
    //         } else {
    //         }
    //     } else {
    //         // while($.inArray(id.toString(), temp)!=-1){
    //         //     var index = $.inArray(id.toString(), temp);
    //         //     temp.splice( index, 1 );
    //         // }
    //         if ( index === -1 ) {
    //         } else {
    //             selected_stones.splice( index, 1 );
    //         }
    //     }
    //     console.log(selected_stones)
    // } );

    // handle_load_case();
    load_filters_state()
});


function load_filters_state() {
    var url = window.location.search;
    var arr = url.split("=");
    var diamonds_state = {};
    // console.log(window.location.search, arr);
    if (arr[0]==='?resume') {
        diamonds_state = get_state_to_resume(arr[1]);
        // console.log('load search', diamonds_state);
        update_diamonds_filters(diamonds_state, true);
        get_set_interactive_count();
    } else {
        if ('diamonds_state' in localStorage) {
            // console.log('array found');
            diamonds_state = JSON.parse(localStorage.getItem('diamonds_state'));
            update_diamonds_filters(diamonds_state, false);
            get_set_interactive_count();
        }
    }
    diamonds_ready = true;
    diamond_table_server.ajax.reload();
}

function save_filters_state() {
    var diamonds_state = get_search_data();
    localStorage.setItem( 'diamonds_state', JSON.stringify(diamonds_state));
}


function get_state_to_resume(id) {
    var res = {};
    $.ajax({
        url: '/diamonds/get_activity_log/',
        data: {
          'id': id
        },
        type: "POST",
        async: false,
        dataType: 'json',
        success: function (data) {
            // console.log(data.state);
            res = JSON.parse(data.state);
        },
        error: function(xhr, error){
          console.log('error on state get');
        }
    });
    return res;

}


function handle_load_case() {
    var url = window.location.search;
    var arr = url.split("=");
    // console.log(window.location.search, arr);
    if (arr[0]==='?load' && arr[1]==='true') {
        // console.log('load search');

        // user_filters_data();
        window.localStorage.removeItem('keyName');
    }

}



function populate_email_stone_table() {
    // var api = email_stone_table.api();
    api.clear();
    var temp = JSON.parse(localStorage.getItem('selected_stones'));
    $.ajax({
        "url": "/diamonds/get_selected_stones/",
        data: {
          'selected_stones': temp
        },
        type: "POST",
        async: true,
        "success": function(res, status, xhr) {
            var count=0;
            var total_p=0;
            var total_price_original=0;
            var total_w=0;
            var total_disc=0;
            var avg = 0;
            var avg_disc = 0;
            api.column(16).visible(true);
            api.column(17).visible(true);
            api.column(18).visible(true);
            api.column(19).visible(true);
            api.column(20).visible(true);
            api.column(21).visible(true);
            api.column(22).visible(true);

            $.each(res.stones, function(key, value){
                var disc = value.fields.rap_discount;
                if (price_type === 'markup') {
                    disc = disc + Number($('#markup-input').val());
                }
                if (price_type === 'global') {
                    disc = Number($('#global-input').val());
                }
                if (price_type === 'remove') {
                    disc = 0;
                    total_price  = 0;
                    total_p  = 0;
                    total_w  = 0;
                    api.column(16).visible(false);
                    api.column(17).visible(false);
                    api.column(18).visible(false);
                    api.column(19).visible(false);
                    api.column(20).visible(false);
                    api.column(21).visible(false);
                    api.column(22).visible(false);
                } else {
                    // Counters
                    disc = Math.round(disc * 100) / 100;
                }
                if (price_type !== 'remove') {
                    total_price_original += (value.fields.weight * value.fields.list_price);
                    var total_price = (value.fields.weight * value.fields.list_price * (1 + (disc * 0.01)));
                    total_price = Math.round(total_price * 100) / 100;
                    count = count + 1;
                    total_p = total_p + total_price;
                    total_w = total_w + Number(value.fields.weight);
                    total_disc += disc;
                }
                api.row.add( [
                    value.pk,
                    value.fields.lot_id,
                    value.fields.shape,
                    value.fields.weight,
                    value.fields.color,
                    value.fields.clarity,
                    value.fields.lab,
                    (value.fields.certificate_filename!==' '&&value.fields.certificate_filename!==''?'<a class="" style="" target="_blank" href="'+value.fields.certificate_filename+'">' + value.fields.cert_no + '</a>':""),
                    (value.fields.diamond_filename!==' '&&value.fields.diamond_filename!==''?'<a class="" style="" target="_blank" href="'+value.fields.diamond_filename+'">' + '<i class="feather icon-jfi-file-image"></i>' + '</a>':""),
                    value.fields.cut,
                    value.fields.polish,
                    value.fields.symmetry,
                    value.fields.fluorescence,
                    value.fields.depth,
                    value.fields.table_percent,
                    value.fields.m1 + 'x' + value.fields.m2 + 'x' + value.fields.m3,
                    value.fields.list_price,
                    disc,
                    value.fields.sale_price,
                    // "$" + total_price.toFixed(2),
                    currencyFormat(total_price),
                    "<input type='number' style='width: 80%;' class='form-control p-1 m-r-5 counter-discount' onchange='myFunction(this.value, " + value.pk + ", " + value.fields.list_price + ", " + value.fields.weight + ", " + count + ")' value='"  + disc + "'>" +
                    "<span hidden id='counter-discount-" + value.pk + "'>" + disc + "</span>",
                    "<span id='counter-sale-price-" + value.pk + "'>" + value.fields.sale_price + "</span>",
                    // "<span id='counter-total-price-" + value.pk + "'>$" + total_price.toFixed(2) + "</span>"
                    "<span id='counter-total-price-" + value.pk + "'>" + currencyFormat(total_price) + "</span>"
//                 $.format.date(value.fields.date_created, longDateFormat),
                ] ).draw();

            });
            total_p = Math.round(total_p * 100) / 100;
            total_w = Math.round(total_w * 100) / 100;
            if (total_w !== 0) {
                avg = total_p / total_w;
                avg = Math.round(avg * 100) / 100;
            }
            // if (count !== 0) {
            //     avg_disc = total_disc / count;
            //     avg_disc = Math.round(avg_disc * 100) / 100;
            // }
            var temp_disc =0;
            if (total_price_original !== 0) {
                temp_disc = (((total_p/total_price_original)-1)*100);
            }
            $("#count").html(count);
            $("#counter-count").html(count);
            // $("#total-disc").html('<b>-$</b>' + (total_price_original - total_p).toFixed(1));
            $("#total-disc").html('' + temp_disc.toFixed(1) + '%');
            // $("#avg-disc").html('' + avg_disc.toFixed(1) + '<b>%</b>');
            $("#counter-total-disc").html('' + temp_disc.toFixed(1) + '%');
            // $("#counter-avg-disc").html('' + avg_disc.toFixed(1) + '<b>%</b>');
            // TOTAL_PRICE = '<b>$</b>' + total_p.toFixed(2);
            TOTAL_PRICE = currencyFormat(total_p);
            $("#total-price").html(TOTAL_PRICE);
            $("#counter-total-price").html(TOTAL_PRICE);
            $("#total-weight").html(total_w.toFixed(2) + 'Cts');
            TOTAL_WEIGHT = total_w.toFixed(2);
            $("#counter-total-weight").html( TOTAL_WEIGHT + 'Cts');

            $('#avg').html(currencyFormat(avg));
            $('#counter-avg').html(currencyFormat(avg));
        },
        "error": function(xhr, error){
            // $("#more-loader").hide();
            console.debug(xhr);
            console.debug(error);
        },
        "complete": function (xhr, error) {
            // console.log('stone fetch complete');
            // update_email_editor_content();
            init_ckeditor();

        }

    });
}


function update_email_editor_content() {
    // console.log('content update func');
    // $('#editor-stones-table').html($('#email-stone-table').clone());
    // $('#editor-stones-summary').html($('#stones-summary').clone());

    if (CKEDITOR.instances.email_editor) {
        set_ckeditor_data();
        return;
    }
    init_ckeditor();
}



function set_ckeditor_data() {
    var ck_content = '';
    var editor_header = $('#editor-header').html();
    if (editor_header !== "") {
        ck_content += editor_header + '<p></p><p></p>';
    }


    // var stone_table = $('#editor-stones-table').html();
    // if (stone_table !== "") {
    //     ck_content += stone_table + '<p></p>';
    // }
    // var stone_summary = $('#editor-stones-summary').html();
    // if (stone_summary !== "") {
    //     ck_content += stone_summary + '<p></p><p></p><p></p>';
    // }


    var old_signature = $('#old-signature').text();
    if (old_signature !== "") {
        ck_content += old_signature;
    }

    CKEDITOR.instances.email_editor.setData(ck_content);
    var data = CKEDITOR.instances.email_editor.getData();
    $("#email_body").val(data);
    // editor.setData(ck_content);
    // $("#email_body").val(editor.getData());

}


function init_ckeditor() {
    // CKEDITOR 5 START
    // DecoupledEditor.create( document.querySelector( '#editor' ), {
    //     ckfinder: {
    //         // Upload the images to the server using the CKFinder QuickUpload command.
    //         // uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
    //         uploadUrl: '/accounts/upload_image/?private=false'
    //     }
    // }).then( editor => {
    //     // console.log( 'Editor initialized');
    //     ckeditor = editor;
    //     // console.log(editor.plugins);
    //     // editor.plugins.get( 'CKFinder' );
    //     const toolbarContainer = document.querySelector( '#toolbar-container' );
    //     toolbarContainer.appendChild( editor.ui.view.toolbar.element );
    //
    //     set_ckeditor_data(ckeditor);
    //
    //     editor.model.document.on('change:data', () => {
    //         // console.log('The data has changed!', );
    //         $("#email_body").val(editor.getData());
    //     });
    //
    // }).catch( error => {
    //     console.error( error );
    // });
    // CKEDITOR 5 END

    // CKEDITOR 4 START
    CKEDITOR.replace('email_editor', {

        // events
        on: {
            instanceReady: function( evt ) {
                // console.log('CK 4 instance ready');
                set_ckeditor_data();
            },

            change: function (evt) {
               $("#email_body").val(evt.editor.getData());
            }
        },

        // Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
        // The standard preset from CDN which we used as a base provides more features than we need.
        // Also by default it comes with a 2-line toolbar. Here we put all buttons in a single row.
        toolbar: [{
            name: 'clipboard',
            items: ['Undo', 'Redo']
        }, {
            name: 'styles',
            items: ['Styles', 'Format', 'Font', 'FontSize']
        }, {
            name: 'basicstyles',
            items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
        }, {
            name: 'paragraph',
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        }, {
            name: 'links',
            items: ['Link', 'Unlink']
        }, {
            name: 'colors',
            items: ['TextColor', 'BGColor']
        }, {
            name: 'align',
            items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        // }, {
        //     name: 'color',
        //     items: ['TextColor2', 'BGColor2']
        }, {
            name: 'insert',
            items: ['Image', 'StonesNSummary', 'Table']    //, 'EmbedSemantic'
        }, {
            name: 'tools',
            items: ['Maximize']
        // }, {
        //     name: 'editing',
        //     items: ['Scayt']
        }
        ],

        // colorButton_enableAutomatic = false;
        // Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
        // One HTTP request less will result in a faster startup time.
        // For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
        customConfig: '',
        extraAllowedContent: '*(email-table-wrapper,email-table-1,email-table-2)',
        allowedContent: true,

        // Enabling extra plugins, available in the standard-all preset: http://ckeditor.com/presets-all
        extraPlugins: 'font,image2,uploadimage,uploadfile,stones_n_summary,colorbutton,justify',
        // autoembed,embedsemantic,enhancedcolorbutton,panelbutton
        imageUploadUrl: '/accounts/upload_image/?private=false',
        uploadUrl: '/accounts/upload_image/?private=false',
        /*********************** File management support ***********************/
        // In order to turn on support for file uploads, CKEditor has to be configured to use some server side
        // solution with file upload/management capabilities, like for example CKFinder.
        // For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration

        // Uncomment and correct these lines after you setup your local CKFinder instance.
        filebrowserBrowseUrl: '/accounts/gallery/',
        filebrowserUploadUrl: '/accounts/upload_image/?private=false',
        /*********************** File management support ***********************/

        // Remove the default image plugin because image2, which offers captions for images, was enabled above.
        removePlugins: 'image',        //,colorbutton

        // Make the editing area bigger than default.
        height: 600,


        // This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
        bodyClass: 'article-editor',

        // Reduce the list of block elements listed in the Format dropdown to the most commonly used.
        format_tags: 'p;h1;h2;h3;pre',

        // Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
        // removeDialogTabs: 'image:advanced;link:advanced',

        // Define the list of styles which should be available in the Styles dropdown list.
        // If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
        // (and on your website so that it rendered in the same way).
        // Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
        // that file, which means one HTTP request less (and a faster startup).
        // For more information see http://docs.ckeditor.com/#!/guide/dev_styles
        stylesSet: [
            /* Inline Styles */
            {
                name: 'Marker',
                element: 'span',
                attributes: {
                    'class': 'marker'
                }
            }, {
                name: 'Cited Work',
                element: 'cite'
            }, {
                name: 'Inline Quotation',
                element: 'q'
            },

            /* Object Styles */
            {
                name: 'Special Container',
                element: 'div',
                styles: {
                    padding: '5px 10px',
                    background: '#eee',
                    border: '1px solid #ccc'
                }
            }, {
                name: 'Compact table',
                element: 'table',
                attributes: {
                    cellpadding: '5',
                    cellspacing: '0',
                    border: '1',
                    bordercolor: '#ccc'
                },
                styles: {
                    'border-collapse': 'collapse'
                }
            }, {
                name: 'Borderless Table',
                element: 'table',
                styles: {
                    'border-style': 'hidden',
                    'background-color': '#E6E6FA'
                }
            }, {
                name: 'Square Bulleted List',
                element: 'ul',
                styles: {
                    'list-style-type': 'square'
                }
            },
            // Media embed
            {
                name: '240p',
                type: 'widget',
                widget: 'embedSemantic',
                attributes: {
                    'class': 'embed-240p'
                }
            }, {
                name: '360p',
                type: 'widget',
                widget: 'embedSemantic',
                attributes: {
                    'class': 'embed-360p'
                }
            }, {
                name: '480p',
                type: 'widget',
                widget: 'embedSemantic',
                attributes: {
                    'class': 'embed-480p'
                }
            }, {
                name: '720p',
                type: 'widget',
                widget: 'embedSemantic',
                attributes: {
                    'class': 'embed-720p'
                }
            }, {
                name: '1080p',
                type: 'widget',
                widget: 'embedSemantic',
                attributes: {
                    'class': 'embed-1080p'
                }
            }
        ]
    });
    // CKEDITOR 4 END
}

var api;
var email_stone_table = $('#email-stone-table').DataTable({
    "initComplete": function(settings, json) {
        // console.log('table initialized');
        api = this.api();
        populate_email_stone_table();
    },

    dom: 'Bfrtip',
    paging: false,
    searching: false,
    ordering: false,
    colReorder: false,
    info: false,
    destroy: false,
    stateSave: false,
    // columns: [
    //     { "orderable": false , targets: 0}
    // ],
    columnDefs: [{
        orderable: false,
        className: 'noVis',
        targets: 0,
        visible: false
    }],
    buttons: [
        // {
        //     extend: 'selectNone',
        //     text: 'Reset Selection',
        //     className: "btn btn-warning"
        // },
        {
            className: "btn btn-primary",
            text: "Add Stones",
            action: function (e, dt, node, config)
            {
                window.location.href = "/diamonds/";
            }
        },
        {
            className: "btn btn-info",
            text: "Add Stones (by ID)",
            action: function (e, dt, node, config)
            {
                $("#stone-id-Modal").modal('show');
            }
        },
        {
            extend: 'selected',
            text: 'Delete',
            className: "btn btn-warning",
            action: function ( e, dt, node, config ) {
                var temp = JSON.parse(localStorage.getItem('selected_stones'));
                $.each(dt.rows({ selected: true }).data(), function() {
                    var id = this[0];
                    // var index = $.inArray(id.toString(), temp);
                    while($.inArray(id.toString(), temp)!=-1){
                        var index = $.inArray(id.toString(), temp);
                        temp.splice( index, 1 );
                    }
                    // if ( index === -1 ) {
                    //     // selected_users.push( id );
                    // } else {
                    //     temp.splice( index, 1 );
                    // }
                });
                localStorage.setItem('selected_stones', JSON.stringify(temp));
                // Option 1
                // dt.rows({ selected: true }).remove().draw();
                // populate_email_stone_table();

                // Option 2
                window.location.href = "/accounts/compose_email/?tab=stone";
            }
        },
        {
            className: "btn btn-danger",
            text: "Delete All",
            action: function ( e, dt, node, config ) {
                localStorage.setItem('selected_stones', JSON.stringify([]));
                // Option 1
                // dt.rows().remove().draw();
                // populate_email_stone_table();

                // Option 2
                window.location.href = "/accounts/compose_email/?tab=stone";
            }
        },
        {
            text: 'Download to Excel',
            className: "btn btn-primary",
            action: function (e, dt, node, config) {
                var temp = JSON.parse(localStorage.getItem('selected_stones'));
                var data = {
                  'selected_stones': temp,
                  'file_name': 'email_stones_list.xlsx'
                };
                var request = new XMLHttpRequest();
                request.open('POST', '/diamonds/export_diamonds/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function (e) {
                    if (this.status === 200) {
                        var filename = "";
                        var disposition = request.getResponseHeader('Content-Disposition');
                        // check if filename is given
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }
                        var blob = this.response;
                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveBlob(blob, filename);
                        }
                        else {
                            var downloadLink = window.document.createElement('a');
                            var contentTypeHeader = request.getResponseHeader("Content-Type");
                            downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                            downloadLink.download = filename;
                            document.body.appendChild(downloadLink);
                            downloadLink.click();
                            document.body.removeChild(downloadLink);
                        }
                    } else {
                        alert('Download failed.')
                    }
                };
                request.send(JSON.stringify(data));

            }
        }
        // {
        //     extend: 'excel',
        //     //Name the CSV
        //     filename: 'email_stones',
        //     text: 'Download to Excel',
        //     className: "btn btn-primary",
        //     exportOptions: {
        //         columns: ':visible'
        //     }
        // }
    ],
    select: {
        // style: 'multi'
        style: 'os'
    },
    // scrollY: 650,

});


// function s2ab(s) {
//   var buf = new ArrayBuffer(s.length);
//   var view = new Uint8Array(buf);
//   for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
//   return buf;
// }


// row select case
diamond_table_server.on('select', function (e, dt, type, indexes) {
    if (type === 'row') {
        for (var index of indexes) {
            // this.api().row( index ).select();
            var id = dt.row(index).data().DT_RowId.toString();
            index = $.inArray(id, selected_stones);
            // console.log(id, index);

            if (index === -1) {
                selected_stones.push( id );
            }
        }
        // console.log(selected_stones)
    }
} );

diamond_table_server.on('init', function (e, settings, json) {
    $('th[aria-label="%RAP: activate to sort column ascending"]').click()
});

// row deselect case
diamond_table_server.on('deselect', function (e, dt, type, indexes) {
    if (type === 'row') {
        for (var index of indexes) {
            // this.api().row( index ).select();
            var id = dt.row(index).data().DT_RowId.toString();
            index = $.inArray(id, selected_stones);
            // console.log(id, index);

            if (index !== -1) {
                selected_stones.splice( index, 1 );
            }
        }
        // console.log(selected_stones)
    }
} );

function update_email_stones() {
    var ids = $('#email_lot_id').val();
    $.ajax({
        url: '/diamonds/get_stone_ids/',
        data: {
          'lot_ids': ids
        },
        type: "POST",
        dataType: 'json',
        success: function (response) {
            // console.log('success', response);
            $('#email_lot_id').val("");

            var temp = [];
            if ('selected_stones' in localStorage) {
                // console.log('array found');
                temp = JSON.parse(localStorage.getItem('selected_stones'));
            }
            temp = temp.concat(response);
            localStorage.setItem('selected_stones', JSON.stringify(temp));
            // Option 1
            // populate_email_stone_table();

            // Option 2
            window.location.href = "/accounts/compose_email/?tab=stone";

        },
        error: function(xhr, error){
          alert('error on stone adding');
        }
    });

}


function get_search_data() {
    get_id();
    return {
      'shape': shape_list,
      'id': id_list,
      'weight_from': weight_from,
      'weight_to': weight_to,
      'color': color_list,
      'clarity': clarity_list,
      'lab': lab_list,
      'location': location_list,
      'feed': feed_list,
      'polish': polish_list,
      'symmetry': symmetry_list,
      'cut': cut_list,
      'fluorescence': fluorescence_list,
      'depth_from': depth_from,
      'depth_to': depth_to,
      'table_from': table_from,
      'table_to': table_to,
      'discount_from': discount_from,
      'discount_to': discount_to
    };
}


function create_activity_log() {
    var data = get_search_data();
    var search_source = 'Customer Search';
    $.ajax({
        url: 'create_activity_log/',
        data: {
          'search_source': search_source,
          'results_no': interactive_count,
          'state': JSON.stringify(data)
        },
        type: "POST",
        // async: false,
        dataType: 'json',
        success: function (data) {
          // console.log('success on activity log');
        },
        error: function(xhr, error){
          console.log('error on activity log');
        }
    });


}


function update_diamonds_filters(state, pure) {

    if (pure) {
        // SKIP ROUND
        shape_list = [];
        $('#Round').prop('checked', false);
    }

    // shape case
    $.each(state.shape, function(key, value) {
        if (value === 'Sq Emerald') value = 'Asscher';
        $('#' + value).prop('checked', true);
        var index = shape_list.indexOf(value);
        if (index === -1) shape_list.push(value);
    });

    // color case
    $.each(state.color, function(key, value) {
        if (value === 'Yellow' || value === 'Pink' || value === 'Others') {
            $('#' + value).prop('checked', true);
            $("input[value='type2']").prop('checked', true);
            $("input.type2").prop('disabled', false);
        } else {
            // if (value === 'Asscher') value = 'Sq Emerald';
            $('#color_' + value).prop('checked', true);
            $("input[value='type1']").prop('checked', true);
            $("input.type1").prop('disabled', false);
        }
        color_list.push(value);
    });

    // clarity case
    $.each(state.clarity, function(key, value) {
        $('#clarity_' + value).prop('checked', true);
        clarity_list.push(value);
    });

    // lab case
    $.each(state.lab, function(key, value) {
        $('#lab_' + value).prop('checked', true);
        lab_list.push(value);
    });

    // location case
    $.each(state.location, function(key, value) {
        if (value === 'Hong Kong') value = 'Hong_Kong';
        $('#location_' + value).prop('checked', true);
        location_list.push(value);
    });


    // stonegroup case
    $.each(state.feed, function(key, value) {
        if (value === 'LSDCO IL') value = 'LSDCO_IL';
        // console.log(value);
        $('#stonegroup_' + value).prop('checked', true);
        feed_list.push(value);
    });

    // polish case
    $.each(state.polish, function(key, value) {
        if (value === 'VERY GOOD') value = 'VERY_GOOD';
        $('#polish_' + value).prop('checked', true);
        polish_list.push(value);
    });

    // symmetry case
    $.each(state.symmetry, function(key, value) {
        if (value === 'VERY GOOD') value = 'VERY_GOOD';
        $('#symmetry_' + value).prop('checked', true);
        symmetry_list.push(value);
    });


    // fluorescence case
    $.each(state.fluorescence, function(key, value) {
        if (value === 'VERY GOOD') value = 'VERY_GOOD';
        $('#fluorescence_' + value).prop('checked', true);
        fluorescence_list.push(value);
    });

    // cut case
    $.each(state.cut, function(key, value) {
        // console.log(value);
        if (value === 'VERY GOOD') value = 'VERY_GOOD';
        if (value === 'Other') value = 'other';
        $('#cut_' + value).prop('checked', true);
        cut_list.push(value);
    });

    $("#lotid").val(state.id);
    if (state.id) {
        // console.log('id', state.id[0]);
        // id_list.push(parseInt(state.id[0]));
        id_list = state.id;
    }

    $("#depth_from").val(state.depth_from);
    $("#depth_to").val(state.depth_to);
    depth_from = state.depth_from;
    depth_to = state.depth_to;

    $("#table_from").val(state.table_from);
    $("#table_to").val(state.table_to);
    table_from = state.table_from;
    table_to = state.table_to;

    $("#discount_from").val(state.discount_from);
    $("#discount_to").val(state.discount_to);
    discount_from = state.discount_from;
    discount_to = state.discount_to;

    $("#weight_from").val(state.weight_from);
    $("#weight_to").val(state.weight_to);
    weight_from = state.weight_from;
    weight_to = state.weight_to;


    // $('#sales_manager').val((user.data[0].fields.sales_manager));


      //     $("#more-user-loader").hide();
      // // console.log(data);
      // $('#user_image').attr('src', user.data[0].fields.image);
      // $('#user_type').val(user.data[0].fields.user_type);
      // $('#company_name').val((user.data[0].fields.company_name));
      // $('#email').val((user.data[0].fields.email));
      // $('#title').val((user.data[0].fields.title));
      // $('#first_name').val((user.data[0].fields.first_name));
      // $('#last_name').val((user.data[0].fields.last_name));
      // $('#password').val('');
      //
      //
      // // $('#sales_person').empty().append('<option value="">No Sales Persons</option>');
      // // $.each(user.sales_persons, function(key, value) {
      // //     $('#sales_person').append(
      // //       '<option value=' + value.pk + '>' + value.fields.email + '</option>'
      // //     )
      // // });
      // // $('#sales_person').val((user.data[0].fields.sales_person));
      //
      // $('#user_status').val((user.data[0].fields.user_status));
      // $('#customer_type').val((user.data[0].fields.customer_type));
      // $('#mass_email').val((user.data[0].fields.mass_email).toString());
      // $('#language').val((user.data[0].fields.language));
      // $('#markup').val((user.data[0].fields.markup));
      //
      // suppliers_list = [];
      // $('.suppliers').prop('checked', false);
      // $.each(user.suppliers, function(key, value) {
      //     suppliers_list.push(value.fields.supplier);
      //     $('#' + value.fields.supplier).prop('checked', true);
      //     // console.log('suppliers_LIST: ', suppliers_list);
      // });
      //
      // sale_modules_list = [];
      // $('.sale_modules').prop('checked', false);
      // $.each(user.sale_modules, function(key, value) {
      //     sale_modules_list.push(value.fields.sale_module);
      //     $('#' + value.fields.sale_module).prop('checked', true);
      //     console.log('sale_modules_LIST: ', sale_modules_list);
      // });
      //
      //
      // $('#comments').val((user.data[0].fields.comment));
      // $('#street').val((user.data[0].fields.street));
      // $('#city').val((user.data[0].fields.city));
      // $('#zip_code').val((user.data[0].fields.zip_code));
      // $('#country').val((user.data[0].fields.country));
      // $('#tel').val((user.data[0].fields.tel));
      // $('#mobile').val((user.data[0].fields.mobile));
      // $('#region_state').val((user.data[0].fields.region_state));
      // $('#close_to_city').val((user.data[0].fields.close_to_city));
      // $('#tel3').val((user.data[0].fields.tel3));
      //
      // if (user.data[0].fields.birth_date) {
      //     var now = new Date(user.data[0].fields.birth_date);
      //     var day = ("0" + now.getDate()).slice(-2);
      //     var month = ("0" + (now.getMonth() + 1)).slice(-2);
      //     var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
      // }
      //
      // $('#birth_date').val(today);
      // $('#business_type').val((user.data[0].fields.business_type));
      // $('#web_site').val((user.data[0].fields.web_site));
      // $('#skype_user').val((user.data[0].fields.skype_user));
      // $('#qq').val((user.data[0].fields.qq));
      // $('#facebook').val((user.data[0].fields.facebook));
      // $('#twitter').val((user.data[0].fields.twitter));
      // $('#ref_name').val((user.data[0].fields.ref_name));
      // $('#ref_phone').val((user.data[0].fields.ref_phone));
      // $('#jbt').val((user.data[0].fields.jbt));
      // $('#vat').val((user.data[0].fields.vat));
      // $('#whitesite_markup').val((user.data[0].fields.whitesite_markup));
      // $('#dept').val((user.data[0].fields.dept));
      // $('#r1').val((user.data[0].fields.r1));
      // $('#address_4').val((user.data[0].fields.address_4));
      // $('#fantasy_no').val((user.data[0].fields.fantasy_no));
      // $('#harmony_no').val((user.data[0].fields.harmony_no));
      // $('#data_source').val((user.data[0].fields.data_source));
      //
      // if (user.data[0].fields.is_verified) {
      //     $('#verified').prop('checked', true);
      // }
      // // $('#verified').val((user.data[0].fields.is_verified));


}


//Price case

var price_type = 'original';

//price type case
$('input:radio[name="price_type"]').change(function(){
    $("input.price-input").attr("disabled", true);
    price_type = $(this).val();
    // console.log('type', price_type);
    if ($(this).is(':checked') && $(this).val() === 'original'){
        // console.log('original');
       //
       // $("div.type2").removeClass('checkbox-primary').addClass('checkbox-default');
       //
       //
       // $("div.type1").removeClass('checkbox-default').addClass('checkbox-primary');
    }
    if ($(this).is(':checked') && $(this).val() === 'markup'){
        // console.log('markup');
        $('input#markup-input').attr("disabled", false);
    }
    if ($(this).is(':checked') && $(this).val() === 'global'){
        // console.log('global');
        $('input#global-input').attr("disabled", false);
    }
    if ($(this).is(':checked') && $(this).val() === 'remove'){
        // console.log('remove');
    }
});



$("#price-update").unbind().click(function () {
    populate_email_stone_table();
 });


// problem
// $(".counter-discount").change(function () {
//     console.log(this.value);
// });


function myFunction(value, id, list_price, weight, count) {
    // console.log(value, id, list_price, weight, count);
    var disc = Number(value);
    // $("#counter-sale-price-" + id.toString()).html('<b>$</b>' + ((100 + disc) * 0.01 * list_price).toFixed(2));
    // $("#counter-discount-" + id.toString()).text(disc);

    $("#counter-sale-price-" + id.toString()).html(((100 + disc) * 0.01 * list_price).toFixed(0));
    var total_price = (weight * list_price * (1 + (disc * 0.01)));
    total_price = Math.round(total_price * 100) / 100;
    // $("#counter-total-price-" + id.toString()).html('<b>$</b>' + total_price.toFixed(2));
    $("#counter-total-price-" + id.toString()).html(currencyFormat(total_price));
    api.cell(count-1, 22).data("<span id='counter-total-price-'" + id + ">" + currencyFormat(total_price) + "</span>");
    api.cell(count-1, 20).data(
        "<input type='number' style='width: 80%;' class='form-control p-1 m-r-5 counter-discount' " +
        "onchange='myFunction(this.value, " + id + ", " + list_price + ", " + weight + ", " + count + ")' " +
        "value='"  + value + "'>" +
        "<span hidden id='counter-discount-" + id + "'>" + value + "</span>"
    );

    // sophisticated_to_int(sale_price)
    update_summary();
}


function update_summary() {
    var count = 0;
    var avg = 0;
    var counter_total_sum = 0;
    var counter_total_disc=0;
    var counter_avg_disc = 0;
    var counter_weight_sum = 0;
    var total_price_original = 0;
    api.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        count ++;
        var data = this.data();
        // var total_counter = Number($(data[22]).text().substr(1));
        var total_counter = currencyUnformat($(data[22]).text());
        var list_price = currencyUnformat(data[16].toString());
        // total_price_original += (Number(data[3]) * Number(data[16]));
        total_price_original += (Number(data[3]) * list_price);
        counter_total_sum += total_counter;
        counter_weight_sum += Number(data[3]);
        // console.log('data', counter_total_sum, counter_weight_sum);
        counter_total_disc += Number($(data[20]).val());
        // console.log('data', Number($(data[17]).val()));
    } );

    if (counter_weight_sum !== 0) {
        avg = counter_total_sum / counter_weight_sum;
        // avg = Math.round(avg * 100) / 100;
    }
    // if (count !== 0) {
    //     counter_avg_disc = counter_total_disc / count;
    //     // counter_avg_disc = Math.round(counter_avg_disc * 100) / 100;
    // }
    var temp_disc = 0;
    if (total_price_original !== 0) {
        temp_disc = (((counter_total_sum/total_price_original)-1)*100);
    }

    $("#counter-count").html(count);
    // $("#counter-total-disc").html('<b>-$</b>' + (total_price_original - counter_total_sum).toFixed(2));
    $("#counter-total-disc").html('' + temp_disc.toFixed(1) + '%');
    // $("#counter-avg-disc").html('' + counter_avg_disc.toFixed(1) + '<b>%</b>');
    // $("#counter-total-price").html('<b>$</b>' + counter_total_sum.toFixed(2));
    $("#counter-total-price").html(currencyFormat(counter_total_sum));
    $("#counter-total-weight").html(counter_weight_sum.toFixed(2) + 'Cts');
    // $('#counter-avg').html('<b>$</b>' + avg.toFixed(2));
    $('#counter-avg').html(currencyFormat(avg));

    // update_email_editor_content();

}



function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function currencyFormat(num) {
  return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function currencyUnformat(num) {
    return Number(num.replace(/[^0-9\.]+/g,""));
}


// BUILD REQUIRED FUNCTION
// insert stones and summary in editor
function insert_stones_n_summary() {
    // var table_template = '<table>&nbsp;</table>';
    var table_template = '<table>\n</table>';
    // <td></td><td></td><td></td>  // Counter cols
    var summary_row = '<tr role="row" class="odd"><td></td><td></td><td>'+TOTAL_WEIGHT+'</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>'+TOTAL_PRICE+'</td></tr></tbody>';
    // const imageUrl = prompt( 'Image URL' );
    // var stone_table = $('#email-stone-table').prop('outerHTML');
    // console.log('hello guys', stone_table);
    // console.log('hello guys', ckeditor.getData());

    // ckeditor.model.change( writer => {
    //     const tableElement = writer.createElement( 'table');
    //     // Insert the image in the current selection location.
    //     ckeditor.model.insertContent( tableElement, ckeditor.model.document.selection );
    // } );

    // var ck_content = ckeditor.getData();
    var ck_content = CKEDITOR.instances.email_editor.getData();
    // console.log('hello guys', ck_content);

    api.column(20).visible(false);
    api.column(21).visible(false);
    api.column(22).visible(false);
    // $('#stones-summary td:nth-child(2)').remove();
    var summary_table = $('#stones-summary');
    var temp_table = $('#temp-table');

    var stone_table = $('#email-stone-table').prop('outerHTML');
    summary_table.clone(true).appendTo('#temp-table');
    $('#temp-table td:nth-child(2)').remove();
    summary_table = temp_table.prop('outerHTML');
    temp_table.empty();

    // var html = $.parseHTML(stone_summary).filter("td:eq(2)").remove();
    // $(stone_summary).find("td:eq(2)").remove();

    // $(".tableClassName tbody tr").each(function() {
    //     $(this).find("td:eq(3)").remove();
    // });
    // var stone_table = $('#editor-stones-table').html();
    // var stone_summary = $('#editor-stones-summary').html();
    // console.log(html);

    api.column(20).visible(true);
    api.column(21).visible(true);
    api.column(22).visible(true);
    // $('#stones-summary td:nth-child(2)').show();


    if (stone_table !== "") {
        stone_table = replaceAll(stone_table, '<i class="feather icon-jfi-file-image"></i>', 'view');
        stone_table = stone_table.replace('<table id="email-stone-table" class="display nowrap compact dataTable no-footer" role="grid"', '<table style="width:100%;" align="left" id="email-table-1" cellpadding="3" cellspacing="0" border="1"');
        stone_table = stone_table.replace('</tbody>', summary_row);
        stone_table = replaceAll(stone_table, '<td>', '<td style="text-align:center; vertical-align:middle;">');
        summary_table = summary_table.replace('<table border="0" id="stones-summary"', '<table align="right" id="email-table-2" cellpadding="3" cellspacing="0" border="0" ');
        summary_table = replaceAll(summary_table, '<td>', '<td style="text-align:left; vertical-align:middle;">');
        summary_table = replaceAll(summary_table, '<th>', '<th style="text-align:right; vertical-align:middle;">');
        ck_content = ck_content.replace(table_template, stone_table + '<p></p>' + summary_table + '<p></p><p></p><p></p>');
        // ck_content = ck_content.replace('<table id="email-stone-table" class="display nowrap compact dataTable no-footer" role="grid" style="width: 0px;">', '<table cellpadding="3" cellspacing="0" border="1">');
        // ck_content = ck_content.replace('<table border="0" id="stones-summary">', '<table cellpadding="3" cellspacing="0" border="1">');
    }
    // console.log('Check these: ', stone_table, stone_summary, ck_content);

    // if (stone_summary !== "") {
    //     ck_content = ck_content.replace(table_template, stone_summary + '<p></p><p></p><p></p>');
    // }


    CKEDITOR.instances.email_editor.setData(ck_content);
    var data = CKEDITOR.instances.email_editor.getData();
    // ckeditor.setData(ck_content);
    $("#email_body").val(data);
    // console.log(data, $("#email_body").val());
}


// User filters module
function email_template_callback(value) {
    var template_body = '';
    var template_subject = '';
    if (value !== "") {
        var template = get_email_template(value);
        template_body = template.body;
        template_subject = template.subject;
    }

    var ck_content = '';
    var editor_header = $('#editor-header').html();
    if (editor_header !== "") {
        ck_content += editor_header + '<p></p>';
    }

    ck_content += '<p></p>' + template_body + '<p></p>';

    var old_signature = $('#old-signature').text();
    if (old_signature !== "") {
        ck_content += old_signature;
    }

    // ckeditor.setData(ck_content);
    // $("#email_body").val(ckeditor.getData());
    CKEDITOR.instances.email_editor.setData(ck_content);
    var data = CKEDITOR.instances.email_editor.getData();
    $("#email_body").val(data);
    $("#subject").val(template_subject);
}


function get_email_template(id) {
    var res = {};
    $.ajax({
        url: '/accounts/get_email_template/',
        data: {
          'id': id
        },
        type: "POST",
        async: false,
        dataType: 'json',
        success: function (data) {
            res = data;
        },
        error: function(xhr, error){
          console.log('error on template get');
        }
    });
    return res;
}


var diamonds_countDownDate;
var diamonds_x;

function set_diamonds_counter_distance(settings) {
    diamonds_countDownDate = new Date();
    diamonds_countDownDate.setSeconds(diamonds_countDownDate.getSeconds() + 5);
    diamonds_countDownDate = diamonds_countDownDate.getTime();
    count_down_diamonds_state_save(settings);
}


function count_down_diamonds_state_save(settings) {
    if (diamonds_x) {
        clearInterval(diamonds_x);
    }

    // Update the count down every 1 second
   diamonds_x = setInterval(function() {
      var now = new Date().getTime();
      var distance = diamonds_countDownDate - now;
      // console.log(distance);
      if (distance < 0) {
        clearInterval(diamonds_x);
        save_diamonds_table_state(settings);
        // console.log("State saved Now");
      }
    }, 1000);
}


function save_diamonds_table_state(settings) {
    var data = localStorage.getItem(settings.sInstance );

    $.ajax({
        url: '/accounts/update_state/',
        data: {
          'table_name': settings.sInstance,
          'table_state': data
        },
        type: "POST",
        async: true,
        dataType: 'json',
        success: function (response) {
            // data = JSON.parse(data);
            // console.log(data.ColReorder, data.order);
        },
        error: function(xhr, error){
          console.log('error on state update');
        }
    });
}
