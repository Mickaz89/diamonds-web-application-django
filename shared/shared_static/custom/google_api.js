let SKIP_LIST = ['CATEGORY_PERSONAL', 'CATEGORY_SOCIAL', 'IMPORTANT', 'CATEGORY_UPDATES', 'CATEGORY_FORUMS', 'CHAT',
    'CATEGORY_PROMOTIONS', 'UNREAD', 'INBOX', 'STARRED', 'SPAM', 'DRAFT', 'SENT', 'TRASH'];
let MESSAGES_LIST = [];
let THREADS_LIST = [];
let ATTACHMENTS_LIST = [];
let TO_EMAILS = '';
let SELECTED_THREADS = [];
let SYSTEM_LABELS = [];
let USER_LABELS = [];
let REPLY_ALL = false;
let query = '';
let promises = [];

// Client ID and API key from the Developer Console
let CLIENT_ID = '394633445617-6enmb8r2suc4lrrujnlpnu1ubjm87m6s.apps.googleusercontent.com';
let API_KEY = 'AIzaSyD1TiTp4SU9Qt6ieEgE6jYujEANprOTezY';

// Array of API discovery doc URLs for APIs used by the quickstart
let DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
let SCOPES = 'https://mail.google.com/ ' +
             'profile ' +
             'https://www.googleapis.com/auth/gmail.readonly ' +
             'https://www.googleapis.com/auth/gmail.send '+
             'https://www.googleapis.com/auth/gmail.modify';

// var authorizeButton = document.getElementById('authorize_button');
// var signoutButton = document.getElementById('signout_button');

/**
*  On load, called to load the auth2 library and API client library.
*/
function handleClientLoad() {
    gapi.load('client:auth2', initClient);
    // console.log('handle client load');
}


//   for profile details
//   var profile = auth2.currentUser.get().getBasicProfile();
//   console.log('ID: ' + profile.getId());
//   console.log('Full Name: ' + profile.getName());
//   console.log('Given Name: ' + profile.getGivenName());
//   console.log('Family Name: ' + profile.getFamilyName());
//   console.log('Image URL: ' + profile.getImageUrl());
//   console.log('Email: ' + profile.getEmail());


/**
*  Initializes the API client library and sets up sign-in state
*  listeners.
*/
function initClient() {
    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES
    }).then(function () {
      // Listen for sign-in state changes.
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

      // Handle the initial sign-in state.
      updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      // authorizeButton.onclick = handleAuthClick;
      // signoutButton.onclick = handleSignoutClick;

    }, function(error) {
        // appendPre(JSON.stringify(error, null, 2));
        console.log(error);
    });
}

/**
*  Called when the signed in status changes, to update the UI
*  appropriately. After a sign-in, the API is called.
*/
function updateSigninStatus(isSignedIn) {
    var url = window.location.pathname;
    var arr = url.split("/");
    if (arr[1]==='accounts') {
        if (arr[2] && arr[2] === 'compose_email') {
            if (!isSignedIn) {
                handleAuthClick();
            }
        }
        if (arr[2] && arr[2] === 'mailboxes') {
            // console.log('handle gapi');
            // called from import, useless here
            if (isSignedIn) {
                // authorizeButton.style.display = 'none';
                // signoutButton.style.display = 'block';
                listLabels();
                // displayInbox();

                displayThreads(['INBOX']);
            } else {
                // authorizeButton.style.display = 'block';
                // signoutButton.style.display = 'none';
                handleAuthClick();

            }
        }
    }
}

/**
*  Sign in the user upon button click.
*/
function handleAuthClick(event) {
    // gapi.auth2.getAuthInstance().isSignedIn.get()
    gapi.auth2.getAuthInstance().signIn().then(function (GoogleUser) {
        // console.log('done', GoogleUser.getBasicProfile().getEmail());
        localStorage.setItem( 'test_email', JSON.stringify(GoogleUser.getBasicProfile().getEmail()) );
    });
    // console.log('handle auth');

  // var profile = gapi.auth2.currentUser.get().getBasicProfile();
  // console.log('ID: ' + profile.getId());
  // console.log('Full Name: ' + profile.getName());
  // console.log('Given Name: ' + profile.getGivenName());
  // console.log('Family Name: ' + profile.getFamilyName());
  // console.log('Image URL: ' + profile.getImageUrl());
  // console.log('Email: ' + profile.getEmail());
}

/**
*  Sign out the user upon button click.
*/
function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
    // console.log('handle signout');
}

/**
* Append a pre element to the body containing the given message
* as its text node. Used to display the results of the API call.
*
* @param {string} message Text to be placed in pre element.
*/
function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}


/**
* Print all Labels in the authorized user's inbox. If no labels
* are found an appropriate message is printed.
*/
function listLabels() {
    gapi.client.gmail.users.labels.list({
      'userId': 'me'
    }).then(function(response) {
      var labels = response.result.labels;
      if (labels && labels.length > 0) {
        for (i = 0; i < labels.length; i++) {
            var label = labels[i];
            // appendPre(label.name);
            if(label.type === 'user') {
                USER_LABELS.push(label);
            } else {
                SYSTEM_LABELS.push(label)
            }
        }
      } else {
        // appendPre('No Labels found.');
        console.log('No Labels found.');
      }
        // console.log(SYSTEM_LABELS);
        USER_LABELS.sort(compare);
        // console.log(SYSTEM_LABELS);
        populate_labels(USER_LABELS);

    });
}


function populate_labels(labels) {
    for (var i = 0; i < labels.length; i++) {
        var label = labels[i];
        var index = $.inArray(label.name, SKIP_LIST);
        var label_name = label.name;
        label_name = label_name[0].toUpperCase() + label_name.slice(1).toLowerCase();
        if ( index === -1 ) {
            $('#labels').append(
                '<li class="nav-item mail-section">' +
                    '<a class="nav-link" href="#" onclick="filter_messages(\''+label.id+'\')">\n' +
                        '<i class="icofont icofont-square"></i> ' + label_name +
                    '</a>' +
                '</li>'
                // '<li class=""><a class="mail-' + label_name + '" href="#"  onclick="filter_messages(\''+label.id+'\')">' + label_name + '</a></li>'
            );
            $('#move-to-labels').append(
                '<a class="nav-link" href="#" onclick="filter_messages(\''+label.id+'\')">\n' +
                    '<i class="icofont icofont-square"></i> ' + label_name +
                '</a>'
            );
        }
    }
}


function populate_messages(threads) {
    for (var i = 0; i < threads.length; i++) {
        appendMessageRow(threads[i].messages[0]);
    }
}


function compare( a, b ) {
  if ( a.name < b.name ){
    return -1;
  }
  if ( a.name > b.name ){
    return 1;
  }
  return 0;
}


function compare_date( a, b ) {
    var keyA = parseInt(a.messages[0].internalDate),
    keyB = parseInt(b.messages[0].internalDate);
    // var keyA = new Date(a.messages[0].internalDate),
    // keyB = new Date(b.messages[0].internalDate);
    // Compare the 2 dates
    if(keyA < keyB) return 1;
    if(keyA > keyB) return -1;
    return 0;
}


function displayInbox(labels) {
    $("#gmail-messages-none").hide();
    $("#gmail-messages-loader").show();
    var request = gapi.client.gmail.users.messages.list({
      'userId': 'me',
      'labelIds': labels,
      'maxResults': 10
      // 'pageToken': nextPageToken,
      // 'q': query
    });

    request.execute(function(response) {
      // var nextPageToken = response.nextPageToken;
        var total_messages = 0;
      $.each(response.messages, function() {
          total_messages += 1;
        var messageRequest = gapi.client.gmail.users.messages.get({
          'userId': 'me',
          'id': this.id
        });

        messageRequest.execute(function (message) {
            MESSAGES_LIST.push(message);
            appendMessageRow(message)
        });
        // messageRequest.execute(appendMessageRow);
      });
      if(total_messages === 0) {
          $("#gmail-messages-none").show();
      }
      $("#gmail-messages-loader").hide();
    });
}

function appendMessageRow(message) {
    var class_name = '';
    var index = $.inArray('UNREAD', message.labelIds);
    if(index !== -1) {
        class_name = 'unread';
    }

    $('#inbox').append(
        '<tr class="' + class_name + '" id="thread-'+ message.threadId +'">' +
            // '<a href="email-read.html" class="email-name">' +
                '<td>' +
                    '<div class="check-star">' +
                        '<div class="checkbox-color checkbox-primary">' +
                            '<input class="mail" type="checkbox" name="mail" id="'+ message.threadId +'" onchange="prepare_threads_list(\''+ message.threadId +'\')">' +
                            '<label style="margin-bottom: 1.3rem!important;" for="'+ message.threadId +'"></label>' +
                        '</div>' +
                        // '<div class="checkbox-fade fade-in-primary checkbox">' +
                        //     '<label>' +
                        //         '<input type="checkbox" value="">' +
                        //             '<span class="cr"><i class="cr-icon icofont icofont-verification-check txt-primary"></i></span>' +
                        //     '</label>' +
                        // '</div>' +
                        '<i class="icofont icofont-star text-warning"></i>' +
                    '</div>' +
                '</td>' +
                '<td class="truncate-from"><a onclick="read_thread(\''+ message.id +'\',\''+ message.threadId +'\')">'+getHeader(message.payload.headers, "From")+'</a></td>' +
                '<td class="truncate-subject"><a onclick="read_thread(\''+ message.id +'\',\''+ message.threadId +'\')">'+getHeader(message.payload.headers, 'Subject')+'</a></td>' +
                '<td class="email-attch"><i class="icofont icofont-clip"></i></td>' +
                '<td class="email-time">'+getHeader(message.payload.headers, "Date")+'</td>' +
            // '</a>' +
        '</tr>'

    );

    //   var ifrm = $('#message-iframe-'+message.id)[0].contentWindow.document;
    //   $('body', ifrm).html(getBody(message.payload));
    // });
}

function getHeader(headers, index) {
    var header = '';

    $.each(headers, function(){
      if(this.name === index){
        header = this.value;
      }
    });
    return header;
}

function getBody(message) {
    var encodedBody = '';
    if(typeof message.parts === 'undefined')
    {
      encodedBody = message.body.data;
    }
    else
    {
      encodedBody = getHTMLPart(message.parts);
    }
    encodedBody = encodedBody.replace(/-/g, '+').replace(/_/g, '/').replace(/\s/g, '');
    return decodeURIComponent(escape(window.atob(encodedBody)));
}

function getHTMLPart(arr) {
    for(var x = 0; x <= arr.length; x++)
    {
      if(typeof arr[x].parts === 'undefined')
      {
        if(arr[x].mimeType === 'text/html')
        {
          return arr[x].body.data;
        }
      }
      else
      {
        return getHTMLPart(arr[x].parts);
      }
    }
    return '';
}


function filter_messages(label) {
    SELECTED_THREADS = [];
    next_page_token = '';
    // console.log(label);
    $(".mail-body > #thread-read").hide();
    $(".mail-body > #mails-listing").show();
    $("#inbox > tr").remove();
    // displayInbox(label)
    displayThreads(label);
    update_header_controllers(label);
}


function update_header_controllers(label) {
    if (label === 'DRAFT' || label === 'SPAM' || label === 'TRASH') {
        $("#archive-button").hide();
        $("#trash-button").hide();
    } else {
        $("#archive-button").show();
        $("#trash-button").show();
    }
}


function read_message(thread_id, message_id) {
    // console.log(thread_id, message_id);
    // console.log(MESSAGES_LIST);
    let message = MESSAGES_LIST.find(o => o.id === message_id);
    // console.log();
    var msg_html = '' +
    '<div class="card">' +
        '<div class="card-header">' +
            '<h5>' + getHeader(message.payload.headers, 'Subject') + '</h5>' +
            '<h6 class="f-right">'+getHeader(message.payload.headers, "Date")+'</h6>' +
        '</div>' +
        '<div class="card-block">' +
            '<div class="media m-b-20">' +
                '<div class="media-left photo-table">' +
                    '<a href="#">' +
                        '<img class="media-object img-radius" src="https://via.placeholder.com/100x100?text=Profile" alt="E-mail User">' +
                    '</a>' +
                '</div>' +
                '<div class="media-body photo-contant">' +
                    '<a href="#">' +
                        '<h6 class="user-name txt-primary">John Doe</h6>' +
                    '</a>' +
                    '<a class="user-mail txt-muted" href="#">' +
                        '<h6>From:<span class="__cf_email__" data-cfemail="6a000502040e050f5b5f5e592a0d070b030644090507">[email&#160;protected]</span></h6>' +
                    '</a>' +
                '<div>' +
                '<h6 class="email-welcome-txt">Hello Dear...</h6>' +
                '<div class="email-content">' +
                    getBody(message.payload) +
                '</div>' +
            '</div>' +
            '<div class="m-t-15">' +
                '<i class="icofont icofont-clip f-20 m-r-10"></i>Attachments <b>(3)</b>' +
                '<div class="row mail-img">' +
                    '<div class="col-sm-4 col-md-2 col-xs-12">' +
                        '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 1" alt="Card image cap"></a>' +
                    '</div>' +
                    '<div class="col-sm-4 col-md-2 col-xs-12">' +
                        '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 2" alt="Card image cap"></a>' +
                    '</div>' +
                    '<div class="col-sm-4 col-md-2 col-xs-12">' +
                        '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 3" alt="Card image cap"></a>' +
                    '</div>' +
                '</div>' +
                '<textarea class="form-control m-t-30 col-xs-12 email-textarea" id="exampleTextarea-1" placeholder="Reply Your Thoughts" rows="4"></textarea>' +
            '</div>' +
        '</div>' +
    '</div>';

    $(".mail-body > #mails-listing").hide();
    $(".mail-body > #thread-read").html(msg_html).show();
    // $(".mail-body > #thread-read");
}


/**
 * Retrieve Threads in the user's mailbox matching query.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} query String used to filter the Threads listed.
 * @param  {Function} callback Function to call when the request is complete.
 */
function listThreads(userId, query, callback) {
  var getPageOfThreads = function(request, result) {
    request.execute(function (resp) {
      result = result.concat(resp.threads);
      var nextPageToken = resp.nextPageToken;
      if (nextPageToken) {
        request = gapi.client.gmail.users.threads.list({
          'userId': userId,
          'q': query,
          'pageToken': nextPageToken
        });
        getPageOfThreads(request, result);
      } else {
        callback(result);
      }
    });
  };
  var request = gapi.client.gmail.users.threads.list({
    'userId': userId,
    'q': query
  });
  getPageOfThreads(request, []);
}


/**
 * Get Thread with given ID.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} threadId ID of Thread to get.
 * @param  {Function} callback Function to call when the request is complete.
 */
function getThread(userId, threadId, callback) {
  var request = gapi.client.gmail.users.threads.get({
    'userId': userId,
    'id': threadId
  });
  request.execute(function (thread) {
      THREADS_LIST.push(thread);
      // appendThreadRow(thread)
      // console.log(thread);
  });


}

var next_page_token = '';
var current_labels = '';

function displayThreads(labels) {
    current_labels = labels;
    THREADS_LIST = [];
    promises = [];
    $('#load-more-button').hide();
    $("#gmail-messages-none").hide();
    $("#gmail-messages-loader").show();
    let request = gapi.client.gmail.users.threads.list({
      'userId': 'me',
      'labelIds': labels,
      'maxResults': 10,
      'pageToken': next_page_token,
      'q': query
    });

    // if (next_page_token === '') {
    //     console.log('first page');
    // } else {
    //     console.log('Not first page');
    //     // var request = gapi.client.gmail.users.threads.list({
    //     //   'userId': 'me',
    //     //   'labelIds': labels,
    //     //   'maxResults': 10
    //     //   // 'pageToken': nextPageToken
    //     //   // 'q': query
    //     // });
    // }

    request.execute(function(response) {
        if (response.nextPageToken) {
            next_page_token = response.nextPageToken;
            $('#load-more-button').show();
        }

      //   var total_threads = 0;
        $.each(response.threads, function() {
             // total_threads += 1;
             // promises.push(getThread('me', this.id, null));
             promises.push(gapi.client.gmail.users.threads.get({
                'userId': 'me',
                'id': this.id
             }));
         });

      Promise.all(promises)
          .then(response => {
              // console.log('all done', response)
              $.each(response, function() {
                  THREADS_LIST.push(this.result);
              });

              // console.log(THREADS_LIST);
              THREADS_LIST.sort(compare_date);
              // console.log(THREADS_LIST);
              populate_messages(THREADS_LIST);

              if(response.length === 0) {
                  $("#gmail-messages-none").show();
              }
              $("#gmail-messages-loader").hide();
      });

    });
}


function appendThreadRow(thread) {
    $('#inbox').append(
        '<tr class="unread" >' +
            // '<a href="email-read.html" class="email-name">' +
                '<td>' +
                    '<div class="check-star">' +
                        '<div class="checkbox-color checkbox-primary">' +
                            '<input class="mail" type="checkbox" name="mail" id="'+ message.id +'" onchange="prepare_threads_list(\''+ message.threadId +'\')">' +
                            '<label for="'+ message.id +'"></label>' +
                        '</div>' +
                        // '<div class="checkbox-fade fade-in-primary checkbox">' +
                        //     '<label>' +
                        //         '<input type="checkbox" value="">' +
                        //             '<span class="cr"><i class="cr-icon icofont icofont-verification-check txt-primary"></i></span>' +
                        //     '</label>' +
                        // '</div>' +
                        '<i class="icofont icofont-star text-warning"></i>' +
                    '</div>' +
                '</td>' +
                '<td class="truncate-from">'+getHeader(message.payload.headers, "From")+'</td>' +
                '<td class="truncate-subject">'+getHeader(message.payload.headers, 'Subject')+'</td>' +
                '<td class="email-attch"><i class="icofont icofont-clip"></i></td>' +
                '<td class="email-time">'+getHeader(message.payload.headers, "Date")+'</td>' +
            // '</a>' +
        '</tr>'

    );
}

var reply_to = '';
var cc = '';
var bcc = '';
var reply_subject = '';
var message_id = '';
var in_reply_to = '';
var references = '';

function read_thread(message_id, thread_id) {
    modifyThread('me', thread_id, [], ['UNREAD'],null);
    $('input#'+thread_id).prop('checked', true);
    prepare_threads_list(thread_id);
    // console.log(thread_id, message_id);
    // console.log(MESSAGES_LIST);
    let thread = THREADS_LIST.find(o => o.id === thread_id);

    var msg_list_html = '';
    var msg_card_html = '' +
    '<div class="card">' +
        '<div class="card-header">' +
            '<h5>' + getHeader(thread.messages[0].payload.headers, 'Subject') + '</h5>' +
            '<h6 class="f-right">'+getHeader(thread.messages[0].payload.headers, "Date")+'</h6>' +
        '</div>' + // end card-header
        '<div class="card-block">' +
            '<div id="mesg_list">';


    $.each(thread.messages, function(key, value) {
        // console.log(value.payload.headers);
        var temp_message = '' +
            '<div class="media m-b-20">' +
                '<div class="media-left photo-table">' +
                    '<a href="#">' +
                        '<img class="media-object img-radius" src="https://via.placeholder.com/100x100?text=Profile" alt="E-mail User">' +
                    '</a>' +
                '</div>' +
                '<div class="media-body photo-contant">' +
                    // '<a href="#">' +
                        '<h6 class="user-name txt-primary"></h6>' +
                    // '</a>' +
                    // '<a class="user-mail txt-muted" href="#">' +
                        // '<h6>From:<span class="__cf_email__" data-cfemail="6a000502040e050f5b5f5e592a0d070b030644090507">[email&#160;protected]</span></h6>' +
                        '<h6>From: <span class="__cf_email__">' + getHeader(value.payload.headers, 'From') + '</span></h6>' +
                    // '</a>' +
                // '<h6 class="email-welcome-txt">Hello Dear...</h6>' +
                    getBody(value.payload) +
                    // '<iframe width="100%" height="100%" style="border: 0" srcdoc="' + getBody(value.payload) + '"></iframe>' +
                '</div>' +
            '</div>' +
            // '<div class="m-t-15">' +
            //     '<i class="icofont icofont-clip f-20 m-r-10"></i>Attachments <b>(3)</b>' +
            //     '<div class="row mail-img">' +
            //         '<div class="col-sm-4 col-md-2 col-xs-12">' +
            //             '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 1" alt="Card image cap"></a>' +
            //         '</div>' +
            //         '<div class="col-sm-4 col-md-2 col-xs-12">' +
            //             '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 2" alt="Card image cap"></a>' +
            //         '</div>' +
            //         '<div class="col-sm-4 col-md-2 col-xs-12">' +
            //             '<a href="#"><img class="card-img-top img-fluid img-thumbnail" src="https://via.placeholder.com/780x520?text=File 3" alt="Card image cap"></a>' +
            //         '</div>' +
            //     '</div>' +
            // '</div>' +
            '<hr style="margin-top: 30px!important;"/>';


        message_id = getHeader(value.payload.headers, 'Message-ID');
        in_reply_to = getHeader(value.payload.headers, 'In-Reply-To');
        reply_to = (getHeader(value.payload.headers, 'Reply-To') !== '' ?
                    getHeader(value.payload.headers, 'Reply-To') :
                    getHeader(value.payload.headers, 'From')).replace(/\"/g, '&quot;');
        cc = getHeader(value.payload.headers, 'Cc');
        bcc = getHeader(value.payload.headers, 'Bcc');

        if (getHeader(value.payload.headers, 'References') !== '') {
            references = getHeader(value.payload.headers, 'References') + ' ' + message_id;
        } else if (in_reply_to !== '') {
             references = in_reply_to + ' ' + message_id;
        } else {
            references = message_id;
        }
        references = references.replace(/\"/g, '&quot;');

        reply_subject = 'Re: '+getHeader(value.payload.headers, 'Subject').replace(/\"/g, '&quot;');
        // reply_subject = 'Re: '+getHeader(thread.messages[0].payload.headers, 'Subject');

        // console.log(reply_to, reply_subject, message_id);

        msg_list_html = msg_list_html + temp_message
    });

    var read_thread_html = msg_card_html + msg_list_html +
            '</div>' +
            '<div id="ctrl-area">' +
                '<button class="btn btn-primary" onclick="setReplyCriteria(' + false + ')">Reply</button>' +
                '<button class="btn btn-info m-l-20" onclick="setReplyCriteria(' + true + ')">Reply All</button>' +
            '</div>' +
            '<div id="reply-area" style="display: none">' +
                '<textarea class="form-control m-t-30 col-xs-12 email-textarea" id="reply-message" placeholder="Reply Your Thoughts" rows="4"></textarea>' +
                '<button type="submit" id="reply-button" class="btn btn-primary" onclick="sendReply(\'' + thread_id + '\')">Send</button>' +
            '</div>' +
        '</div>' +  // end car-block
    '</div>';  // end card

    $(".mail-body > #mails-listing").hide();
    $(".mail-body > #thread-read").html(read_thread_html).show();
    // $(".mail-body > #thread-read");
}


function sendEmail() {
    $('#send-button').addClass('disabled');
    
    var body = getEmailBody();

    // sendMessage(
    //   {
    //     // 'To': 'ateeqsuhail@gmail.com, a4ateeqsuhail@gmail.com',
    //     'To': TO_EMAILS,
    //     'Cc': $('#cc').val(),
    //     'Bcc': $('#bcc').val(),
    //     'Subject': $('#subject').val(),
    //     'Content-Type': 'text/html; charset="UTF-8"; boundary="foo_bar"',
    //     'Content-Transfer-Encoding': 'base64'
    //   },
    //   body,
    //   composeTidy
    // );
    send_gmail_email(
      {
        'To': TO_EMAILS,
        'Cc': $('#cc').val(),
        'Bcc': $('#bcc').val(),
        'Subject': $('#subject').val()
      },
      body,
      composeTidy
    );
}


function getEmailBody() {
    return '<html>' +
            '<head>' +
                '<style>' +
                    // '#email-table-1 {' +
                    // '   width: 100%;' +
                    // '}' +
                    // '#email-table-1 td {' +
                    // '    text-align: center; ' +
                    // '    vertical-align: middle;' +
                    // '}' +
                    // '#email-table-2{' +
                    // '    allign: right; ' +
                    // '    padding-right: 5%;' +
                    // '}' +
                    // '#email-table-2 td{' +
                    // '    text-align: left; ' +
                    // '    vertical-align: middle;' +
                    // '}' +
                    // '#email-table-2 th{' +
                    // '    text-align: right; ' +
                    // '    vertical-align: middle;' +
                    // '}' +
                '</style>' +
            '</head>' +
            '<body>' + $('#email_body').val() + '</body></html>';
}



function sendTestEmail() {
    var test_email_button = $('#test-email-button');
    test_email_button.addClass('disabled');

    // var body = '<html><body>' + $('#email_body').val() + '</body></html>';
    var body = getEmailBody();

    // console.log(body);
    send_gmail_email(
      {
        // 'To': 'ateeqsuhail@gmail.com',
        'To': JSON.parse(localStorage.getItem('test_email')),
        'Subject': $('#subject').val()
        // 'Content-Type': 'multipart/mixed; boundary="foo_bar"',
        // 'Content-Type': 'text/html; charset="UTF-8"',
        // 'Content-Transfer-Encoding': 'base64'
      },
      body,
      test_mail_sent
    );
    test_email_button.removeClass('disabled');
}


function composeTidy() {
    // $('#compose-modal').modal('hide');

    // $('#compose-to').val('');
    $('#subject').val('');
    $('#email_body').val('');

    $('#send-button').removeClass('disabled');
    // return false;
    $('#messages').append(
        '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Success! Message Sent!' + '</div>');
    $("#email-send-loader").hide();
}


function test_mail_sent() {
    $('#messages').append(
        '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Success! Test Email Sent to yourself!' + '</div>');
    $("#email-send-loader").hide();
}


function sendReply(thread_id) {
    $('#reply-button').addClass('disabled');
    if(REPLY_ALL === false) {
        cc = '';
        bcc = '';
    }

    // sendMessage(
    //   {
    //     'To': reply_to,
    //     'Cc': cc,
    //     'Bcc': bcc,
    //     'Subject': reply_subject,
    //     'In-Reply-To': message_id,
    //     'threadId': thread_id,
    //     'References': references
    //   },
    //   $('#reply-message').val(),
    //   replyTidy
    // );
    send_gmail_email(
      {
        'To': reply_to,
        'Cc': cc,
        'Bcc': bcc,
        'Subject': reply_subject,
        'In-Reply-To': message_id,
        'threadId': thread_id,
        'References': references
      },
      $('#reply-message').val(),
      replyTidy
    );

    return false;
}

function replyTidy() {
    // $('#reply-modal').modal('hide');

    $('#reply-button').removeClass('disabled');
    insert_message();

    $('#messages').append(
        '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Success! Message Sent!' + '</div>');

    $('#reply-message').val('');
    $("#email-send-loader").hide();
    // $('#ctrl-area').show();
    // $('#reply-area').hide();
}


function setReplyCriteria(value) {
    REPLY_ALL = value;
    $('#ctrl-area').hide();
    $('#reply-area').show();
    // console.log(REPLY_ALL);
}


function insert_message() {
    $('#mesg_list').append(
        '<div class="media m-b-20">' +
            '<div class="media-left photo-table">' +
                '<a href="#">' +
                    '<img class="media-object img-radius" src="https://via.placeholder.com/100x100?text=Profile" alt="E-mail User">' +
                '</a>' +
            '</div>' +
            '<div class="media-body photo-contant">' +
                    '<h6 class="user-name txt-primary"></h6>' +
                    '<h6>From: <span class="__cf_email__">' + 'me' + '</span></h6>' +
                $('#reply-message').val() +
            '</div>' +
        '</div>' +
        '<hr style="margin-top: 30px!important;"/>'
    );
}


function fillInReply(to, subject, message_id) {
    $('#reply-to').val(to);
    $('#reply-subject').val(subject);
    $('#reply-message-id').val(message_id);
}

function sendMessage(headers_obj, message, callback) {
    var email = '';

    for(var header in headers_obj)
      email += header += ": "+headers_obj[header]+"\r\n";

    email += "\r\n" + message;

    // console.log(headers_obj['threadId']);
    var sendRequest = gapi.client.gmail.users.messages.send({
      'userId': 'me',
      'resource': {
        'threadId': headers_obj['threadId'],
        'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
      }
    });

    return sendRequest.execute(callback);
}


/**
 * Modify the Labels a Thread is associated with.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} threadId ID of Thread to modify.
 * @param  {Array} labelsToAdd Array of Labels to add.
 * @param  {Array} labelsToRemove Array of Labels to remove.
 * @param  {Function} callback Function to call when the request is complete.
 */
function modifyThread(userId, threadId, labelsToAdd, labelsToRemove, callback) {
  var request = gapi.client.gmail.users.threads.modify({
    'userId': userId,
    'id': threadId,
    'addLabelIds': labelsToAdd,
    'removeLabelIds': labelsToRemove
  });
  request.execute(callback);
}



// prepare selected threads list -> option 2
// $( ".mail" ).click(function(mail) {
// $('.mail').on('click', ':checkbox', function(mail) {
//     var id = mail.target.id;
//     prepare_threads_list(id)
// });


function prepare_threads_list(id) {
    // console.log(id);
    var index = SELECTED_THREADS.indexOf(id);
    if ($('#' + id).is(":checked")) {
        if (index === -1) SELECTED_THREADS.push(id);
    } else {
        if (index !== -1) SELECTED_THREADS.splice(index, 1);
    }
    if (SELECTED_THREADS.length > 0) {
        $("#spam-button").attr("disabled", false);
        $("#archive-button").attr("disabled", false);
        $("#trash-button").attr("disabled", false);
        $("#unread-button").attr("disabled", false);
        $("#read-button").attr("disabled", false);
        $("#move-to-button").attr("disabled", false);
    } else {
        $("#spam-button").attr("disabled", true);
        $("#archive-button").attr("disabled", true);
        $("#trash-button").attr("disabled", true);
        $("#unread-button").attr("disabled", true);
        $("#read-button").attr("disabled", true);
        $("#move-to-button").attr("disabled", true);
    }
    // console.log('SELECTED_THREADS: ', SELECTED_THREADS);
}


/**
 * Trash a Thread.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} threadId ID of Thread to modify.
 * @param  {Function} callback Function to call when the request is complete.
 */
function trashThread(userId, threadId, callback) {
    // console.log(userId, threadId, callback);
    // gapi.client.gmail.users.messages.trash(userId, threadId).execute(callback);
    gapi.client.gmail.users.messages.trash({
        'userId': userId,
        'id': threadId
    }).execute(callback);
}

function trash_threads(){
    $.each(SELECTED_THREADS, function(key, value){
        trashThread('me', value, done);
        // modifyThread('me', value, ['TRASH'], ['INBOX', 'SENT', 'DRAFT', 'STARRED'],done);
    });
}


function mark_unread_threads(){
    $.each(SELECTED_THREADS, function(key, value){
        modifyThread('me', value, ['UNREAD'], [],done);
        $("#thread-" + value).addClass('unread');
    });
}


function mark_read_threads(){
    $.each(SELECTED_THREADS, function(key, value){
        modifyThread('me', value, [], ['UNREAD'],done);
        $("#thread-" + value).removeClass('unread');
    });
}


function archive_threads(){
    $.each(SELECTED_THREADS, function(key, value){
        // console.log(value, this);
        modifyThread('me', value, [], ['INBOX'],done);
        $("#thread-" + value).remove();
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! Thread Archived' + '</div>');

    });
}


// function spam_threads(){
//     $.each(SELECTED_THREADS, function(key, value){
//         modifyThread('me', value, [], ['SPAM'],null);
//     });
// }


function done() {
    // $(".theme-loader").show();
    console.log('done');
    // debugger;
}


function attachFiles() {
    var files = document.getElementById('file').files;
    $.each(files, function(key, value){
      // getBase64(value);
      readImageBinary(value);
    });
    $("#file").val(null);
    ATTACHMENTS_LIST = [];
}

// Pure JS way
// document.getElementById('button').addEventListener('click', function() {
//   var files = document.getElementById('file').files;
//   $.each(files, function(key, value){
//       // getBase64(value);
//       readImageBinary(value);
//   });
//   $("#file").val(null);
//   ATTACHMENTS_LIST = [];
// });


function getBase64(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
       ATTACHMENTS_LIST.push({
          type: file.type,
          name: file.name,
          data: reader.result
       });
      // console.log(ATTACHMENTS_LIST);
      // console.log(file.name);
      // console.log(file.type);
      // console.log(reader.result);
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}


function readImageBinary(file) {
   var reader = new FileReader();
   reader.readAsBinaryString(file);
   reader.onload = function () {
       ATTACHMENTS_LIST.push({
          type: file.type,
          name: file.name,
          data: reader.result
       });
      // console.log(ATTACHMENTS_LIST);
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}





// Gmail body creator

/**
 * Takes a key-value object representing headers and their values and turns it
 * into a header string.
 * @param  {object} headers
 * @return {string}
 */
function createHeaders(headers) {
  if (!headers || headers.length === 0) {
    return '';
  }

  var result = [];
  for (var h in headers) {
    if (headers.hasOwnProperty(h)) {
      result.push(h + ': ' + headers[h] + '\r\n');
    }
  }

  return result.join('');
}

/**
 * The only field in the json part of the message is the threadId.
 * If it is given, it is placed in the json. Otherwise, nothing will be added at all.
 * @param  {string} [threadId] - Id representing the thread the message should be put into
 * @return {string}
 */
function createJson(threadId) {
  return [
    'Content-Type: application/json; charset="UTF-8"\r\n\r\n',

    '{\r\n',
      (threadId ? '  "threadId": "' + threadId + '"\r\n' : ''),
    '}'
  ].join('');
}

/**
 * Creates a plain text message string.
 * @param  {string} textPlain
 * @return {string}
 */
function createPlain(textPlain) {
  return [
    'Content-Type: text/plain; charset="UTF-8"\r\n',
    'MIME-Version: 1.0\r\n',
    'Content-Transfer-Encoding: 7bit\r\n\r\n',

    textPlain
  ].join('');
}

/**
 * Creates a html text message string.
 * @param  {string} textHtml
 * @return {string}
 */
function createHtml(textHtml) {
  return [
    'Content-Type: text/html; charset="UTF-8"\r\n',
    'MIME-Version: 1.0\r\n',
    'Content-Transfer-Encoding: base64\r\n\r\n',

    textHtml
  ].join('');
}

/**
 * If both plain text and html text representations are given, both of them
 * are given as alternatives.
 * @param  {string} textPlain
 * @param  {string} textHtml
 * @return {string}
 */
function createAlternative(textPlain, textHtml) {
  return [
    'Content-Type: multipart/alternative; boundary="foo"\r\n\r\n',

    '--foo\r\n',
    createPlain(textPlain), '\r\n\r\n',

    '--foo\r\n',
    createHtml(textHtml), '\r\n\r\n',

    '--foo--'
  ].join('');
}

/**
 * Creates a proper representaion of the text that is given. If neither a
 * plain text or html text is given, an empty string is returned.
 * @param  {string} [textPlain]
 * @param  {string} [textHtml]
 * @return {string}
 */
function createText(textPlain, textHtml) {
  if (textPlain && textHtml) {
    return createAlternative(textPlain, textHtml);
  } else if (textPlain) {
    return createPlain(textPlain);
  } else if (textHtml) {
    return createHtml(textHtml);
  } else {
    return '';
  }
}

/**
 * Creates a proper representaion of the attachments that are given.
 * If no attachments are given, an empty string is returned.
 * @param  {object[]} [attachments]
 * @param  {string} attachments[].[name]
 * @param  {string} attachments[].type
 * @param  {string} attachments[].data
 * @return {string}
 */
function createAttachments(attachments) {
  if (!attachments || attachments.length === 0) {
    return '';
  }

  var result = [];
  for (var i = 0; i < attachments.length; i++) {
    var a = attachments[i];
    result = result.concat([
      '--foo_bar\r\n',
      'Content-Type: ', a.type, '\r\n',
      'MIME-Version: 1.0\r\n',
      'Content-Transfer-Encoding: base64\r\n',
      'Content-Disposition: attachment', (a.name ? '; filename="' + a.name + '"' : ''), '\r\n\r\n',

      a.data, '\r\n\r\n'
    ]);
  }

  return result.join('');
}

/**
 * Creates a message body that can be used for the Gmail API simple upload urls.
 * @param  {object}   params
 * @param  {object}   params.[headers]            - Key-value object representing headers and their values
 * @param  {string}   params.[threadId]           - Id of the thread the message should be put into
 * @param  {string}   params.[textPlain]          - Plain text representation of the message
 * @param  {string}   params.[textHtml]           - Html text representation of the message
 * @param  {object[]} params.[attachments]
 * @param  {string}   params.attachments[].type   - Attachment type ('image/jpeg', 'image/png', ...)
 * @param  {string}   params.attachments[].[name] - Name of the attachment
 * @param  {string}   params.attachments[].data   - Base64 representation of the attachment data
 * @return {string}
 */
function createBody(params) {
  // var json = createJson(params.threadId);
  var headers = createHeaders(params.headers);
  var text = createText(params.textPlain, params.textHtml);
  var attachments = createAttachments(params.attachments);

  return [
    // '--foo_bar_baz\r\n',
    // json, '\r\n\r\n',

    // '--foo_bar_baz\r\n',
    // 'Content-Type: message/rfc822; boundary="foo_bar"\r\n',

    // 'Content-Type: multipart/alternative; charset="UTF-8"; boundary="foo_bar"\r\n',
    'Content-Type: multipart/mixed; boundary="foo_bar"\r\n',
    'MIME-Version: 1.0\r\n',
    headers, '\r\n\r\n',

    '--foo_bar\r\n',
    text, '\r\n\r\n',

    attachments,

    '--foo_bar--'

    // '--foo_bar_baz--'
  ].join('');
}


function send_gmail_email(headers_obj, message, callback) {
    $("#email-send-loader").show();

    var body = createBody({
        headers: headers_obj,
        textHtml: message,
        // textPlain: 'Thanks for last time, *buddy.*',
        // threadId: '1536195a8ad6a354',
        attachments: ATTACHMENTS_LIST
    });
    // console.log(body);
    // console.log(window.btoa(body).replace(/\+/g, '-').replace(/\//g, '_'));
    // body = Base64.encodeURI(body);
    // var Base64 = require('js-base64').Base64;

    var sendRequest = gapi.client.gmail.users.messages.send({
      'userId': 'me',
      // 'resource': body
      'resource': {
        'threadId': headers_obj['threadId'],
        // 'raw': Base64.encodeURI(body)
        // 'raw': base64EncodeUnicode(body)
        // 'raw': btoa(unescape(encodeURIComponent(body))).replace(/\+/g, '-').replace(/\//g, '_')
        // 'raw': Base64.encodeURI(body)
        // 'raw': window.btoa(body)
        'raw': window.btoa(body).replace(/\+/g, '-').replace(/\//g, '_')
      }
    });

    return sendRequest.execute(callback);



    // var email = '';
    //
    // for(var header in headers_obj)
    //   email += header += ": "+headers_obj[header]+"\r\n";
    //
    // email += "\r\n" + message + "\r\n" + createAttachments(ATTACHMENTS_LIST);
    //
    // // console.log(headers_obj['threadId']);
    // var sendRequest = gapi.client.gmail.users.messages.send({
    //   'userId': 'me',
    //   'resource': {
    //     'threadId': headers_obj['threadId'],
    //     'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
    //   }
    // });
    //
    // return sendRequest.execute(callback);

}


function base64EncodeUnicode(str) {
    // First we escape the string using encodeURIComponent to get the UTF-8 encoding of the characters,
    // then we convert the percent encodings into raw bytes, and finally feed it to btoa() function.
    var utf8Bytes = encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
            return String.fromCharCode('0x' + p1);
    });

    return btoa(utf8Bytes);
}




var email_user_table = $('#email-user-table').DataTable({
    "initComplete": function(settings, json) {
        var api = this.api();
        var temp = JSON.parse(localStorage.getItem('selected_users'));
        $.ajax({
            "url": "/accounts/get_selected_users/",
            data: {
              'selected_users': temp
            },
            type: "POST",
            "success": function(res, status, xhr) {
                $.each(res.users, function(key, value){
                    api.row.add( [
                        value.pk,
                        value.fields.username,
                        value.fields.company_name,
                        value.fields.first_name,
                        value.fields.sales_manager,
                        value.fields.region_state,
                        value.fields.email


        //                 value.fields.cut,
        //                 (value.fields.certificate_filename!==' '&&value.fields.certificate_filename!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value.fields.certificate_filename+'">View</a>':""),
        //                 (value.fields.diamond_filename!==' '&&value.fields.diamond_filename!==''?'<a class="btn btn-info " style="padding:1px 4px;" target="_blank" href="'+value.fields.diamond_filename+'">View</a>':""),
        //                 $.format.date(value.fields.date_created, longDateFormat),
        //                 value.fields.feed,
                    ] ).draw();
                    TO_EMAILS = TO_EMAILS + ',' +value.fields.email

                });
            },
            "error": function(xhr, error){
                // $("#more-loader").hide();
                console.debug(xhr);
                console.debug(error);
            }

        });

    },

    dom: 'Bfrtip',
    paging: false,
    searching: false,
    ordering: false,
    colReorder: false,
    info: false,
    destroy: false,
    stateSave: false,
    // columns: [
    //     { "orderable": false , targets: 0}
    // ],
    columnDefs: [{
        orderable: false,
        className: 'noVis',
        targets: 0,
        visible: false
    }],
    buttons: [
        // {
        //     extend: 'selectNone',
        //     text: 'Reset Selection',
        //     className: "btn btn-warning"
        // },
        {
            className: "btn btn-primary",
            text: "Add Users",
            action: function (e, dt, node, config)
            {
                window.location.href = "/accounts/users";
            }
        },
        {
            extend: 'selectedSingle',
            text: 'Edit',
            className: "btn btn-info",
            action: function ( e, dt, node, config ) {
                $("#edit-user-Modal").modal('show');
                $('#edit-user-form fieldset').removeAttr("disabled");
                $('.modal-title').text('Edit User');
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                      get_user(this[0]);
                });

                // Enable disabled button
                var button = $("#wizard").find('a[href="#' + 'finish' + '-disabled"]');
                button.attr("href", '#' + 'finish');
                button.parent().removeClass();
            }
        },
        {
            extend: 'selected',
            text: 'Delete',
            className: "btn btn-warning",
            action: function ( e, dt, node, config ) {
                var temp = JSON.parse(localStorage.getItem('selected_users'));
                $.each(dt.rows({ selected: true }).data(), function() {
                    var id = this[0];
                    // var index = $.inArray(id.toString(), temp);
                    while($.inArray(id.toString(), temp)!=-1){
                        var index = $.inArray(id.toString(), temp);
                        temp.splice( index, 1 );
                    }
                });
                dt.rows({ selected: true }).remove().draw( false );
                localStorage.setItem('selected_users', JSON.stringify(temp));
            }
        },
        {
            className: "btn btn-danger",
            text: "Delete All",
            action: function ( e, dt, node, config ) {
                dt.rows().remove().draw( false );
                localStorage.setItem('selected_users', JSON.stringify([]));
            }
        }


    ],
    select: {
        // style: 'multi'
        style: 'os'
    }
});


// $("#send_email").unbind().click(function () {
//     // console.log('send email', $('table#email-stone-table').prop('outerHTML'));
//     var temp = JSON.parse(localStorage.getItem('selected_users'));
//     $.ajax({
//         type: "POST",
//         async: false,
//         url: "/accounts/send_email/",
//         data: {
//             'from_email': $('#from_email').val(),
//             'to_email': temp,
//             'cc': $('#cc').val(),
//             'bcc': $('#bcc').val(),
//             'subject': $('#subject').val(),
//             'email_body': $('#email_body').val()
//         },
//         // + $('table#email-stone-table').prop('outerHTML')
//         dataType:"json",
//         success: function(msg) {
//             $('#messages').append(
//             '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
//             msg.msg + '</div>');
//         },
//         error: function(xhr, error){
//             $('#messages').append(
//             '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
//             xhr.responseJSON.msg + '</div>');
//         },
//         complete: function(data) {
//             // window.location.href = "/accounts/compose_email/";
//         }
//     });
// });



// $("#send_test_email").unbind().click(function () {
//     // console.log('send email', $('table#email-stone-table').prop('outerHTML'));
//     var temp = JSON.parse(localStorage.getItem('selected_users'));
//     $.ajax({
//         type: "POST",
//         async: false,
//         url: "/accounts/send_email/",
//         data: {
//             'from_email': $('#from_email').val(),
//             'to_email': temp,
//             'test': true,
//             'cc': $('#cc').val(),
//             'bcc': $('#bcc').val(),
//             'subject': $('#subject').val(),
//             'email_body': $('#email_body').val()
//         },
//         dataType:"json",
//         success: function(msg) {
//             $('#messages').append(
//             '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
//             msg.msg + '</div>');
//         },
//         error: function(xhr, error){
//             console.log(xhr, error);
//             $('#messages').append(
//             '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
//             xhr.responseJSON.msg + '</div>');
//         },
//         complete: function(data) {
//             // window.location.href = "/accounts/compose_email/";
//         }
//     });
// });

/**
 * Retrieve Threads in the user's mailbox matching query.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} query String used to filter the Threads listed.
 * @param  {Function} callback Function to call when the request is complete.
 */
function listThreads(userId, query, callback) {
  var getPageOfThreads = function(request, result) {
    request.execute(function (resp) {
      result = result.concat(resp.threads);
      var nextPageToken = resp.nextPageToken;
      if (nextPageToken) {
        request = gapi.client.gmail.users.threads.list({
          'userId': userId,
          'q': query,
          'pageToken': nextPageToken
        });
        getPageOfThreads(request, result);
      } else {
        callback(result);
      }
    });
  };
  var request = gapi.client.gmail.users.threads.list({
    'userId': userId,
    'q': query
  });
  getPageOfThreads(request, []);
}


function load_more_threads() {
    displayThreads(current_labels);
    // console.log('load more threads');
}


function search_threads() {
    query = $('#email-search-query').val();
    filter_messages(current_labels);
    // console.log('load more threads');
}

