'use strict';
$(document).ready(function() {

    // $(window).on('resize', function() {
    //     dashboardEcharts();
    // });

    // $(window).on('load', function() {
    //     dashboardEcharts();
    // });


    // $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
    //     dashboardEcharts();
    // });

    //line chart
    // function dashboardEcharts() {
    //     /*line chart*/
    //     var myChart = echarts.init(document.getElementById('main'));
    //     var option = {
    //         tooltip: {
    //             trigger: 'item',
    //             formatter: function(params) {
    //                 var date = new Date(params.value[0]);
    //                 var data = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    //                 return data + '<br/>' + params.value[1] + ', ' + params.value[2];
    //             },
    //             responsive: true
    //         },
    //         dataZoom: {
    //             show: true,
    //             start: 70
    //         },
    //         legend: {
    //             data: ['Profit']
    //         },
    //         grid: {
    //             y2: 80
    //         },
    //         xAxis: [{
    //             type: 'time',
    //             splitNumber: 10
    //         }],
    //         yAxis: [{
    //             type: 'value'
    //         }],
    //         series: [{
    //             name: 'Profit',
    //             type: 'line',
    //             showAllSymbol: true,
    //             symbolSize: function(value) {
    //                 return Math.round(value[2] / 10) + 2;
    //             },
    //             data: (function() {
    //                 var d = [];
    //                 var len = 0;
    //                 var now = new Date();
    //                 var value;
    //                 while (len++ < 200) {
    //                     d.push([
    //                         new Date(2014, 9, 1, 0, len * 10000),
    //                         (Math.random() * 30).toFixed(2) - 0,
    //                         (Math.random() * 100).toFixed(2) - 0
    //                     ]);
    //                 }
    //                 return d;
    //             })()
    //         }]
    //     };
    //     myChart.setOption(option);
    // }


    //for responsive all datatable

    $('#simpletable').DataTable({
        "paging": true,
        "ordering": false,
        "bLengthChange": true,
        "info": true,
        "searching": false
    });

    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    // //    Edit information of user-profile
    // $('#edit-cancel').on('click', function() {
    //
    //     var c = $('#edit-btn').find("i");
    //     c.removeClass('icofont-close');
    //     c.addClass('icofont-edit');
    //     $('.view-info').show();
    //     $('.edit-info').hide();
    //
    // });
    //
    // $('.edit-info').hide();


    // $('#edit-btn').on('click', function() {
    //     var b = $(this).find("i");
    //     var edit_class = b.attr('class');
    //     if (edit_class == 'icofont icofont-edit') {
    //         b.removeClass('icofont-edit');
    //         b.addClass('icofont-close');
    //         $('.view-info').hide();
    //         $('.edit-info').show();
    //     } else {
    //         b.removeClass('icofont-close');
    //         b.addClass('icofont-edit');
    //         $('.view-info').show();
    //         $('.edit-info').hide();
    //     }
    // });



    function handle_active_tab() {
        var url = window.location.pathname;
        var arr = url.split("/");
        if (arr[1]==='accounts') {
            if (arr[2] && arr[2] === 'email_templates') {
                init_template_ckeditor();
            }
            if (arr[2] && arr[2] === 'user-profile') {
                init_signature_ckeditor();
                init_template_ckeditor();
                populate_gallery(true);
            }
        }

        url = window.location.search;
        arr = url.split("=");
        // console.log(window.location.search, arr);
        if (arr[0]==='?tab' && arr[1]==='template') {
            $('#email-template').addClass('active');
            $('#email-signature').removeClass('active');
            $('a[href*="#email-template"]').addClass('active');
            $('a[href*="#email-signature"]').removeClass('active');
            // $("ul#mytab").children("li:first").children("a:first").addClass('active');
            // console.log('second tab');

            // user_filters_data();
            // window.localStorage.removeItem('keyName');
        }
    }
    handle_active_tab();

    // var signature_ckeditor5;
    function init_signature_ckeditor() {
        // CKEDITOR 5 START
        // DecoupledEditor.create( document.querySelector( '#signature' ), {
        //     ckfinder: {
        //         uploadUrl: '/accounts/upload_image/?private=false'
        //     }
        // }).then( editor => {
        //     // console.log( 'Editor initialized');
        //     signature_ckeditor5 = editor;
        //     var old_signature = $('#old-signature').text();
        //     // console.log(old_signature);
        //     if (old_signature !== "") {
        //         signature_ckeditor5.setData(old_signature);
        //     }
        //     const toolbarContainer = document.querySelector( '#signature-toolbar-container' );
        //     toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        //
        //     editor.model.document.on('change:data', () => {
        //         // console.log('The data has changed!', );
        //         // $("#email_body").val(editor.getData());
        //         $( "#edit-save-btn" ).prop( "disabled", false );
        //     });
        //
        // }).catch( error => {
        //     console.error( error );
        // });
        // CKEDITOR 5 END



        // CKEDITOR 4 START
        CKEDITOR.replace('signature_editor', {

            // events
            on: {
                instanceReady: function( evt ) {
                    // console.log('CK 4 instance ready');
                    var old_signature = $('#old-signature').text();
                    // console.log(old_signature);
                    if (old_signature !== "") {
                        evt.editor.setData(old_signature);
                    }

                }
            },

            // Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
            // The standard preset from CDN which we used as a base provides more features than we need.
            // Also by default it comes with a 2-line toolbar. Here we put all buttons in a single row.
            toolbar: [{
                name: 'clipboard',
                items: ['Undo', 'Redo']
            }, {
                name: 'styles',
                items: ['Styles', 'Format', 'Font', 'FontSize']
            }, {
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
            }, {
                name: 'paragraph',
                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
            }, {
                name: 'links',
                items: ['Link', 'Unlink']
            }, {
                name: 'colors',
                items: ['TextColor', 'BGColor']
            }, {
                name: 'align',
                items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            // }, {
            //     name: 'color',
            //     items: ['TextColor2', 'BGColor2']
            }, {
                name: 'insert',
                items: ['Image']    //, 'EmbedSemantic', 'Table', 'StonesNSummary'
            }, {
                name: 'tools',
                items: ['Maximize']
            // }, {
            //     name: 'editing',
            //     items: ['Scayt']
            }
            ],

            // colorButton_enableAutomatic = false;
            // Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
            // One HTTP request less will result in a faster startup time.
            // For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
            customConfig: '',
            // extraAllowedContent: 'img[width,height,align]',


            // Enabling extra plugins, available in the standard-all preset: http://ckeditor.com/presets-all
            extraPlugins: 'font,image2,uploadimage,uploadfile,stones_n_summary,colorbutton,justify',
            // autoembed,embedsemantic,enhancedcolorbutton,panelbutton
            imageUploadUrl: '/accounts/upload_image/?private=false',
            uploadUrl: '/accounts/upload_image/?private=false',
            /*********************** File management support ***********************/
            // In order to turn on support for file uploads, CKEditor has to be configured to use some server side
            // solution with file upload/management capabilities, like for example CKFinder.
            // For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration

            // Uncomment and correct these lines after you setup your local CKFinder instance.
            filebrowserBrowseUrl: '/accounts/gallery/',
            filebrowserUploadUrl: '/accounts/upload_image/?private=false',
            /*********************** File management support ***********************/

            // Remove the default image plugin because image2, which offers captions for images, was enabled above.
            removePlugins: 'image',        //,colorbutton

            // Make the editing area bigger than default.
            height: 400,


            // This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
            bodyClass: 'article-editor',

            // Reduce the list of block elements listed in the Format dropdown to the most commonly used.
            format_tags: 'p;h1;h2;h3;pre',

            // Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
            // removeDialogTabs: 'image:advanced;link:advanced',

            // Define the list of styles which should be available in the Styles dropdown list.
            // If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
            // (and on your website so that it rendered in the same way).
            // Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
            // that file, which means one HTTP request less (and a faster startup).
            // For more information see http://docs.ckeditor.com/#!/guide/dev_styles
            stylesSet: [
                /* Inline Styles */
                {
                    name: 'Marker',
                    element: 'span',
                    attributes: {
                        'class': 'marker'
                    }
                }, {
                    name: 'Cited Work',
                    element: 'cite'
                }, {
                    name: 'Inline Quotation',
                    element: 'q'
                },

                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                }, {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                }, {
                    name: 'Borderless Table',
                    element: 'table',
                    styles: {
                        'border-style': 'hidden',
                        'background-color': '#E6E6FA'
                    }
                }, {
                    name: 'Square Bulleted List',
                    element: 'ul',
                    styles: {
                        'list-style-type': 'square'
                    }
                },
                // Media embed
                {
                    name: '240p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-240p'
                    }
                }, {
                    name: '360p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-360p'
                    }
                }, {
                    name: '480p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-480p'
                    }
                }, {
                    name: '720p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-720p'
                    }
                }, {
                    name: '1080p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-1080p'
                    }
                }
            ]
        });
        // CKEDITOR 4 END
    }


    // save button handler
    $('#edit-save-btn').on('click', function() {
        var data = CKEDITOR.instances.signature_editor.getData();
        // var data = signature_ckeditor5.getData();
        // console.log(data);
        $.ajax({
            url: '/accounts/update_signature/',
            data: {
              'signature': data,
              'signature_settings': settings
            },
            type: "POST",
            // async: false,
            dataType: 'json',
            success: function (response) {
                $('#messages').append(
                '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                'Success! Signature Updated' + '</div>');
            },
            error: function(xhr, error){
                $('#messages').append(
                '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                'Failed! Something went wrong, please refresh the page' + '</div>');
            }
        });

    });


    //edit user description
    $('#edit-cancel-btn').on('click', function() {

        // var c = $('#edit-info-btn').find("i");
        // c.removeClass('icofont-close');
        // c.addClass('icofont-edit');
        // $('.view-desc').show();
        // $('.edit-desc').hide();

    });

    $('#old-signature').hide();


    var settings = true;
    // initial case
    if ($('#sig_fal').is(':checked')) {
        settings = false;
    }
    // change case
    $('input:radio[name="signature_type"]').change(function(){
        if ($(this).is(':checked') && $(this).val() === '0'){
            settings = false;
        }
        if ($(this).is(':checked') && $(this).val() === '1'){
            settings = true;
        }
    });



    // var template_ckeditor5;
    function init_template_ckeditor() {
        // CKEDITOR 5 START
        // DecoupledEditor.create( document.querySelector( '#template-editor'), {
        //     ckfinder: {
        //         uploadUrl: '/accounts/upload_image/?private=false'
        //     }
        // }).then( editor => {
        //     // console.log( 'Editor initialized');
        //     template_ckeditor5 = editor;
        //     const toolbarContainer = document.querySelector( '#template-toolbar-container' );
        //     toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        //
        //     editor.model.document.on('change:data', () => {
        //         // console.log('The data has changed!', );
        //         $( "#template-save-btn" ).prop( "disabled", false );
        //     });
        //
        // }).catch( error => {
        //     console.error( error );
        // });
        // CKEDITOR 5 END



        // CKEDITOR 4 START
        CKEDITOR.replace('template_editor', {

            // events
            on: {
                instanceReady: function( evt ) {
                    // console.log('CK 4 instance ready');
                }
            },

            // Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
            // The standard preset from CDN which we used as a base provides more features than we need.
            // Also by default it comes with a 2-line toolbar. Here we put all buttons in a single row.
            toolbar: [{
                name: 'clipboard',
                items: ['Undo', 'Redo']
            }, {
                name: 'styles',
                items: ['Styles', 'Format', 'Font', 'FontSize']
            }, {
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
            }, {
                name: 'paragraph',
                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
            }, {
                name: 'links',
                items: ['Link', 'Unlink']
            }, {
                name: 'colors',
                items: ['TextColor', 'BGColor']
            }, {
                name: 'align',
                items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            // }, {
            //     name: 'color',
            //     items: ['TextColor2', 'BGColor2']
            }, {
                name: 'insert',
                items: ['Image']    //, 'EmbedSemantic', 'Table', 'StonesNSummary'
            }, {
                name: 'tools',
                items: ['Maximize']
            // }, {
            //     name: 'editing',
            //     items: ['Scayt']
            }
            ],

            // colorButton_enableAutomatic = false;
            // Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
            // One HTTP request less will result in a faster startup time.
            // For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
            customConfig: '',
            // extraAllowedContent: 'img[width,height,align]',


            // Enabling extra plugins, available in the standard-all preset: http://ckeditor.com/presets-all
            extraPlugins: 'font,image2,uploadimage,uploadfile,stones_n_summary,colorbutton,justify',
            // autoembed,embedsemantic,enhancedcolorbutton,panelbutton
            imageUploadUrl: '/accounts/upload_image/?private=false',
            uploadUrl: '/accounts/upload_image/?private=false',
            /*********************** File management support ***********************/
            // In order to turn on support for file uploads, CKEditor has to be configured to use some server side
            // solution with file upload/management capabilities, like for example CKFinder.
            // For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration

            // Uncomment and correct these lines after you setup your local CKFinder instance.
            filebrowserBrowseUrl: '/accounts/gallery/',
            filebrowserUploadUrl: '/accounts/upload_image/?private=false',
            /*********************** File management support ***********************/

            // Remove the default image plugin because image2, which offers captions for images, was enabled above.
            removePlugins: 'image',        //,colorbutton

            // Make the editing area bigger than default.
            height: 400,


            // This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
            bodyClass: 'article-editor',

            // Reduce the list of block elements listed in the Format dropdown to the most commonly used.
            format_tags: 'p;h1;h2;h3;pre',

            // Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
            // removeDialogTabs: 'image:advanced;link:advanced',

            // Define the list of styles which should be available in the Styles dropdown list.
            // If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
            // (and on your website so that it rendered in the same way).
            // Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
            // that file, which means one HTTP request less (and a faster startup).
            // For more information see http://docs.ckeditor.com/#!/guide/dev_styles
            stylesSet: [
                /* Inline Styles */
                {
                    name: 'Marker',
                    element: 'span',
                    attributes: {
                        'class': 'marker'
                    }
                }, {
                    name: 'Cited Work',
                    element: 'cite'
                }, {
                    name: 'Inline Quotation',
                    element: 'q'
                },

                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                }, {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                }, {
                    name: 'Borderless Table',
                    element: 'table',
                    styles: {
                        'border-style': 'hidden',
                        'background-color': '#E6E6FA'
                    }
                }, {
                    name: 'Square Bulleted List',
                    element: 'ul',
                    styles: {
                        'list-style-type': 'square'
                    }
                },
                // Media embed
                {
                    name: '240p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-240p'
                    }
                }, {
                    name: '360p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-360p'
                    }
                }, {
                    name: '480p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-480p'
                    }
                }, {
                    name: '720p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-720p'
                    }
                }, {
                    name: '1080p',
                    type: 'widget',
                    widget: 'embedSemantic',
                    attributes: {
                        'class': 'embed-1080p'
                    }
                }
            ]
        });
        // CKEDITOR 4 END

    }


    var template_table_server = $('#template-table-server').DataTable({
        "initComplete": function(settings, json) {
            // $('.dataTables_scrollBody').css('min-height', '600px');
            // this.api().columns.adjust();
        },

        // dom: 'Bfritip',
        dom: 'Biti',
        "serverSide": true,
        "searching": false,
        "colReorder": false,
        "ordering": false,

        buttons: [
            {
                className: "btn btn-primary",
                text: "Add New Template",
                action: function (e, dt, node, config) {
                    window.location.href = "/accounts/user-profile?tab=template";
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Edit',
                className: "btn waves-effect",
                action: function ( e, dt, node, config ) {
                    // console.log('Here we go');
                    $("#edit-template-Modal").modal('show');
                    $.each(dt.rows({ selected: true }).data(), function() {
                        // console.log(this['DT_RowId'], this);
                        $('#template-id').val(this['0']);
                        $('#template-name').val(this['1']);
                        $('#template-subject').val(this['3']);
                        $('#template-language').val(this['2']);
                        // template_ckeditor5.setData(this['6']);
                        CKEDITOR.instances.template_editor.setData(this['6']);
                        // $('#template-name').val(this['1']);
                        // get_user(this['DT_RowId']);
                    });
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                className: "btn btn-danger",
                action: function ( e, dt, node, config ) {
                    var arr = [];
                    $.each(dt.rows({ selected: true }).data(), function() {
                        arr.push(this[0]);
                    });
                    delete_templates(arr, dt);
                }
            }

        ],

        columnDefs: [
            {
                targets: [0,6],
                className: 'noVis',
                visible: false
            }
        ],
        ajax: function ( data, callback, settings ) {
            $.ajax({
            url: '/accounts/dt_list_templates/',
            data: Object.assign({}, data, {
              // 'custom_order': stones_col_order_local,
              // 'shape': shape_list,
              // 'id': id_list,
              // 'weight_from': weight_from,
              // 'weight_to': weight_to,
              // 'color': color_list,
              // 'clarity': clarity_list,
              // 'lab': lab_list,
              // 'location': location_list,
              // 'feed': feed_list,
              // 'polish': polish_list,
              // 'symmetry': symmetry_list,
              // 'cut': cut_list,
              // 'fluorescence': fluorescence_list,
              // 'depth_from': depth_from,
              // 'depth_to': depth_to,
              // 'table_from': table_from,
              // 'table_to': table_to,
              // 'discount_from': discount_from,
              // 'discount_to': discount_to,
            }),
            type: "POST",
            dataType: 'json',
            success: function (data) {
                count = data.count;
                next = data.next;
                previous = data.previous;
                var out = [];
                $.each(data.data, function(key, value) {
                    // console.log(value);
                    // out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
                    // this.api.row.add( [
                    out.push( {
                    "0": value[0],
                    "1": value[1],
                    "2": value[2],
                    "3": value[3],
                    "4": value[4],
                    "5": value[5],
                    "6": value[6],
                    // "10": '<a class="btn btn-info " style="padding: 1px 4px;" onclick="repeat_search(' + params + ')">Repeat Search</a>',
                    "DT_RowId": value[0]
                    } );

                });

                callback( {
                    draw: data.draw,
                    data: out,
                    recordsTotal: data.recordsTotal,
                    recordsFiltered: data.recordsFiltered
                } );

            }
            });

        },
        scrollY: "100%",
        // scrollX: "100%",
        // scrollXInner: "125%",  //Use This (Add This Line)
        // scrollCollapse: true,
        // scrollInfinite: true,
        scroller: {
            loadingIndicator: true
        },
        select: {
            // style: 'multi'
            style: 'os'
        }

    });


    function delete_templates(arr, dt) {
      $.ajax({
        url: '/accounts/delete_templates/',
        data: {
          'ids': arr
        },
        type: "POST",
        dataType: 'json',
        success: function (data) {
            $('#messages').append(
                '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                data.message + '</div>');
            dt.rows({ selected: true }).remove().draw( false );
        },
        error: function(xhr, error){
            $('#messages').append(
            '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            xhr.responseJSON.error + '</div>');
            console.debug(xhr); console.debug(error);
        }
      });
    }


    // update button handler
    $('#template-update-btn').on('click', function() {
        // var data = template_ckeditor5.getData();
        var data = CKEDITOR.instances.template_editor.getData();

        // console.log($('#template-body').val());
        $.ajax({
            url: '/accounts/update_template/',
            data: {
              'id': $('#template-id').val(),
              'name': $('#template-name').val(),
              'language': $('#template-language').val(),
              'subject': $('#template-subject').val(),
              'body': data
            },
            type: "POST",
            // async: false,
            dataType: 'json',
            success: function (response) {
                $("#edit-template-Modal").modal('hide');
                $('#template-id').val("");
                $('#template-name').val("");
                $('#template-language').val("EN");
                $('#template-subject').val("");
                // template_ckeditor5.setData("");
                CKEDITOR.instances.template_editor.setData("");
                template_table_server.ajax.reload();

                $('#messages').append(
                '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                response.message + '</div>');
            },
            error: function(xhr, error){
                $('#messages').append(
                '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                xhr.responseJSON.error + '</div>');
            }
        });
    });



    // save button handler
    $('#template-save-btn').on('click', function() {
        var data = CKEDITOR.instances.template_editor.getData();
        // var data = template_ckeditor5.getData();
        // console.log($('#template-body').val());
        $.ajax({
            url: '/accounts/create_template/',
            data: {
              'name': $('#template-name').val(),
              'language': $('#template-language').val(),
              'subject': $('#template-subject').val(),
              'body': data
            },
            type: "POST",
            // async: false,
            dataType: 'json',
            success: function (response) {
                $('#template-name').val("");
                $('#template-language').val("EN");
                $('#template-subject').val("");
                // template_ckeditor5.setData("");
                CKEDITOR.instances.template_editor.setData("");

                $('#messages').append(
                '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                response.message + '</div>');
            },
            error: function(xhr, error){
                $('#messages').append(
                '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                xhr.responseJSON.error + '</div>');
            }
        });

    });


    //edit user description
    $('#template-cancel-btn').on('click', function() {

    });

    $('#template-old-signature').hide();

    //END Email JS







    // $('.edit-desc').hide();


    // $('#edit-info-btn').on('click', function() {
    //     console.log('setting data');
    //     var signature = "<blockquote>\n" +
    //         "<p>Ateeq Suhail</p>\n" +
    //         "\n" +
    //         "<p>Full Stack Developer</p>\n" +
    //         "</blockquote>";
    //     // var data = CKEDITOR.instances.description.setData(signature);
    //     var data = signature_ckeditor5.setData(signature);
    //
    //     var b = $(this).find("i");
    //     var edit_class = b.attr('class');
    //     if (edit_class == 'icofont icofont-edit') {
    //         b.removeClass('icofont-edit');
    //         b.addClass('icofont-close');
    //         $('.view-desc').hide();
    //         $('.edit-desc').show();
    //     } else {
    //         b.removeClass('icofont-close');
    //         b.addClass('icofont-edit');
    //         $('.view-desc').show();
    //         $('.edit-desc').hide();
    //     }
    // });


    // Minimum setup
    // $('#datetimepicker1').datetimepicker({
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // Using Locales
    // $('#datetimepicker2').datetimepicker({
    //     locale: 'ru',
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // Custom Formats
    // $('#datetimepicker3').datetimepicker({
    //     format: 'LT',
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // No Icon (input field only)
    // $('#datetimepicker4').datetimepicker({
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // Enabled/Disabled Dates
    // $('#datetimepicker5').datetimepicker({
    //     defaultDate: "11/1/2013",
    //     disabledDates: [
    //         moment("12/25/2013"),
    //         new Date(2013, 11 - 1, 21),
    //         "11/22/2013 00:53"
    //     ],
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // Linked Pickers
    // $('#datetimepicker6').datetimepicker({
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // $('#datetimepicker7').datetimepicker({
    //     useCurrent: false, //Important! See issue #1075
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // $("#datetimepicker6").on("dp.change", function(e) {
    //     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    // });
    // $("#datetimepicker7").on("dp.change", function(e) {
    //     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    // });

    // Custom icons
    // $('#datetimepicker8').datetimepicker({
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down"
    //     }
    // });

    // View Mode
    // $('#datetimepicker9').datetimepicker({
    //     viewMode: 'years',
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });

    // Min View Mode
    // $('#datetimepicker10').datetimepicker({
    //     viewMode: 'years',
    //     format: 'MM/YYYY',
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });
    // Disabled Days of the Week
    // $('#datetimepicker11').datetimepicker({
    //     daysOfWeekDisabled: [0, 6],
    //     icons: {
    //         time: "icofont icofont-clock-time",
    //         date: "icofont icofont-ui-calendar",
    //         up: "icofont icofont-rounded-up",
    //         down: "icofont icofont-rounded-down",
    //         next: "icofont icofont-rounded-right",
    //         previous: "icofont icofont-rounded-left"
    //     }
    // });

    // $('input[name="daterange"]').daterangepicker();
    // $(function() {
    //     $('input[name="birthdate"]').daterangepicker({
    //             singleDatePicker: true,
    //             showDropdowns: true
    //         },
    //         function(start, end, label) {
    //             var years = moment().diff(start, 'years');
    //             alert("You are " + years + " years old.");
    //         });
    //
    //     $('input[name="datefilter"]').daterangepicker({
    //         autoUpdateInput: false,
    //         locale: {
    //             cancelLabel: 'Clear'
    //         }
    //     });
    //     $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
    //         $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    //     });
    //
    //     $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
    //         $(this).val('');
    //     });
    //
    //     var start = moment().subtract(29, 'days');
    //     var end = moment();
    //
    //     function cb(start, end) {
    //         $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    //     }
    //
    //     $('#reportrange').daterangepicker({
    //         startDate: start,
    //         endDate: end,
    //         "drops": "up",
    //         ranges: {
    //             'Today': [moment(), moment()],
    //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //             'This Month': [moment().startOf('month'), moment().endOf('month')],
    //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //         }
    //     }, cb);
    //
    //     cb(start, end);
    //
    //     $('.input-daterange input').each(function() {
    //         $(this).datepicker();
    //     });
    //     $('#sandbox-container .input-daterange').datepicker({
    //         todayHighlight: true
    //     });
    //     $('.input-group-date-custom').datepicker({
    //         todayBtn: true,
    //         clearBtn: true,
    //         keyboardNavigation: false,
    //         forceParse: false,
    //         todayHighlight: true,
    //         defaultViewDate: {
    //             year: '2017',
    //             month: '01',
    //             day: '01'
    //         }
    //     });
    //     $('.multiple-select').datepicker({
    //         todayBtn: true,
    //         clearBtn: true,
    //         multidate: true,
    //         keyboardNavigation: false,
    //         forceParse: false,
    //         todayHighlight: true,
    //         defaultViewDate: {
    //             year: '2017',
    //             month: '01',
    //             day: '01'
    //         }
    //     });
    //     $('#config-demo').daterangepicker({
    //         "singleDatePicker": true,
    //         "showDropdowns": true,
    //         "timePicker": true,
    //         "timePicker24Hour": true,
    //         "timePickerSeconds": true,
    //         "showCustomRangeLabel": false,
    //         "alwaysShowCalendars": true,
    //         "startDate": "11/30/2016",
    //         "endDate": "12/06/2016",
    //         "drops": "up"
    //     }, function(start, end, label) {
    //         console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    //     });
    // });

    // Date-dropper js start

    // $("#dropper-default").dateDropper({
    //         dropWidth: 200,
    //         dropPrimaryColor: "#1abc9c",
    //         dropBorder: "1px solid #1abc9c"
    // });
    // Date-dropper js end




    // Mini-color js start
    // $('.demo').each(function() {
    //         //
    //         // Dear reader, it's actually very easy to initialize MiniColors. For example:
    //         //
    //         //  $(selector).minicolors();
    //         //
    //         // The way I've done it below is just for the demo, so don't get confused
    //         // by it. Also, data- attributes aren't supported at this time...they're
    //         // only used for this demo.
    //         //
    //         $(this).minicolors({
    //             control: $(this).attr('data-control') || 'hue',
    //             defaultValue: $(this).attr('data-defaultValue') || '',
    //             format: $(this).attr('data-format') || 'hex',
    //             keywords: $(this).attr('data-keywords') || '',
    //             inline: $(this).attr('data-inline') === 'true',
    //             letterCase: $(this).attr('data-letterCase') || 'lowercase',
    //             opacity: $(this).attr('data-opacity'),
    //             position: $(this).attr('data-position') || 'bottom left',
    //             swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
    //             change: function(value, opacity) {
    //                 if (!value) return;
    //                 if (opacity) value += ', ' + opacity;
    //                 if (typeof console === 'object') {
    //                     console.log(value);
    //                 }
    //             },
    //             theme: 'bootstrap'
    //         });
    //
    //     });
    // Mini-color js ends
});
