'use strict';
$(document).ready(function() {
    // console.log('in gallery');
    var url = window.location.pathname;
    var arr = url.split("/");
    if (arr[1]==='accounts') {
        if (arr[2] && arr[2] === 'gallery') {
            populate_gallery(false);
            populate_gallery(true);
        }
    }

});



function populate_gallery(is_private) {
  $.ajax({
    url: '/accounts/list_images?private=' + is_private,
    type: "GET",
    dataType: 'json',
    success: function (res) {
      // console.log(res);
      // res = JSON.parse(res);
      var temp = 0;
      $.each(res.files, function(key, value) {
        var col_index = temp%4;
        insert_figure(value, col_index, is_private);
        temp += 1;
      });
      // $("#gallery-Modal").modal('show');
      // window.close();
    },
    error: function(xhr, error){
        console.debug(xhr); console.debug(error);
    }
  });
}

// Copies a string to the clipboard. Must be called from within an event handler such as click.
// May return false if it failed, but this is not always
// possible. Browser support for Chrome 43+, Firefox 42+, Edge and IE 10+.
// No Safari support, as of (Nov. 2015). Returns false.
// IE: The clipboard feature may be disabled by an adminstrator. By default a prompt is
// shown the first time the clipboard is used (per session).
function copyToClipboard(url, temp_id) {
    // console.log('in func', url);
    if (window.clipboardData && window.clipboardData.setData) {
        // IE specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", url.toString());

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        $(temp_id).val(url).select();
        // .setSelectionRange(0, 99999);

        try {
            // console.log('here', input.select());
            document.execCommand("copy");  // Security exception may be thrown by some browsers.
            $('#messages').append(
                '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                'Success! Image URL copied to clipboard' + '</div>');
            return true;
        } catch (ex) {
            $('#messages').append(
            '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Failed! Something went wrong' + '</div>');
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            // $("#gallery-Modal").modal('hide');
        }
    }
}


function delete_image(url) {
    var response = get_name_id_privacy(url);
    var image_name = response[0];
    var image_id = response[1];
    var is_private = response[2];

    // console.log('to be deleted');
  $.ajax({
    url: '/accounts/delete_image/',
    data: {
        "id": image_name,
        "private": is_private
    },
    type: 'POST',
    dataType: 'json',
    success: function (res) {
      // console.log(res);
      // res = JSON.parse(res);
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! File has been deleted' + '</div>');
        $("#" + image_id).remove();
        // $("#gallery-Modal").modal('hide');
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        console.debug(xhr); console.debug(error);
    }
  });
}


// elem3.onclick = function () {
//     document.execCommand('copy');
// }
//
// elem3.addEventListener('copy', function (e) {
//
//     e.preventDefault();
//     if (e.clipboardData) {
//         e.clipboardData.setData('text/plain', 'custom content from click');
//     } else if (window.clipboardData) {
//         window.clipboardData.setData('Text', 'custom content from click');
//     }
//
// });

// Option 1
// $("a[title='Browse Server']").click(function(){
//     alert("a")
// });
//
// $('a[title="Browse Server"]').unbind().click(function (e) {
//     console.log('clicked');
//     e.preventDefault();
// });

function get_name_id_privacy(url) {
    var arr = url.split("/");
    var is_private = false;
    var image_name = '';

    if(arr[4] === 'private_gallery') {
        image_name = arr[6];
        is_private = true;
    } else {
        image_name = arr[5];
    }
    var image_id = image_name.split(".")[0];

    return [image_name, image_id, is_private];
}


function insert_figure(url, col_index, is_private) {
    col_index = col_index || 0;
    var response = get_name_id_privacy(url);
    // var image_name = response[0];
    var image_id = response[1];
    // var is_private = response[2];
    // console.log(value.replace(/['"]+/g, ''));
    // var tempered_value = value.replace(/['"]+/g, '');
    var temp_id = '#tab-public #col-' + col_index.toString();
    var temp_id2 = '#tab-public #image_url';

    if (is_private) {
        temp_id = '#tab-private #col-' + col_index.toString();
        temp_id2 = '#tab-private #image_url';
    }

    $(temp_id).append(
      '<figure class="effect-kira" id="' + image_id + '" style="border: 1px solid #01787b; margin-bottom: 5px;">' +
          '<img src="' + url + '" alt="image" />' +
           '<figcaption style="margin-top: -40%;">' +
                '<h2><span></span></h2>' +
                '<p>' +
                    '<a href="#" onclick="copyToClipboard(\''+ url +'\',\''+ temp_id2 +'\')"><i class="fa fa-fw fa-clipboard"></i></a>' +
                    // '<a href="#" onclick="copyToClipboard(\'' + url + '\')"><i class="fa fa-fw fa-clipboard"></i></a>' +
                    '<a href="#" onclick="delete_image(\'' + url + '\')"><i class="fa fa-fw fa-trash"></i></a>' +
                '</p>' +
            '</figcaption>' +
      '</figure>'
    );

}


$('#process-file-button').on('click', function (e) {
    var files = new FormData(), // you can consider this as 'data bag'
        url = '/accounts/upload_image/?private=true';

    files.append('upload', $('#file')[0].files[0]); // append selected file to the bag named 'file'

    $.ajax({
        type: 'post',
        url: url,
        processData: false,
        contentType: false,
        data: files,
        success: function (response) {
            $("#file").val('');
            $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! File Upload Complete' + '</div>');
            // window.reload(true);
            insert_figure(response.url, 0, true);
            // console.log(response);
        },
        error: function (err) {
            $('#messages').append(
            '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Failed! Something went wrong, please report' + '</div>');
            // console.log(err);
        }
    });
});


