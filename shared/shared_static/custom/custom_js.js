var col_order_local = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,
    36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
var users_col_vis_local = [{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}];

var user_type_list = [];
var countries = [{"countryName":"Afghanistan","iso2":"AF","iso3":"AFG","phoneCode":"93"},{"countryName":"Albania","iso2":"AL","iso3":"ALB","phoneCode":"355"},{"countryName":"Algeria","iso2":"DZ","iso3":"DZA","phoneCode":"213"},{"countryName":"American Samoa","iso2":"AS","iso3":"ASM","phoneCode":"1 684"},{"countryName":"Andorra","iso2":"AD","iso3":"AND","phoneCode":"376"},{"countryName":"Angola","iso2":"AO","iso3":"AGO","phoneCode":"244"},{"countryName":"Anguilla","iso2":"AI","iso3":"AIA","phoneCode":"1 264"},{"countryName":"Antarctica","iso2":"AQ","iso3":"ATA","phoneCode":"672"},{"countryName":"Antigua and Barbuda","iso2":"AG","iso3":"ATG","phoneCode":"1 268"},{"countryName":"Argentina","iso2":"AR","iso3":"ARG","phoneCode":"54"},{"countryName":"Armenia","iso2":"AM","iso3":"ARM","phoneCode":"374"},{"countryName":"Aruba","iso2":"AW","iso3":"ABW","phoneCode":"297"},{"countryName":"Australia","iso2":"AU","iso3":"AUS","phoneCode":"61"},{"countryName":"Austria","iso2":"AT","iso3":"AUT","phoneCode":"43"},{"countryName":"Azerbaijan","iso2":"AZ","iso3":"AZE","phoneCode":"994"},{"countryName":"Bahamas","iso2":"BS","iso3":"BHS","phoneCode":"1 242"},{"countryName":"Bahrain","iso2":"BH","iso3":"BHR","phoneCode":"973"},{"countryName":"Bangladesh","iso2":"BD","iso3":"BGD","phoneCode":"880"},{"countryName":"Barbados","iso2":"BB","iso3":"BRB","phoneCode":"1 246"},{"countryName":"Belarus","iso2":"BY","iso3":"BLR","phoneCode":"375"},{"countryName":"Belgium","iso2":"BE","iso3":"BEL","phoneCode":"32"},{"countryName":"Belize","iso2":"BZ","iso3":"BLZ","phoneCode":"501"},{"countryName":"Benin","iso2":"BJ","iso3":"BEN","phoneCode":"229"},{"countryName":"Bermuda","iso2":"BM","iso3":"BMU","phoneCode":"1 441"},{"countryName":"Bhutan","iso2":"BT","iso3":"BTN","phoneCode":"975"},{"countryName":"Bolivia","iso2":"BO","iso3":"BOL","phoneCode":"591"},{"countryName":"Bosnia and Herzegovina","iso2":"BA","iso3":"BIH","phoneCode":"387"},{"countryName":"Botswana","iso2":"BW","iso3":"BWA","phoneCode":"267"},{"countryName":"Brazil","iso2":"BR","iso3":"BRA","phoneCode":"55"},{"countryName":"British Indian Ocean Territory","iso2":"IO","iso3":"IOT","phoneCode":""},{"countryName":"British Virgin Islands","iso2":"VG","iso3":"VGB","phoneCode":"1 284"},{"countryName":"Brunei","iso2":"BN","iso3":"BRN","phoneCode":"673"},{"countryName":"Bulgaria","iso2":"BG","iso3":"BGR","phoneCode":"359"},{"countryName":"Burkina Faso","iso2":"BF","iso3":"BFA","phoneCode":"226"},{"countryName":"Burma (Myanmar)","iso2":"MM","iso3":"MMR","phoneCode":"95"},{"countryName":"Burundi","iso2":"BI","iso3":"BDI","phoneCode":"257"},{"countryName":"Cambodia","iso2":"KH","iso3":"KHM","phoneCode":"855"},{"countryName":"Cameroon","iso2":"CM","iso3":"CMR","phoneCode":"237"},{"countryName":"Canada","iso2":"CA","iso3":"CAN","phoneCode":"1"},{"countryName":"Cape Verde","iso2":"CV","iso3":"CPV","phoneCode":"238"},{"countryName":"Cayman Islands","iso2":"KY","iso3":"CYM","phoneCode":"1 345"},{"countryName":"Central African Republic","iso2":"CF","iso3":"CAF","phoneCode":"236"},{"countryName":"Chad","iso2":"TD","iso3":"TCD","phoneCode":"235"},{"countryName":"Chile","iso2":"CL","iso3":"CHL","phoneCode":"56"},{"countryName":"China","iso2":"CN","iso3":"CHN","phoneCode":"86"},{"countryName":"Christmas Island","iso2":"CX","iso3":"CXR","phoneCode":"61"},{"countryName":"Cocos (Keeling) Islands","iso2":"CC","iso3":"CCK","phoneCode":"61"},{"countryName":"Colombia","iso2":"CO","iso3":"COL","phoneCode":"57"},{"countryName":"Comoros","iso2":"KM","iso3":"COM","phoneCode":"269"},{"countryName":"Cook Islands","iso2":"CK","iso3":"COK","phoneCode":"682"},{"countryName":"Costa Rica","iso2":"CR","iso3":"CRC","phoneCode":"506"},{"countryName":"Croatia","iso2":"HR","iso3":"HRV","phoneCode":"385"},{"countryName":"Cuba","iso2":"CU","iso3":"CUB","phoneCode":"53"},{"countryName":"Cyprus","iso2":"CY","iso3":"CYP","phoneCode":"357"},{"countryName":"Czech Republic","iso2":"CZ","iso3":"CZE","phoneCode":"420"},{"countryName":"Democratic Republic of the Congo","iso2":"CD","iso3":"COD","phoneCode":"243"},{"countryName":"Denmark","iso2":"DK","iso3":"DNK","phoneCode":"45"},{"countryName":"Djibouti","iso2":"DJ","iso3":"DJI","phoneCode":"253"},{"countryName":"Dominica","iso2":"DM","iso3":"DMA","phoneCode":"1 767"},{"countryName":"Dominican Republic","iso2":"DO","iso3":"DOM","phoneCode":"1 809"},{"countryName":"Ecuador","iso2":"EC","iso3":"ECU","phoneCode":"593"},{"countryName":"Egypt","iso2":"EG","iso3":"EGY","phoneCode":"20"},{"countryName":"El Salvador","iso2":"SV","iso3":"SLV","phoneCode":"503"},{"countryName":"Equatorial Guinea","iso2":"GQ","iso3":"GNQ","phoneCode":"240"},{"countryName":"Eritrea","iso2":"ER","iso3":"ERI","phoneCode":"291"},{"countryName":"Estonia","iso2":"EE","iso3":"EST","phoneCode":"372"},{"countryName":"Ethiopia","iso2":"ET","iso3":"ETH","phoneCode":"251"},{"countryName":"Falkland Islands","iso2":"FK","iso3":"FLK","phoneCode":"500"},{"countryName":"Faroe Islands","iso2":"FO","iso3":"FRO","phoneCode":"298"},{"countryName":"Fiji","iso2":"FJ","iso3":"FJI","phoneCode":"679"},{"countryName":"Finland","iso2":"FI","iso3":"FIN","phoneCode":"358"},{"countryName":"France","iso2":"FR","iso3":"FRA","phoneCode":"33"},{"countryName":"French Polynesia","iso2":"PF","iso3":"PYF","phoneCode":"689"},{"countryName":"Gabon","iso2":"GA","iso3":"GAB","phoneCode":"241"},{"countryName":"Gambia","iso2":"GM","iso3":"GMB","phoneCode":"220"},{"countryName":"Gaza Strip","iso2":"","iso3":"","phoneCode":"970"},{"countryName":"Georgia","iso2":"GE","iso3":"GEO","phoneCode":"995"},{"countryName":"Germany","iso2":"DE","iso3":"DEU","phoneCode":"49"},{"countryName":"Ghana","iso2":"GH","iso3":"GHA","phoneCode":"233"},{"countryName":"Gibraltar","iso2":"GI","iso3":"GIB","phoneCode":"350"},{"countryName":"Greece","iso2":"GR","iso3":"GRC","phoneCode":"30"},{"countryName":"Greenland","iso2":"GL","iso3":"GRL","phoneCode":"299"},{"countryName":"Grenada","iso2":"GD","iso3":"GRD","phoneCode":"1 473"},{"countryName":"Guam","iso2":"GU","iso3":"GUM","phoneCode":"1 671"},{"countryName":"Guatemala","iso2":"GT","iso3":"GTM","phoneCode":"502"},{"countryName":"Guinea","iso2":"GN","iso3":"GIN","phoneCode":"224"},{"countryName":"Guinea-Bissau","iso2":"GW","iso3":"GNB","phoneCode":"245"},{"countryName":"Guyana","iso2":"GY","iso3":"GUY","phoneCode":"592"},{"countryName":"Haiti","iso2":"HT","iso3":"HTI","phoneCode":"509"},{"countryName":"Holy See (Vatican City)","iso2":"VA","iso3":"VAT","phoneCode":"39"},{"countryName":"Honduras","iso2":"HN","iso3":"HND","phoneCode":"504"},{"countryName":"Hong Kong","iso2":"HK","iso3":"HKG","phoneCode":"852"},{"countryName":"Hungary","iso2":"HU","iso3":"HUN","phoneCode":"36"},{"countryName":"Iceland","iso2":"IS","iso3":"IS","phoneCode":"354"},{"countryName":"India","iso2":"IN","iso3":"IND","phoneCode":"91"},{"countryName":"Indonesia","iso2":"ID","iso3":"IDN","phoneCode":"62"},{"countryName":"Iran","iso2":"IR","iso3":"IRN","phoneCode":"98"},{"countryName":"Iraq","iso2":"IQ","iso3":"IRQ","phoneCode":"964"},{"countryName":"Ireland","iso2":"IE","iso3":"IRL","phoneCode":"353"},{"countryName":"Isle of Man","iso2":"IM","iso3":"IMN","phoneCode":"44"},{"countryName":"Israel","iso2":"IL","iso3":"ISR","phoneCode":"972"},{"countryName":"Italy","iso2":"IT","iso3":"ITA","phoneCode":"39"},{"countryName":"Ivory Coast","iso2":"CI","iso3":"CIV","phoneCode":"225"},{"countryName":"Jamaica","iso2":"JM","iso3":"JAM","phoneCode":"1 876"},{"countryName":"Japan","iso2":"JP","iso3":"JPN","phoneCode":"81"},{"countryName":"Jersey","iso2":"JE","iso3":"JEY","phoneCode":""},{"countryName":"Jordan","iso2":"JO","iso3":"JOR","phoneCode":"962"},{"countryName":"Kazakhstan","iso2":"KZ","iso3":"KAZ","phoneCode":"7"},{"countryName":"Kenya","iso2":"KE","iso3":"KEN","phoneCode":"254"},{"countryName":"Kiribati","iso2":"KI","iso3":"KIR","phoneCode":"686"},{"countryName":"Kosovo","iso2":"","iso3":"","phoneCode":"381"},{"countryName":"Kuwait","iso2":"KW","iso3":"KWT","phoneCode":"965"},{"countryName":"Kyrgyzstan","iso2":"KG","iso3":"KGZ","phoneCode":"996"},{"countryName":"Laos","iso2":"LA","iso3":"LAO","phoneCode":"856"},{"countryName":"Latvia","iso2":"LV","iso3":"LVA","phoneCode":"371"},{"countryName":"Lebanon","iso2":"LB","iso3":"LBN","phoneCode":"961"},{"countryName":"Lesotho","iso2":"LS","iso3":"LSO","phoneCode":"266"},{"countryName":"Liberia","iso2":"LR","iso3":"LBR","phoneCode":"231"},{"countryName":"Libya","iso2":"LY","iso3":"LBY","phoneCode":"218"},{"countryName":"Liechtenstein","iso2":"LI","iso3":"LIE","phoneCode":"423"},{"countryName":"Lithuania","iso2":"LT","iso3":"LTU","phoneCode":"370"},{"countryName":"Luxembourg","iso2":"LU","iso3":"LUX","phoneCode":"352"},{"countryName":"Macau","iso2":"MO","iso3":"MAC","phoneCode":"853"},{"countryName":"Macedonia","iso2":"MK","iso3":"MKD","phoneCode":"389"},{"countryName":"Madagascar","iso2":"MG","iso3":"MDG","phoneCode":"261"},{"countryName":"Malawi","iso2":"MW","iso3":"MWI","phoneCode":"265"},{"countryName":"Malaysia","iso2":"MY","iso3":"MYS","phoneCode":"60"},{"countryName":"Maldives","iso2":"MV","iso3":"MDV","phoneCode":"960"},{"countryName":"Mali","iso2":"ML","iso3":"MLI","phoneCode":"223"},{"countryName":"Malta","iso2":"MT","iso3":"MLT","phoneCode":"356"},{"countryName":"Marshall Islands","iso2":"MH","iso3":"MHL","phoneCode":"692"},{"countryName":"Mauritania","iso2":"MR","iso3":"MRT","phoneCode":"222"},{"countryName":"Mauritius","iso2":"MU","iso3":"MUS","phoneCode":"230"},{"countryName":"Mayotte","iso2":"YT","iso3":"MYT","phoneCode":"262"},{"countryName":"Mexico","iso2":"MX","iso3":"MEX","phoneCode":"52"},{"countryName":"Micronesia","iso2":"FM","iso3":"FSM","phoneCode":"691"},{"countryName":"Moldova","iso2":"MD","iso3":"MDA","phoneCode":"373"},{"countryName":"Monaco","iso2":"MC","iso3":"MCO","phoneCode":"377"},{"countryName":"Mongolia","iso2":"MN","iso3":"MNG","phoneCode":"976"},{"countryName":"Montenegro","iso2":"ME","iso3":"MNE","phoneCode":"382"},{"countryName":"Montserrat","iso2":"MS","iso3":"MSR","phoneCode":"1 664"},{"countryName":"Morocco","iso2":"MA","iso3":"MAR","phoneCode":"212"},{"countryName":"Mozambique","iso2":"MZ","iso3":"MOZ","phoneCode":"258"},{"countryName":"Namibia","iso2":"NA","iso3":"NAM","phoneCode":"264"},{"countryName":"Nauru","iso2":"NR","iso3":"NRU","phoneCode":"674"},{"countryName":"Nepal","iso2":"NP","iso3":"NPL","phoneCode":"977"},{"countryName":"Netherlands","iso2":"NL","iso3":"NLD","phoneCode":"31"},{"countryName":"Netherlands Antilles","iso2":"AN","iso3":"ANT","phoneCode":"599"},{"countryName":"New Caledonia","iso2":"NC","iso3":"NCL","phoneCode":"687"},{"countryName":"New Zealand","iso2":"NZ","iso3":"NZL","phoneCode":"64"},{"countryName":"Nicaragua","iso2":"NI","iso3":"NIC","phoneCode":"505"},{"countryName":"Niger","iso2":"NE","iso3":"NER","phoneCode":"227"},{"countryName":"Nigeria","iso2":"NG","iso3":"NGA","phoneCode":"234"},{"countryName":"Niue","iso2":"NU","iso3":"NIU","phoneCode":"683"},{"countryName":"Norfolk Island","iso2":"","iso3":"NFK","phoneCode":"672"},{"countryName":"North Korea","iso2":"KP","iso3":"PRK","phoneCode":"850"},{"countryName":"Northern Mariana Islands","iso2":"MP","iso3":"MNP","phoneCode":"1 670"},{"countryName":"Norway","iso2":"NO","iso3":"NOR","phoneCode":"47"},{"countryName":"Oman","iso2":"OM","iso3":"OMN","phoneCode":"968"},{"countryName":"Pakistan","iso2":"PK","iso3":"PAK","phoneCode":"92"},{"countryName":"Palau","iso2":"PW","iso3":"PLW","phoneCode":"680"},{"countryName":"Panama","iso2":"PA","iso3":"PAN","phoneCode":"507"},{"countryName":"Papua New Guinea","iso2":"PG","iso3":"PNG","phoneCode":"675"},{"countryName":"Paraguay","iso2":"PY","iso3":"PRY","phoneCode":"595"},{"countryName":"Peru","iso2":"PE","iso3":"PER","phoneCode":"51"},{"countryName":"Philippines","iso2":"PH","iso3":"PHL","phoneCode":"63"},{"countryName":"Pitcairn Islands","iso2":"PN","iso3":"PCN","phoneCode":"870"},{"countryName":"Poland","iso2":"PL","iso3":"POL","phoneCode":"48"},{"countryName":"Portugal","iso2":"PT","iso3":"PRT","phoneCode":"351"},{"countryName":"Puerto Rico","iso2":"PR","iso3":"PRI","phoneCode":"1"},{"countryName":"Qatar","iso2":"QA","iso3":"QAT","phoneCode":"974"},{"countryName":"Republic of the Congo","iso2":"CG","iso3":"COG","phoneCode":"242"},{"countryName":"Romania","iso2":"RO","iso3":"ROU","phoneCode":"40"},{"countryName":"Russia","iso2":"RU","iso3":"RUS","phoneCode":"7"},{"countryName":"Rwanda","iso2":"RW","iso3":"RWA","phoneCode":"250"},{"countryName":"Saint Barthelemy","iso2":"BL","iso3":"BLM","phoneCode":"590"},{"countryName":"Saint Helena","iso2":"SH","iso3":"SHN","phoneCode":"290"},{"countryName":"Saint Kitts and Nevis","iso2":"KN","iso3":"KNA","phoneCode":"1 869"},{"countryName":"Saint Lucia","iso2":"LC","iso3":"LCA","phoneCode":"1 758"},{"countryName":"Saint Martin","iso2":"MF","iso3":"MAF","phoneCode":"1 599"},{"countryName":"Saint Pierre and Miquelon","iso2":"PM","iso3":"SPM","phoneCode":"508"},{"countryName":"Saint Vincent and the Grenadines","iso2":"VC","iso3":"VCT","phoneCode":"1 784"},{"countryName":"Samoa","iso2":"WS","iso3":"WSM","phoneCode":"685"},{"countryName":"San Marino","iso2":"SM","iso3":"SMR","phoneCode":"378"},{"countryName":"Sao Tome and Principe","iso2":"ST","iso3":"STP","phoneCode":"239"},{"countryName":"Saudi Arabia","iso2":"SA","iso3":"SAU","phoneCode":"966"},{"countryName":"Senegal","iso2":"SN","iso3":"SEN","phoneCode":"221"},{"countryName":"Serbia","iso2":"RS","iso3":"SRB","phoneCode":"381"},{"countryName":"Seychelles","iso2":"SC","iso3":"SYC","phoneCode":"248"},{"countryName":"Sierra Leone","iso2":"SL","iso3":"SLE","phoneCode":"232"},{"countryName":"Singapore","iso2":"SG","iso3":"SGP","phoneCode":"65"},{"countryName":"Slovakia","iso2":"SK","iso3":"SVK","phoneCode":"421"},{"countryName":"Slovenia","iso2":"SI","iso3":"SVN","phoneCode":"386"},{"countryName":"Solomon Islands","iso2":"SB","iso3":"SLB","phoneCode":"677"},{"countryName":"Somalia","iso2":"SO","iso3":"SOM","phoneCode":"252"},{"countryName":"South Africa","iso2":"ZA","iso3":"ZAF","phoneCode":"27"},{"countryName":"South Korea","iso2":"KR","iso3":"KOR","phoneCode":"82"},{"countryName":"Spain","iso2":"ES","iso3":"ESP","phoneCode":"34"},{"countryName":"Sri Lanka","iso2":"LK","iso3":"LKA","phoneCode":"94"},{"countryName":"Sudan","iso2":"SD","iso3":"SDN","phoneCode":"249"},{"countryName":"Suriname","iso2":"SR","iso3":"SUR","phoneCode":"597"},{"countryName":"Svalbard","iso2":"SJ","iso3":"SJM","phoneCode":""},{"countryName":"Swaziland","iso2":"SZ","iso3":"SWZ","phoneCode":"268"},{"countryName":"Sweden","iso2":"SE","iso3":"SWE","phoneCode":"46"},{"countryName":"Switzerland","iso2":"CH","iso3":"CHE","phoneCode":"41"},{"countryName":"Syria","iso2":"SY","iso3":"SYR","phoneCode":"963"},{"countryName":"Taiwan","iso2":"TW","iso3":"TWN","phoneCode":"886"},{"countryName":"Tajikistan","iso2":"TJ","iso3":"TJK","phoneCode":"992"},{"countryName":"Tanzania","iso2":"TZ","iso3":"TZA","phoneCode":"255"},{"countryName":"Thailand","iso2":"TH","iso3":"THA","phoneCode":"66"},{"countryName":"Timor-Leste","iso2":"TL","iso3":"TLS","phoneCode":"670"},{"countryName":"Togo","iso2":"TG","iso3":"TGO","phoneCode":"228"},{"countryName":"Tokelau","iso2":"TK","iso3":"TKL","phoneCode":"690"},{"countryName":"Tonga","iso2":"TO","iso3":"TON","phoneCode":"676"},{"countryName":"Trinidad and Tobago","iso2":"TT","iso3":"TTO","phoneCode":"1 868"},{"countryName":"Tunisia","iso2":"TN","iso3":"TUN","phoneCode":"216"},{"countryName":"Turkey","iso2":"TR","iso3":"TUR","phoneCode":"90"},{"countryName":"Turkmenistan","iso2":"TM","iso3":"TKM","phoneCode":"993"},{"countryName":"Turks and Caicos Islands","iso2":"TC","iso3":"TCA","phoneCode":"1 649"},{"countryName":"Tuvalu","iso2":"TV","iso3":"TUV","phoneCode":"688"},{"countryName":"Uganda","iso2":"UG","iso3":"UGA","phoneCode":"256"},{"countryName":"Ukraine","iso2":"UA","iso3":"UKR","phoneCode":"380"},{"countryName":"United Arab Emirates","iso2":"AE","iso3":"ARE","phoneCode":"971"},{"countryName":"United Kingdom","iso2":"GB","iso3":"GBR","phoneCode":"44"},{"countryName":"United States","iso2":"US","iso3":"USA","phoneCode":"1"},{"countryName":"Uruguay","iso2":"UY","iso3":"URY","phoneCode":"598"},{"countryName":"US Virgin Islands","iso2":"VI","iso3":"VIR","phoneCode":"1 340"},{"countryName":"Uzbekistan","iso2":"UZ","iso3":"UZB","phoneCode":"998"},{"countryName":"Vanuatu","iso2":"VU","iso3":"VUT","phoneCode":"678"},{"countryName":"Venezuela","iso2":"VE","iso3":"VEN","phoneCode":"58"},{"countryName":"Vietnam","iso2":"VN","iso3":"VNM","phoneCode":"84"},{"countryName":"Wallis and Futuna","iso2":"WF","iso3":"WLF","phoneCode":"681"},{"countryName":"West Bank","iso2":"","iso3":"","phoneCode":"970"},{"countryName":"Western Sahara","iso2":"EH","iso3":"ESH","phoneCode":""},{"countryName":"Yemen","iso2":"YE","iso3":"YEM","phoneCode":"967"},{"countryName":"Zambia","iso2":"ZM","iso3":"ZMB","phoneCode":"260"},{"countryName":"Zimbabwe","iso2":"ZW","iso3":"ZWE","phoneCode":"263"}]
var selected_users = [];
var MAX_COL_LENGTH = 18;
var users_ready = false;
var history_ready = false;
var history_email_title = 'All Emails';


$(window).on('load', function(){
    // console.log('on load called');
    // $('.phoneNumber').inputmask("+(999) 999-9999");
    setTimeout(function(){
        $('.phoneNumber').inputmask("+999999999999");
    },2000);

});


//User Input
$("#filer_input_user").filer({
    limit: null,
    maxSize: null,
    extensions: ['csv'],
    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag & Drop User(.CSV) files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn btn-primary waves-effect waves-light">Browse Files</a></div></div>',
    showThumbs: true,
    theme: "dragdropbox",
    templates: {
        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
        item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-left">\
                                    <li>{{fi-progressBar}}</li>\
                                </ul>\
                                <ul class="list-inline pull-right">\
                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
        itemAppend: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
        progressBar: '<div class="bar"></div>',
        itemAppendToEnd: false,
        removeConfirmation: true,
        _selectors: {
            list: '.jFiler-items-list',
            item: '.jFiler-item',
            progressBar: '.bar',
            remove: '.jFiler-item-trash-action'
        }
    },
    dragDrop: {
        dragEnter: null,
        dragLeave: null,
        drop: null
    },
    uploadFile: {
        url: "/accounts/upload_file/",
        data: {
        },
        type: 'POST',
        enctype: 'multipart/form-data',
        beforeSend: function(){},
        success: function(data, el){
            // console.log('el: ', el);
            // console.log('data: ', data);
            $('#control_row').toggle();
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
            });
        },
        error: function(el){
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
            });
        },
        statusCode: null,
        onProgress: null,
        onComplete: null
    },
    files: [
        // {
        // 	name: "Desert.jpg",
        // 	size: 145,
        // 	type: "image/jpg",
        // 	file: "../files/assets/images/file-upload/Desert.jpg"
        // },
        // {
        // 	name: "overflow.jpg",
        // 	size: 145,
        // 	type: "image/jpg",
        // 	file: "../files/assets/images/file-upload/Desert.jpg"
        // }
    ],
    addMore: false,
    clipBoardPaste: true,
    excludeName: null,
    beforeRender: null,
    afterRender: null,
    beforeShow: null,
    beforeSelect: null,
    onSelect: null,
    afterShow: null,
    onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
        $.post('/accounts/remove_file/', {file: file.name});
    },
    onEmpty: null,
    options: null,
    captions: {
        button: "Choose Files",
        feedback: "Choose files To Upload",
        feedback2: "files were chosen",
        drop: "Drop file here to Upload",
        removeConfirmation: "Are you sure you want to remove this file?",
        errors: {
            filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
            filesType: "Only CSV files are allowed to be uploaded.",
            filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
            filesSizeAll: "Files you've chosen are too large! Please upload files up to {{fi-maxSize}} MB."
        }
    }
});


//H1 import
$("#h1_filer_input_user").filer({
    limit: null,
    maxSize: null,
    extensions: ['xlsx'],
    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag & Drop H1(.XLSX) files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn btn-primary waves-effect waves-light">Browse Files</a></div></div>',
    showThumbs: true,
    theme: "dragdropbox",
    templates: {
        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
        item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-left">\
                                    <li>{{fi-progressBar}}</li>\
                                </ul>\
                                <ul class="list-inline pull-right">\
                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
        itemAppend: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
        progressBar: '<div class="bar"></div>',
        itemAppendToEnd: false,
        removeConfirmation: false,
        _selectors: {
            list: '.jFiler-items-list',
            item: '.jFiler-item',
            progressBar: '.bar',
            remove: '.jFiler-item-trash-action'
        }
    },
    dragDrop: {
        dragEnter: null,
        dragLeave: null,
        drop: null
    },
    uploadFile: {
        url: "/accounts/upload_file/",
        data: {
        },
        type: 'POST',
        enctype: 'multipart/form-data',
        beforeSend: function(){},
        success: function(data, el){
            $('#h1_control_row').toggle();
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
            });
        },
        error: function(el){
            var parent = el.find(".jFiler-jProgressBar").parent();
            el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
            });
        },
        statusCode: null,
        onProgress: null,
        onComplete: null
    },
    files: [],
    addMore: false,
    clipBoardPaste: true,
    excludeName: null,
    beforeRender: null,
    afterRender: null,
    beforeShow: null,
    beforeSelect: null,
    onSelect: null,
    afterShow: null,
    onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
        $.post('/accounts/remove_file/', {file: file.name});
    },
    onEmpty: null,
    options: null,
    captions: {
        button: "Choose Files",
        feedback: "Choose files To Upload",
        feedback2: "files were chosen",
        drop: "Drop file here to Upload",
        removeConfirmation: "Are you sure you want to remove this file?",
        errors: {
            filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
            filesType: "Only XLSX files are allowed to be uploaded.",
            filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
            filesSizeAll: "Files you've chosen are too large! Please upload files up to {{fi-maxSize}} MB."
        }
    }
});



function fix_col_issues() {
    multi_table_users.button(0).remove();
    multi_table_users.button().add(0,
    {
        extend: 'colvis',
        text: 'Show/Hide Columns',
        className: "btn btn-primary",
        autoClose: false,
        fade: 0,
        columns: ':not(.noColVis)',
        colVis: { showAll: "Show all" }
    });

}



var multi_table_users = $('#multi-select-users').DataTable({
    "initComplete": function(settings, json) {
        this.api().button(0).remove();
        this.api().button().add(0,
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            className: "btn btn-primary",
            autoClose: false,
            fade: 0,
            columns: ':not(.noColVis)',
            colVis: { showAll: "Show all" }
        });

    },

    dom: 'Bfrtip',
    paging: false,
    searching: false,
    colReorder: true,
    info: false,
    destroy: false,
    stateSave: true,
    stateDuration: 60 * 60 * 24 * 365,
    stateSaveCallback: function(settings,data) {
      // console.log('state save called');
      localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data) )
    },
    stateLoadCallback: function(settings) {
      // console.log('state load called');
      return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) )
    },
    // retrieve: false,
    // oLanguage: {
    //     // sProcessing: "<img src='loading.gif'>"
    // },
    // processing : true,

    language: {
        decimal: ".",
        thousands: ","
    },
    // columns: [
    //     { "orderable": false , targets: 0}
    // ],
    //colReorder: { realtime: false}
    columnDefs: [
        {
            targets: [4,7,9,10,11,12,13,14,15,17,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40],
            // className: 'noVis',
            visible: false
        },
        {
            orderable: false,
            // className: 'select-checkbox',
            targets: 0,
            className: 'noVis',
            visible: false
        }
    ],
    // buttons: [
    //     'selectAll',
    //     'selectRows',
    //     'selectColumns',
    //     'selectCells'
    // ],
    buttons: [
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            columns: ':not(.noVis)',
            className: "btn btn-primary"
        },
        {
            extend: 'selectNone',
            text: 'Reset Selection',
            className: "btn btn-warning"
            // render: function (data, type, full) {
            //     return '<button type=\"button\" class=\"btn btn-warning alert-confirm m-b-10\" onclick=\"_gaq.push([\'_trackEvent\', \'example\', \'try\', \'alert-confirm\']);\">Confirm</button>';
            // }
        },
        {
            extend: 'selectedSingle',
            text: 'View',
            className: "btn",
            action: function ( e, dt, node, config ) {
                $("#edit-user-Modal").modal('show');
                // $('#new-user-form input, #new-user-form input:checkbox, #new-user-form select').attr('readonly', 'readonly');
                $('#edit-user-form fieldset.support-view-mode').attr('disabled', 'disabled');
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                      get_user(this['DT_RowId']);
                });

                $('#user-modal-title').text('View User');

                // Disable enabled button
                var button = $("#wizard").find('a[href="#' + 'finish' + '"]');
                button.attr("href", '#' + 'finish' + '-disabled');
                button.parent().addClass("disabled");
            }
        },
        {
            extend: 'selectedSingle',
            text: 'Edit',
            className: "btn waves-effect",
            action: function ( e, dt, node, config ) {
                $("#edit-user-Modal").modal('show');
                $('#edit-user-form fieldset.support-view-mode').removeAttr("disabled");
                $('#user-modal-title').text('Edit User');
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                      get_user(this['DT_RowId']);
                });

                // Enable disabled button
                var button = $("#wizard").find('a[href="#' + 'finish' + '-disabled"]');
                button.attr("href", '#' + 'finish');
                button.parent().removeClass();
            }
        },
        {
            extend: 'selected',
            text: 'Delete',
            className: "btn btn-danger",
            action: function ( e, dt, node, config ) {
                var arr = [];
                $.each(dt.rows({ selected: true }).data(), function() {
                    arr.push(this[0]);
                });
                delete_users(arr, dt);
            }
        }
    ],
    select: {
        // style: 'multi'
        // selector: 'td:first-child'
        style: 'os'
    }
});


function user_filters_data() {
    $.ajax({
    url: '/accounts/user_filters_data/',
    processData : false,
    contentType: false,
    // cache : false,
    dataType    : 'json',
    type: "GET",
    async: false,

    success: function (data) {
        $.each(data.sales_persons, function(key, value){
            $('#sales-person-filter').append(
            '<option value="' + value.fields.first_name + ' ' + value.fields.last_name + '">' +
                value.fields.first_name + ' ' + value.fields.last_name + '</option>');
        });
        $.each(data.departments, function(key, value){
            $('#dept-filter').append(
            '<option value="' + value[0] + '">' +
                value[1] + '</option>');
        });
        $.each(data.customer_types, function(key, value){
            $('#cu-type-filter').append(
            '<option value="' + value[0] + '">' +
                value[1] + '</option>');
        });
        $.each(data.user_statuses, function(key, value){
            $('#u-status-filter').append(
            '<option value="' + value[0] + '">' +
                value[1] + '</option>');
        });
        $.each(data.countries, function(key, value){
            $('#country-filter').append(
            '<option value="' + value.iso3 + '">' +
                value.countryName + '</option>');
        });
        $.each(data.cities, function(key, value){
            $('#city-filter').append(
            '<option value="' + value + '">' + value + '</option>');
        });
        $.each(data.region_states, function(key, value){
            $('#region-state-filter').append(
            '<option value="' + value + '">' + value + '</option>');
        });
        $.each(data.close_city, function(key, value){
            $('#close-to-city-filter').append(
            '<option value="' + value + '">' + value + '</option>');
        });

        // $("#new-user-loader").hide();
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong, please refresh the page' + '</div>');
        // $("#new-user-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });

}


// $('#shw_pswd').click(function(suppliers) {
//     var id = suppliers.target.id;
//     if ($('#' + id).is(":checked")) {
//         suppliers.checked=!suppliers.checked;
//         suppliers.type = "text";
//     } else {
//         suppliers.type = "password";
//     }
//     console.log('here');
// });

// $('#shw_pswd :checkbox').change(function() {
//     console.log('here');
//     // this will contain a reference to the checkbox
//     if (this.checked) {
//         // the checkbox is now checked
//         this.type = "text";
//     } else {
//         // the checkbox is now no longer checked
//         this.type = "password";
//     }
// });

function show_password() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

var suppliers_list = ["AMC", "ERAN", "EZ", "NIRU", "GUTFRUND", "KIRAN", "LSDCO_IL", "SDE", "SRK"];
var sale_modules_list = ["Request", "Mazal", "dQuote"];
var user_image = null;

'use strict';
$(document).ready(function() {
    //
    // $('input[id=mobile]').change(function() {
    //     console.log('change called');
    //     $('#mobile').rules("add", {
    //      uniqueMobile : true,
    //      // messages : { uniqueMobile : 'Already registered' }
    //   });
    // });

    // $("#mobile").on('change keyup paste', function () {
    //     console.log('change called');
    //     $('#mobile').rules("add", {
    //          uniqueMobile : true,
    //          // messages : { uniqueMobile : 'Already registered' }
    //     });
    // });

    // issues on type OS
    // $('#user-table-server tbody').on('click', 'tr', function () {
    //     // console.log(this);
    //     var id = this.id;
    //     var index = $.inArray(id, selected_users);
    //
    //     if ( index === -1 ) {
    //         selected_users.push( id );
    //     } else {
    //         selected_users.splice( index, 1 );
    //     }
    //
    //     $(this).toggleClass('selected');
    //     // console.log(selected_users)
    // } );

    // select2 search customization
    function matchStart (term, text) {
      if (text.toUpperCase().indexOf(term.toUpperCase()) === 0) {
        return true;
      }
      return false;
    }

    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
      $(".js-example-basic-single").select2({
        matcher: oldMatcher(matchStart)
      })
    });

    var url = window.location.pathname;
    var arr = url.split("/");
    if (arr[1]==='accounts') {
        // console.log('fetch filters data');
        user_filters_data();
        load_user_filters_state();
        if (arr[2] && arr[2]==='compose_email' || arr[2] && arr[2]==='mailboxes') {
            // console.log('handle gapi');
            // called from import, useless here
            // handleAuthClick();
        }
        if (arr[2] && arr[2]==='compose_email') {
            // console.log('handle email active tab');
            handle_email_active_tab();
        }
        else if (arr[2] && arr[2]==='history') {
            load_history_filters_state();
        }
        else if (arr[2] && arr[2]==='follow-ups') {
            // load_history_filters_state();
        }
    }

  // $('#date,#datejoin').bootstrapMaterialDatePicker({
  //        time: false,
  //        clearButton: true
  //    });
  //  $("#example-date-inputS").bootstrapMaterialDatePicker({
  //                time: false,
  //                clearButton: true
  //            });

  $("#basic-forms").steps({
      headerTag: "h3",
      bodyTag: "fieldset",
      transitionEffect: "slideLeft",
      autoFocus: true
  });

    var form = $("#new-user-form").show();
    var edit_user_form = $("#edit-user-form").show();
    var unique;


  $.validator.addMethod(
    "uniqueEmail",
    function(value, element) {
        $.ajax({
            type: "POST",
            async: false,
            url: "/accounts/unique_user/",
            data: {'email': $('#email').val()},
            dataType:"json",
            success: function(msg)
            {
                //If username exists, set response to true
                unique = true;
            },
            error: function(xhr, error){
                unique = false;
            }
         });
        return unique;
    },
    "Username is Already Taken"
  );

  $.validator.addMethod(
    "uniqueMobile",
    function(value, element) {
        $.ajax({
            type: "POST",
            async: false,
            url: "/accounts/unique_mobile/",
            data: {'mobile': $('#mobile').val()},
            dataType:"json",
            success: function(msg)
            {
                //If username exists, set response to true
                unique_mobile = true;
            },
            error: function(xhr, error){
                $("#mobile-1-modal").modal('show');
                unique_mobile = false;
            }
         });
        return unique_mobile;
    }
  );

  form.steps({
      headerTag: "h3",
      bodyTag: "fieldset",
      transitionEffect: "slideLeft",
      onStepChanging: function(event, currentIndex, newIndex) {

          // Always allow previous action even if the current form is not valid!
          if (currentIndex > newIndex) {
              return true;
          }
          // Forbid next action on "Warning" stop if the user is to young
          if (newIndex === 3 && Number($("#age-2").val()) < 18) {
              return false;
          }
          // Needed in some cases if the user went back (clean up)
          if (currentIndex < newIndex) {
              // To remove error styles
              form.find(".body:eq(" + newIndex + ") label.error").remove();
              form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
          }
          form.validate().settings.ignore = ":disabled,:hidden";
          return form.valid();
      },
      onStepChanged: function(event, currentIndex, priorIndex) {

          // Used to skip the "Warning" step if the user is old enough.
          if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
              form.steps("next");
          }
          // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
          if (currentIndex === 2 && priorIndex === 3) {
              form.steps("previous");
          }
      },
      onFinishing: function(event, currentIndex) {

          form.validate().settings.ignore = ":disabled";
          return form.valid();
      },
      onFinished: function(event, currentIndex) {
          // var newMessages = $(responseText).find('.hidden-messages').html();
          // $('#messages').append(newMessages);
          create_user();
      }
  }).validate({
      errorPlacement: function errorPlacement(error, element) {

          element.before(error);
      },
      rules: {
          confirm: {
              equalTo: "#password"
          },
          zip_code: {
              required: true,
              digits: true,
              minlength: 4,
              maxlength: 5
          },
          email: {
            uniqueEmail: true
          },
          mobile: {
            uniqueMobile: true
          }
      },
      messages: {
        zip_code: {
          minlength: "ZipCode must be at least 4 digits",
          maxlength: "Please use a 4-5 digit ZipCode",
          required: "ZipCode is required"
        },
        email: {
          uniqueEmail: "This email is already registered"
        },
        mobile: {
          uniqueMobile: "This mobile no. is already registered"
        }
      }
  });


  edit_user_form.steps({
      headerTag: "h3",
      bodyTag: "fieldset",
      transitionEffect: "slideLeft",
      showFinishButtonAlways: true,
      enableFinishButton: true,

      onStepChanging: function(event, currentIndex, newIndex) {

          // Always allow previous action even if the current form is not valid!
          if (currentIndex > newIndex) {
              return true;
          }
          // Forbid next action on "Warning" stop if the user is to young
          if (newIndex === 3 && Number($("#age-2").val()) < 18) {
              return false;
          }
          // Needed in some cases if the user went back (clean up)
          if (currentIndex < newIndex) {
              // To remove error styles
              edit_user_form.find(".body:eq(" + newIndex + ") label.error").remove();
              edit_user_form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
          }
          edit_user_form.validate().settings.ignore = ":disabled,:hidden";
          return edit_user_form.valid();
      },
      onStepChanged: function(event, currentIndex, priorIndex) {

          // Used to skip the "Warning" step if the user is old enough.
          if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
              edit_user_form.steps("next");
          }
          // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
          if (currentIndex === 2 && priorIndex === 3) {
              edit_user_form.steps("previous");
          }
      },
      onFinishing: function(event, currentIndex) {

          edit_user_form.validate().settings.ignore = ":disabled";
          return edit_user_form.valid();
      },
      onFinished: function(event, currentIndex) {
          update_user();
      }
  }).validate({
      errorPlacement: function errorPlacement(error, element) {
          element.before(error);
      },
      rules: {
          confirm: {
              equalTo: "#password"
          },
          zip_code: {
              required: true,
              digits: true,
              minlength: 4,
              maxlength: 5
          // },
          // email: {
          //   UniqueEmail: true
          }
      },
      messages: {
        zip_code: {
          minlength: "ZipCode must be at least 4 digits",
          maxlength: "Please use a 4-5 digit ZipCode",
          required: "ZipCode is required"
        // },
        // email: {
        //   UniqueEmail: "This email is already registered"
        }
      }
  });


    // prepare suppliers list
    $( "input.suppliers" ).click(function(suppliers) {
    // $(".suppliers input:checkbox").change(function(suppliers) {
    //     console.log('here');
        var id = suppliers.target.id;
        if ($('#' + id).is(":checked")) {
            suppliers.checked=!suppliers.checked;
            suppliers_list.push(id);
        } else {
            var index = suppliers_list.indexOf(id);
            if (index !== -1) suppliers_list.splice(index, 1);
        }
        // console.log('suppliers_LIST: ', suppliers_list);
    });


    // prepare sale_modules list
    $( "input.sale_modules" ).click(function(sale_modules) {
        var id = sale_modules.target.id;
        if ($('#' + id).is(":checked")) {
            sale_modules.checked=!sale_modules.checked;
            sale_modules_list.push(id);
        } else {
            var index = sale_modules_list.indexOf(id);
            if (index !== -1) sale_modules_list.splice(index, 1);
        }
        // console.log('sale_modules_LIST: ', sale_modules_list);
    });


    $(".zip_code").on('change paste', function () {
        var c_iso2 = countries.find(x => x.iso3 === $("#country").val()).iso2;
        var zip = this.value;
        $.ajax({
            // url: 'http://www.zipcodeapi.com/rest/DqG0WZTTFgyQydZ2MUmEZvuMW85lPaBqUu8nDfD1ltF6J2FjeJCXjS7OOkqPnRND/info.json/' + this.value + '/degrees',
            url: 'http://api.zippopotam.us/' + c_iso2 +'/' + zip,
            dataType    : 'json',
            type: "GET",

            success: function (data) {
                $("#city").val(data.places[0]["place name"]);
                $("#region_state").val(data.places[0]["state"]);
            },
            error: function(xhr, error){
                $('#messages').append(
                '<div class="alert alert-dismissable alert-warning"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
                'Failed! No data found for this ZipCode' + '</div>');
                // $("#new-user-loader").hide();
                // console.debug(xhr);
                // console.debug(error);
            }
        });
    });

});

// User filters module
function user_type_callback(value) {
    // console.log('here it is', value);
    if (value === 'CU') {
        $("#sales_manager").attr("disabled", true);
    } else {
        $("#sales_manager").attr("disabled", false);
    }
}



function create_user() {
  // alert("Backend implementation in progress!");
  $("#new-user-loader").show();
  var formData = new FormData();
  if (user_image) {
    formData.set("user_image", user_image , user_image.name);
  }
  // formData.set("suppliers[]", JSON.stringify(suppliers_list));
  // formData.set("sale_modules[]", JSON.stringify(sale_modules_list));

  for (var i = 0; i < suppliers_list.length; i++) {
    formData.append('suppliers[]', suppliers_list[i]);
  }
  for (var i = 0; i < sale_modules_list.length; i++) {
    formData.append('sale_modules[]', sale_modules_list[i]);
  }

  var temp_data = {
      'user_type': $('#user_type').val(),
      'company_name': $('#company_name').val(),
      'card_name': $('#card_name').val(),
      'email': $('#email').val(),
      'title': $('#title').val(),
      'first_name': $('#first_name').val(),
      'last_name': $('#last_name').val(),
      'password': $('#password').val(),
      'sales_manager': $('#sales_manager').val(),
      'sales_person': $('#sales_person').val(),
      // 'sales_person_name': $('#sales_person').text(),
      'user_status': $('#user_status').val(),
      'customer_type': $('#customer_type').val(),
      'mass_email': $('#mass_email').val(),
      'language': $('#language').val(),
      'markup': $('#markup').val(),
      // 'suppliers': suppliers_list,
      // 'sale_modules': sale_modules_list,
      // 'user_image': user_image.name,
      'street': $('#street').val(),
      'comment': $('#comments').val(),
      'anc_interest': $('#anc_interest').val(),
      'city': $('#city').val(),
      'zip_code': $('#zip_code').val(),
      'country': $('#country').val(),
      'tel': $('#tel').val(),
      'mobile': $('#mobile').val(),
      'region_state': $('#region_state').val(),
      'close_to_city': $('#close_to_city').val(),
      'tel3': $('#tel3').val(),
      'birth_date': $('#birth_date').val(),
      'business_type': $('#business_type').val(),
      'web_site': $('#web_site').val(),
      'skype_user': $('#skype_user').val(),
      'qq': $('#qq').val(),
      'facebook': $('#facebook').val(),
      'twitter': $('#twitter').val(),
      'ref_name': $('#ref_name').val(),
      'ref_phone': $('#ref_phone').val(),
      'jbt': $('#jbt').val(),
      'vat': $('#vat').val(),
      'whitesite_markup': $('#whitesite_markup').val(),
      'dept': $('#dept').val(),
      'r1': $('#r1').val(),
      'address_4': $('#address_4').val(),
      'fantasy_no': $('#fantasy_no').val(),
      'harmony_no': $('#harmony_no').val(),
      'data_source': $('#data_source').val(),
      'verified': $('#verified').is(":checked"),
      'shape_box': $('#shape_box').val(),
      'size_box': $('#size_box').val(),
      'color_box': $('#color_box').val(),
      'clarity_box': $('#clarity_box').val(),
      'lab_box': $('#lab_box').val(),
      'make_box': $('#make_box').val(),
      'download_excel': $('#download_excel').is(":checked"),
      'view_prices': $('#view_prices').is(":checked")
      // 'page': page
    };

  $.each(temp_data, function(key, value){
    formData.append(key, value);
  });

  $.ajax({
    url: '/accounts/new_user/',
    processData : false,
    contentType: false,
    // cache : false,
    dataType    : 'json',
    type: "POST",
    enctype: 'multipart/form-data',
    // data: $("#new-user-form").serialize(),

    data: formData,

    success: function (data) {
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! User has been created' + '</div>');

        $("#new-user-loader").hide();
        reset_form();
        $('#mobile').rules("add", {
            uniqueMobile : true,
            // messages : { uniqueMobile : 'Already registered' }
        });
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        $("#new-user-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


function reset_form() {
  $('.content input[type="text"]').val('');
  $('.content input[type="email"]').val('');
  $('.content input[type="password"]').val('');
}

// // row select case
// user_table_server.on( 'select', function ( e, dt, type, indexes ) {
//     if ( type === 'row' ) {
//         // var count = multi_table_users.rows( { selected: true } ).count();
//         // var rowData = multi_table_users.rows( { selected: true } ).data()[0];
//         // var data = multi_table_users.rows( indexes ).data().pluck( 'email' );
//         // console.log('row selected', count, rowData, data);
//         // // do something with the ID of the selected items
//         // // table[ type ]( indexes ).nodes().to$().addClass( 'custom-selected' );
//     }
// } );

// // row select case
// user_table_server.on( 'deselect', function ( e, dt, type, indexes ) {
//     if ( type === 'row' ) {
//         // var data = multi_table_users.rows( indexes ).data()[0]['email'];
//         // console.log('row deselect', data);
//         // // do something with the ID of the deselected items
//     }
// } );


multi_table_users.on('column-reorder', function () {
    fix_col_issues();
});

// multi_table_users.on('init', function () {
//   alert( 'Table event' );
//   fix_col_issues();
// });
//
// $('#multi-select-users').on( 'draw.dt', function () {
//     alert( 'Table redrawn' );
// } );

function delete_users(arr, dt) {
  $("#more-user-loader").show();
  $.ajax({
    url: '/accounts/delete_user/',
    data: {
      'ids': arr
    },
    type: "POST",
    dataType: 'json',
    success: function (data) {
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! User has been deleted' + '</div>');

        $("#more-user-loader").hide();

        dt.rows({ selected: true }).remove().draw( false );
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        $("#more-user-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


function get_user(id) {
  $.ajax({
    url: '/accounts/get_user/',
    data: {
      'id': id
    },
    type: "POST",
    dataType: 'json',
    success: function (user) {
      $("#more-user-loader").hide();
      // console.log(data);
      $('#user_image').attr('src', user.data[0].fields.image);
      $('#user_type').val(user.data[0].fields.user_type);
      $('#company_name').val((user.data[0].fields.company_name));
      $('#card_name').val((user.data[0].fields.card_name));
      $('#email').val((user.data[0].fields.email));
      $('#title').val((user.data[0].fields.title));
      $('#first_name').val((user.data[0].fields.first_name));
      $('#last_name').val((user.data[0].fields.last_name));
      $('#password').val('');

      if (user.data[0].fields.user_type === 'CU') {
          $("#sales_manager").attr("disabled", true);
      }

      $('#sales_manager').empty().append('<option value="">No Manager</option>');
      $.each(user.managers, function(key, value) {
          $('#sales_manager').append(
            '<option value=' + value.pk + '>' + value.fields.email + '</option>'
          )
      });
      $('#sales_manager').val((user.data[0].fields.sales_manager));

      var temp_sales_person_id;
      $('#sales_person').empty().append('<option value="">No Sales Person</option>');
      $.each(user.sales_persons, function(key, value) {
          $('#sales_person').append(
            '<option value=' + value.pk + '>' + value.fields.email + '</option>'
          );
          // if (user.data[0].fields.sales_person_name === value.fields.first_name + ' ' + value.fields.last_name) {
          //     temp_sales_person_id = value.pk;
          // }
      });
      $('#sales_person').val(user.data[0].fields.sales_person);

      $('#user_status').val((user.data[0].fields.user_status));
      $('#customer_type').val((user.data[0].fields.customer_type));
      $('#mass_email').val((user.data[0].fields.mass_email).toString());
      $('#language').val((user.data[0].fields.language));
      $('#markup').val((user.data[0].fields.markup));

      suppliers_list = [];
      $('.suppliers').prop('checked', false);
      $.each(user.suppliers, function(key, value) {
          suppliers_list.push(value.fields.supplier);
          $('#' + value.fields.supplier).prop('checked', true);
          // console.log('suppliers_LIST: ', suppliers_list);
      });

      sale_modules_list = [];
      $('.sale_modules').prop('checked', false);
      $.each(user.sale_modules, function(key, value) {
          sale_modules_list.push(value.fields.sale_module);
          $('#' + value.fields.sale_module).prop('checked', true);
          // console.log('sale_modules_LIST: ', sale_modules_list);
      });


      $('#comments').val((user.data[0].fields.comment));
      $('#anc_interest').val((user.data[0].fields.anc_interest));
      $('#street').val((user.data[0].fields.street));
      $('#city').val((user.data[0].fields.city));
      $('#zip_code').val((user.data[0].fields.zip_code));
      $('#country').val((user.data[0].fields.country));
      $('#tel').val((user.data[0].fields.tel));
      $('#mobile').val((user.data[0].fields.mobile));
      $('#region_state').val((user.data[0].fields.region_state));
      $('#close_to_city').val((user.data[0].fields.close_to_city));
      $('#tel3').val((user.data[0].fields.tel3));

      if (user.data[0].fields.birth_date) {
          var now = new Date(user.data[0].fields.birth_date);
          var day = ("0" + now.getDate()).slice(-2);
          var month = ("0" + (now.getMonth() + 1)).slice(-2);
          var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
      }

      $('#birth_date').val(today);
      $('#business_type').val((user.data[0].fields.business_type));
      $('#web_site').val((user.data[0].fields.web_site));
      $('#skype_user').val((user.data[0].fields.skype_user));
      $('#qq').val((user.data[0].fields.qq));
      $('#facebook').val((user.data[0].fields.facebook));
      $('#twitter').val((user.data[0].fields.twitter));
      $('#ref_name').val((user.data[0].fields.ref_name));
      $('#ref_phone').val((user.data[0].fields.ref_phone));
      $('#jbt').val((user.data[0].fields.jbt));
      $('#vat').val((user.data[0].fields.vat));
      $('#whitesite_markup').val((user.data[0].fields.whitesite_markup));
      $('#dept').val((user.data[0].fields.dept));
      $('#r1').val((user.data[0].fields.r1));
      $('#address_4').val((user.data[0].fields.address_4));
      $('#fantasy_no').val((user.data[0].fields.fantasy_no));
      $('#harmony_no').val((user.data[0].fields.harmony_no));
      $('#data_source').val((user.data[0].fields.data_source));
      $('#shape_box').val((user.data[0].fields.shape_box));
      $('#size_box').val((user.data[0].fields.size_box));
      $('#color_box').val((user.data[0].fields.color_box));
      $('#clarity_box').val((user.data[0].fields.clarity_box));
      $('#lab_box').val((user.data[0].fields.lab_box));
      $('#make_box').val((user.data[0].fields.make_box));

      if (user.data[0].fields.is_verified) {
          $('#verified').prop('checked', true);
      }
      if (user.data[0].fields.can_download_excel) {
          $('#download_excel').prop('checked', true);
      }
      if (user.data[0].fields.can_view_prices) {
          $('#view_prices').prop('checked', true);
      }
      // $('#verified').val((user.data[0].fields.is_verified));
      init_card_follow_up_table(user.follow_ups);
    },
    error: function(xhr, error){
        $("#more-user-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


function update_user() {
  // alert("Backend implementation in progress!");
  $("#edit-user-loader").show();
  var formData = new FormData();
  if (user_image) {
    formData.set("user_image", user_image , user_image.name);
  }
  // formData.set("suppliers[]", JSON.stringify(suppliers_list));
  // formData.set("sale_modules[]", JSON.stringify(sale_modules_list));

  for (var i = 0; i < suppliers_list.length; i++) {
    formData.append('suppliers[]', suppliers_list[i]);
  }
  for (var i = 0; i < sale_modules_list.length; i++) {
    formData.append('sale_modules[]', sale_modules_list[i]);
  }

  var temp_data = {
      'user_type': $('#user_type').val(),
      'company_name': $('#company_name').val(),
      'card_name': $('#card_name').val(),
      'email': $('#email').val(),
      'title': $('#title').val(),
      'first_name': $('#first_name').val(),
      'last_name': $('#last_name').val(),
      'password': $('#password').val(),
      'sales_manager': $('#sales_manager').val(),
      'sales_person': $('#sales_person').val(),
      // 'sales_person_name': $('#sales_person').text(),
      'user_status': $('#user_status').val(),
      'customer_type': $('#customer_type').val(),
      'mass_email': $('#mass_email').val(),
      'language': $('#language').val(),
      'markup': $('#markup').val(),
      // 'suppliers': suppliers_list,
      // 'sale_modules': sale_modules_list,
      // 'user_image': user_image.name,
      'street': $('#street').val(),
      'comment': $('#comments').val(),
      'anc_interest': $('#anc_interest').val(),
      'city': $('#city').val(),
      'zip_code': $('#zip_code').val(),
      'country': $('#country').val(),
      'tel': $('#tel').val(),
      'mobile': $('#mobile').val(),
      'region_state': $('#region_state').val(),
      'close_to_city': $('#close_to_city').val(),
      'tel3': $('#tel3').val(),
      'birth_date': $('#birth_date').val(),
      'business_type': $('#business_type').val(),
      'web_site': $('#web_site').val(),
      'skype_user': $('#skype_user').val(),
      'qq': $('#qq').val(),
      'facebook': $('#facebook').val(),
      'twitter': $('#twitter').val(),
      'ref_name': $('#ref_name').val(),
      'ref_phone': $('#ref_phone').val(),
      'jbt': $('#jbt').val(),
      'vat': $('#vat').val(),
      'whitesite_markup': $('#whitesite_markup').val(),
      'dept': $('#dept').val(),
      'r1': $('#r1').val(),
      'address_4': $('#address_4').val(),
      'fantasy_no': $('#fantasy_no').val(),
      'harmony_no': $('#harmony_no').val(),
      'data_source': $('#data_source').val(),
      'verified': $('#verified').is(":checked"),
      'shape_box': $('#shape_box').val(),
      'size_box': $('#size_box').val(),
      'color_box': $('#color_box').val(),
      'clarity_box': $('#clarity_box').val(),
      'lab_box': $('#lab_box').val(),
      'make_box': $('#make_box').val(),
      'download_excel': $('#download_excel').is(":checked"),
      'view_prices': $('#view_prices').is(":checked")
      // 'page': page
    };

  $.each(temp_data, function(key, value){
    formData.append(key, value);
  });
  $.ajax({
    url: '/accounts/update_user/',
    // data: $("#new-user-form").serialize(),
    processData : false,
    contentType: false,
    // cache : false,
    dataType    : 'json',
    type: "POST",
    enctype: 'multipart/form-data',
    // data: $("#new-user-form").serialize(),

    data: formData,

    // data: {
    //   'user_type': $('#user_type').val(),
    //   'company_name': $('#company_name').val(),
    //   'email': $('#email').val(),
    //   'title': $('#title').val(),
    //   'first_name': $('#first_name').val(),
    //   'last_name': $('#last_name').val(),
    //   'password': $('#password').val(),
    //   // 'sales_manager': $('#sales_manager').val(),
    //   'sales_person': $('#sales_person').val(),
    //   'user_status': $('#user_status').val(),
    //   'customer_type': $('#customer_type').val(),
    //   'mass_email': $('#mass_email').val(),
    //   'language': $('#language').val(),
    //   'markup': $('#markup').val(),
    //   'suppliers': suppliers_list,
    //   'street': $('#street').val(),
    //   'city': $('#city').val(),
    //   'zip_code': $('#zip_code').val(),
    //   'country': $('#country').val(),
    //   'tel': $('#tel').val(),
    //   'mobile': $('#mobile').val(),
    //   'region_state': $('#region_state').val(),
    //   'close_to_city': $('#close_to_city').val(),
    //   'tel3': $('#tel3').val(),
    //   'birth_date': $('#birth_date').val(),
    //   'business_type': $('#business_type').val(),
    //   'web_site': $('#web_site').val(),
    //   'skype_user': $('#skype_user').val(),
    //   'qq': $('#qq').val(),
    //   'facebook': $('#facebook').val(),
    //   'twitter': $('#twitter').val(),
    //   'ref_name': $('#ref_name').val(),
    //   'ref_phone': $('#ref_phone').val(),
    //   'jbt': $('#jbt').val(),
    //   'vat': $('#vat').val(),
    //   'whitesite_markup': $('#whitesite_markup').val(),
    //   'dept': $('#dept').val(),
    //   'r1': $('#r1').val(),
    //   'address_4': $('#address_4').val(),
    //   'fantasy_no': $('#fantasy_no').val(),
    //   'harmony_no': $('#harmony_no').val(),
    //   'data_source': $('#data_source').val(),
    //   'verified': $('#verified').is(":checked"),
    //   // 'verified': $('#verified').val()
    // },

    success: function (data) {
        user_table_server.ajax.reload();
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! User details has been updated' + '</div>');

    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        console.debug(xhr); console.debug(error);
    },
    complete: function(data) {
        $("#edit-user-loader").hide();
        $("#edit-user-Modal").modal('hide');
    }
  });
}


function select_image(e){
   $("#user_image_input").click();
   e.preventDefault();
}


function change_image(e){
    user_image = e.target.files[0];
   $('#user_image').attr('src', URL.createObjectURL(e.target.files[0]));
}

function find_index(element) {
  return element === 4;
}


// ajax data table with server side processing
var user_table_server = $('#user-table-server').DataTable({
    "initComplete": function(settings, json) {
        // console.log('init complete');
        this.api().button(0).remove();
        this.api().button().add(0,
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            className: "btn btn-primary",
            autoClose: false,
            fade: 0,
            columns: ':not(.noColVis)',
            colVis: { showAll: "Show all" }
        });

    },

    // dom: 'Bfritip',
    dom: 'Biti',
    // "dom": '<"top"i>rt<"bottom"flp><"clear">',
    // "processing": true,
    "serverSide": true,
    // "paging": true,
    // "pageLength": 50,
    // "length ": 50,
    "searching": false,
    // rowId: 'DT_RowId',
    // "colReorder": true,
    colReorder: {
        // true create problems on bigger data.
        realtime: false
    },
    // "info": true,
    // deferRender: true,
    // rowId: 'extn',
    // select: true,
    stateSave: true,
    stateDuration: 60 * 60 * 24 * 365,

    stateSaveCallback: function(settings,data) {
        // console.log(stones_col_order_local, data.ColReorder);
        // delete data.start;
        // console.log(JSON.stringify(data.columns));

        // if (JSON.stringify(col_order_local) === JSON.stringify(data.ColReorder) &&
        //     JSON.stringify(users_col_vis_local) === JSON.stringify(data.columns)) {
        //     // console.log('state save skipped');
        //     return;
        // }

        // col_order_local = data.ColReorder;
        data.start = 0;
        data.scroller.topRow = 0;
        data.scroller.baseScrollTop = 0;
        data.scroller.baseRowTop = 0;
        // console.log('start: ', data.start);
        localStorage.setItem(settings.sInstance, JSON.stringify(data));
        set_counter_distance(settings);
        users_col_vis_local = data.columns;
        col_order_local = data.ColReorder;

    },
    stateLoadCallback: function(settings) {
        // console.log('state load called');
        var o = "";
        $.ajax({
            url: '/accounts/get_state/',
            data: {
              'table_name': settings.sInstance
            },
            type: "POST",
            async: false,
            dataType: 'json',
            success: function (data) {
                o = JSON.parse(data.state);
                col_order_local = o.ColReorder;
                users_col_vis_local = o.columns;
                // return JSON.parse(data.state);
                // return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
            },
            error: function(xhr, error){
              console.log('error on state get');
              // var temp = JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
              // // console.log('local state: ', temp);
              // if (temp) {
              //   col_order_local = temp.ColReorder;
              //   // console.log('local copy updated', col_order_local)
              // } else {
              //     o = col_order_local
              // }
              // o = temp;
            }
        });
        return o;
        // return JSON.parse( localStorage.getItem( 'DataTables_' + settings.sInstance ) );
    },
    stateLoaded: function (e, settings, data) {
        // console.log('state loaded with:', e, settings, data);
    },
    // stateLoadParams: function( settings, data ) {
    //   console.log('state load param');
    //   if (data.start) delete data.start;
    // },
    columnDefs: [
        // className: "truncate",
        { targets: "_all", "createdCell": function (td, cellData, rowData, row, col) {
            // cellData = rowData[col_order_local[col]];
            // $(td).attr("title", rowData[col_order_local[col]]);
            // $(td).attr("title", rowData[col]);
            var title_value = "";
            var html_value = (cellData) ? cellData.toString() : "";
            // console.log(cellData);
            if (typeof cellData === 'string' && cellData !== "" ) {
                var html = $.parseHTML(cellData.toString());
                // console.log(html);
                title_value = html[0].textContent;
                html_value = title_value;
            }
            html_value = html_value.substring(0,MAX_COL_LENGTH);
            // $(td).attr("title", title_value);

            if (html && html[0].tagName === 'A') {
                html[0].innerHTML = html_value;
                html_value = html[0].outerHTML;
                // console.log('here', html_value);
            }
            // $(td).html(html_value);
            this.api().cell(row, col).data('<span title="' + title_value + '">' + html_value + '</span>');
            // this.api().cell(row, col).data(html_value);
            // $(this.api().cell(row, col)).attr('title', title_value)

            // if (row === 0) {
            //    // console.log(td, cellData, rowData, row, col, html, title_value, html_value);
            //    console.log(html);
            // }
            // if ( cellData < 1 ) {
            //   $(td).css('color', 'red')
            // }
        }},
        {
            targets: [col_order_local[0],col_order_local[3],col_order_local[5],col_order_local[6],col_order_local[8],
                col_order_local[9],col_order_local[10],col_order_local[11],col_order_local[12],col_order_local[13],
                col_order_local[15],col_order_local[18],col_order_local[19],col_order_local[20],col_order_local[21],
                col_order_local[22],col_order_local[23],col_order_local[24],col_order_local[25],col_order_local[26],
                col_order_local[27],col_order_local[28],col_order_local[29],col_order_local[30],col_order_local[31],
                col_order_local[32],col_order_local[33],col_order_local[34],col_order_local[35],col_order_local[ 36],
                col_order_local[37],col_order_local[38],col_order_local[39],col_order_local[40],col_order_local[42],
                col_order_local[45], col_order_local[50]],
            className: 'noVis',
            visible: false
        },
        {
            orderable: false,
            targets: [col_order_local[45]]
        // },
        // {
        //     className: 'truncate-1',
        //     targets: [col_order_local[46]]
        // },
        // {
        //     className: 'truncate-2',
        //     targets: [col_order_local[47]]
        }
    ],
    // createdRow: function(row, data, rowIndex){
    //     // console.log(row, data, rowIndex);
    //     var td1 = $(row).find(".truncate-1");
    //     td1.attr("title", td1.html());
    //     var td2 = $(row).find(".truncate-2");
    //     td2.attr("title", td2.html());
    // },
    rowCallback: function(row, data, displayNum, displayIndex, dataIndex ) {
        // console.log(data.DT_RowId);
        if ( $.inArray(data.DT_RowId.toString(), selected_users) !== -1 ) {
            if (!$(row).hasClass("selected")) {
                $(row).addClass('selected');
            }
            this.api().row( dataIndex ).select();
            // var indexes = this.api().rows( {selected: true} ).indexes();
            // for (var index of indexes) {
            //     this.api().row( index ).select();
            // }
            // this.api().rows('.selected').select();
        }
    },

    "order": [[ 1, "desc" ]],
    buttons: [
        {
            extend: 'colvis',
            text: 'Show/Hide Columns',
            columns: ':not(.noVis)',
            className: "btn btn-primary"
        },
        {
            extend: 'selectNone',
            text: 'Reset Selection',
            className: "btn btn-warning",
            action: function (e, dt, node, config) {
                dt.rows().deselect();
                selected_users = [];
            }
        },
        // {
        //     className: "btn btn-primary",
        //     text: "Export Users (Filtered)",
        //     action: function (e, dt, node, config)
        //     {
        //         // Option 1
        //         // complex in case of filtering
        //         // var params = dt.ajax.params();
        //         // window.location.href = 'export_full_inventory/?dt=' + params;
        //
        //         // Option 2
        //         // $.ajax({
        //         //     url: "export_users_filtered_csv/",
        //         //     data: Object.assign({}, dt.ajax.params(), {
        //         //       'custom_order': col_order_local,
        //         //       'user_type': user_type_list,
        //         //       'email': email_filter,
        //         //       'sales_person_name': sales_person_filter,
        //         //       'dept': dept_filter,
        //         //       'cu_types': cu_types_filter,
        //         //       'u_status': u_status_filter,
        //         //       'country': country_filter,
        //         //       'city': city_filter,
        //         //       'region_state': region_state_filter,
        //         //       'close_to_city': close_to_city_filter
        //         //     }),
        //         //     type: "POST",
        //         //
        //         //     success: function(res, status, xhr) {
        //         //         var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
        //         //         var csvURL = window.URL.createObjectURL(csvData);
        //         //         var tempLink = document.createElement('a');
        //         //         tempLink.href = csvURL;
        //         //         tempLink.setAttribute('download', 'users.csv');
        //         //         tempLink.click();
        //         //     }
        //         // });
        //
        //         // Option 3
        //         var data = Object.assign({}, dt.ajax.params(), {
        //               'custom_order': col_order_local,
        //               'user_type': user_type_list,
        //               'email': email_filter,
        //               'sales_person_name': sales_person_filter,
        //               'dept': dept_filter,
        //               'cu_types': cu_types_filter,
        //               'u_status': u_status_filter,
        //               'country': country_filter,
        //               'city': city_filter,
        //               'region_state': region_state_filter,
        //               'close_to_city': close_to_city_filter
        //             });
        //         var request = new XMLHttpRequest();
        //         request.open('POST', 'export_users_filtered/', true);
        //         request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        //         request.responseType = 'blob';
        //
        //         request.onload = function (e) {
        //             if (this.status === 200) {
        //                 var filename = "";
        //                 var disposition = request.getResponseHeader('Content-Disposition');
        //                 // check if filename is given
        //                 if (disposition && disposition.indexOf('attachment') !== -1) {
        //                     var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        //                     var matches = filenameRegex.exec(disposition);
        //                     if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        //                 }
        //                 var blob = this.response;
        //                 if (window.navigator.msSaveOrOpenBlob) {
        //                     window.navigator.msSaveBlob(blob, filename);
        //                 }
        //                 else {
        //                     var downloadLink = window.document.createElement('a');
        //                     var contentTypeHeader = request.getResponseHeader("Content-Type");
        //                     downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
        //                     downloadLink.download = filename;
        //                     document.body.appendChild(downloadLink);
        //                     downloadLink.click();
        //                     document.body.removeChild(downloadLink);
        //                 }
        //             } else {
        //                 alert('Download failed.')
        //             }
        //         };
        //         request.send(JSON.stringify(data));
        //     }
        // },
        {
            className: "btn btn-primary",
            text: "Export All",
            action: function (e, dt, node, config) {
                var data = {
                  'is_full': true,
                  'file_name': 'all users.xlsx'
                };
                var request = new XMLHttpRequest();
                request.open('POST', 'export_users/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function (e) {
                    if (this.status === 200) {
                        var filename = "";
                        var disposition = request.getResponseHeader('Content-Disposition');
                        // check if filename is given
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }
                        var blob = this.response;
                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveBlob(blob, filename);
                        }
                        else {
                            var downloadLink = window.document.createElement('a');
                            var contentTypeHeader = request.getResponseHeader("Content-Type");
                            downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                            downloadLink.download = filename;
                            document.body.appendChild(downloadLink);
                            downloadLink.click();
                            document.body.removeChild(downloadLink);
                        }
                    } else {
                        alert('Download failed.')
                    }
                };
                request.send(JSON.stringify(data));
            }
        },
        {
            extend: 'selected',
            className: "btn btn-primary",
            // id: 'ExportButton',
            text: "Export Selected",
            action: function (e, dt, node, config) {
                var data = {
                  'selected_users': selected_users
                };
                var request = new XMLHttpRequest();
                request.open('POST', 'export_users/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function (e) {
                    if (this.status === 200) {
                        var filename = "";
                        var disposition = request.getResponseHeader('Content-Disposition');
                        // check if filename is given
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                        }
                        var blob = this.response;
                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveBlob(blob, filename);
                        }
                        else {
                            var downloadLink = window.document.createElement('a');
                            var contentTypeHeader = request.getResponseHeader("Content-Type");
                            downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                            downloadLink.download = filename;
                            document.body.appendChild(downloadLink);
                            downloadLink.click();
                            document.body.removeChild(downloadLink);
                        }
                    } else {
                        alert('Download failed.')
                    }
                };
                request.send(JSON.stringify(data));

            }
        },
        {
            extend: 'selectedSingle',
            text: 'View',
            className: "btn",
            action: function ( e, dt, node, config ) {
                $("#edit-user-Modal").modal('show');
                // $('#new-user-form input, #new-user-form input:checkbox, #new-user-form select').attr('readonly', 'readonly');
                $('#edit-user-form fieldset.support-view-mode').attr('disabled', 'disabled');
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                      get_user(this['DT_RowId']);
                });

                $('#user-modal-title').text('View User');

                // Disable enabled button
                var button = $("#wizard").find('a[href="#' + 'finish' + '"]');
                button.attr("href", '#' + 'finish' + '-disabled');
                button.parent().addClass("disabled");
            }
        },
        {
            extend: 'selectedSingle',
            text: 'Edit',
            className: "btn waves-effect",
            action: function ( e, dt, node, config ) {
                $("#edit-user-Modal").modal('show');
                $('#edit-user-form fieldset.support-view-mode').removeAttr("disabled");
                $('#user-modal-title').text('Edit User');
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                      get_user(this['DT_RowId']);
                });

                // Enable disabled button
                var button = $("#wizard").find('a[href="#' + 'finish' + '-disabled"]');
                button.attr("href", '#' + 'finish');
                button.parent().removeClass();
            }
        },
        {
            extend: 'selected',
            text: 'Delete',
            className: "btn btn-danger",
            action: function ( e, dt, node, config ) {
                var arr = [];
                $.each(dt.rows({ selected: true }).data(), function() {
                    arr.push(this['DT_RowId']);
                });
                delete_users(arr, dt);
            }
        },
        {
            className: "btn btn-primary",
            text: "Add New User",
            action: function (e, dt, node, config) {
                window.location.href = "/accounts/new_user/";
            }
        },
        {
            extend: 'selectAll',
            text: 'Select All Users',
            className: "btn btn-primary",
            action : function(e, dt, node, config) {
                // Option 1
                // $.ajax({
                //     url: '/accounts/get_user_ids/',
                //     data: Object.assign({}, dt.ajax.params(), {
                //       'custom_order': col_order_local,
                //       'user_type': user_type_list,
                //       'email': email_filter,
                //       'sales_person_name': sales_person_filter,
                //       'dept': dept_filter,
                //       'cu_types': cu_types_filter,
                //       'u_status': u_status_filter,
                //       'country': country_filter,
                //       'city': city_filter,
                //       'region_state': region_state_filter,
                //       'close_to_city': close_to_city_filter
                //     }),
                //
                //     type: "POST",
                //     dataType: 'json',
                //     success: function (response) {
                //         // console.log('success', response);
                //         for (var id of response) {
                //             var index = $.inArray(id, selected_users);
                //             if (index === -1) {
                //                 selected_users.push( id );
                //             }
                //         }
                //     },
                //     error: function(xhr, error){
                //       alert('error on getting user IDs');
                //     }
                // });

                // Option 2
                var data = Object.assign({}, dt.ajax.params(), {
                      'custom_order': col_order_local,
                      'user_type': user_type_list,
                      'email': email_filter,
                      'sales_person_name': sales_person_filter,
                      'dept': dept_filter,
                      'cu_types': cu_types_filter,
                      'u_status': u_status_filter,
                      'country': country_filter,
                      'city': city_filter,
                      'region_state': region_state_filter,
                      'close_to_city': close_to_city_filter
                    });
                var request = new XMLHttpRequest();
                request.open('POST', '/accounts/get_user_ids/', true);
                request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                request.responseType = 'json';

                request.onload = function (e) {
                    if (this.status === 200) {
                        // console.log('success', this.response);
                        for (var id of this.response) {
                            var index = $.inArray(id, selected_users);
                            if (index === -1) {
                                selected_users.push( id );
                            }
                        }
                    } else {
                        alert('error on getting user IDs');
                    }
                };
                request.send(JSON.stringify(data));

                dt.rows().select();
            }
        },
        {
            extend: 'selected',
            className: "btn btn-primary",
            // id: 'ExportButton',
            text: "Add to Email",
            action: function (e, dt, node, config)
            {
                var temp = [];
                if ('selected_users' in localStorage) {
                    // console.log('array found');
                    temp = JSON.parse(localStorage.getItem('selected_users'));
                } else {
                    // console.log('array not found', selected_users)
                }
                temp = temp.concat(selected_users);
                localStorage.setItem('selected_users', JSON.stringify(temp));
                window.location.href = "/accounts/compose_email/?tab=user";
            }
        },
        // 'selected',
        // 'selectedSingle',
        // 'selectAll',
        // 'selectNone',
        // 'selectRows',
        // 'selectColumns',
        // 'selectCells'
    ],
    "select": {
        // style: 'multi'
        style: 'os'
    },
    ajax: function ( data, callback, settings ) {
        if (users_ready === false) {
            return;
        }

        prepare_type_list();
        $.ajax({
        url: '/accounts/dt_list_users/',
        data: Object.assign({}, data, {
          'custom_order': col_order_local,
          'user_type': user_type_list,
          'email': email_filter,
          'sales_person_name': sales_person_filter,
          'dept': dept_filter,
          'cu_types': cu_types_filter,
          'u_status': u_status_filter,
          'country': country_filter,
          'city': city_filter,
          'region_state': region_state_filter,
          'close_to_city': close_to_city_filter
          // 'weight_to': weight_to,
          // 'color': color_list,
          // 'clarity': clarity_list,
          // 'lab': lab_list,
          // 'location': location_list,
          // 'feed': feed_list,
          // 'polish': polish_list,
          // 'symmetry': symmetry_list,
          // 'cut': cut_list,
          // 'fluorescence': fluorescence_list,
          // 'depth_from': depth_from,
          // 'depth_to': depth_to,
          // 'table_from': table_from,
          // 'table_to': table_to,
          // 'discount_from': discount_from,
          // 'discount_to': discount_to,
          // 'page': page
        }),
        type: "POST",
        dataType: 'json',
        success: function (data) {
            count = data.count;
            pages = data.pages;
            next = data.next;
            previous = data.previous;
            page = parseInt(data.page, 10);
            var out = [];
            // console.log(col_order_local);
            $.each(data.data, function(key, value) {
                // console.log(value);
                out.push( {
                "0": value[col_order_local[0]],
                "1": value[col_order_local[1]],
                "2": value[col_order_local[2]],
                "3": value[col_order_local[3]],
                "4": value[col_order_local[4]],
                "5": value[col_order_local[5]],

                "6": value[col_order_local[6]],
                "7": value[col_order_local[7]],
                "8": value[col_order_local[8]],
                "9": value[col_order_local[9]],
                "10": value[col_order_local[10]],

                "11": value[col_order_local[11]],
                "12": value[col_order_local[12]],
                "13": value[col_order_local[13]],
                "14": value[col_order_local[14]],
                "15": value[col_order_local[15]],

                "16": value[col_order_local[16]],
                "17": value[col_order_local[17]],
                "18": value[col_order_local[18]],
                "19": value[col_order_local[19]],
                "20": value[col_order_local[20]],
                // (!value[col_order_local[20]] || value[col_order_local[20]]===''?"":$.format.date(value[col_order_local[20]], longDateFormat)),

                "21": value[col_order_local[21]],
                "22": value[col_order_local[22]],
                "23": value[col_order_local[23]],
                "24": value[col_order_local[24]],
                "25": value[col_order_local[25]],

                "26": value[col_order_local[26]],
                "27": value[col_order_local[27]],
                "28": value[col_order_local[28]],
                "29": value[col_order_local[29]],
                "30": value[col_order_local[30]],

                "31": value[col_order_local[31]],
                "32": value[col_order_local[32]],
                "33": value[col_order_local[33]],
                "34": value[col_order_local[34]],
                "35": value[col_order_local[35]],

                "36": value[col_order_local[36]],
                "37": value[col_order_local[37]],
                "38": value[col_order_local[38]],
                // (!value[col_order_local[39]] || value[col_order_local[39]]===''?"":$.format.date(value[col_order_local[39]], longDateFormat)),
                // (!value[col_order_local[40]] || value[col_order_local[40]]===''?"":$.format.date(value[col_order_local[40]], longDateFormat)),
                "39": value[col_order_local[39]],
                "40": value[col_order_local[40]],

                // (value[col_order_local[41]]!==' '&&value[col_order_local[41]]!==''?'<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="'+value[col_order_local[41]]+'">View</a>':""),
                "41": value[col_order_local[41]],
                "42": value[col_order_local[42]],
                "43": value[col_order_local[43]],
                "44": value[col_order_local[44]],
                "45": value[col_order_local[45]],

                "46": value[col_order_local[46]],
                "47": value[col_order_local[47]],
                "48": value[col_order_local[48]],
                "49": value[col_order_local[49]],
                "50": value[col_order_local[50]],
                "DT_RowId": value[0]
                })

            });
            // setTimeout( function () {
            //     callback( {
            //         draw: data.draw,
            //         data: out,
            //         recordsTotal: 5000000,
            //         recordsFiltered: 5000000
            //     } );
            // }, 50 );

            // multi_table.draw();
            running = false;
            page += 1;
            $("#more-loader").hide();

            // console.log(col_order_local, out);

            callback( {
                draw: data.draw,
                data: out,
                recordsTotal: data.recordsTotal,
                recordsFiltered: data.recordsFiltered
            } );

        },
        // error: function(xhr, error){
        //     $("#more-loader").hide();
        //     console.debug(xhr); console.debug(error);
        // },
        //   success:function(data){
        //     callback(data);
        //     // Do whatever you want.
        //   }
        });
    },
    scrollY: 650,
    scrollX: "100%",
    scrollCollapse: true,
    scrollInfinite: true,
    scroller: {
        loadingIndicator: true
    }
});

// row select case
user_table_server.on('select', function (e, dt, type, indexes) {
    if (type === 'row') {
        for (var index of indexes) {
            // this.api().row( index ).select();
            var id = dt.row(index).data().DT_RowId.toString();
            index = $.inArray(id, selected_users);
            // console.log(id, index);

            if (index === -1) {
                selected_users.push( id );
            }
        }
        // console.log(selected_users)
    }
} );


// row deselect case
user_table_server.on('deselect', function (e, dt, type, indexes) {
    if (type === 'row') {
        for (var index of indexes) {
            // this.api().row( index ).select();
            var id = dt.row(index).data().DT_RowId.toString();
            index = $.inArray(id, selected_users);
            // console.log(id, index);

            if (index !== -1) {
                selected_users.splice( index, 1 );
            }
        }
        // console.log(selected_stones)
    }
} );


function fix_col_reorder_issues(table) {
    // Visibility problem
    table.button(0).remove();
    table.button().add(0,
    {
        extend: 'colvis',
        text: 'Show/Hide Columns',
        className: "btn btn-primary",
        autoClose: false,
        fade: 0,
        columns: ':not(.noColVis)',
        colVis: { showAll: "Show all" }
    });
    // console.log('col re-ordered!');
    // incorrect columns problem
    table.ajax.reload();
    // table.draw(); also cal API
}


user_table_server.on('column-reorder', function (e, settings, details) {
    fix_col_reorder_issues(user_table_server);
    // console.log(e);
    // console.log(settings);
    // console.log(details);
    //     var url = window.location.pathname;
    //     var arr = url.split("/");
    //     if (arr[2]==='customers') {
    //         // console.log(col_order_local);
    //         // this.api().columns(col_order_local.findIndex(find_index)).visible(false);
    //         // user_table_server.columns(col_order_local.findIndex(find_index)).visible(false);
    //         var index = user_table_server.columns().names().indexOf('ID');
    //         console.log(index);
    //         // user_table_server.columns(index).visible(false);
    //     }
});

//
// user_table_server.on( 'select deselect', function () {
//     var selectedRows = user_table_server.rows( { selected: true } ).count();
//     user_table_server.button(1,2,3,4,5).enable( selectedRows === 1 );
//     user_table_server.button(1,4,5).enable( selectedRows > 0 );
// } );


function prepare_type_list() {
    var url = window.location.pathname;
    var arr = url.split("/");
    if (arr[2]==='sales_persons') {
        user_type_list = ['SP'];
    } else if (arr[2]==='customers') {
        user_type_list = ['CU', 'VI'];
    } else if (arr[2]==='sales_managers') {
        user_type_list = ['SM'];
    } else if (arr[2]==='admins') {
        user_type_list = ['AD'];
    } else {
        user_type_list = [];
    }
    // console.log(user_type_list);
}


var sales_person_filter = '';
var email_filter = '';
var dept_filter = '';
var cu_types_filter = '';
var u_status_filter = '';
var country_filter = '';
var city_filter = '';
var region_state_filter = '';
var close_to_city_filter = '';

// User filters module
$("#sales-person-filter").change(function () {
    sales_person_filter = this.value;
    // var name = $('#sales-person-filter').val();
});
$("#email-filter").change(function () {
    email_filter = this.value;
});
$("#dept-filter").change(function () {
    dept_filter = this.value;
});
$("#cu-type-filter").change(function () {
    cu_types_filter = this.value;
});
$("#u-status-filter").change(function () {
    u_status_filter = this.value;
});
$("#country-filter").change(function () {
    country_filter = this.value;
});
$("#city-filter").change(function () {
    city_filter = this.value;
});
$("#region-state-filter").change(function () {
    region_state_filter = this.value;
});
$("#close-to-city-filter").change(function () {
    close_to_city_filter = this.value;
});

$('input[id=email-filter]').change(function() {
    email_filter = this.value;
});


$("#search-users").unbind().click(function () {
    // multi_table.clear().draw();
    // get_id();
    user_table_server.ajax.reload();
    save_user_filters_state();
});

// on reset
$("#reset-users").unbind().click(function () {
    $("#user-count-loader").hide();
    $('#interactive-user-count').html('All filters cleared!');

    // $('#email-filter').val(null);
    $('#email-filter').val("").trigger("change");
    email_filter = '';
    $('#sales-person-filter').val("").trigger("change");
    sales_person_filter = '';
    $('#dept-filter').val("").trigger("change");
    dept_filter = '';
    $('#cu-type-filter').val("").trigger("change");
    cu_types_filter = '';
    $('#u-status-filter').val("").trigger("change");
    u_status_filter = '';
    $('#country-filter').val("").trigger("change");
    country_filter = '';
    $('#city-filter').val("").trigger("change");
    city_filter = '';
    $('#region-state-filter').val("").trigger("change");
    region_state_filter = '';
    $('#close-to-city-filter').val("").trigger("change");
    close_to_city_filter = '';

    // // shape case
    // $('.shp').prop('checked', false);
    // shape_list = [];
    //
    // // weight case
    // $('#weight_from').val(null);
    // $('#weight_to').val(null);
    // weight_from = 0;
    // weight_to = 200;
    //
    // // color case
    // $('input.type1').prop('checked', false);
    // $('input.type2').prop('checked', false);
    // // $('input.type1').prop('checked', false).attr("disabled", false);
    // // $('input.type2').prop('checked', false).attr("disabled", true);
    // // $("div.type2").removeClass('checkbox-primary').addClass('checkbox-default');
    // // $("div.type1").removeClass('checkbox-default').addClass('checkbox-primary');
    // color_list = [];
    //
    // // clarity case
    // $('input.clarity').prop('checked', false);
    // clarity_list = [];
    //
    // $('input.lab').prop('checked', false);
    // lab_list = [];
    //
    // $('input.location').prop('checked', false);
    // location_list = [];
    //
    // $('input.stonegroup').prop('checked', false);
    // feed_list = [];
    //
    // $('input.polish').prop('checked', false);
    // polish_list = [];
    //
    // $('input.symmetry').prop('checked', false);
    // symmetry_list = [];
    //
    // $('input.cut').prop('checked', false);
    // cut_list = [];
    //
    // $('input.fluorescence').prop('checked', false);
    // fluorescence_list = [];
    //
    // $('#depth_from').val(null);
    // $('#depth_to').val(null);
    // depth_from = 0;
    // depth_to = 200;
    //
    // $('#table_from').val(null);
    // $('#table_to').val(null);
    // table_from = 0;
    // table_to = 200;
    //
    // $('#discount_from').val(null);
    // $('#discount_to').val(null);
    // discount_from = -200;
    // discount_to = 200;
    //
    // $('#lotid').val(null);
    save_user_filters_state();

    // Option 1 -> count problems
    // user_table_server.clear().draw(false);
    // user_table_server.ajax.reload();
    // Option 2
    window.location.reload(false);
    // Option 3
    // $('.dataTables_scrollBody').css('min-height', '600px');
    // user_table_server.ajax.reload();
});


// $('#email-filter').autocomplete({
//     // source: false,
//     // options.
//     autocompleteOptions: {
//         "source": false
//     },
//     serviceUrl: 'accounts/autocomplete/email/',
//     onSelect: function (suggestion) {
//         alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
//     }
// });

// $("#email-filter").autocomplete(
// {
//     search: function () {},
//     source: function (request, response)
//     {
//         $.ajax({
//             url: 'autocomplete/email/',
//             dataType: "json",
//             data: {
//                 q: request.term
//             },
//             success: function (data) {
//                 response(data);
//             }
//         });
//     },
//     minLength: 2,
//     select: function (event, ui) {
//         var test = ui.item ? ui.item.id : 0;
//         if (test > 0) {
//            alert(test);
//         }
//     }
// });

function formatRepo(repo) {
    if (repo.loading) return repo.text;

    var markup = "<div class='select2-result-repository clearfix'>" +
        // "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
        // "<div class='select2-result-repository__meta'>" +
        // "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
        "<div class='select2-result-repository__title'>" + repo.label + "</div>";

    // if (repo.description) {
    //     markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
    // }

    markup += "<div class='select2-result-repository__statistics'>" +
        // "<div class='select2-result-repository__forks'><i class='icofont icofont-flash'></i> " + repo.forks_count + " Forks</div>" +
        // "<div class='select2-result-repository__stargazers'><i class='icofont icofont-star'></i> " + repo.stargazers_count + " Stars</div>" +
        // "<div class='select2-result-repository__watchers'><i class='icofont icofont-eye-alt'></i> " + repo.watchers_count + " Watchers</div>" +
        // "</div>" +
        "</div></div>";

    return markup;
}

function formatRepoSelection(repo) {
    // return repo.full_name || repo.text;
    // return repo.full_name;
    return repo.value || repo.text;
}

$(".email-filter-ajax").each(function() {
    var thisId = this.id;
    var $p = $(this).parent();

    $(this).select2({
        ajax: {
            // url: "https://api.github.com/search/repositories",
            url: "/accounts/autocomplete/email/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                // console.log(params);
                return {
                    q: params.term,  // search term
                    id: thisId
                };
            },
            processResults: function (data, params) {
                // console.log(data);
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                // params.page = params.page || 1;

                return {
                    results: data
                    // results: data.items,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        dropdownParent: $p,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
});


//
// $('#email-user-table').on('load-success.bs.table', function() {
//     $('#email-user-table').Tabledit({
//         url: '/pg/as_CategoriaArticolo/Update',
//         columns: {
//             identifier: [0, 'index'],
//             editable: [, [1, 'denominazione'], [2, 'denominazione_breve']]
//         }
//     });
// });

var unique_mobile;

function set_mobile_unique(value) {
    if (value === true) {
        $('#mobile').rules('remove', 'uniqueMobile');
        $("#new-user-form").validate().element("#mobile");
        // $('#mobile-error').remove();
        // $('#mobile').change();
        // $('#mobile').rules('remove');
        // $('#mobile').rules('add', { required: true });
        $("#mobile-1-modal").modal('hide');
        $("#mobile-2-modal").modal('hide');
    }
    // unique_mobile = value;
}


function logout_callback() {
    // console.log('logout called');
    localStorage.setItem('selected_stones', JSON.stringify([]));
    localStorage.setItem('selected_users', JSON.stringify([]));
    window.localStorage.removeItem('diamonds_state');
    window.localStorage.removeItem('users_state');
    window.localStorage.removeItem('history_state');
    handleSignoutClick();
}



function load_user_filters_state() {
    if ('users_state' in localStorage) {
        // console.log('array found');
        var users_state = JSON.parse(localStorage.getItem('users_state'));
        update_user_filters(users_state);
    }
    users_ready = true;
    user_table_server.ajax.reload();
}


function load_history_filters_state() {
    if ('history_state' in localStorage) {
        // console.log('array found');
        var history_state = JSON.parse(localStorage.getItem('history_state'));
        update_history_filters(history_state);
    }
    history_ready = true;
    activity_table_server.ajax.reload();
}

function save_user_filters_state() {
    var users_state = get_user_search_data();
    localStorage.setItem( 'users_state', JSON.stringify(users_state));
}


function save_history_filters_state() {
    var history_state = get_history_search_data();
    localStorage.setItem( 'history_state', JSON.stringify(history_state));
}


function update_user_filters(state) {
    $('#email-filter').val(state.email).trigger("change");
    email_filter = state.email;
    $('#select2-email-filter-container').html(state.email || 'All Emails');
    $('#sales-person-filter').val(state.sales_person_name).trigger("change");
    sales_person_filter = state.sales_person_name;
    $('#dept-filter').val(state.dept).trigger("change");
    dept_filter = state.dept;
    $('#cu-type-filter').val(state.cu_types).trigger("change");
    cu_types_filter = state.cu_types;
    $('#u-status-filter').val(state.u_status).trigger("change");
    u_status_filter = state.u_status;
    $('#country-filter').val(state.country).trigger("change");
    country_filter = state.country;
    $('#city-filter').val(state.city).trigger("change");
    city_filter = state.city;
    $('#region-state-filter').val(state.region_state).trigger("change");
    region_state_filter = state.region_state;
    $('#close-to-city-filter').val(state.close_to_city).trigger("change");
    close_to_city_filter = state.close_to_city;
    // user_table_server.ajax.reload();
}


function update_history_filters(state) {
    $('#history-email-filter').val(state.email).trigger("change");
    history_email_filter = state.email;
    // <span class="select2-selection__rendered" id="select2-history-email-filter-container" title="">ateeqsuhail@gmail.com</span>
    $('#select2-history-email-filter-container').html(state.email_title);

}



function get_user_search_data() {
    return {
      'email': email_filter,
      'sales_person_name': sales_person_filter,
      'dept': dept_filter,
      'cu_types': cu_types_filter,
      'u_status': u_status_filter,
      'country': country_filter,
      'city': city_filter,
      'region_state': region_state_filter,
      'close_to_city': close_to_city_filter
    };
}


function get_history_search_data() {
    return {
      'email': history_email_filter,
      'email_title': history_email_title
    };
}


function handle_email_active_tab() {
    var url = window.location.search;
    var arr = url.split("=");
    if (arr[0]==='?tab' && arr[1]==='stone') {
        $('#email-compose').removeClass('active');
        $('#email-stone-list').addClass('active');
        $('#email-user-list').removeClass('active');

        $('a[href*="#email-compose"]').removeClass('active');
        $('a[href*="#email-stone-list"]').addClass('active');
        $('a[href*="#email-user-list"]').removeClass('active');
    } else if (arr[0]==='?tab' && arr[1]==='user') {
        $('#email-compose').removeClass('active');
        $('#email-stone-list').removeClass('active');
        $('#email-user-list').addClass('active');

        $('a[href*="#email-compose"]').removeClass('active');
        $('a[href*="#email-stone-list"]').removeClass('active');
        $('a[href*="#email-user-list"]').addClass('active');
    }
}
// handle_email_active_tab();



function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}



// History Filters


var activity_table_server = $('#history-table-server').DataTable({
    "initComplete": function(settings, json) {
        // $('.dataTables_scrollBody').css('min-height', '600px');
    },

    // dom: 'Bfritip',
    dom: 'Biti',
    "serverSide": true,
    "searching": false,
    "colReorder": false,
    "ordering": true,
    // AutoWidth: false,

    buttons: [
    ],

    columnDefs: [
        // className: "truncate",
        { targets: [0,1,2,3,4,5,6,7,8,9], "createdCell": function (td, cellData, rowData, row, col) {
            // $(td).attr("title", rowData[col_order_local[col]]);
            // $(td).attr("title", rowData[col]);
            var title_value = "";
            var html_value = cellData.toString();
            if (typeof cellData === 'string' && cellData !== "" ) {
                var data = cellData.toString();
                if (col === 6 || col === 7) {
                    // data.replace(/[\r\n]/g, ' ');
                    data = replaceAll(data, '<b>', '');
                    data = replaceAll(data, '</b>', '');
                    data = replaceAll(data, '</br>', '\n');
                    // console.log(data);
                }
                var html = $.parseHTML(data);
                title_value = html[0].textContent;
                html_value = title_value;
            }

            html_value = html_value.substring(0,MAX_COL_LENGTH);
            if (html && html[0].tagName === 'A') {
                html[0].innerHTML = html_value;
                html_value = html[0].outerHTML;
                // console.log(cellData);
                // console.log('here', html_value);
            }
            $(td).attr("title", title_value);
            // $(td).html(html_value);
            this.api().cell(row, col).data(html_value);
            // console.log(html_value, title_value);

            // if (row === 0) {
            //    console.log(td, cellData, rowData, row, col, data, html, title_value, html_value);
            // }
            // if ( cellData < 1 ) {
            //   $(td).css('color', 'red')
            // }
        }},
        // { className: "truncate-3em", targets: [9], "createdCell": function (td, cellData, rowData, row, col) {
        //     var temp_value = "";
        //     // console.log(cellData);
        //     if (typeof cellData === 'string' && cellData !== "" ) {
        //         var data = cellData.toString();
        //         var html = $.parseHTML(data);
        //         temp_value = html[0].textContent;
        //     }
        //     $(td).attr("title", temp_value);
        // }},
        {
            targets: [0],
            className: 'noVis',
            visible: false
        },
        {
            targets: [0, 6, 7, 10],
            orderable: false
        }
    ],
    ajax: function ( data, callback, settings ) {
        $.ajax({
        url: '/diamonds/dt_list_activities/',
        data: Object.assign({}, data, {
          // 'custom_order': stones_col_order_local,
          'email': history_email_filter,
          'from_date': $('#from_date').val(),
          'to_date': $('#to_date').val(),

          // 'id': id_list,
          // 'weight_from': weight_from,
          // 'weight_to': weight_to,
          // 'color': color_list,
          // 'clarity': clarity_list,
          // 'lab': lab_list,
          // 'location': location_list,
          // 'feed': feed_list,
          // 'polish': polish_list,
          // 'symmetry': symmetry_list,
          // 'cut': cut_list,
          // 'fluorescence': fluorescence_list,
          // 'depth_from': depth_from,
          // 'depth_to': depth_to,
          // 'table_from': table_from,
          // 'table_to': table_to,
          // 'discount_from': discount_from,
          // 'discount_to': discount_to,
        }),
        type: "POST",
        dataType: 'json',
        success: function (data) {
            count = data.count;
            pages = data.pages;
            next = data.next;
            previous = data.previous;
            page = parseInt(data.page, 10);
            var out = [];
            $.each(data.data, function(key, value) {
                var params = prepare_for_display(value[6]);
                // console.log(value);
                // out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
                // this.api.row.add( [
                if (params.simple !== '' || params.complex !== '') {
                    out.push( {
                    "0": value[0],
                    "1": value[1],
                    "2": value[2],
                    "3": '<a href="javascript:void(0);" onClick="open_user_modal(' + value[11] + ')">' + value[3] + '</a>',
                    "4": '<a href="javascript:void(0);" onClick="addressed_email(' + value[11] + ')">' + value[4] + '</a>',
                    "5": value[5],

                    "6": params.simple,
                    "7": params.complex,
                    "8": value[8],
                    "9": value[9],
                    "10": '<a class="btn btn-info " style="padding: 1px 4px;" onclick="repeat_search(' + value[0] + ')">Repeat</a>',
                    "DT_RowId": value[0]
                    } );
                }

            });
            running = false;
            page += 1;
            $('#history-weekly-count').text(data.recordsWeekly);
            $('#history-monthly-count').text(data.recordsMonthly);
            $('#history-total-count').text(data.recordsTotal);
            // $('#history-total-sales').text(data.recordsTotal);

            callback( {
                draw: data.draw,
                data: out,
                recordsTotal: data.recordsTotal,
                recordsFiltered: data.recordsFiltered
            } );

        }
        });

    },
    scrollY: 650,
    scrollX: "100%",
    scrollCollapse: true,
    scrollInfinite: true,
    // "scrollY": $(window).scrollTop() >= $('#diamond-table-server').offset().top + $('#diamond-table-server').outerHeight() - window.innerHeight,
    scroller: {
        loadingIndicator: true
    }
});

function open_user_modal(id) {
    $("#edit-user-Modal").modal('show');
    // $('#new-user-form input, #new-user-form input:checkbox, #new-user-form select').attr('readonly', 'readonly');
    $('#edit-user-form fieldset.support-view-mode').attr('disabled', 'disabled');

    get_user(id);

    $('#user-modal-title').text('View User');

    // Disable enabled button
    var button = $("#wizard").find('a[href="#' + 'finish' + '"]');
    button.attr("href", '#' + 'finish' + '-disabled');
    button.parent().addClass("disabled");
}

function addressed_email(id) {
    var temp = [id];
    localStorage.setItem('selected_users', JSON.stringify(temp));
    window.location.href = "/accounts/compose_email/?tab=user";
}

function addressed_history(id, email) {
    // console.log(id, email);
    history_email_filter = id;
    history_email_title = email;
    save_history_filters_state();
    window.location.href = "/accounts/history/";
}

function repeat_search(search_id) {
    window.location.href = "/diamonds/?resume=" + search_id;
    // console.log('search_id', search_id);
}


function prepare_for_display(data) {
    var parameters = JSON.parse(data);
    var simple = '';
    var complex = '';
    var first = true;

    $.each(parameters.shape, function(key, value) {
        if (value === 'Asscher') value = 'Sq Emerald';
        if (first === true) {
            simple += '<b>Shape: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.color, function(key, value) {
        if (first === true) {
            simple += '</br><b>Color: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.clarity, function(key, value) {
        if (first === true) {
            simple += '</br><b>Clarity: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.lab, function(key, value) {
        if (first === true) {
            simple += '</br><b>Lab: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.location, function(key, value) {
        if (value === 'Hong_Kong') value = 'Hong Kong';
        if (first === true) {
            simple += '</br><b>Location: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    // first = true;
    // $.each(parameters.feed, function(key, value) {
    //     if (value === 'LSDCO_IL') value = 'LSDCO IL';
    //     if (first === true) {
    //         simple += '</br><b>Group: </b>' + value
    //     } else {
    //         simple += ', ' + value;
    //     }
    //     first = false;
    // });
    first = true;
    $.each(parameters.polish, function(key, value) {
        if (value === 'VERY_GOOD') value = 'VERY GOOD';
        if (first === true) {
            complex += '</br><b>Polish: </b>' + value
        } else {
            complex += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.symmetry, function(key, value) {
        if (value === 'VERY_GOOD') value = 'VERY GOOD';
        if (first === true) {
            complex += '</br><b>Symmetry: </b>' + value
        } else {
            complex += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.fluorescence, function(key, value) {
        if (value === 'VERY_GOOD') value = 'VERY GOOD';
        if (first === true) {
            complex += '</br><b>Fluorescence: </b>' + value
        } else {
            complex += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.cut, function(key, value) {
        if (value === 'VERY_GOOD') value = 'VERY GOOD';
        if (value === 'other') value = 'Other';
        if (first === true) {
            complex += '</br><b>Cut: </b>' + value
        } else {
            complex += ', ' + value;
        }
        first = false;
    });
    first = true;
    $.each(parameters.id, function(key, value) {
        if (first === true) {
            simple += '</br><b>ID: </b>' + value
        } else {
            simple += ', ' + value;
        }
        first = false;
    });
    first = true;

    if (parameters.depth_from !== depth_from || parameters.depth_to !== depth_to) {
        complex += '</br><b>Depth: </b>' + parameters.depth_from + ' - ' + parameters.depth_to
    }
    if (parameters.table_from !== table_from || parameters.table_to !== table_to) {
        complex += '</br><b>Table: </b>' + parameters.table_from + ' - ' + parameters.table_to
    }
    if (parameters.discount_from !== discount_from || parameters.discount_to !== discount_to) {
        complex += '</br><b>Discount: </b>' + parameters.discount_from + ' - ' + parameters.discount_to
    }
    if (parameters.weight_from !== weight_from || parameters.weight_to !== weight_to) {
        simple += '</br><b>Cts: </b>' + parameters.weight_from + ' - ' + parameters.weight_to
    }
    // if (parameters. ==  || parameters. == ) {
    //     complex += '</br><b>: </b>' + parameters. + ' - ' + parameters.
    // }
    if (simple.startsWith("</br>")) {
        simple = simple.replace("</br>", "");
        // console.log(simple);
    }
    if (complex.startsWith("</br>")) {
        complex = complex.replace("</br>", "");
        // console.log(complex);
    }
    return {'simple': simple, 'complex': complex}
}




var history_email_filter = '';

$("#history-email-filter").change(function () {
    history_email_filter = this.value;
});


$("#search-activities").unbind().click(function () {
    activity_table_server.ajax.reload();
});

// on reset
$("#reset-activities").unbind().click(function () {
    $("#user-count-loader").hide();
    $('#interactive-user-count').html('All filters cleared!');

    $('#history-email-filter').val("").trigger("change");
    history_email_filter = '';
    $('#select2-history-email-filter-container').html('All Emails');

    // $('#sales-person-filter').val("").trigger("change");
    // sales_person_filter = '';
    // $('#dept-filter').val("").trigger("change");
    // dept_filter = '';
    // $('#cu-type-filter').val("").trigger("change");
    // cu_types_filter = '';
    // $('#u-status-filter').val("").trigger("change");
    // u_status_filter = '';
    // $('#country-filter').val("").trigger("change");
    // country_filter = '';
    // $('#city-filter').val("").trigger("change");
    // city_filter = '';
    // $('#region-state-filter').val("").trigger("change");
    // region_state_filter = '';
    // $('#close-to-city-filter').val("").trigger("change");
    // close_to_city_filter = '';
    save_history_filters_state();


    // Option 1 -> count problems
    // user_table_server.clear().draw(false);
    // user_table_server.ajax.reload();
    // Option 2
    window.location.reload(false);
    // Option 3
    // $('.dataTables_scrollBody').css('min-height', '600px');
    // user_table_server.ajax.reload();
});


// FOLLOW UPS

var follow_up_table_server = $('#follow-up-table-server').DataTable({
    "initComplete": function(settings, json) {
    },

    dom: 'Biti',
    "serverSide": true,
    "searching": false,
    "colReorder": false,
    "ordering": true,

    buttons: [
        {
            text: 'Add Follow Up',
            className: "btn",
            action: function ( e, dt, node, config ) {
                $("#follow-up-modal").modal('show');
                // $("#edit-user-Modal").modal('show');

                // $('#new-user-form input, #new-user-form input:checkbox, #new-user-form select').attr('readonly', 'readonly');
                // $('#edit-user-form fieldset.support-view-mode').attr('disabled', 'disabled');
                // $.each(dt.rows({ selected: true }).data(), function() {
                //       // console.log(this);
                //       get_user(this['DT_RowId']);
                // });
                //
                $('#follow-up-modal-title').text('Add Follow Up');
                $("#follow-up-form").on('submit', {id: null, dt:dt}, create_update_followup);
                //
                // // Disable enabled button
                // var button = $("#wizard").find('a[href="#' + 'finish' + '"]');
                // button.attr("href", '#' + 'finish' + '-disabled');
                // button.parent().addClass("disabled");
            }
        },
        {
            extend: 'selected',
            text: 'Delete',
            className: "btn btn-danger",
            action: function ( e, dt, node, config ) {
                var arr = [];
                $.each(dt.rows({ selected: true }).data(), function() {
                    arr.push(this['DT_RowId']);
                });
                delete_follow_ups(arr, dt);
            }
        },
        {
            extend: 'selectedSingle',
            text: 'Edit Follow Up',
            className: "btn",
            action: function ( e, dt, node, config ) {
                $("#follow-up-modal").modal('show');
                var temp_id;
                $.each(dt.rows({ selected: true }).data(), function() {
                      // console.log(this);
                    temp_id = this['DT_RowId'];
                    get_follow_up(temp_id);
                });

                $('#follow-up-modal-title').text('Edit Follow Up');
                $("#follow-up-form").on('submit', {id: temp_id, dt:dt}, create_update_followup);
            }
        }
    ],

    columnDefs: [
        // { targets: [0,1,2,3,4,5,6,7,8,9], "createdCell": function (td, cellData, rowData, row, col) {
        //     // $(td).attr("title", rowData[col_order_local[col]]);
        //     // $(td).attr("title", rowData[col]);
        //     var title_value = "";
        //     var html_value = cellData.toString();
        //     if (typeof cellData === 'string' && cellData !== "" ) {
        //         var data = cellData.toString();
        //         if (col === 6 || col === 7) {
        //             // data.replace(/[\r\n]/g, ' ');
        //             data = replaceAll(data, '<b>', '');
        //             data = replaceAll(data, '</b>', '');
        //             data = replaceAll(data, '</br>', '\n');
        //             // console.log(data);
        //         }
        //         var html = $.parseHTML(data);
        //         title_value = html[0].textContent;
        //         html_value = title_value;
        //     }
        //
        //     html_value = html_value.substring(0,MAX_COL_LENGTH);
        //     if (html && html[0].tagName === 'A') {
        //         html[0].innerHTML = html_value;
        //         html_value = html[0].outerHTML;
        //         // console.log(cellData);
        //         // console.log('here', html_value);
        //     }
        //     $(td).attr("title", title_value);
        //     // $(td).html(html_value);
        //     this.api().cell(row, col).data(html_value);
        //     // console.log(html_value, title_value);
        //
        //     // if (row === 0) {
        //     //    console.log(td, cellData, rowData, row, col, data, html, title_value, html_value);
        //     // }
        //     // if ( cellData < 1 ) {
        //     //   $(td).css('color', 'red')
        //     // }
        // }},
        // { className: "truncate-3em", targets: [9], "createdCell": function (td, cellData, rowData, row, col) {
        //     var temp_value = "";
        //     // console.log(cellData);
        //     if (typeof cellData === 'string' && cellData !== "" ) {
        //         var data = cellData.toString();
        //         var html = $.parseHTML(data);
        //         temp_value = html[0].textContent;
        //     }
        //     $(td).attr("title", temp_value);
        // }},
        {
            targets: [0, 9, 10],
            className: 'noVis',
            visible: false
        }
    ],
    ajax: function ( data, callback, settings ) {
        $.ajax({
        url: '/accounts/dt_list_follow_ups/',
        data: Object.assign({}, data, {
          'type': $('#filter-follow-up-type').val(),
          'status': $('#filter-follow-up-status').val(),
          'subject': $('#filter-follow-up-subject').val(),
          'notes': $('#filter-follow-up-notes').val(),
          'assigned_to': $('#filter-follow-up-assignee').val()
        }),
        type: "POST",
        dataType: 'json',
        success: function (data) {
            count = data.count;
            pages = data.pages;
            next = data.next;
            previous = data.previous;
            page = parseInt(data.page, 10);
            var out = [];
            $.each(data.data, function(key, value) {
                out.push( {
                    "0": value[0],
                    "1": value[1],
                    "2": value[2],
                    "3": '<a href="javascript:void(0);" onClick="open_user_modal(' + value[9] + ')">' + value[3] + '</a>',
                    "4": '<a href="javascript:void(0);" onClick="addressed_email(' + value[9] + ')">' + value[4] + '</a>',

                    "5": value[5],
                    "6": value[6],
                    "7": value[7],
                    "8": value[8],
                    "9": value[10],
                    "10": value[11],
                    "DT_RowId": value[0]
                } );
            });
            running = false;
            page += 1;
            // $('#history-weekly-count').text(data.recordsWeekly);
            // $('#history-monthly-count').text(data.recordsMonthly);
            // $('#history-total-count').text(data.recordsTotal);
            // $('#history-total-sales').text(data.recordsTotal);

            callback( {
                draw: data.draw,
                data: out,
                recordsTotal: data.recordsTotal,
                recordsFiltered: data.recordsFiltered
            } );

        }
        });

    },
    scrollY: 650,
    scrollX: "100%",
    scrollCollapse: true,
    scrollInfinite: true,
    scroller: {
        loadingIndicator: true
    },
    select: {
        // style: 'multi'
        // selector: 'td:first-child'
        style: 'os'
    }
});


$("#search-follow-up").unbind().click(function () {
    follow_up_table_server.ajax.reload();
});

$("#reset-follow-up").unbind().click(function () {
    $('#filter-follow-up-type').val('');
    $('#filter-follow-up-status').val('');
    $('#filter-follow-up-subject').val('');
    $('#filter-follow-up-notes').val('');

    $("#follow-up-count-loader").hide();
    $('#interactive-follow-up-count').html('All filters cleared!');

    $('#filter-follow-up-assignee').val("").trigger("change");
    $('#select2-filter-follow-up-assignee-container').html('All');

    // save_history_filters_state();

    // Option 1 -> count problems
    // user_table_server.clear().draw(false);
    follow_up_table_server.ajax.reload();
    // Option 2
    // window.location.reload(false);
    // Option 3
    // $('.dataTables_scrollBody').css('min-height', '600px');
    // user_table_server.ajax.reload();
});



function get_follow_up(id) {
    $("#follow-up-form-loader").show();
  $.ajax({
    url: '/accounts/get_follow_up/',
    data: {
      'id': id
    },
    type: "POST",
    dataType: 'json',
    success: function (follow_up) {
        $("#follow-up-form-loader").hide();
        populate_followup(follow_up);
    },
    error: function(xhr, error){
        $("#follow-up-form-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


function populate_followup(object) {
    // console.log(object);
    // $('#follow-up-customer').val(object.customer).trigger("change");
    // $('#select2-follow-up-customer-container').html(object.customer_email || 'Select Customer');

    // Set the value, creating a new option if necessary
    if ($('#follow-up-customer').find("option[value='" + object.customer + "']").length) {
        $('#follow-up-customer').val(object.customer).trigger('change');
    } else {
        // Create a DOM Option and pre-select by default
        var newOption = new Option(object.customer_email, object.customer, true, true);
        // Append it to the select
        $('#follow-up-customer').append(newOption).trigger('change');
    }


    $('#follow-up-subject').val(object.subject);
    $('#follow-up-type').val(object.type);

    // $('#follow-up-assignee').val(object.customer).trigger("change");
    // $('#select2-follow-up-assignee-container').html(object.assignee_email || 'Select One');
    // Set the value, creating a new option if necessary
    if ($('#follow-up-assignee').find("option[value='" + object.assigned_to + "']").length) {
        $('#follow-up-assignee').val(object.assigned_to).trigger('change');
    } else {
        // Create a DOM Option and pre-select by default
        newOption = new Option(object.assigned_to_email, object.assigned_to, true, true);
        // Append it to the select
        $('#follow-up-assignee').append(newOption).trigger('change');
    }

    $('#follow-up-status').val(object.status);
    $('#follow-up-due-datetime').val(object.due_date_time.format('yyyy-MM-ddThh:mm'));
    $('#follow-up-notes').val(object.notes);
    $('#follow-up-impressions').val(object.impressions);
    $('#follow-up-commands').val(object.commands);
}


function delete_follow_ups(ids, dt) {
  $("#follow-up-count-loader").show();
  $.ajax({
    url: '/accounts/delete_follow_ups/',
    data: {
      'ids': ids
    },
    type: "POST",
    dataType: 'json',
    success: function (data) {
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! Follow Up(s) has been deleted' + '</div>');

        $("#follow-up-count-loader").hide();
        dt.rows({ selected: true }).remove().draw( false );
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        $("#follow-up-count-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}


function create_update_followup(event) {
    // console.log('create follow up!');
    $("#follow-up-form-loader").show();
  var temp_data = {
      'subject': $('#follow-up-subject').val(),
      'customer_id': $('#follow-up-customer').val(),
      'type': $('#follow-up-type').val(),
      'assigned_to': $('#follow-up-assignee').val(),
      'status': $('#follow-up-status').val(),
      'due_date_time': $('#follow-up-due-datetime').val(),
      'notes': $('#follow-up-notes').val(),
      'impressions': $('#follow-up-impressions').val(),
      'commands': $('#follow-up-commands').val()
    };
  if (event.data.id) {
    temp_data['id'] = event.data.id;
  }

  $.ajax({
    url: '/accounts/create_update_follow_up/',
    dataType    : 'json',
    type: "POST",
    data: temp_data,

    success: function (data) {
        $('#messages').append(
            '<div class="alert alert-dismissable alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
            'Success! Follow Ups Updated!' + '</div>');

        $("#follow-up-form-loader").hide();
        $("#follow-up-modal").modal('hide');
        reset_followup_form();
        event.data.dt.ajax.reload();
        // $("#edit-user-Modal").hide();
    },
    error: function(xhr, error){
        $('#messages').append(
        '<div class="alert alert-dismissable alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a>' +
        'Failed! Something went wrong' + '</div>');
        $("#follow-up-form-loader").hide();
        console.debug(xhr); console.debug(error);
    }
  });
}



function reset_followup_form() {
  $('#follow-up-modal input[type="text"]').val('');
  $('#follow-up-modal textarea').val('');
}


// USER STATE OPTIMIZATION
var countDownDate;
var x;


function set_counter_distance(settings) {
    // Set the date we're counting down to
    // var countDownDate = new Date("Dec 16, 2019 17:37:25").getTime();

    // Get today's date and time
    countDownDate = new Date();
    // Find the distance between now and the count down date
    countDownDate.setSeconds(countDownDate.getSeconds() + 5);
    countDownDate = countDownDate.getTime();

    count_down_state_save(settings);
}


function count_down_state_save(settings) {
    if (x) {
        clearInterval(x);
    }

    // Update the count down every 1 second
    x = setInterval(function() {
      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      // var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      // var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"
      // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
      // + minutes + "m " + seconds + "s ";
      // console.log(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");

      // If the count down is over, write some text
      //   console.log(distance);
      if (distance < 0) {
        clearInterval(x);
        save_user_table_state(settings)
        // document.getElementById("demo").innerHTML = "EXPIRED";
        // console.log("State saved Now");
      }
    }, 1000);
}


function save_user_table_state(settings) {
    var data = localStorage.getItem(settings.sInstance );

    // // to handle initial case
    // col_order_local = data.ColReorder;
    // users_col_vis_local = temp.columns;

    $.ajax({
        url: '/accounts/update_state/',
        data: {
          'table_name': settings.sInstance,
          'table_state': data
        },
        type: "POST",
        async: true,
        dataType: 'json',
        success: function (response) {
          // localStorage.setItem( 'DataTables_' + settings.sInstance, JSON.stringify(data));
          // var temp = JSON.parse(data);
          // if (temp) {
          //   col_order_local = temp.ColReorder;
          //   // console.log('local copy updated', col_order_local)
          // }
        },
        error: function(xhr, error){
          console.log('error on state update');
        }
    });

}


function init_card_follow_up_table(data) {
    var follow_up_card_table = $('#follow-up-card-table').DataTable({
        "initComplete": function(settings, json) {
            var api = this.api();
            api.clear();
            $.each(data, function(key, value){
                api.row.add({
                    "0": value[0],
                    "1": value[1],
                    "2": value[2],
                    "3": value[3],
                    "4": '<a href="javascript:void(0);" onClick="addressed_email(' + value[9] + ')">' + value[4] + '</a>',

                    "5": value[5],
                    "6": value[6],
                    "7": value[7],
                    "8": value[8],
                    "9": value[10],
                    "10": value[11],
                    "DT_RowId": value[0]
                });
                // api.row.add( [
                //     value[0],
                //     value[1],
                //     value[2],
                //     value[3],
                //     '<a href="javascript:void(0);" onClick="addressed_email(' + value[9] + ')">' + value[4] + '</a>',
                //
                //     value[5],
                //     value[6],
                //     value[7],
                //     value[8],
                //     value[10],
                //     value[11]
                // ] ).node().id = value[0];
            });
            api.draw();
        },

        dom: 'Bfrtip',
        paging: false,
        searching: false,
        ordering: false,
        colReorder: false,
        info: false,
        destroy: true,
        stateSave: false,
        // columns: [
        //     { "orderable": false , targets: 0}
        // ],
        columnDefs: [{
            orderable: false,
            className: 'noVis',
            targets: [0,9,10],
            visible: false
        }],
        buttons: [
            {
                className: "btn btn-primary",
                text: "View All",
                action: function (e, dt, node, config) {
                    window.location.href = "/accounts/follow-ups/";
                }
            },
            {
                text: 'Add Follow Up',
                className: "btn",
                action: function ( e, dt, node, config ) {
                    $("#follow-up-modal").modal('show');
                    // $("#edit-user-Modal").modal('show');

                    // $('#new-user-form input, #new-user-form input:checkbox, #new-user-form select').attr('readonly', 'readonly');
                    // $('#edit-user-form fieldset.support-view-mode').attr('disabled', 'disabled');
                    // $.each(dt.rows({ selected: true }).data(), function() {
                    //       // console.log(this);
                    //       get_user(this['DT_RowId']);
                    // });
                    //
                    $('#follow-up-modal-title').text('Add Follow Up');
                    $("#follow-up-form").on('submit', {id: null, dt:follow_up_table_server}, create_update_followup);
                    // // Disable enabled button
                    // var button = $("#wizard").find('a[href="#' + 'finish' + '"]');
                    // button.attr("href", '#' + 'finish' + '-disabled');
                    // button.parent().addClass("disabled");
                }
            },
            {
                extend: 'selected',
                text: 'Delete',
                className: "btn btn-danger",
                action: function ( e, dt, node, config ) {
                    var arr = [];
                    $.each(dt.rows({ selected: true }).data(), function() {
                        arr.push(this['DT_RowId']);
                    });
                    delete_follow_ups(arr, dt);
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Edit Follow Up',
                className: "btn",
                action: function ( e, dt, node, config ) {
                    $("#follow-up-modal").modal('show');
                    var temp_id;
                    $.each(dt.rows({ selected: true }).data(), function() {
                        // console.log(this);
                        temp_id = this['DT_RowId'];
                        get_follow_up(temp_id);
                    });

                    $('#follow-up-modal-title').text('Edit Follow Up');
                    $("#follow-up-form").on('submit', {id: temp_id, dt:follow_up_table_server}, create_update_followup);
                }
            }
            // {
            //     className: "btn btn-danger",
            //     text: "Delete All",
            //     action: function ( e, dt, node, config ) {
            //         dt.rows().remove().draw( false );
            //         localStorage.setItem('selected_users', JSON.stringify([]));
            //     }
            // }
        ],
        select: {
            // style: 'multi'
            style: 'os'
        }
    });
}


$('#follow-up-modal').on('hidden.bs.modal', function () {
    // console.log('modal closed');
    $("#follow-up-form").off('submit', create_update_followup);
});
