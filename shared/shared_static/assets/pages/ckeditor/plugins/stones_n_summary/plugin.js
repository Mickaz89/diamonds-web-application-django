CKEDITOR.plugins.add( 'stones_n_summary', {
    // icons: 'stones_n_summary',
    // icon: this.path + 'icons/stones_n_summary.png',
    init: function( editor ) {
        editor.addCommand( 'insert_stones_n_summary', {
            exec: function( editor ) {
                editor.insertHtml('<table></table>');
                insert_stones_n_summary();
            }
        });
        editor.ui.addButton( 'StonesNSummary', {
            label: 'Insert Stones & Summary',
            command: 'insert_stones_n_summary',
            toolbar: 'insert,0',
            icon: this.path + 'icons/stones_n_summary.png'
        });
    }
});

