from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from YCS.shared_utils import UploadToPathAndRename

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the "
                                                               "format: '+999999999999'. Up to 15 digits allowed.")


class User(AbstractUser):
    DISPLAY_ABBREVIATIONS = {
        'SP': 'Sales Person',
        'SM': 'Sales Manager',
        'CU': 'Customer',
        'AD': 'Admin',
        'Mr.': 'Mr.',
        'Mrs': 'Mrs.',
        'Ms.': 'Ms.',
        'AC': 'Active',
        'BL': 'Blocked',
        'RE': 'Requested',
        '1': 'Active',
        '3': 'Blocked',
        '4': 'Requested',
        'DW': 'Diamond Wholesaler',
        'JR': 'Jewellery Retailer',
        'JM': 'Jewellery Manufacturer',
        'Own': 'Owner',
        'OT': 'Other',
        '': '',
    }

    DISPLAY_CUSTOMER_TYPE_ABBR = {
        'UN': 'Unproven',
        'PO': 'Potential',
        'CU': 'Customer',
        'VI': 'VIP Customer',
        '1': 'Unproven',
        '2': 'Potential',
        '3': 'Customer',
        '4': 'VIP Customer',
        '5': 'Semi Partner',
        '7': 'Buffer',
        '8': 'Travel',
        '9': 'Nov 2009',
        '11': 'itsaak_list',
        '12': 'Europe',
        '13': 'UnSubscribe',
        '': '',
    }

    Sales_Person = 'SP'
    Sales_Manager = 'SM'
    Customer = 'CU'
    Admin = 'AD'
    USER_TYPE_CHOICES = (
      (Sales_Person, 'Sales Person'),
      (Sales_Manager, 'Sales Manager'),
      (Customer, 'Customer'),
      (Admin, 'Admin'),
    )

    mr = 'Mr.'
    mrs = 'Mrs'
    ms = 'Ms.'
    TITLE_CHOICES = (
      (mr, 'Mr.'),
      (mrs, 'Mrs.'),
      (ms, 'Ms.'),
    )

    Active = 'AC'
    Blocked = 'BL'
    Requested = 'RE'
    USER_STATUS_CHOICES = (
      (Active, 'Active'),
      (Blocked, 'Blocked'),
      (Requested, 'Requested'),
      ("1", 'Active'),
      ("3", 'Blocked'),
      ("4", 'Requested'),
    )

    Unproven = 'UN'
    Potential = 'PO'
    Customer = 'CU'
    VIP_Customer = 'VI'
    CUSTOMER_TYPE_CHOICES = (
      (Unproven, 'Unproven'),
      (Potential, 'Potential'),
      (Customer, 'Customer'),
      (VIP_Customer, 'VIP Customer'),
      ("1", 'Unproven'),
      ("2", 'Potential'),
      ("3", 'Customer'),
      ("4", 'VIP Customer'),
      ("8", 'Travel'),
      ("12", 'Europe'),
      ("13", 'UnSubscribe')
    )

    Diamond_Wholesaler = 'DW'
    Jewellery_Retailer = 'JR'
    Jewellery_Manufacturer = 'JM'
    Other = 'OT'
    BUSINESS_TYPE_CHOICES = (
      (Diamond_Wholesaler, 'Diamond Wholesaler'),
      (Jewellery_Retailer, 'Jewellery Retailer'),
      (Jewellery_Manufacturer, 'Jewellery Manufacturer'),
      (Other, 'Other'),
    )

    Belgium = 'Belgium'
    China = 'China'
    France = 'France'
    HK = 'HK'
    USA = 'USA'
    Italy = 'Italy'
    Israel = 'Israel'
    DEPT_TYPE_CHOICES = (
      (Belgium, 'Belgium'),
      (China, 'China'),
      (France, 'France'),
      (HK, 'HK'),
      (USA, 'USA'),
      (Italy, 'Italy'),
      (Israel, 'Israel'),
    )

    Harmony = 'Harmony'
    Fantasy = 'Fantasy'
    SOURCE_TYPE_CHOICES = (
      (Harmony, 'Harmony'),
      (Fantasy, 'Fantasy'),
    )

    user_type = models.CharField(max_length=2, choices=USER_TYPE_CHOICES, null=False, blank=False)
    company_name = models.CharField(max_length=255, null=False, blank=False)
    card_name = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=3, choices=TITLE_CHOICES, null=False, blank=False)
    sales_manager = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True,
                                      related_name='sales_managers')
    sales_person = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True,
                                     related_name='sales_persons')
    sales_person_name = models.CharField(max_length=64, null=True, blank=True)

    user_status = models.CharField(max_length=2, choices=USER_STATUS_CHOICES, default=Active)
    customer_type = models.CharField(max_length=2, choices=CUSTOMER_TYPE_CHOICES, null=True, blank=True)
    mass_email = models.BooleanField(_('mass email'), default=True,
                                     help_text=_('Designates whether mass emails will be sent or not'))
    language = models.CharField(max_length=2, null=False, blank=False)
    markup = models.IntegerField(default=0)
    # suppliers

    street = models.CharField(max_length=64, null=True, blank=True)
    city = models.CharField(max_length=64, null=False, blank=False)
    zip_code = models.CharField(max_length=64, null=True, blank=True)
    country = models.CharField(max_length=3, null=False, blank=False)
    tel = models.CharField(validators=[phone_regex], max_length=15, null=True, blank=True)

    mobile = models.CharField(validators=[phone_regex], max_length=15, null=False, blank=False)
    region_state = models.CharField(max_length=64, null=True, blank=True)
    close_to_city = models.CharField(max_length=64, null=True, blank=True)
    tel3 = models.CharField(validators=[phone_regex], max_length=15, null=True, blank=True)
    birth_date = models.DateTimeField(null=True, blank=True)

    business_type = models.CharField(max_length=2, choices=BUSINESS_TYPE_CHOICES, default=Diamond_Wholesaler)
    web_site = models.URLField(max_length=200, null=True, blank=True)
    skype_user = models.CharField(max_length=200, null=True, blank=True)
    qq = models.URLField(max_length=200, null=True, blank=True)
    facebook = models.URLField(max_length=200, null=True, blank=True)

    twitter = models.URLField(max_length=200, null=True, blank=True)
    ref_name = models.CharField(max_length=200, null=True, blank=True)
    ref_phone = models.CharField(validators=[phone_regex], max_length=15, null=True, blank=True)
    jbt = models.CharField(max_length=200, null=True, blank=True)
    vat = models.CharField(max_length=200, null=True, blank=True)

    whitesite_markup = models.IntegerField(default=1)
    dept = models.CharField(max_length=16, choices=DEPT_TYPE_CHOICES, null=True, blank=True)
    r1 = models.CharField(max_length=200, null=True, blank=True)
    address_4 = models.CharField(max_length=200, null=True, blank=True)
    fantasy_no = models.CharField(max_length=200, null=True, blank=True)

    harmony_no = models.CharField(max_length=200, null=True, blank=True)
    data_source = models.CharField(max_length=16, choices=SOURCE_TYPE_CHOICES, null=True, blank=True)
    is_verified = models.BooleanField(_('is verified'), default=False,
                                      help_text=_('Designates whether verified or not'))
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    image = models.ImageField(upload_to=UploadToPathAndRename('Images/User/'),
                              default='Images/User/default.png', max_length=200)
    comment = models.TextField(null=True, blank=True)
    anc_interest = models.CharField(max_length=512, null=True, blank=True)
    signature = models.TextField(null=True, blank=True)
    signature_settings = models.BooleanField(_('signature_settings verified'), default=True,
                                             help_text=_('Use signature or not'))
    shape_box = models.CharField(max_length=512, null=True, blank=True)
    size_box = models.CharField(max_length=512, null=True, blank=True)
    color_box = models.CharField(max_length=512, null=True, blank=True)
    clarity_box = models.CharField(max_length=512, null=True, blank=True)
    lab_box = models.CharField(max_length=512, null=True, blank=True)
    make_box = models.CharField(max_length=512, null=True, blank=True)
    can_download_excel = models.BooleanField(_('can_download_excel'), default=False,
                                             help_text=_('Designates whether can_download_excel or not'))
    can_view_prices = models.BooleanField(_('can_view_prices'), default=False,
                                          help_text=_('Designates whether can_view_prices or not'))

    # MANAGERS
    # objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    # TO STRING METHOD
    def __str__(self):
        return str(self.username)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class UserSuppliers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    supplier = models.CharField(max_length=20, null=False, blank=False)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'UserSupplier'
        verbose_name_plural = 'UsersSuppliers'

    # TO STRING METHOD
    def __str__(self):
        return str(self.user)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class UserSaleModule(models.Model):
    Request = 'Request'
    Mazal = 'Mazal'
    dQuote = 'dQuote'
    MODULE_TYPE_CHOICES = (
      (Request, 'Request'),
      (Mazal, 'Mazal'),
      (dQuote, 'dQuote'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sale_module = models.CharField(max_length=8, choices=MODULE_TYPE_CHOICES, null=False, blank=False)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Sale Module'
        verbose_name_plural = 'Sale Modules'

    # TO STRING METHOD
    def __str__(self):
        return str(self.sale_module)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class TableState(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, null=False, blank=False)
    state = models.TextField(null=False, blank=False)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Table State'
        verbose_name_plural = 'Table States'

    # TO STRING METHOD
    def __str__(self):
        return str(self.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class SearchHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, null=False, blank=False)
    state = models.TextField(null=False, blank=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Search history'
        verbose_name_plural = 'Search Histories'

    # TO STRING METHOD
    def __str__(self):
        return str(self.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class EmailTemplate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=256, null=False, blank=False)
    language = models.CharField(max_length=2, null=False, blank=False)
    subject = models.CharField(max_length=256, null=False, blank=False)
    body = models.TextField(null=False, blank=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Email Template'
        verbose_name_plural = 'Email Templates'

    # TO STRING METHOD
    def __str__(self):
        return str(self.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


class FollowUps(models.Model):
    Phone = 1
    Email = 2
    rr = 3
    FOLLOWUP_TYPE_CHOICES = (
      (Phone, 'Phone Call'),
      (Email, 'Email'),
      (rr, 'rr'),
    )
    Open = 1
    Close = 2
    Reschedule = 3
    STATUS_TYPE_CHOICES = (
      (Open, 'Open'),
      (Close, 'Close'),
      (Reschedule, 'Close & Reschedule'),
    )

    subject = models.CharField(max_length=256, null=False, blank=False)
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customers')
    type = models.PositiveIntegerField(default=Phone, choices=FOLLOWUP_TYPE_CHOICES, null=False, blank=False)
    assigned_to = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    due_date_time = models.DateTimeField(null=False, blank=False)
    status = models.PositiveIntegerField(default=Open, choices=STATUS_TYPE_CHOICES, null=False, blank=False)
    notes = models.TextField(null=False, blank=False)
    impressions = models.TextField(null=True, blank=True)
    commands = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # MANAGERS
    objects = models.Manager()

    # META CLASS
    class Meta:
        verbose_name = 'Follow Up'
        verbose_name_plural = 'Follow Ups'

    # TO STRING METHOD
    def __str__(self):
        return str(self.subject)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.


