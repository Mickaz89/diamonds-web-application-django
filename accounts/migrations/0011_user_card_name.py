# Generated by Django 2.1.3 on 2019-07-01 10:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20190613_2107'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='card_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
