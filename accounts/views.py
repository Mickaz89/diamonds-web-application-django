import os
# import glob

# from django.conf import settings
import json
# from urllib.parse import parse_qs
import xlsxwriter
import io

from django.core.serializers import serialize
from django.forms import model_to_dict
from django.http import JsonResponse, HttpResponse
from django.urls import reverse_lazy
from django.views import generic
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

from .forms import CustomUserCreationForm
from .models import User, UserSuppliers, UserSaleModule, TableState, SearchHistory, EmailTemplate, FollowUps
from YCS.shared_utils import smart_str2bool

import csv
import glob
from datetime import datetime
from django.conf import settings
from diamonds.models import Diamond, CronLog, ActivityLog
from YCS.shared_utils import sophisticated_to_int, simple_to_float, remove_spaces, send_simple_email, \
    send_custom_email, unformat_phone_no, get_country_iso3, get_language_iso2, get_business_type, get_specific_value
# from copy import deepcopy
from django.db.models.functions import Lower
from django.db import IntegrityError
import uuid
import pandas as pd
import pytz
# User = settings.AUTH_USER_MODEL


user_columns = [
    'id', 'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status', 'customer_type',  # 7
    'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile', 'region_state',  # 17
    'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq', 'facebook', 'twitter',  # 26
    'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4', 'fantasy_no', 'harmony_no',  # 36
    'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment', 'email', 'name',  # 44
    'suppliers', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest']  # 50
# sales_person

follow_up_columns = [
                'id', 'status', 'type', 'name', 'customer__email', 'date_created', 'subject',
                'due_date_time', 'notes', 'impressions', 'commands']


direction_symbol = {
    'asc': '-',
    'desc': ''
}


class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!', extra_tags='Success')
            return redirect('home')
        else:
            messages.error(request, 'Please correct the error below.', extra_tags='Failed')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/password_change.html', {
        'form': form
    })


@login_required
def list_users(request):
    users_list = User.objects.filter(is_superuser=False).order_by('-date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(users_list, 10)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'base_users.html', {'users': users})


@login_required
def list_customers(request):
    users_list = User.objects.filter(user_type__in=[User.Customer, User.VIP_Customer]).order_by('-date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(users_list, 10)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'customer.html', {'users': users})


@login_required
def list_sales_persons(request):
    users_list = User.objects.filter(user_type__in=[User.Sales_Person]).order_by('-date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(users_list, 10)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'sales_person.html', {'users': users})


@login_required
def list_sales_managers(request):
    users_list = User.objects.filter(user_type__in=[User.Sales_Manager]).order_by('-date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(users_list, 10)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'sales_manager.html', {'users': users})


@login_required
def list_admins(request):
    users_list = User.objects.filter(user_type__in=[User.Admin]).order_by('-date_created')
    page = request.GET.get('page', 1)
    paginator = Paginator(users_list, 10)

    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        messages.warning(request, 'This page has no data', extra_tags='Failed')

    return render(request, 'admin.html', {'users': users})


@csrf_exempt
@login_required
def new_user(request):
    if request.method == 'POST':
        user_image = request.FILES.get('user_image', None)
        user_type = request.POST.get('user_type', None)
        company_name = request.POST.get('company_name', None)
        card_name = request.POST.get('card_name', None)
        e_mail = request.POST.get('email', None)
        title = request.POST.get('title', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        password = request.POST.get('password', None)
        sales_manager = request.POST.get('sales_manager', None)
        sales_person = request.POST.get('sales_person', None)
        # sales_person_name = request.POST.get('sales_person_name', None)
        user_status = request.POST.get('user_status', None)
        customer_type = request.POST.get('customer_type', None)
        mass_email = request.POST.get('mass_email', None)
        language = request.POST.get('language', None)
        markup = request.POST.get('markup', None)
        suppliers = request.POST.getlist('suppliers[]', None)
        sale_modules = request.POST.getlist('sale_modules[]', None)
        comment = request.POST.get('comment', None)
        anc_interest = request.POST.get('anc_interest', None)
        street = request.POST.get('street', None)
        city = request.POST.get('city', None)
        zip_code = request.POST.get('zip_code', None)
        country = request.POST.get('country', None)
        tel = request.POST.get('tel', None)
        mobile = request.POST.get('mobile', None)
        region_state = request.POST.get('region_state', None)
        close_to_city = request.POST.get('close_to_city', None)
        tel3 = request.POST.get('tel3', None)
        birth_date = request.POST.get('birth_date', None)
        business_type = request.POST.get('business_type', None)
        web_site = request.POST.get('web_site', None)
        skype_user = request.POST.get('skype_user', None)
        qq = request.POST.get('qq', None)
        facebook = request.POST.get('facebook', None)
        twitter = request.POST.get('twitter', None)
        ref_name = request.POST.get('ref_name', None)
        ref_phone = request.POST.get('ref_phone', None)
        jbt = request.POST.get('jbt', None)
        vat = request.POST.get('vat', None)
        whitesite_markup = request.POST.get('whitesite_markup', None)
        dept = request.POST.get('dept', None)
        r1 = request.POST.get('r1', None)
        address_4 = request.POST.get('address_4', None)
        fantasy_no = request.POST.get('fantasy_no', None)
        harmony_no = request.POST.get('harmony_no', None)
        data_source = request.POST.get('data_source', None)
        verified = smart_str2bool(request.POST.get('verified', None))
        shape_box = request.POST.get('shape_box', None)
        size_box = request.POST.get('size_box', None)
        color_box = request.POST.get('color_box', None)
        clarity_box = request.POST.get('clarity_box', None)
        lab_box = request.POST.get('lab_box', None)
        make_box = request.POST.get('make_box', None)
        can_download_excel = smart_str2bool(request.POST.get('download_excel', None))
        can_view_prices = smart_str2bool(request.POST.get('view_prices', None))

        try:

            if not birth_date or birth_date == '':
                birth_date = None

            if e_mail:
                if User.objects.filter(username__icontains=e_mail.lower()).exists():
                    return JsonResponse({'error': 'Failed! Email already registered'}, safe=True, status=400)

            person = None
            if sales_person and sales_person != '':
                person = User.objects.get(id=sales_person)

            user = User.objects.create(
                user_type=user_type,
                card_name=card_name,
                company_name=company_name,
                username=e_mail.lower(),
                email=e_mail.lower(),
                title=title,
                first_name=first_name,
                last_name=last_name,
                is_active=True,
                is_staff=True,
                # password=password,
                sales_manager_id=sales_manager,
                sales_person=person,
                sales_person_name=person.get_full_name(),
                user_status=user_status,
                customer_type=customer_type,
                mass_email=json.loads(mass_email),
                language=language,
                markup=markup,
                # suppliers: suppliers_list,
                comment=comment,
                anc_interest=anc_interest,
                street=street,
                city=city,
                zip_code=zip_code,
                country=country,
                tel=tel,
                mobile=mobile,
                region_state=region_state,
                close_to_city=close_to_city,
                tel3=tel3,
                birth_date=birth_date,
                business_type=business_type,
                web_site=web_site,
                skype_user=skype_user,
                qq=qq,
                facebook=facebook,
                twitter=twitter,
                ref_name=ref_name,
                ref_phone=ref_phone,
                jbt=jbt,
                vat=vat,
                whitesite_markup=whitesite_markup,
                dept=dept,
                r1=r1,
                address_4=address_4,
                fantasy_no=fantasy_no,
                harmony_no=harmony_no,
                data_source=data_source,
                is_verified=verified,
                shape_box=shape_box,
                size_box=size_box,
                color_box=color_box,
                clarity_box=clarity_box,
                lab_box=lab_box,
                make_box=make_box,
                can_download_excel=can_download_excel,
                can_view_prices=can_view_prices,
            )
            if password:
                user.set_password(password)
            if user_image:
                user.image = user_image
            user.save()

            if suppliers:
                for supplier in suppliers:
                    UserSuppliers.objects.create(user=user, supplier=supplier)
            if sale_modules:
                for sale_module in sale_modules:
                    UserSaleModule.objects.create(user=user, sale_module=sale_module)
        except Exception as e:
            print(e)
            return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

        return JsonResponse({'message': 'Success! User Created'}, safe=True, status=200)
    else:
        # handle GET
        managers = User.objects.filter(user_type=User.Sales_Manager).order_by('-date_created')
        # sales_persons = User.objects.filter(user_type__in=[User.Sales_Person]).order_by('-date_created')
        sales_persons = get_sales_persons_plus()
        return render(request, 'new_customer.html', {'managers': managers, 'sales_persons': sales_persons})


@csrf_exempt
@login_required
def unique_user(request):
    e_mail = request.POST.get('email', None)
    if e_mail:
        if User.objects.filter(username__icontains=e_mail.lower()).exists():
            return JsonResponse({'ok': False}, status=400)
    return JsonResponse({'ok': True}, status=200)


@csrf_exempt
@login_required
def unique_mobile(request):
    mobile = request.POST.get('mobile', None)
    if mobile:
        if User.objects.filter(mobile__icontains=mobile).exists():
            return JsonResponse({'ok': False}, status=400)
    return JsonResponse({'ok': True}, status=200)


@csrf_exempt
@login_required
def user_filters_data(request):
    from YCS.shared_utils import Countries
    # sales_persons = User.objects.filter(user_type__in=[User.Sales_Person])
    sales_persons = get_sales_persons_plus()
    sales_persons = json.loads(serialize('json', sales_persons))
    cities = list(User.objects.order_by('city').exclude(city__exact='').values_list('city', flat=True).distinct())
    region_state = list(User.objects.order_by('region_state').exclude(region_state__isnull=True).exclude(region_state__exact='').
                        values_list('region_state', flat=True).distinct())
    close_city = list(User.objects.order_by('close_to_city').exclude(close_to_city__isnull=True).exclude(close_to_city__exact='').
                      values_list('close_to_city', flat=True).distinct())
    return JsonResponse({'sales_persons': sales_persons, 'departments': User.DEPT_TYPE_CHOICES,
                         'customer_types': User.CUSTOMER_TYPE_CHOICES, 'user_statuses': User.USER_STATUS_CHOICES,
                         'countries': Countries, 'cities': cities, 'region_states': region_state,
                         'close_city': close_city}, status=200)


@csrf_exempt
@login_required
def delete_user(request):
    id_list = request.POST.getlist('ids[]')

    try:
        if len(id_list) > 0:
            User.objects.filter(id__in=id_list).delete()
        else:
            return JsonResponse({'ok': False}, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'ok': False}, status=400)

    return JsonResponse({'ok': True}, status=200)


@csrf_exempt
@login_required
def get_user(request):
    id = request.POST.get('id', None)
    temp = dict()
    try:
        user = User.objects.get(id=id)
        # problem with image field
        # temp['data'] = json.dumps(model_to_dict(user))
        temp['data'] = json.loads(serialize('json', [user]))
        temp['data'][0]['fields']['image'] = request.build_absolute_uri(user.image.url)
        suppliers = UserSuppliers.objects.filter(user=user)
        temp['suppliers'] = json.loads(serialize('json', suppliers))  # multiple records
        sale_modules = UserSaleModule.objects.filter(user=user)
        temp['sale_modules'] = json.loads(serialize('json', sale_modules))  # multiple records
        managers = User.objects.filter(user_type=User.Sales_Manager)  # .values('id', 'email')
        temp['managers'] = json.loads(serialize('json', managers))
        sales_persons = User.objects.filter(user_type=User.Sales_Person)  # .values('id', 'email')
        admins = User.objects.filter(user_type=User.Admin)  # .values('id', 'email')
        cust_sales_persons = admins | managers | sales_persons
        temp['sales_persons'] = json.loads(serialize('json', cust_sales_persons))

        follow_ups = FollowUps.objects.filter(customer_id=id)
        temp_list = []
        for follow_up in follow_ups:
            temp_list.append(get_follow_up_dt(follow_up))
        temp['follow_ups'] = temp_list

    except User.DoesNotExist:
        return JsonResponse(temp, safe=True, status=400)

    return JsonResponse(temp, safe=True, status=200)


def get_sales_persons_plus():
    admins = User.objects.filter(user_type=User.Admin)
    managers = User.objects.filter(user_type=User.Sales_Manager)
    sales_persons = User.objects.filter(user_type=User.Sales_Person)
    plus_sales_persons = admins | managers | sales_persons
    return plus_sales_persons


@csrf_exempt
@login_required
def get_selected_users(request):
    id_list = request.POST.getlist('selected_users[]', None)
    temp = dict()
    temp_list = []
    try:
        users = User.objects.filter(id__in=id_list)
        # for user in users:
        #     # problem with image & datetime
        #     # temp['data'] = json.dumps(model_to_dict(user))
        #     temp['data'] = json.loads(serialize('json', users))
        #     temp['data'][0]['fields']['image'] = request.build_absolute_uri(user.image.url)
        #     suppliers = UserSuppliers.objects.filter(user=user)
        #     temp['suppliers'] = json.loads(serialize('json', suppliers))  # multiple records
        #     sale_modules = UserSaleModule.objects.filter(user=user)
        #     temp['sale_modules'] = json.loads(serialize('json', sale_modules))  # multiple records
        #     managers = User.objects.filter(user_type=User.Sales_Manager)  # .values('id', 'email')
        #     temp['managers'] = json.loads(serialize('json', managers))
        #     sales_persons = User.objects.filter(user_type=User.Sales_Person)  # .values('id', 'email')
        #     temp['sales_persons'] = json.loads(serialize('json', sales_persons))
        #     temp_list.append(temp)

    except Exception as e:
        print(e)
        return JsonResponse(temp_list, safe=False, status=400)

    return JsonResponse({"users": json.loads(serialize('json', users))}, safe=False, status=200)


@csrf_exempt
@login_required
def update_user(request):
    user_image = request.FILES.get('user_image', None)
    user_type = request.POST.get('user_type', None)
    company_name = request.POST.get('company_name', None)
    card_name = request.POST.get('card_name', None)
    e_mail = request.POST.get('email', None)
    title = request.POST.get('title', None)
    first_name = request.POST.get('first_name', None)
    last_name = request.POST.get('last_name', None)
    password = request.POST.get('password', None)
    sales_manager = request.POST.get('sales_manager', None)
    sales_person = request.POST.get('sales_person', None)
    # sales_person_name = request.POST.get('sales_person_name', None)
    user_status = request.POST.get('user_status', None)
    customer_type = request.POST.get('customer_type', None)
    mass_email = request.POST.get('mass_email', None)
    language = request.POST.get('language', None)
    language = 'EN' if (not language or language == 'null') else language
    markup = request.POST.get('markup', None)
    suppliers = request.POST.getlist('suppliers[]', None)
    sale_modules = request.POST.getlist('sale_modules[]', None)
    comment = request.POST.get('comment', None)
    anc_interest = request.POST.get('anc_interest', None)
    street = request.POST.get('street', None)
    city = request.POST.get('city', None)
    zip_code = request.POST.get('zip_code', None)
    country = request.POST.get('country', None)
    tel = request.POST.get('tel', None)
    mobile = request.POST.get('mobile', None)
    region_state = request.POST.get('region_state', None)
    close_to_city = request.POST.get('close_to_city', None)
    tel3 = request.POST.get('tel3', None)
    birth_date = request.POST.get('birth_date', None)
    business_type = request.POST.get('business_type', None)
    web_site = request.POST.get('web_site', None)
    skype_user = request.POST.get('skype_user', None)
    qq = request.POST.get('qq', None)
    facebook = request.POST.get('facebook', None)
    twitter = request.POST.get('twitter', None)
    ref_name = request.POST.get('ref_name', None)
    ref_phone = request.POST.get('ref_phone', None)
    jbt = request.POST.get('jbt', None)
    vat = request.POST.get('vat', None)
    whitesite_markup = request.POST.get('whitesite_markup', None)
    dept = request.POST.get('dept', None)
    r1 = request.POST.get('r1', None)
    address_4 = request.POST.get('address_4', None)
    fantasy_no = request.POST.get('fantasy_no', None)
    harmony_no = request.POST.get('harmony_no', None)
    data_source = request.POST.get('data_source', None)
    verified = smart_str2bool(request.POST.get('verified', None))
    shape_box = request.POST.get('shape_box', None)
    size_box = request.POST.get('size_box', None)
    color_box = request.POST.get('color_box', None)
    clarity_box = request.POST.get('clarity_box', None)
    lab_box = request.POST.get('lab_box', None)
    make_box = request.POST.get('make_box', None)
    can_download_excel = smart_str2bool(request.POST.get('download_excel', None))
    can_view_prices = smart_str2bool(request.POST.get('view_prices', None))

    try:

        if not birth_date or birth_date == '':
            birth_date = None

        if not e_mail:
            return JsonResponse({'error': 'Failed! Invalid email'}, safe=True, status=400)

        user = User.objects.get(email__iexact=e_mail.lower())

        if password:
            user.set_password(password)

        if user_image:
            user.image = user_image

        manager = None
        if sales_manager and sales_manager != '':
            manager = User.objects.get(id=sales_manager)

        person = None
        if sales_person and sales_person != '':
            person = User.objects.get(id=sales_person)

        user.user_type = user_type
        user.card_name = card_name.title()
        user.company_name = company_name.title()
        user.username = e_mail.lower()
        user.email = e_mail.lower()
        user.title = title
        user.first_name = first_name
        user.last_name = last_name
        user.is_active = True
        user.is_staff = True
        user.sales_manager = manager
        user.comment = comment
        user.anc_interest = anc_interest
        user.sales_person = person
        user.sales_person_name = person.get_full_name()
        user.user_status = user_status
        user.customer_type = customer_type
        user.mass_email = json.loads(mass_email)
        user.language = language
        user.markup = markup
        user.street = street
        user.city = city
        user.zip_code = zip_code
        user.country = country
        user.tel = tel
        user.mobile = mobile
        user.region_state = region_state
        user.close_to_city = close_to_city
        user.tel3 = tel3
        user.birth_date = birth_date
        user.business_type = business_type
        user.web_site = web_site
        user.skype_user = skype_user
        user.qq = qq
        user.facebook = facebook
        user.twitter = twitter
        user.ref_name = ref_name
        user.ref_phone = ref_phone
        user.jbt = jbt
        user.vat = vat
        user.whitesite_markup = whitesite_markup
        user.dept = dept
        user.r1 = r1
        user.address_4 = address_4
        user.fantasy_no = fantasy_no
        user.harmony_no = harmony_no
        user.data_source = data_source
        user.is_verified = verified
        user.shape_box = shape_box
        user.size_box = size_box
        user.color_box = color_box
        user.clarity_box = clarity_box
        user.lab_box = lab_box
        user.make_box = make_box
        user.can_download_excel = can_download_excel
        user.can_view_prices = can_view_prices

        user.save()

        UserSuppliers.objects.filter(user=user).delete()
        for supplier in suppliers:
            UserSuppliers.objects.create(user=user, supplier=supplier)

        UserSaleModule.objects.filter(user=user).delete()
        for sale_module in sale_modules:
            UserSaleModule.objects.create(user=user, sale_module=sale_module)

    except User.DoesNotExist:
        return JsonResponse({'error': 'Failed! User not valid'}, safe=True, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! User Updated'}, safe=True, status=200)


@csrf_exempt
@login_required
def upload_file(request):
    try:
        files = request.FILES['files[]']
        path = os.path.join(settings.BASE_DIR, "media", "CSV_Files", "Users", '%s') % files.name
        destination = open(path, 'wb+')
        for chunk in files.chunks():
            destination.write(chunk)
        destination.close()
        messages.success(request, 'Files uploaded without error', extra_tags='Success')
        # return status to client
        return HttpResponse(status=201)
    except Exception as e:
        print(e)
        return HttpResponse(status=400)


@csrf_exempt
@login_required
def remove_file(request):
    """ Deletes file from filesystem. """
    file_name = request.POST.get('file', None)
    path = os.path.join(settings.BASE_DIR, "media", "CSV_Files", "Users", '%s') % file_name
    if os.path.isfile(path):
        os.remove(path)
        messages.success(request, 'Files removed without error', extra_tags='Success')
        return HttpResponse(status=204)
    return HttpResponse(status=400)


@csrf_exempt
@login_required
def process_user_files(request):
    """ process files from filesystem. """
    suppliers_list = ["AMC", "ERAN", "NIRU", "EZ", "GUTFRUND", "KIRAN", "LSDCO_IL", "SDE", "SRK"]
    sale_modules_list = ["Request", "Mazal", "dQuote"]
    try:
        files_list = [i for i in
                      glob.glob(os.path.join(settings.CSV_PATH, 'Users') + '/*.{}'.format(settings.CSV_EXTENSION))]
        for file in files_list:
            with open(file, "r", newline="", encoding="utf-8", errors='ignore') as f_in:   #
                reader = csv.DictReader(f_in)
                # users_list = []
                for row in reader:
                    try:
                        mobile = remove_spaces(row['Mobile'])[:15]
                        tel = remove_spaces(row['Tel'])[:15]
                        tel3 = remove_spaces(row['Tel3'])[:15]

                        user_type = row['Card Type'][:2]
                        if row['Card Type'] == 'CUST':
                            user_type = 'CU'

                        user_name = row['Email']
                        if row['Email'] == '':
                            user_name = row['Fantasy No'] + '@fantasy.com'

                        sales_person = None
                        if row['Sales Person'] != '':
                            sales_person = row['Sales Person']
                            # name = row['Sales Person'].split()
                            # try:
                            #     sales_person = User.objects.get(last_name__iexact=row['Sales Person'])
                            # except User.DoesNotExist:
                            #     sales_person = User.objects.get(first_name__iexact=name[0], last_name__iexact=name[1])
                            #     # try:
                            #     #     sales_person = User.objects.get(first_name__iexact=name[0], last_name__iexact=name[1])
                            #     # except User.DoesNotExist:
                            #     #     sales_person = User.objects.create(
                            #     #         first_name=name[0],
                            #     #         last_name=name[1],
                            #     #         user_type='SM',
                            #     #
                            #     #     )

                        # if row['Sales Person'] != '':
                        #     sales_person = row['Sales Person']

                        # temp = deepcopy(temp_obj)
                        # temp = dict()
                        # print(row)
                        # temp['DateCreated'] = row['here']
                        # users_list.append(
                        user = User.objects.create(
                            # first_name=None,
                            last_name=row['Last Name'],
                            card_name=row['Card Name'],
                            user_type=user_type,
                            company_name=row['Company Name'].title(),
                            email=user_name,
                            username=user_name,
                            # title=row[''],
                            # sales_manager=row[''],
                            # sales_person=sales_person,
                            sales_person_name=sales_person,
                            # user_status=row[''],
                            # customer_type=row[''],
                            # mass_email=row[''],
                            # language=row[''],
                            # markup=row[''],
                            # suppliers
                            street=row['Street'] + row['Address1'],
                            city=row['City  (Address3)'],
                            zip_code=row['Zip Code'],
                            country=row['Country'][:3],
                            tel=tel,
                            mobile=mobile,
                            region_state=row['Region/State'],
                            close_to_city=row['Close To - City'],
                            tel3=tel3,
                            # birth_date=row[''],
                            # business_type=row[''],
                            # web_site=row[''],
                            # skype_user=row[''],
                            # qq=row[''],
                            # facebook=row[''],
                            # twitter=row[''],
                            # ref_name=row[''],
                            # ref_phone=row[''],
                            # jbt=row[''],
                            vat=row['VAT'],
                            # whitesite_markup=row[''],
                            # dept=row[''],
                            r1=row['R1'],
                            address_4=row['Address4'],
                            fantasy_no=row['Fantasy No'],
                            harmony_no=row['Harmony No'],
                            data_source=row['Data Source'],
                            # is_verified=row['']
                        )
                        # )

                        for supplier in suppliers_list:
                            UserSuppliers.objects.create(user=user, supplier=supplier)

                        for sale_module in sale_modules_list:
                            UserSaleModule.objects.create(user=user, sale_module=sale_module)

                        # if len(users_list) == 10000:
                        #     print('bulk create phase')
                        #     User.objects.bulk_create(users_list)
                        #     users_list = []

                    except IntegrityError as e:
                        from psycopg2 import errorcodes as pg_errorcodes
                        if e.__cause__.pgcode == pg_errorcodes.UNIQUE_VIOLATION:
                            user = User.objects.get(username=user_name)
                            user.last_name = get_specific_value(row['Last Name'], user.last_name)
                            user.card_name = get_specific_value(row['Card Name'], user.card_name)
                            user.user_type = get_specific_value(user_type, user.user_type)
                            user.company_name = get_specific_value(row['Company Name'].title(), user.company_name)
                            # user.sales_person = sales_person
                            user.sales_person_name = get_specific_value(sales_person, user.sales_person_name)
                            user.street = get_specific_value(row['Street'] + row['Address1'], user.street)
                            user.city = get_specific_value(row['City  (Address3)'], user.city)
                            user.zip_code = get_specific_value(row['Zip Code'], user.zip_code)
                            user.country = get_specific_value(row['Country'][:3], user.country)
                            user.tel = get_specific_value(tel, user.tel)
                            user.mobile = get_specific_value(mobile, user.mobile)
                            user.region_state = get_specific_value(row['Region/State'], user.region_state)
                            user.close_to_city = get_specific_value(row['Close To - City'], user.close_to_city)
                            user.tel3 = get_specific_value(tel3, user.tel3)
                                # birth_date = row[''],
                                # business_type = row[''],
                                # web_site = row[''],
                                # skype_user = row[''],
                                # qq = row[''],
                                # facebook = row[''],
                                # twitter = row[''],
                                # ref_name = row[''],
                                # ref_phone = row[''],
                                # jbt = row[''],
                            user.vat = get_specific_value(row['VAT'], user.vat)
                                # whitesite_markup = row[''],
                                # dept = row[''],
                            user.r1 = get_specific_value(row['R1'], user.r1)
                            user.address_4 = get_specific_value(row['Address4'], user.address_4)
                            user.fantasy_no = get_specific_value(row['Fantasy No'], user.fantasy_no)
                            user.harmony_no = get_specific_value(row['Harmony No'], user.harmony_no)
                            user.data_source = get_specific_value(row['Data Source'], user.data_source)
                                # is_verified = row['']
                            user.save()

                    except Exception as e:
                        print(e)
            # if len(users_list) > 0:
            #     print('bulk create phase last')
            #     User.objects.bulk_create(users_list)

            os.rename(file, os.path.join(settings.CSV_PATH, 'Users', 'Archived', 'user')
                      + datetime.now().strftime("-%Y-%m-%d-%H-%M-%S.") + settings.CSV_EXTENSION)

        messages.success(request, 'Files processed', extra_tags='Success')
    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return redirect('new_user')


@csrf_exempt
@login_required
def process_h1_files(request):
    """ process files from filesystem. """
    try:
        files_list = [i for i in
                      glob.glob(os.path.join(settings.CSV_PATH, 'Users') + '/*.{}'.format(settings.EXCEL_EXTENSION))]
        for file in files_list:
            df = pd.read_excel(file)
            df = df.fillna('')
            for index, row in df.iterrows():
                try:
                    email = row['Email'].lower()
                    if email == '':
                        print('skip it')
                        raise Exception
                    user, created = User.objects.get_or_create(username=email)
                    user.username = get_specific_value(email, user.username)
                    user.email = get_specific_value(email, user.email)
                    user.first_name = get_specific_value(row['FirstName'], user.first_name)
                    user.last_name = get_specific_value(row['LastName'], user.last_name)
                    if row['Passwd']:
                        user.set_password(row['Passwd'])
                    user.user_status = get_specific_value(row['UserStatusID'], user.user_status)
                    if row['JMarkupID'] is not '':
                        user.markup = get_specific_value(row['JMarkupID'], user.markup)
                    user.language = get_specific_value(get_language_iso2(row['Lang']), user.language)

                    lookup_value = 'Liat'
                    if row['ManagerID'] == 15:
                        lookup_value = 'Didier'
                    elif row['ManagerID'] == 31:
                        lookup_value = 'Liat'
                    elif row['ManagerID'] == 39:
                        lookup_value = 'GlobalReports'
                    elif row['ManagerID'] == 82:
                        lookup_value = 'Angello'
                    elif row['ManagerID'] == 94:
                        lookup_value = 'Grazia'

                    manager = User.objects.filter(first_name__icontains=lookup_value).first()
                    if manager:
                        user.sales_person_id = get_specific_value(manager.id, user.sales_person_id)
                        user.sales_person_name = get_specific_value(manager.get_full_name(), user.sales_person_name)

                    harmony_number = row['InternalUserID']
                    if harmony_number is not '':
                        user.harmony_no = get_specific_value(row['InternalUserID'], user.harmony_no)
                        user.data_source = User.Harmony

                    user.title = get_specific_value(row['Title'][:3], user.title)
                    user.company_name = get_specific_value(row['CompanyName'], user.company_name)
                    user.street = get_specific_value(row['Address'], user.street)
                    user.city = get_specific_value(row['City'], user.city)
                    user.zip_code = get_specific_value(row['Zip'], user.zip_code)
                    user.region_state = get_specific_value(row['State'], user.region_state)
                    user.country = get_specific_value(get_country_iso3(row['Country']), user.country)
                    user.tel = get_specific_value(unformat_phone_no(row['Phone'])[:15], user.tel)
                    user.tel3 = get_specific_value(unformat_phone_no(row['Phone2'])[:15], user.tel3)
                    user.mobile = get_specific_value(unformat_phone_no(row['Mobile'])[:15], user.mobile)
                    user.web_site = get_specific_value(row['InternetSite'], user.web_site)
                    user.jbt = get_specific_value(row['JBT'], user.jbt)
                    user.vat = get_specific_value(row['VAT'], user.vat)
                    user.comment = get_specific_value(row['Comments'] + row['GStr6'], user.comment)
                    user.shape_box = get_specific_value(row['ShapeBox'], user.shape_box)
                    user.size_box = get_specific_value(row['SizeBox'], user.size_box)
                    user.color_box = get_specific_value(row['ColorBox'], user.color_box)
                    user.clarity_box = get_specific_value(row['ClarityBox'], user.clarity_box)
                    user.lab_box = get_specific_value(row['LabBox'], user.lab_box)
                    user.make_box = get_specific_value(row['MakeBox'], user.make_box)
                    user.can_download_excel = get_specific_value(smart_str2bool(row['CanDownloadJExcel']), user.can_download_excel)
                    user.user_type = User.Customer
                    user.customer_type = get_specific_value(row['UserTypeID'], user.customer_type)
                    user.can_view_prices = get_specific_value(smart_str2bool(row['ShowPrices']), user.can_view_prices)

                    date_created = datetime.now()
                    if row['DateActivated'] is not '':
                        date_created = pytz.utc.localize(row['DateActivated'])
                    user.date_created = get_specific_value(date_created, user.date_created)

                    date_modified = datetime.now()
                    if row['DateModified'] is not '':
                        date_modified = pytz.utc.localize(row['DateModified'])
                    user.date_modified = get_specific_value(date_modified, user.date_modified)

                    user.business_type = get_specific_value(get_business_type(row['BusinessType']), user.business_type)
                    user.save()

                except Exception as e:
                    print(e)
                    print('row skipped: ', row['UserID'])

            os.rename(file, os.path.join(settings.CSV_PATH, 'Users', 'Archived', 'h1')
                      + datetime.now().strftime("-%Y-%m-%d-%H-%M-%S.") + settings.EXCEL_EXTENSION)

        messages.success(request, 'H1 Files Processed!', extra_tags='Success')
    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report!', extra_tags='Failed')

    return redirect('new_user')


@csrf_exempt
@login_required
def get_state(request):
    user_id = request.user.id
    table_name = request.POST.get('table_name', None)
    try:
        table = TableState.objects.get(user_id=user_id, name=table_name)
        data = model_to_dict(table)
        # temp['data'] = json.loads(serialize('json', [table]))
        # temp['data'][0]['fields']['image'] = request.build_absolute_uri(user.image.url)

    except TableState.DoesNotExist:
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(data, safe=True, status=200)


@csrf_exempt
@login_required
def update_state(request):
    user_id = request.user.id
    table_name = request.POST.get('table_name', None)
    table_state = request.POST.get('table_state', None)

    try:
        table = TableState.objects.get(user_id=user_id, name=table_name)
        table.state = table_state
        table.save()

    except TableState.DoesNotExist:
        TableState.objects.create(user_id=user_id, name=table_name, state=table_state)
        return JsonResponse({'message': 'Warning! New State Created'}, safe=True, status=200)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! State Updated'}, safe=True, status=200)


@csrf_exempt
@login_required
def dt_list_users(request):
    # , id=10101
    users = User.objects.filter(is_superuser=False)
    return base_response(request, users)


def get_filtered_users(request, users):
    order_col = request.get('order')[0]['column']
    order_dir = request.get('order')[0]['dir']
    custom_order = request.get('custom_order')

    order_by = direction_symbol[order_dir] + user_columns[int(custom_order[int(order_col)])]

    user_type_list = request.get('user_type')
    email = request.get('email', None)
    sales_person_name = request.get('sales_person_name', None)
    dept = request.get('dept', None)
    cu_types = request.get('cu_types', None)
    u_status = request.get('u_status', None)
    country = request.get('country', None)
    city = request.get('city', None)
    region_state = request.get('region_state', None)
    close_to_city = request.get('close_to_city', None)
    if len(user_type_list) > 0:
        users = users.filter(user_type__in=user_type_list)
    if email:
        users = users.filter(email__contains=email)
    if sales_person_name:
        users = users.filter(sales_person_name__contains=sales_person_name)
    if dept:
        users = users.filter(dept__iexact=dept)
    if cu_types:
        users = users.filter(customer_type__iexact=cu_types)
    if u_status:
        users = users.filter(user_status__iexact=u_status)
    if country:
        users = users.filter(country__iexact=country)
    if city:
        users = users.filter(city__iexact=city)
    if region_state:
        users = users.filter(region_state__iexact=region_state)
    if close_to_city:
        users = users.filter(close_to_city__iexact=close_to_city)

    if user_columns[int(custom_order[int(order_col)])] == 'name':
        if order_dir == 'asc':
            users = users.order_by(Lower('first_name').asc(), Lower('last_name').asc())
        else:
            users = users.order_by(Lower('first_name').desc(), Lower('last_name').desc())
    elif user_columns[int(custom_order[int(order_col)])] == 'sales_manager':
        if order_dir == 'asc':
            users = users.order_by(Lower('sales_manager__first_name').asc(), Lower('sales_manager__last_name').asc())
        else:
            users = users.order_by(Lower('sales_manager__first_name').desc(), Lower('sales_manager__last_name').desc())
    elif user_columns[int(custom_order[int(order_col)])] == 'id' or \
            user_columns[int(custom_order[int(order_col)])] == 'mass_email' or \
            user_columns[int(custom_order[int(order_col)])] == 'date_created' or \
            user_columns[int(custom_order[int(order_col)])] == 'date_modified' or \
            user_columns[int(custom_order[int(order_col)])] == 'last_login' or \
            user_columns[int(custom_order[int(order_col)])] == 'birth_date' or \
            user_columns[int(custom_order[int(order_col)])] == 'whitesite_markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'is_verified':
        users = users.order_by(order_by)
    else:
        if order_dir == 'asc':
            users = users.order_by(Lower(user_columns[int(custom_order[int(order_col)])]).asc())
        else:
            users = users.order_by(Lower(user_columns[int(custom_order[int(order_col)])]).desc())

    return users


def base_response(request, users):
    draw = request.POST.get('draw')
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    order_col = request.POST.get('order[0][column]')
    order_dir = request.POST.get('order[0][dir]')
    custom_order = request.POST.getlist('custom_order[]')

    order_by = direction_symbol[order_dir] + user_columns[int(custom_order[int(order_col)])]
    # print(order_by)

    user_type_list = request.POST.getlist('user_type[]')
    # id_list = request.POST.getlist('id[]')
    # # id_list = [int(i) for i in id_list]
    email = request.POST.get('email', None)
    sales_person_name = request.POST.get('sales_person_name', None)
    dept = request.POST.get('dept', None)
    cu_types = request.POST.get('cu_types', None)
    u_status = request.POST.get('u_status', None)
    country = request.POST.get('country', None)
    city = request.POST.get('city', None)
    region_state = request.POST.get('region_state', None)
    close_to_city = request.POST.get('close_to_city', None)
    # color_list = request.POST.getlist('color[]')
    # clarity_list = request.POST.getlist('clarity[]')
    # lab_list = request.POST.getlist('lab[]')
    # location_list = request.POST.getlist('location[]')
    # feed_list = request.POST.getlist('feed[]')
    # polish_list = request.POST.getlist('polish[]')
    # symmetry_list = request.POST.getlist('symmetry[]')
    # cut_list = request.POST.getlist('cut[]')
    # fluorescence_list = request.POST.getlist('fluorescence[]')
    # depth_from = request.POST.get('depth_from')
    # depth_to = request.POST.get('depth_to')
    # table_from = request.POST.get('table_from')
    # table_to = request.POST.get('table_to')
    # discount_from = request.POST.get('discount_from')
    # discount_to = request.POST.get('discount_to')
    #
    if len(user_type_list) > 0:
        users = users.filter(user_type__in=user_type_list)
    # if len(id_list) > 0:
    #     temp_diamonds = diamonds.filter(cert_no__in=id_list)
    #     temp_diamonds_2 = diamonds.filter(lot_id__in=id_list)
    #     diamonds = temp_diamonds | temp_diamonds_2
    #
    if email:
        users = users.filter(email__contains=email)
    if sales_person_name:
        users = users.filter(sales_person_name__contains=sales_person_name)
    if dept:
        users = users.filter(dept__iexact=dept)
    if cu_types:
        users = users.filter(customer_type__iexact=cu_types)
    if u_status:
        users = users.filter(user_status__iexact=u_status)
    if country:
        users = users.filter(country__iexact=country)
    if city:
        users = users.filter(city__iexact=city)
    if region_state:
        users = users.filter(region_state__iexact=region_state)
    if close_to_city:
        users = users.filter(close_to_city__iexact=close_to_city)
    # if len(color_list) > 0:
    #     diamonds = diamonds.filter(color__in=color_list)
    # if len(clarity_list) > 0:
    #     diamonds = diamonds.filter(clarity__in=clarity_list)
    # if len(lab_list) > 0:
    #     diamonds = diamonds.filter(lab__in=lab_list)
    # if len(location_list) > 0:
    #     location_list = [x.upper() for x in location_list]
    #     diamonds = diamonds.filter(country_location__in=location_list)
    # if len(feed_list) > 0:
    #     feed_list = [x.upper() for x in feed_list]
    #     diamonds = diamonds.filter(feed__in=feed_list)
    # if len(polish_list) > 0:
    #     polish_list = [x.upper() for x in polish_list]
    #     diamonds = diamonds.filter(polish__in=polish_list)
    # if len(symmetry_list) > 0:
    #     symmetry_list = [x.upper() for x in symmetry_list]
    #     diamonds = diamonds.filter(symmetry__in=symmetry_list)
    # if len(cut_list) > 0:
    #     cut_list = [x.upper() for x in cut_list]
    #     diamonds = diamonds.filter(cut__in=cut_list)
    # if len(fluorescence_list) > 0:
    #     fluorescence_list = [x.upper() for x in fluorescence_list]
    #     diamonds = diamonds.filter(fluorescence__in=fluorescence_list)
    # if not(depth_from == '0' and depth_to == '200'):
    #     diamonds = diamonds.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    # if not(table_from == '0' and table_to == '200'):
    #     diamonds = diamonds.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    # if not(discount_from == '-200' and discount_to == '200'):
    #     diamonds = diamonds.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    if user_columns[int(custom_order[int(order_col)])] == 'name':
        if order_dir == 'asc':
            users = users.order_by(Lower('first_name').asc(), Lower('last_name').asc())
        else:
            users = users.order_by(Lower('first_name').desc(), Lower('last_name').desc())
    elif user_columns[int(custom_order[int(order_col)])] == 'sales_manager':
        if order_dir == 'asc':
            users = users.order_by(Lower('sales_manager__first_name').asc(), Lower('sales_manager__last_name').asc())
        else:
            users = users.order_by(Lower('sales_manager__first_name').desc(), Lower('sales_manager__last_name').desc())
    elif user_columns[int(custom_order[int(order_col)])] == 'id' or \
            user_columns[int(custom_order[int(order_col)])] == 'mass_email' or \
            user_columns[int(custom_order[int(order_col)])] == 'date_created' or \
            user_columns[int(custom_order[int(order_col)])] == 'date_modified' or \
            user_columns[int(custom_order[int(order_col)])] == 'last_login' or \
            user_columns[int(custom_order[int(order_col)])] == 'birth_date' or \
            user_columns[int(custom_order[int(order_col)])] == 'whitesite_markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'markup' or \
            user_columns[int(custom_order[int(order_col)])] == 'is_verified':
        users = users.order_by(order_by)
    else:
        if order_dir == 'asc':
            users = users.order_by(Lower(user_columns[int(custom_order[int(order_col)])]).asc())
        else:
            users = users.order_by(Lower(user_columns[int(custom_order[int(order_col)])]).desc())

    temp = dict()
    temp['draw'] = int(draw)
    temp['recordsTotal'] = users.count()
    temp['recordsFiltered'] = users.count()

    temp_list = []
    users = users[start:start+length]

    for user_a in users:
        suppliers_temp = ''
        for item in user_a.usersuppliers_set.all():
            suppliers_temp += item.supplier+', '

        # # "41": "('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="' +
        # #  request.build_absolute_uri(user_a.image.url) + '">View</a>' if (request.build_absolute_uri(user_a.image.url) != ' ' and request.build_absolute_uri(user_a.image.url) != '') else ""),
        #
        # temp_list.append(
        #     {
        #         "0": user_a.id, "1": user_a.get_user_type_display(), "2": user_a.company_name, "3": user_a.get_title_display(),
        #         "4": user_a.sales_manager.get_full_name() if user_a.sales_manager else "",
        #         # user_a.sales_person,
        #         "5": user_a.sales_person_name,
        #         "6": user_a.get_user_status_display(),
        #         "7": user_a.get_customer_type_display(), "8": user_a.mass_email, "9": user_a.language, "10": user_a.markup, "11": user_a.street,
        #         "12": user_a.city, "13": user_a.zip_code, "14": user_a.country, "15": user_a.tel, "16": user_a.mobile, "17": user_a.region_state,
        #         "18": user_a.close_to_city, "19": user_a.tel3, "20": user_a.birth_date.strftime("%B %d, "": %Y") if user_a.birth_date else "",
        #         "21": user_a.get_business_type_display(), "22": user_a.web_site, "23": user_a.skype_user, "24": user_a.qq, "25": user_a.facebook,
        #         "26": user_a.twitter, "27": user_a.ref_name, "28": user_a.ref_phone, "29": user_a.jbt, "30": user_a.vat, "31": user_a.whitesite_markup,
        #         "32": user_a.dept, "33": user_a.r1, "34": user_a.address_4, "35": user_a.fantasy_no, "36": user_a.harmony_no, "37": user_a.data_source,
        #         "38": user_a.is_verified, "39": user_a.date_created.strftime("%B %d, "": %Y"), "40": user_a.date_modified.strftime("%B %d, "": %Y"),
        #         "41": "",
        #         "42": user_a.comment, "43": user_a.email, "44": user_a.get_full_name(), "45": suppliers_temp, "46": user_a.first_name,
        #         "47": user_a.last_name, "48": user_a.card_name, "DT_RowId": str(user_a.id)
        #     }
        # )
        temp_list.append(
            [
                user_a.id, user_a.get_user_type_display(), user_a.company_name, user_a.get_title_display(),
                user_a.sales_manager.get_full_name() if user_a.sales_manager else "",
                # user_a.sales_person,
                user_a.sales_person_name,
                user_a.get_user_status_display(),
                user_a.get_customer_type_display(), user_a.mass_email, user_a.language, user_a.markup, user_a.street,
                user_a.city, user_a.zip_code, user_a.country, user_a.tel, user_a.mobile, user_a.region_state,
                user_a.close_to_city, user_a.tel3, user_a.birth_date.strftime("%B %d, %Y") if user_a.birth_date else "",
                user_a.get_business_type_display(), user_a.web_site, user_a.skype_user, user_a.qq, user_a.facebook,
                user_a.twitter, user_a.ref_name, user_a.ref_phone, user_a.jbt, user_a.vat, user_a.whitesite_markup,
                user_a.dept, user_a.r1, user_a.address_4, user_a.fantasy_no, user_a.harmony_no, user_a.data_source,
                user_a.is_verified, user_a.date_created.strftime("%B %d, %Y"), user_a.date_modified.strftime("%B %d, %Y"),
                ('<a class="btn btn-info " style="padding: 1px 4px;" target="_blank" href="' +
                 request.build_absolute_uri(user_a.image.url) + '">View</a>' if (
                     request.build_absolute_uri(user_a.image.url) != ' ' and
                     request.build_absolute_uri(user_a.image.url) != '') else ""),
                user_a.comment,
                '<a href="javascript:void(0);" onClick="addressed_email(' + str(user_a.id) + ')">' + user_a.email + '</a>' if user_a.email else '',
                user_a.get_full_name(), suppliers_temp, user_a.first_name, user_a.last_name,
                '<a href="javascript:void(0);" onClick="open_user_modal(' + str(user_a.id) + ')">' + user_a.card_name + '</a>' if user_a.card_name else '',
                # ('<a href="history/">' + user_a.last_login.strftime("%b %d, %Y %H:%M") + '</a>' if user_a.last_login else ''),
                '<a href="javascript:void(0);" onClick="addressed_history(' + str(user_a.id) + ',\'' + user_a.email + '\')">' + user_a.last_login.strftime("%b %d, %Y %H:%M") + '</a>' if user_a.last_login else '',
                user_a.anc_interest
            ]
        )

    temp['data'] = temp_list
    # print(temp_list)
    return JsonResponse(temp, safe=True)


@csrf_exempt
@login_required
def export_users_csv(request):
    request_post = json.loads(request.body)
    users = User.objects.filter(is_superuser=False)
    users = get_filtered_users(request_post, users)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'

    writer = csv.writer(response, delimiter=',')
    # writer = csv.writer(response, delimiter="\t")

    fieldnames = (
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest')

    writer.writerow(fieldnames)

    users = users.values_list(
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest')

    for user in users:
        temp = list(user)
        temp[0] = User.DISPLAY_ABBREVIATIONS[user[0]]
        temp[2] = User.DISPLAY_ABBREVIATIONS[user[2]]
        temp[5] = User.DISPLAY_ABBREVIATIONS[user[5]]
        temp[6] = User.DISPLAY_CUSTOMER_TYPE_ABBR[user[6]]
        # temp[14] = '=TEXT('+user[14]+';"+")' if user[14] else ''
        # temp[15] = '"'+str(user[15])+'"' if user[15] else ''
        # temp[18] = '\''+str(user[18])+'\'' if user[18] else ''
        temp[19] = user[19].strftime("%b. %d %Y") if user[19] else ''
        temp[20] = User.DISPLAY_ABBREVIATIONS[user[20]]
        # temp[27] = '\''+str(user[27])+'\'' if user[27] else ''
        temp[38] = user[38].strftime("%b. %d %Y %H:%M %p") if user[38] else ''
        temp[39] = user[39].strftime("%b. %d %Y %H:%M %p") if user[39] else ''
        temp[40] = '<a href="'+settings.HTTP_SCHEME+settings.HOST_NAME+'/'+user[40]+'">View</a>'
        temp[46] = user[46].strftime("%b. %d %Y %H:%M %p") if user[46] else ''
        # temp.pop(19)
        writer.writerow(temp)

    return response


@csrf_exempt
@login_required
def export_users_filtered(request):
    request_post = json.loads(request.body)
    users = User.objects.filter(is_superuser=False)
    users = get_filtered_users(request_post, users)

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=users.xlsx'

    fieldnames = [
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest']

    users = users.values_list(
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest')

    xlsx_data = create_excel_users(users, fieldnames)
    response.write(xlsx_data)

    return response


def create_excel_users(users, fieldnames):
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    top_border = workbook.add_format({'top': 3, 'top_color': '#000000'})   # , 'top_pattern': 1
    bold = workbook.add_format({'bold': True})
    weight = workbook.add_format({'num_format': '0.00'})
    depth = workbook.add_format({'num_format': '0.0'})
    money = workbook.add_format({'num_format': '$#,##0.00'})
    h_center = workbook.add_format({'align': 'center'})
    h_left = workbook.add_format({'align': 'left'})
    v_center = workbook.add_format({'align': 'vcenter'})
    bold.set_align('center')
    money.set_align('center')
    weight.set_align('center')
    depth.set_align('center')

    worksheet = workbook.add_worksheet()
    worksheet.set_landscape()
    worksheet.set_paper(9)         # A4
    # worksheet.fit_to_pages(1, 0)   # 1 page wide and as long as necessary.
    worksheet.hide_gridlines(0)    # 1 hide printed, 0 hide nothing
    worksheet.set_column(1, 1, 15)
    worksheet.set_column(6, 6, 10)
    worksheet.set_column(10, 10, 25)
    worksheet.set_column(11, 11, 20)
    worksheet.set_column(14, 15, 15)
    worksheet.set_column(18, 18, 15)
    worksheet.set_column(20, 20, 20)
    worksheet.set_column(29, 29, 15)
    worksheet.set_column(38, 39, 20)
    worksheet.set_column(41, 41, 35)
    worksheet.set_column(42, 42, 30)
    worksheet.set_column(45, 46, 25)

    row = 0
    column = 0
    for item in fieldnames:
        worksheet.write(row, column, item, h_center)
        column += 1

    for user in users:
        temp = list(user)
        temp[0] = User.DISPLAY_ABBREVIATIONS[user[0]] if user[0] else ''
        temp[2] = User.DISPLAY_ABBREVIATIONS[user[2]] if user[2] else ''
        temp[5] = User.DISPLAY_ABBREVIATIONS[user[5]] if user[5] else ''
        temp[6] = User.DISPLAY_CUSTOMER_TYPE_ABBR[user[6]] if user[6] else ''
        # temp[14] = '=TEXT('+user[14]+';"+")' if user[14] else ''
        # temp[15] = '"'+str(user[15])+'"' if user[15] else ''
        # temp[18] = '\''+str(user[18])+'\'' if user[18] else ''
        temp[19] = user[19].strftime("%b. %d %Y") if user[19] else ''
        temp[20] = User.DISPLAY_ABBREVIATIONS[user[20]] if user[20] else ''
        # temp[27] = '\''+str(user[27])+'\'' if user[27] else ''
        temp[38] = user[38].strftime("%b. %d %Y %H:%M %p") if user[38] else ''
        temp[39] = user[39].strftime("%b. %d %Y %H:%M %p") if user[39] else ''
        temp[40] = settings.HTTP_SCHEME+settings.HOST_NAME+'/'+user[40] if user[40] else ''
        temp[46] = user[46].strftime("%b. %d %Y %H:%M %p") if user[46] else ''

        column = 0
        row += 1
        for item in temp:
            if column == 40:

                if item and item != '':
                    worksheet.write_url(row=row, col=column, url=item, cell_format=h_center, string='View')
                else:
                    worksheet.write(row, column, item, v_center)
        #     elif column == 2:
        #         worksheet.write(row, column, item, weight)
        #     elif column == 12:
        #         worksheet.write(row, column, item, depth)
        #     elif column == 18:
        #         worksheet.write(row, column, item, money)
            else:
                worksheet.write(row, column, item, h_left)
            column += 1

    # if not is_full:
    #     row += 1
    #     worksheet.write(row, 2, '', top_border)
    #     worksheet.write(row, 18, '', top_border)
    #     row += 1
    #     worksheet.write(row, 1, 'Total Cts', bold)
    #     worksheet.write(row, 2, total_weight, weight)
    #     # worksheet.write(row, 3, '=SUM(C2:C' + str(row-2) + ')')
    #     worksheet.write(row, 17, 'Total$', bold)
    #     worksheet.write(row, 18, total_price, money)
    #     # worksheet.write(row, 17, '=SUM(R2:R' + str(row-2) + ')', money)

    # close workbook
    workbook.close()
    xlsx_data = output.getvalue()
    # xlsx_data = output.read()
    return xlsx_data


@csrf_exempt
@login_required
def export_users(request):
    request_post = json.loads(request.body)
    id_list = request_post.get('selected_users', None)
    is_full = request_post.get('is_full', False)
    file_name = request_post.get('file_name', 'selected_users.xlsx')
    users = User.objects.filter(is_superuser=False)

    if not is_full:
        users = users.filter(id__in=id_list)

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + file_name

    fieldnames = [
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest']

    users = users.values_list(
        'user_type', 'company_name', 'title', 'sales_manager', 'sales_person_name', 'user_status',
        'customer_type', 'mass_email', 'language', 'markup', 'street', 'city', 'zip_code', 'country', 'tel', 'mobile',
        'region_state', 'close_to_city', 'tel3', 'birth_date', 'business_type', 'web_site', 'skype_user', 'qq',
        'facebook', 'twitter', 'ref_name', 'ref_phone', 'jbt', 'vat', 'whitesite_markup', 'dept', 'r1', 'address_4',
        'fantasy_no', 'harmony_no', 'data_source', 'is_verified', 'date_created', 'date_modified', 'image', 'comment',
        'email', 'first_name', 'last_name', 'card_name', 'last_login', 'anc_interest')

    xlsx_data = create_excel_users(users, fieldnames)
    response.write(xlsx_data)
    return response


@csrf_exempt
@login_required
def autocomplete_email(request):
    term = request.GET.get('q', None)
    page = request.GET.get('page', None)
    element_id = request.GET.get('id', None)
    suggestions = []
    try:
        if element_id == 'follow-up-customer':
            users1 = User.objects.filter(is_superuser=False, email__istartswith=term, user_type=User.Customer)
            users2 = User.objects.filter(is_superuser=False, first_name__istartswith=term, user_type=User.Customer)
            users3 = User.objects.filter(is_superuser=False, last_name__istartswith=term, user_type=User.Customer)
            users4 = User.objects.filter(is_superuser=False, company_name__istartswith=term, user_type=User.Customer)
            users = users1 | users2 | users3 | users4
            users = users.values('first_name', 'last_name', 'email', 'id')

            for user in users:
                suggestions.append(
                    {'value': user['email'], 'label': user['email'] + ' - ' + user['first_name'] + ' ' + user['last_name'],
                     "id": user["id"]})

        elif element_id == 'follow-up-assignee' or element_id == 'filter-follow-up-assignee':
            temp_sales_persons_plus = get_sales_persons_plus()
            users1 = temp_sales_persons_plus.filter(is_superuser=False, email__istartswith=term)
            users2 = temp_sales_persons_plus.filter(is_superuser=False, first_name__istartswith=term)
            users3 = temp_sales_persons_plus.filter(is_superuser=False, last_name__istartswith=term)
            users4 = temp_sales_persons_plus.filter(is_superuser=False, company_name__istartswith=term)
            users = users1 | users2 | users3 | users4
            users = users.values('first_name', 'last_name', 'email', 'id')

            for user in users:
                suggestions.append(
                    {'value': user['email'], 'label': user['email'] + ' - ' + user['first_name'] + ' ' + user['last_name'],
                     "id": user["id"]})

        elif element_id == 'history-email-filter':
            users1 = ActivityLog.objects.filter(user__is_superuser=False, user__email__istartswith=term)
            users2 = ActivityLog.objects.filter(user__is_superuser=False, user__first_name__istartswith=term)
            users3 = ActivityLog.objects.filter(user__is_superuser=False, user__last_name__istartswith=term)
            users4 = ActivityLog.objects.filter(user__is_superuser=False, user__company_name__istartswith=term)
            users = users1 | users2 | users3 | users4
            users = users.values('user__first_name', 'user__last_name', 'user__email', 'user_id')

            for user in users:
                temp = {'value': user['user__email'],
                        'label': user['user__email'] + ' - ' + user['user__first_name'] + ' ' + user['user__last_name'],
                        "id": user["user_id"]}
                if temp not in suggestions:
                    suggestions.append(temp)

        else:
            users1 = User.objects.filter(is_superuser=False, email__istartswith=term)
            users2 = User.objects.filter(is_superuser=False, first_name__istartswith=term)
            users3 = User.objects.filter(is_superuser=False, last_name__istartswith=term)
            users4 = User.objects.filter(is_superuser=False, company_name__istartswith=term)
            users = users1 | users2 | users3 | users4
            users = users.values('first_name', 'last_name', 'email', 'id')

            for user in users:
                suggestions.append(
                    {'value': user['email'], 'label': user['email'] + ' - ' + user['first_name'] + ' ' + user['last_name'],
                     "id": user["email"]})

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(suggestions, safe=False, status=200)


@csrf_exempt
@login_required
def list_search(request):
    user_id = request.user.id
    try:
        searches = SearchHistory.objects.filter(user_id=user_id)
        # data = model_to_dict(table)
        data = json.loads(serialize('json', searches))
        # temp['data'][0]['fields']['image'] = request.build_absolute_uri(user.image.url)

    except Exception as e:
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(data, safe=False, status=200)


@csrf_exempt
@login_required
def save_search(request):
    user_id = request.user.id
    name = request.POST.get('name', None)
    state = request.POST.get('state', None)
    try:
        SearchHistory.objects.create(user_id=user_id, name=name, state=state)
        searches = SearchHistory.objects.filter(user_id=user_id)
        data = json.loads(serialize('json', searches))

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(data, safe=False, status=201)


@csrf_exempt
@login_required
def delete_search(request):
    user_id = request.user.id
    search_id = request.POST.get('search_id', None)

    try:
        search = SearchHistory.objects.get(user_id=user_id, id=search_id)
        search.delete()

    except SearchHistory.DoesNotExist:
        return JsonResponse({'message': 'Warning! Invalid id'}, safe=True, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({}, safe=True, status=204)


@csrf_exempt
@login_required
def mailboxes(request):
    return render(request, 'mailboxes.html')


@csrf_exempt
@login_required
def compose_email(request):
    # user_id = request.user.id
    templates = []
    try:
        templates = EmailTemplate.objects.all()
    except EmailTemplate.DoesNotExist:
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return render(request, 'compose_email.html', {'templates': templates})


@csrf_exempt
@login_required
def send_email(request):
    try:
        test = request.POST.get('test', False)
        subject = request.POST.get('subject', None)
        body = request.POST.get('email_body', None)
        # body = 'no body'
        from_email = request.POST.get('from_email', None)
        to_email = [from_email]
        if not test:
            to_email = request.POST.getlist('to_email[]', None)
            to_email = list(map(int, to_email))
            to_email = User.objects.filter(id__in=to_email).values_list('email', flat=True)
        cc = request.POST.get('cc', None)
        cc = cc.split(",")
        bcc = request.POST.get('bcc', None)
        bcc = bcc.split(",")

        # email
        kwargs = {
            'subject': subject,
            'text': body,
            'html': body,
            'cc': cc,
            'bcc': bcc,
            'to_email': to_email,
            'from_email': from_email,
            'body': body,
        }
        # send_simple_email(**kwargs)
        send_custom_email(**kwargs)
        status = 200
        msg = 'Email sent! Please check your spam folder.'
        # messages.success(request, msg, extra_tags='Success')
    except Exception as e:
        print(e)
        status = 400
        msg = 'Something went wrong. Please report'
        # messages.error(request, msg, extra_tags='Failed')

    return JsonResponse({'msg': msg}, safe=True, status=status)


@csrf_exempt
@login_required
def update_signature(request):
    user_id = request.user.id
    signature = request.POST.get('signature', None)
    signature_settings = smart_str2bool(request.POST.get('signature_settings', None))

    try:
        user = User.objects.get(id=user_id)
        user.signature = signature
        user.signature_settings = signature_settings
        user.save()

    except User.DoesNotExist:
        return JsonResponse({'error': 'Failed! Invalid User'}, safe=True, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! Signature Updated'}, safe=True, status=200)


@login_required
def user_profile(request):
    user_id = request.user.id
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    except Exception as e:
        print(e)
        messages.error(request, 'Something went wrong. Please report ', extra_tags='Failed')

    return render(request, 'user-profile.html', {'user': user})


@csrf_exempt
@login_required
def dt_list_templates(request):
    draw = request.POST.get('draw')
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    # order_col = request.POST.get('order[0][column]')
    # order_dir = request.POST.get('order[0][dir]')
    # custom_order = request.POST.getlist('custom_order[]')

    # order_by = direction_symbol[order_dir] + template_a_columns[int(custom_order[int(order_col)])]
    # templates = ActivityLog.objects.all().order_by(order_by)
    templates = EmailTemplate.objects.all()
    # shape_list = request.POST.getlist('shape[]')
    # id_list = request.POST.getlist('id[]')
    # # id_list = [int(i) for i in id_list]
    # weight_from = request.POST.get('weight_from')
    # weight_to = request.POST.get('weight_to')
    # color_list = request.POST.getlist('color[]')
    # clarity_list = request.POST.getlist('clarity[]')
    # lab_list = request.POST.getlist('lab[]')
    # location_list = request.POST.getlist('location[]')
    # feed_list = request.POST.getlist('feed[]')
    # polish_list = request.POST.getlist('polish[]')
    # symmetry_list = request.POST.getlist('symmetry[]')
    # cut_list = request.POST.getlist('cut[]')
    # fluorescence_list = request.POST.getlist('fluorescence[]')
    # depth_from = request.POST.get('depth_from')
    # depth_to = request.POST.get('depth_to')
    # table_from = request.POST.get('table_from')
    # table_to = request.POST.get('table_to')
    # discount_from = request.POST.get('discount_from')
    # discount_to = request.POST.get('discount_to')

    # if len(shape_list) > 0:
    #     templates = templates.filter(shape__in=shape_list)
    # if len(id_list) > 0:
    #     temp_templates = templates.filter(cert_no__in=id_list)
    #     temp_templates_2 = templates.filter(lot_id__in=id_list)
    #     templates = temp_templates | temp_templates_2
    #
    # if not(weight_from == '0' and weight_to == '200'):
    #     templates = templates.filter(weight__gte=Decimal(weight_from), weight__lte=Decimal(weight_to))
    # if len(color_list) > 0:
    #     templates = templates.filter(color__in=color_list)
    # if len(clarity_list) > 0:
    #     templates = templates.filter(clarity__in=clarity_list)
    # if len(lab_list) > 0:
    #     templates = templates.filter(lab__in=lab_list)
    # if len(location_list) > 0:
    #     location_list = [x.upper() for x in location_list]
    #     templates = templates.filter(country_location__in=location_list)
    # if len(feed_list) > 0:
    #     feed_list = [x.upper() for x in feed_list]
    #     templates = templates.filter(feed__in=feed_list)
    # if len(polish_list) > 0:
    #     polish_list = [x.upper() for x in polish_list]
    #     templates = templates.filter(polish__in=polish_list)
    # if len(symmetry_list) > 0:
    #     symmetry_list = [x.upper() for x in symmetry_list]
    #     templates = templates.filter(symmetry__in=symmetry_list)
    # if len(cut_list) > 0:
    #     cut_list = [x.upper() for x in cut_list]
    #     templates = templates.filter(cut__in=cut_list)
    # if len(fluorescence_list) > 0:
    #     fluorescence_list = [x.upper() for x in fluorescence_list]
    #     templates = templates.filter(fluorescence__in=fluorescence_list)
    # if not(depth_from == '0' and depth_to == '200'):
    #     templates = templates.filter(depth__gte=Decimal(depth_from), depth__lte=Decimal(depth_to))
    # if not(table_from == '0' and table_to == '200'):
    #     templates = templates.filter(table_percent__gte=Decimal(table_from), table_percent__lte=Decimal(table_to))
    # if not(discount_from == '-200' and discount_to == '200'):
    #     templates = templates.filter(rap_discount__gte=Decimal(discount_from), rap_discount__lte=Decimal(discount_to))

    temp = dict()
    temp['draw'] = int(draw)
    temp['recordsTotal'] = templates.count()
    temp['recordsFiltered'] = templates.count()
    temp_list = []
    templates = templates[start:start+length]

    for template_a in templates:
        temp_list.append(
            [
                template_a.id, template_a.name, template_a.language,
                template_a.subject, template_a.user.get_full_name(),
                template_a.date_created.strftime("%b %d, %Y %H:%M"), template_a.body
             ]
        )

    temp['data'] = temp_list
    return JsonResponse(temp, safe=True)


@csrf_exempt
@login_required
def create_template(request):
    user_id = request.user.id
    name = request.POST.get('name', None)
    language = request.POST.get('language', None)
    subject = request.POST.get('subject', None)
    body = request.POST.get('body', None)
    try:
        EmailTemplate.objects.create(
            user_id=user_id,
            name=name,
            language=language,
            subject=subject,
            body=body
        )

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! Template Saved'}, safe=True, status=201)


@csrf_exempt
@login_required
def update_template(request):
    id = request.POST.get('id', None)
    name = request.POST.get('name', None)
    language = request.POST.get('language', None)
    subject = request.POST.get('subject', None)
    body = request.POST.get('body', None)
    try:
        template = EmailTemplate.objects.get(id=id)
        template.name = name
        template.language = language
        template.subject = subject
        template.body = body
        template.save()

    except EmailTemplate.DoesNotExist:
        return JsonResponse({'error': 'Failed! Invalid template'}, safe=True, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! Template Updated'}, safe=True, status=201)


@csrf_exempt
@login_required
def delete_templates(request):
    template_ids = request.POST.getlist('ids[]', None)

    try:
        template = EmailTemplate.objects.filter(id__in=template_ids).delete()

    except EmailTemplate.DoesNotExist:
        return JsonResponse({'message': 'Warning! Invalid id'}, safe=True, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({'message': 'Success! Seleted templates deleted'}, safe=True, status=200)


@csrf_exempt
@login_required
def get_email_template(request):
    id = request.POST.get('id', None)
    # user_id = request.user.id

    try:
        template = EmailTemplate.objects.get(id=id)
        data = model_to_dict(template)

    except EmailTemplate.DoesNotExist:
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(data, safe=True, status=200)


@csrf_exempt
@login_required
def upload_image(request):
    file = request.FILES.get('upload', None)
    private = smart_str2bool(request.GET.get('private', False))
    error = {}
    status = 200
    url = ''

    try:
        if file:
            extension = os.path.splitext(file.name)[1]
            file_name = generate_filename(extension)
        else:
            raise()

        if private:
            user_id = request.user.id
            directory = os.path.join(settings.PRIVATE_GALLERY_PATH, str(user_id))

            if not os.path.exists(directory):
                os.makedirs(directory)

            with open(os.path.join(directory, file_name), 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)

            url = settings.HTTP_SCHEME + settings.HOST_NAME + '/media/private_gallery/' + str(user_id) + '/' + file_name

        else:
            with open(os.path.join(settings.CKEDITOR_PATH, file_name), 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)

            url = settings.HTTP_SCHEME + settings.HOST_NAME + '/media/ckeditor_images/' + file_name

    except Exception as e:
        print(e)
        file_name = ''
        status = 400
        error = {
            "number": 400, "message":
            "Something went wrong. Please report!"}

    return JsonResponse({
        "fileName": file_name, "uploaded": 1, "error": error, "url": url}, safe=True, status=status)


def generate_filename(extension):
    new_file_name = str(uuid.uuid4()) + extension
    if os.path.isfile(os.path.join(settings.CKEDITOR_PATH, new_file_name)):
        return generate_filename(extension)
    return new_file_name


# @csrf_exempt
# @login_required
# def upload_private_image(request):
#     file = request.FILES.get('upload', None)
#     user_id = request.user.id
#     directory = os.path.join(settings.PRIVATE_GALLERY_PATH, str(user_id))
#     error = {}
#     status = 200
#     url = ''
#
#     try:
#         if file:
#             extension = os.path.splitext(file.name)[1]
#             file_name = generate_filename(extension)
#         else:
#             raise()
#
#         if not os.path.exists(directory):
#             os.makedirs(directory)
#
#         with open(os.path.join(directory, file_name), 'wb+') as destination:
#             for chunk in file.chunks():
#                 destination.write(chunk)
#
#         url = settings.HTTP_SCHEME + settings.HOST_NAME + '/media/private_gallery/' + str(user_id) + '/' + file_name
#
#     except Exception as e:
#         print(e)
#         file_name = ''
#         status = 400
#         error = {
#             "number": 400, "message":
#             "Something went wrong. Please report!"}
#
#     return JsonResponse({
#         "fileName": file_name, "uploaded": 1, "error": error, "url": url}, safe=True, status=status)


@csrf_exempt
@login_required
def list_images(request):
    private = smart_str2bool(request.GET.get('private', False))

    if private:
        user_id = request.user.id
        path = os.path.join(settings.PRIVATE_GALLERY_PATH, str(user_id))
        url_path = '/media/private_gallery/' + str(user_id) + '/'
    else:
        path = settings.CKEDITOR_PATH
        url_path = '/media/ckeditor_images/'

    try:
        jpeg_list = [generate_url(i, url_path) for i in glob.glob(path + '/*.{}'.format('jpeg'))]
        jpg_list = [generate_url(i, url_path) for i in glob.glob(path + '/*.{}'.format('jpg'))]
        png_list = [generate_url(i, url_path) for i in glob.glob(path + '/*.{}'.format('png'))]
        files_list = jpeg_list + jpg_list + png_list

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse({"files": files_list}, safe=True, status=200)


def generate_url(file_path, url_path):
    from urllib import parse
    file_name = os.path.basename(file_path)
    return parse.urlunparse(
            ('http', settings.HOST_NAME, url_path + file_name, None, None, None))


@csrf_exempt
@login_required
def delete_image(request):
    image_id = request.POST.get('id', None)
    private = smart_str2bool(request.POST.get('private', False))
    path = os.path.join(settings.CKEDITOR_PATH, image_id)

    if private:
        user_id = request.user.id
        path = os.path.join(settings.PRIVATE_GALLERY_PATH, str(user_id), image_id)

    try:
        os.remove(path)

    except Exception as e:
        print(e)
        return JsonResponse({'error': 'Failed! Something went wrong'}, safe=True, status=400)

    return JsonResponse({}, safe=True, status=204)


@csrf_exempt
@login_required
def get_user_ids(request):
    try:
        request_post = json.loads(request.body)
        users = User.objects.filter(is_superuser=False)
        user_ids = list(get_filtered_users(request_post, users).values_list('id', flat=True))
        user_ids = [str(i) for i in user_ids]
    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(user_ids, safe=False, status=200)


@csrf_exempt
@login_required
def dt_list_follow_ups(request):
    draw = request.POST.get('draw')
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    order_col = request.POST.get('order[0][column]')
    order_dir = request.POST.get('order[0][dir]')

    order_by = direction_symbol[order_dir] + follow_up_columns[int(order_col)]

    follow_ups = FollowUps.objects.all()
    type = request.POST.get('type', '')
    status = request.POST.get('status', '')
    subject = request.POST.get('subject', '')
    notes = request.POST.get('notes', '')
    assigned_to = request.POST.get('assigned_to', '')

    if type != '':
        follow_ups = follow_ups.filter(type=type)
    if status != '':
        follow_ups = follow_ups.filter(status=status)
    if subject != '':
        follow_ups = follow_ups.filter(subject__icontains=subject)
    if notes != '':
        follow_ups = follow_ups.filter(notes__icontains=notes)
    if assigned_to != '':
        follow_ups = follow_ups.filter(assigned_to_id=assigned_to)

    if follow_up_columns[int(order_col)] == 'name':
        if order_dir == 'asc':
            follow_ups = follow_ups.order_by(Lower('customer__first_name').asc(), Lower('customer__last_name').asc())
        else:
            follow_ups = follow_ups.order_by(Lower('customer__first_name').desc(), Lower('customer__last_name').desc())

    elif follow_up_columns[int(order_col)] == 'id' or \
            follow_up_columns[int(order_col)] == 'status' or \
            follow_up_columns[int(order_col)] == 'type' or \
            follow_up_columns[int(order_col)] == 'date_created' or \
            follow_up_columns[int(order_col)] == 'due_date_time':
        follow_ups = follow_ups.order_by(order_by)
    else:
        if order_dir == 'asc':
            follow_ups = follow_ups.order_by(Lower(follow_up_columns[int(order_col)]).asc())
        else:
            follow_ups = follow_ups.order_by(Lower(follow_up_columns[int(order_col)]).desc())

    temp = dict()
    temp['draw'] = int(draw)
    temp_list = []
    follow_ups = follow_ups[start:start + length]
    # current_date = datetime.now()
    # week_no = current_date.strftime("%V")
    # month_no = current_date.month
    # weekly_count = 0
    # monthly_count = 0
    total_count = 0

    for follow_up in follow_ups:
        temp_list.append(get_follow_up_dt(follow_up))
        # if activity_a.search_time.strftime("%V") == week_no:
        #     weekly_count += 1
        # if activity_a.search_time.month == month_no:
        #     monthly_count += 1
        total_count += 1

    # print(weekly_count, monthly_count)
    temp['data'] = temp_list
    temp['recordsTotal'] = total_count
    # temp['recordsWeekly'] = weekly_count
    # temp['recordsMonthly'] = monthly_count
    temp['recordsFiltered'] = total_count

    return JsonResponse(temp, safe=True)


@csrf_exempt
@login_required
def create_update_follow_up(request):
    # user_id = request.user.id
    temp_id = request.POST.get('id', None)
    subject = request.POST.get('subject', None)
    customer_id = request.POST.get('customer_id', None)
    follow_type = request.POST.get('type', None)
    assigned_to_id = request.POST.get('assigned_to', None)
    status = request.POST.get('status', None)
    due_date_time = request.POST.get('due_date_time', None)
    notes = request.POST.get('notes', None)
    impressions = request.POST.get('impressions', None)
    commands = request.POST.get('commands', None)

    try:
        if temp_id:
            followup = FollowUps.objects.get(id=temp_id)
            followup.subject = subject
            followup.customer_id = customer_id
            followup.type = follow_type
            followup.assigned_to_id = assigned_to_id
            followup.status = status
            followup.due_date_time = due_date_time
            followup.notes = notes
            followup.impressions = impressions
            followup.commands = commands
            followup.save()
        else:
            FollowUps.objects.create(
                subject=subject,
                customer_id=customer_id,
                type=follow_type,
                assigned_to_id=assigned_to_id,
                status=status,
                due_date_time=due_date_time,
                notes=notes,
                impressions=impressions,
                commands=commands,
            )

    except Exception as e:
        print(e)
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse({}, safe=True, status=201)


@csrf_exempt
@login_required
def get_follow_up(request):
    follow_up_id = request.POST.get('id', None)

    try:
        follow_up = FollowUps.objects.get(id=follow_up_id)
        follow_up_json = model_to_dict(follow_up)
        # print(follow_up_json)
        follow_up_json['due_date_time'] = follow_up.due_date_time.strftime("%Y-%m-%dT%H:%M")
        follow_up_json['customer_email'] = follow_up.customer.email
        follow_up_json['assigned_to_email'] = follow_up.assigned_to.email

    except FollowUps.DoesNotExist:
        return JsonResponse({}, safe=True, status=400)

    return JsonResponse(follow_up_json, safe=True, status=200)


@csrf_exempt
@login_required
def delete_follow_ups(request):
    id_list = request.POST.getlist('ids[]')

    try:
        if len(id_list) > 0:
            FollowUps.objects.filter(id__in=id_list).delete()
        else:
            return JsonResponse({'ok': False}, status=400)

    except Exception as e:
        print(e)
        return JsonResponse({'ok': False}, status=400)

    return JsonResponse({'ok': True}, status=200)


def get_follow_up_dt(follow_up):
    return [
        follow_up.id, follow_up.get_status_display(), follow_up.get_type_display(),
        follow_up.customer.get_full_name(), follow_up.customer.email,
        follow_up.date_created.strftime("%b %d, %Y"), follow_up.subject,
        follow_up.due_date_time.strftime("%b %d, %Y %H:%M:%S"), follow_up.notes, follow_up.customer_id,
        follow_up.impressions, follow_up.commands
    ]

